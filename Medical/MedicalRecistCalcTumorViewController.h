//
//  MedicalRecistCalcTumorViewController.h
//  Medical
//
//  Created by Kirti Nikam on 09/10/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MedicalRecistCalcTumorViewController : UIViewController<UITextFieldDelegate,UIActionSheetDelegate,UIPickerViewDataSource,UIPickerViewDelegate>
{
    int CURRENTCASE;
    UIActionSheet *actionSheet;
    double yaxis;

}
@property (copy,nonatomic) NSMutableString *pickerValue;
@property (readwrite,nonatomic) double baseSLDValue;
@property (readwrite,nonatomic) double currentSLDValue;

@property (strong, nonatomic) IBOutlet UILabel *lblResponse;
@property (strong, nonatomic) IBOutlet UILabel *labelResult;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIPickerView *pickerView;

@property (strong, nonatomic) IBOutlet UIView *viewBaseSLD;
@property (strong, nonatomic) IBOutlet UIView *viewCurrentSLD;

@property (strong,nonatomic) UIView *customView;

@property (strong, nonatomic) IBOutlet UITextField *baseSLDTextField;
@property (strong, nonatomic) IBOutlet UITextField *currentSLDTextField;
@property (strong, nonatomic) IBOutlet UILabel *lblErrBaseSLD;
@property (strong, nonatomic) IBOutlet UILabel *lblErrCurrentSLD;
@property (strong, nonatomic) IBOutlet UIImageView *imgViewBaseSLD;
@property (strong, nonatomic) IBOutlet UIImageView *imgViewCurrentSLD;

@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *lblCollection;
@property (strong, nonatomic) IBOutletCollection(UITextField) NSArray *txtCollection;
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *viewCollection;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *lblErrCollection;

@end
