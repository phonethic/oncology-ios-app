//
//  DiseaseObject.h
//  Medical
//
//  Created by Kirti Nikam on 09/04/14.
//  Copyright (c) 2014 Kirti Nikam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DiseaseObject : NSObject
@property (copy,nonatomic) NSString *dcode;
@property (copy,nonatomic) NSString *dName;
@property (copy,nonatomic) NSString *dDetails;
@end
