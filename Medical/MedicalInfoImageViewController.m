//
//  MedicalInfoImageViewController.m
//  Medical
//
//  Created by Kirti Nikam on 09/10/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import "MedicalInfoImageViewController.h"

@interface MedicalInfoImageViewController ()

@end

@implementation MedicalInfoImageViewController
@synthesize scrollView;
@synthesize infoImageView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (id)initWithimageName:(NSString *)image;
{
    if (self = [super initWithNibName:@"MedicalInfoImageViewController" bundle:nil])
    {
        imageNameString=image;
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    infoImageView.image = [UIImage imageNamed:imageNameString];
	scrollView.maximumZoomScale = 3.0;
    scrollView.minimumZoomScale = 1.0;
	scrollView.clipsToBounds = YES;
	scrollView.delegate = self;
	scrollView.scrollEnabled = YES;
    [scrollView setZoomScale:1.0 animated:YES];
    scrollView.contentSize = CGSizeMake(320, infoImageView.image.size.height);
}

-(UIView *) viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return infoImageView;
}
- (void)scrollViewDidEndZooming:(UIScrollView *)lscrollView withView:(UIView *)view atScale:(float)scale{
    [scrollView setContentSize:CGSizeMake(scale*320, scale*infoImageView.image.size.height)];
}

- (void)viewDidUnload
{
    [self setScrollView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
#pragma mark - Rotation
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

@end
