//
//  MedicalUsefulSitesViewController.m
//  Medical
//
//  Created by Sagar Mody on 05/11/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import "MedicalUsefulSitesViewController.h"
#import "MedicalWebViewController.h"

#import "constants.h"

@interface MedicalUsefulSitesViewController ()

@end

@implementation MedicalUsefulSitesViewController
@synthesize sitesTableView;
@synthesize siteObjdict;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [INEventLogger logEvent:@"Weblink_List"];

    siteObjdict = [[NSMutableDictionary alloc] init];
	[siteObjdict setObject:@"http://www.ncbi.nlm.nih.gov/pubmed" forKey:@"Pubmed"];
	[siteObjdict setObject:@"http://clinicaltrials.gov" forKey:@"ClinicalTrials.gov"];
//	[siteObjdict setObject:@"http://ctep.cancer.gov/protocolDevelopment/default.htm" forKey:@"Protocol Development"];
    [siteObjdict setObject:@"http://seer.cancer.gov" forKey:@"Cancer Statistics"];
    [siteObjdict setObject:@"http://dailymed.nlm.nih.gov" forKey:@"Package Insert"];
//    [siteObjdict setObject:@"http://www.signaling-gateway.org/" forKey:@"Cancer Signaling"];
    [siteObjdict setObject:@"http://www.nejmjobs.org" forKey:@"NEJM Jobs"];
//    [siteObjdict setObject:@"http://careers.jco.org" forKey:@"JCO Jobs"];
//    [siteObjdict setObject:@"http://jobcenter.hematology.org/" forKey:@"Hematology Jobs"];
//    [siteObjdict setObject:@"http://www.crediblemeds.org/pdftemp/pdf/CompositeList.pdf" forKey:@"Drugs Prolonging QTc Interval"];
//    [siteObjdict setObject:@"http://medicine.iupui.edu/clinpharm/ddis/clinical-table/" forKey:@"Cytochrome P450 Drugs Interaction"];
    [siteObjdict setObject:@"http://www.fda.gov/drugs/informationondrugs/approveddrugs/ucm279174.htm" forKey:@"FDA Approvals and Safety Notifications (Hem/Onc)"];

//	DebugLog(@"%@", siteObjdict);
//    
//	NSArray *keys = [siteObjdict allKeys];
//    
//	// values in foreach loop
//	for (NSString *key in keys) {
//		DebugLog(@"%@ is %@",key, [siteObjdict objectForKey:key]);
//	}
}

- (void)viewDidUnload
{
    [self setSitesTableView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}


#pragma mark Table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    //    return 50;
    
    NSString *key       = [[[siteObjdict allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.row];
    NSString *value     = [siteObjdict objectForKey:key];
    NSArray* substrings = [value componentsSeparatedByString: @"/"];
    NSString* link      = [substrings objectAtIndex: 2];
    
    NSString *str       =  [NSString stringWithFormat:@"%@ (%@)",key,link];
    CGSize size         = [str sizeWithFont:DEFAULT_FONT(FONT_STATIC_ROW_HEIGHT) constrainedToSize:CGSizeMake(280, 999) lineBreakMode:NSLineBreakByWordWrapping];
    DebugLog(@"%f",size.height);
    return size.height + 40;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[[siteObjdict allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        
        cell.textLabel.font=DEFAULT_FONT(FONT_STATIC_ROW_HEIGHT);
        cell.textLabel.numberOfLines = 0;
        
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = DEFAULT_COLOR_WITH_ALPHA(0.5);
        cell.selectedBackgroundView = bgColorView;
    }
    NSString *key = [[[siteObjdict allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.row];
    NSString *value = [siteObjdict objectForKey:key];
    if(![value isEqualToString:@""])
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    else
        cell.accessoryType = UITableViewCellAccessoryNone;
    
    NSArray* substrings = [value componentsSeparatedByString: @"/"];
    NSString* link = [substrings objectAtIndex: 2];
    
    DebugLog(@"%@",substrings);
    cell.textLabel.text = [NSString stringWithFormat:@"%@ (%@)",key,link];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:WARNING_TITLE
                                                        message:WARNING_MSG_FOR_WEBLINK
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
	[alertView show];
    
//    NSString *key = [[siteObjdict allKeys] objectAtIndex:indexPath.row];
//    NSString *value = [siteObjdict objectForKey:key];
//    //DebugLog(@"%@",value);
//    if(![value isEqualToString:@""])
//    {
//        MedicalWebViewController *webviewController = [[MedicalWebViewController alloc] initWithNibName:@"MedicalWebViewController" bundle:nil] ;
//        webviewController.medicalLink = value;
//        webviewController.title = key;
//        [self.navigationController pushViewController:webviewController animated:YES];
//    }
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	DebugLog(@"buttonIndex = %d",buttonIndex);
    NSIndexPath *indexPath = [sitesTableView indexPathForSelectedRow];
    if (indexPath != nil) {
        NSString *key = [[[siteObjdict allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.row];
        NSString *value = [siteObjdict objectForKey:key];
        //DebugLog(@"%@",value);
        if(![value isEqualToString:@""])
        {
            NSDictionary *params = @{@"web_name":key,@"link":value};
            [INEventLogger logEvent:@"Weblink_Website" withParams:params];
            
            MedicalWebViewController *webviewController = [[MedicalWebViewController alloc] initWithNibName:@"MedicalWebViewController" bundle:nil] ;
            webviewController.medicalLink = value;
            webviewController.title = key;
            [self.navigationController pushViewController:webviewController animated:YES];
        }
        [sitesTableView deselectRowAtIndexPath:indexPath animated:YES];
    }
}
@end
