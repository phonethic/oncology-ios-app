//
//  MedicalKarnofskyStatusViewController.h
//  Medical
//
//  Created by Kirti Nikam on 17/10/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import <UIKit/UIKit.h>

#define CONTENTSPACE 4
#define IMAGEHEIGHT 3
#define CELLHEIGHT 70

@interface MedicalKarnofskyStatusViewController : UIViewController<UIActionSheetDelegate>

@property(strong,nonatomic) NSMutableArray *karnofskyStatusArry;
@property (strong, nonatomic) IBOutlet UITableView *kartableView;
@property (strong, nonatomic) IBOutlet UIButton *infoBtn;
@property (strong, nonatomic) IBOutlet UILabel *lblPercentage;
@property (strong, nonatomic) IBOutlet UILabel *lblFunctionaCapacity;
@property (strong, nonatomic) IBOutlet UILabel *lblUnderLine;


- (IBAction)infoButtonAction:(id)sender;
@end
