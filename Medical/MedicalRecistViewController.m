//
//  MedicalRecistViewController.m
//  Medical
//
//  Created by Kirti Nikam on 25/10/13.
//  Copyright (c) 2013 Kirti Nikam. All rights reserved.
//

#import "MedicalRecistViewController.h"
#import "MedicalWebViewController.h"
#import "constants.h"
#import "MedicalRecistCalcTumorViewController.h"

#define RECIST_CALC @"RECIST Measurement Tool"

@interface MedicalRecistViewController ()

@end

@implementation MedicalRecistViewController
@synthesize recistDict;
@synthesize recistArray;
@synthesize recistTableView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [INEventLogger logEvent:@"Recist_List"];

    recistArray = [[NSArray alloc] initWithObjects:@"Introduction",@"Measurable Lesions",@"Non Measurable Lesions",@"Special Considerations For Measurability",@"Response (Target lesions)",@"Response (Non target lesions)",@"Controversies and Future Directions",RECIST_CALC,@"References", nil];
    
    recistDict = [[NSMutableDictionary alloc] init];
    [recistDict setObject:@"The Response Evaluation Criterion in Solid Tumors (RECIST) was introduced in 2000 to standardize tumor response, especially in oncologic clinical trials.  In 2009, these guidelines were revised to add clarity and detail to the previous guidelines. Changes introduced included the number of measureable lesions, clarity on lymph nodes, role of FDG PET, etc. In this review, we discuss the salient features of the new guideline and future challenges." forKey:@"Introduction"];
    
    [recistDict setObject:@"(1) Tumor (Longest Diameter):<br>a. 10 mm caliper measurement by clinical examination<br>b. 10 mm by CT scan (Maximum slice thickness of CT scan should be 5 mm)<br>c.  20 mm by CXR<br><br>(2) Lymph Nodes (Short Axis Diameter): ≥ 15 mm when measured by CT scans.<br><br>(3) A maximum of 5 target lesions in total (and maximum of two per organ)" forKey:@"Measurable Lesions"];
    
    [recistDict setObject:@"(1)  Leptomeningeal Disease<br><br>(2)  Ascites<br><br>(3)  Pleural effusion<br><br>(4)  Pericardial Effusion<br><br>(5)  Inflammatory Breast Disease<br><br>(6)  Lymphangitic involvement of skin or lung<br><br>(7)  Abdominal masses not amenable to measurement by stated techniques" forKey:@"Non Measurable Lesions"];
    
    [recistDict setObject:@"(1) Bone Lesions:<br>a. Measurable: Lytic lesions or mixed lytic – blastic lesions with a soft tissue component if the identifiable soft tissue component meets the criterion for measurability.<br>b. Non Measurable: Blastic lesions and lesions measured on bone scan, PET scan or plain films.<br><br>(2) Cystic lesions: Cystic lesions thought to be metastases can be measured as tumor lesions, but non-cystic lesions are preferred.<br><br>(3) Prior Local Treatment: Lesions in areas that have received prior radiation (or other local therapy) are not measurable unless there has been progression in the lesion.  Conditions should be defined in individual study protocols<br><br>(4) Target lesions that become too small to measure: If a lesion or lymph node becomes too small to measure, a default value of 5 mm is to be assigned. If it disappears, the measurement is recorded as 0 mm.<br><br>(5) Target lesions that split on treatment: The longest diameter of the fragmented lesions should be added (non nodal lesions)<br><br>(6) Target lesions that coalesce on treatment: The longest diameter of the lesion  (non nodal lesions)<br><br>(7) Lymph nodes that decrease to less than 10 mm on study should be measured for PR, SD and PD.  For CR, each node must be <10 mm in SAD." forKey:@"Special Considerations For Measurability"];
    
    [recistDict setObject:@"(1) Complete Response: Disappearance of all target lesions. Lymph nodes should be < 10 mm.<br><br>(2) Partial Response: ≥ 30% reduction in sum of target lesions. Baseline measurements taken as reference.<br><br>(3) Progressive Disease (PD): ≥ 20% and at least ≥ 5 mm increase in sum of target lesions.  Sum of smallest diameter during study is taken as reference. New lesions are also regarded as progression.<br><br>(4) Stable Disease: Neither PR or SD. Sum of smallest diameter during study is taken as reference" forKey:@"Response (Target lesions)"];
    
    [recistDict setObject:@"(1) Complete Response: Disappearance of all non target lesions, lymph nodes should be < 10 mm and the tumor markers should return to normal<br><br>(2) Progressive Disease: There should be unequivocal progression of non target lesions" forKey:@"Response (Non target lesions)"];
    
    [recistDict setObject:@"RECIST 1.1 guideline was successful in providing clarity on several key issues like lymph node measurement and non-target lesions. However, there are several issues that need further clarification.  RECIST may not be very sensitive in measuring response to imatinib therapy in GIST, where PET imaging and the Choi response criterion may be of greater utility.  In addition, both a 19% increase in tumor diameter or a  29% decrease in the tumor size  are classified under ‘Stable Disease’ which may not be a true representation of the response to therapy. Further, with the advent of targeted therapies there could be the issue of  'pseudoprogression'. This can occour when treatment results in tumor necrosis that can falsely enlarge the tumor size as in glioblatoma after treatment with chemotherapy and radiation. The advent of functional imaging like dynamic contrast enhanced MRI and FDG PET and their further integration into future guidelines may help us better understand tumor response in this era of targeted therapy. " forKey:@"Controversies and Future Directions"];
    
    [recistDict setObject:@"New guidelines to evaluate the response to treatment in solid tumors. European Organization for Research and Treatment of Cancer, National Cancer Institute of the United States, National Cancer Institute of Canada. Therasse P, et al. J Natl Cancer Inst. 2000 Feb 2;92(3):205-16.<br><br>New response evaluation criteria in solid tumours: revised RECIST guideline (version 1.1). Eisenhauer EA, et al. Eur J Cancer. 2009 Jan;45(2):228-47.<br><br>Use of modified RECIST criteria to improve response assessment in targeted therapies: challenges and opportunities.Rosen MA. Cancer Biol Ther. 2010 Jan;9(1):20-2. Epub 2010 Jan 20.<br><br>We should desist using RECIST, at least in GIST. Benjamin RS, et al. J Clin Oncol. 2007 May 1;25(13):1760-4.<br><br>Response evaluation in gastrointestinal stromal tumours treated with imatinib: misdiagnosis of disease progression on CT due to cystic change in liver metastases. Linton KM, et al. Br J Radiol. 2006 Aug;79(944):e40-4." forKey:@"References"];
    
    [recistTableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setRecistTableView:nil];
    [super viewDidUnload];
}

#pragma mark - Rotation
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark Table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [recistArray count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *str = [recistArray objectAtIndex:indexPath.row];
    CGSize size =   [str sizeWithFont:DEFAULT_FONT(FONT_STATIC_ROW_HEIGHT) constrainedToSize:CGSizeMake(280, 999) lineBreakMode:NSLineBreakByWordWrapping];
    DebugLog(@"%f",size.height);
    return size.height + 40;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"RECISTCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        
        cell.textLabel.font=DEFAULT_FONT(FONT_STATIC_ROW_HEIGHT);
        cell.textLabel.numberOfLines = 0;
        
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = DEFAULT_COLOR_WITH_ALPHA(0.5);
        cell.selectedBackgroundView = bgColorView;
    }
    NSString *key = [recistArray objectAtIndex:indexPath.row];
    NSString *value = [recistDict objectForKey:key];
    if(![value isEqualToString:@""])
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    else
        cell.accessoryType = UITableViewCellAccessoryNone;
    cell.textLabel.text = key;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *key = [recistArray objectAtIndex:indexPath.row];
    NSDictionary *params = @{@"recist_name":key};
    [INEventLogger logEvent:@"Recist_Details" withParams:params];
    
    if ([key isEqualToString:RECIST_CALC]) {
        MedicalRecistCalcTumorViewController *recistViewController = [[MedicalRecistCalcTumorViewController alloc] initWithNibName:@"MedicalRecistCalcTumorViewController" bundle:nil] ;
        recistViewController.title = @"RECIST";
        [self.navigationController pushViewController:recistViewController animated:YES];
    }else{
        NSString *value = [recistDict objectForKey:key];
        if(![value isEqualToString:@""])
        {
            MedicalWebViewController *webviewController = [[MedicalWebViewController alloc] initWithNibName:@"MedicalWebViewController" bundle:nil];
            webviewController.titleString = key;
            webviewController.details = value;
            [self.navigationController pushViewController:webviewController animated:YES];
        }
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
@end
