//
//  MedicalWebViewController.m
//  Medical
//
//  Created by Sagar Mody on 05/11/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import "MedicalWebViewController.h"
#import "MedicalAppDelegate.h"
#import "CommonCallback.h"

@interface MedicalWebViewController ()

@end

@implementation MedicalWebViewController
@synthesize medicalWebView;
@synthesize medicalLink;
@synthesize titleString,details;
@synthesize lblTitle;
@synthesize toolBar,goBackBarBtn,goForwardBarBtn,refreshBarBtn;
@synthesize dictParams;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if (HUD) {
        [HUD removeFromSuperview];
        HUD = nil;
    }
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    if (medicalWebView.loading) {
        [medicalWebView stopLoading];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.backBarButtonItem = backButton;
    
    [self addHUD];

    [toolBar setHidden:YES];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        toolBar.barTintColor = DEFAULT_COLOR;
        [toolBar setTranslucent:NO];
    }
    toolBar.tintColor = DEFAULT_COLOR;
    DebugLog(@"medicalLink -%@-",medicalLink);
    if (medicalLink != nil && ![medicalLink isEqualToString:@""]) {
        [lblTitle setHidden:YES];
        medicalLink = [medicalLink stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        DebugLog(@"after applying trimming, medicalLink -%@-",medicalLink);

        if ([medicalLink hasPrefix:@"http"] || [medicalLink hasPrefix:@"www"]) {
            [toolBar setHidden:NO];
            [self setControlsState];
            if([MEDICAL_APP_DELEGATE networkavailable])
            {
                NSURL *url = [NSURL URLWithString:self.medicalLink];
                NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
                [medicalWebView loadRequest:requestObj];
                medicalWebView.scalesPageToFit = TRUE;
                medicalWebView.frame = CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height-CGRectGetHeight(toolBar.frame));
                toolBar.frame = CGRectMake(toolBar.frame.origin.x,CGRectGetMaxY(medicalWebView.frame),toolBar.frame.size.width, toolBar.frame.size.height);
            } else {
                [CommonCallback showOfflineAlert];
            }
        }else{
            NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:medicalLink ofType:@"html"]isDirectory:NO];
            NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
            DebugLog(@"url %@",url);
            [medicalWebView loadRequest:requestObj];
            medicalWebView.scalesPageToFit = TRUE;
        }
    }else{
        [lblTitle setHidden:FALSE];
        lblTitle.text = titleString;
        lblTitle.font = DEFAULT_BOLD_FONT(20);
        NSString *css = [NSString stringWithFormat:@"<!DOCTYPE html><head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1\"><style type=\"text/css\">h1{font-size:17px;width:300px;background-color:rgb(23,121,172);color:white;text-indent:5px;padding: 5px 0px 5px 5px; font-family: OpenSans-Semibold; font-weight:bold;}ol{font-size:14px;font-family:OpenSans;}ul{font-size:14px;font-family:OpenSans;}a.link {text-decoration:none;color: rgb(23,121,172);letter-spacing: 500;font-weight: bold;font-size:16px;font-family:OpenSans-Bold;}body{font-family:OpenSans; font-size:14px;width:300px;}</style></head><body><div>%@</div></body></html>",details];
                         
//                         h1{font-size:17px;width:300px;background-color:rgb(23,121,172);color:white;text-indent:5px;padding: 5px 0px 5px 5px; font-family: OpenSans-Semibold; font-weight:bold;} body {font-family:OpenSans; font-size:14px;width:300px;}</style></head><body><div>%@</div></body></html>",details];
        medicalWebView.frame = CGRectMake(0, CGRectGetMaxY(lblTitle.frame), 320, self.view.frame.size.height-CGRectGetMaxY(lblTitle.frame));
        [medicalWebView loadHTMLString:css baseURL:nil];
    }
}
- (void)viewDidUnload
{
    [self setMedicalWebView:nil];
    [self setLblTitle:nil];
    [self setToolBar:nil];
    [self setGoBackBarBtn:nil];
    [self setGoForwardBarBtn:nil];
    [self setRefreshBarBtn:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
#pragma mark - Rotation
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma internal methods
- (void)addHUD{
	HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
	[self.navigationController.view addSubview:HUD];
	HUD.delegate    = self;
	HUD.labelText   = @"Loading";
    HUD.labelFont   = DEFAULT_FONT(16.0);
    HUD.color       = DEFAULT_COLOR_WITH_ALPHA(0.9);
    
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureTapDetacted:)];
    [HUD addGestureRecognizer:gesture];
}

//#pragma mark -
//#pragma mark MBProgressHUDDelegate methods
//- (void)hudWasHidden:(MBProgressHUD *)hud {
//	// Remove HUD from screen when the HUD was hidded
//	[HUD removeFromSuperview];
//	HUD = nil;
//}

#pragma UIGestureRecognizer selector methods
-(void)gestureTapDetacted:(UIGestureRecognizer *)sender{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:@"Do you want to cancel loading?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
    [alert show];
}

#pragma UIAlertView delegate methods
- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if ([title isEqualToString:@"YES"]) {
        if (medicalWebView.loading) {
            [medicalWebView stopLoading];
        }
        if (![HUD isHidden]) {
            [HUD hide:YES];
        }
    }
}

#pragma UIWebView delegate methods
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [refreshBarBtn setEnabled:NO];
    [HUD show:YES];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [HUD hide:YES];
    [self setControlsState];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [HUD hide:YES];
    [self setControlsState];
}

-(void)setControlsState{
    if ([toolBar isHidden]) {
        return;
    }
    if([medicalWebView canGoBack])
    {
        [goBackBarBtn setEnabled:YES];
    } else {
        [goBackBarBtn setEnabled:NO];
    }
    if([medicalWebView canGoForward])
    {
        [goForwardBarBtn setEnabled:YES];
    } else {
        [goForwardBarBtn setEnabled:NO];
    }
    [refreshBarBtn setEnabled:YES];
}

- (IBAction)goBackBarBtnPressed:(id)sender {
    if ([medicalWebView canGoBack]) {
        [medicalWebView goBack];
    }else{
        [goBackBarBtn setEnabled:NO];
    }
}

- (IBAction)goForwardBarBtnPressed:(id)sender {
    if ([medicalWebView canGoForward]) {
        [medicalWebView goForward];
    }else{
        [goForwardBarBtn setEnabled:NO];
    }
}

- (IBAction)refreshBarBtnPressed:(id)sender {
    [medicalWebView reload];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    switch (navigationType) {
        case UIWebViewNavigationTypeLinkClicked:
        {
            DebugLog(@"UIWebViewNavigationTypeLinkClicked");
            if (details != nil && ![details isEqualToString:@""]) {
                DebugLog(@"url %@",request.URL);
                [INEventLogger logEvent:@"Oncology_Drugs_Package_Insert" withParams:dictParams];
                [self openURL:[request.URL absoluteString]];
                return NO;
            }
        }
            break;
        case UIWebViewNavigationTypeFormSubmitted:
            DebugLog(@"UIWebViewNavigationTypeFormSubmitted");
            break;
        case UIWebViewNavigationTypeBackForward:
            DebugLog(@"UIWebViewNavigationTypeBackForward");
            break;
        case UIWebViewNavigationTypeReload:
            DebugLog(@"UIWebViewNavigationTypeReload");
            break;
        case UIWebViewNavigationTypeFormResubmitted:
            DebugLog(@"UIWebViewNavigationTypeFormResubmitted");
            break;
        case UIWebViewNavigationTypeOther:
            DebugLog(@"UIWebViewNavigationTypeOther");
            break;
        default:
            DebugLog(@"other %d",navigationType);
            break;
    }
    return YES;
}

-(void)openURL:(NSString *)url
{
    if([url length] > 0)
    {
        MedicalWebViewController *webviewController = [[MedicalWebViewController alloc] initWithNibName:@"MedicalWebViewController" bundle:nil] ;
        webviewController.medicalLink = url;
        webviewController.title = self.title;
        [self.navigationController pushViewController:webviewController animated:YES];
    }
}
@end
