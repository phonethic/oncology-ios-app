//
//  NearByMeObject.h
//  Medical
//
//  Created by Kirti Nikam on 26/11/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NearByMeObject : NSObject
@property (nonatomic, readwrite) NSInteger nearbymerowid;
@property(nonatomic,readwrite) double durationValue;
@property(nonatomic,readwrite) double distanceValue;
@property(nonatomic,copy) NSString *durationText;
@property(nonatomic,copy) NSString *distanceText;
@property(nonatomic,copy) NSString *centerName;
@end
