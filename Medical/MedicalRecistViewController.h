//
//  MedicalRecistViewController.h
//  Medical
//
//  Created by Kirti Nikam on 25/10/13.
//  Copyright (c) 2013 Kirti Nikam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MedicalRecistViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITableView *recistTableView;
@property (strong, nonatomic) NSMutableDictionary *recistDict;
@property (strong, nonatomic) NSArray *recistArray;
@end
