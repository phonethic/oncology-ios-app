//
//  SplashViewController.m
//  Medical
//
//  Created by Kirti Nikam on 21/04/14.
//  Copyright (c) 2014 Kirti Nikam. All rights reserved.
//

#import "SplashViewController.h"
#import "MedicalAppDelegate.h"

@interface SplashViewController ()

@end

@implementation SplashViewController
@synthesize imageView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    imageView.contentMode = UIViewContentModeRedraw;
    
    if ([[UIScreen mainScreen] respondsToSelector: @selector(scale)]) {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        result = CGSizeMake(result.width * [UIScreen mainScreen].scale, result.height * [UIScreen mainScreen].scale);
        DebugLog(@"SplashView: result.height %f",result.height);
        if (result.height == 480){
            // iPhone 1,3,3GS Standard Resolution   (320x480px)
            imageView.image = [UIImage imageNamed:@"Default.png"];
        }else if(result.height == 960){
            // iPhone 4,4S High Resolution          (640x960px)
            imageView.image = [UIImage imageNamed:@"Default@2x.png"];
        }else{
            // iPhone 5 High Resolution             (640x1136px)
            imageView.image = [UIImage imageNamed:@"Default-568h@2x.png"];
        }
    } else{
        // iPhone 1,3,3GS Standard Resolution   (320x480px)
        imageView.image = [UIImage imageNamed:@"Default.png"];
    }
    [self performSelector:@selector(imageTapDetected:) withObject:nil afterDelay:2.0];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewDidUnload {
    [self setImageView:nil];
    [super viewDidUnload];
}

#pragma mark - Rotation
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

-(void)imageTapDetected:(UIGestureRecognizer *)sender {
    DebugLog(@"SplashView: ImageTapped");
    if (self.isViewLoaded && self.view.window) {
        [MEDICAL_APP_DELEGATE removeSplashScreen];
    }
}
@end
