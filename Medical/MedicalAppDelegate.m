//
//  MedicalAppDelegate.m
//  Medical
//
//  Created by Kirti Nikam on 08/10/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import "MedicalAppDelegate.h"
#import "MedicalViewController.h"
#import "MedicalPrintImage.h"
#import "Flurry.h"
#import "CommonCallback.h"
#import "SplashViewController.h"
#import "TermsAndPrivacyViewController.h"
#import "MedicalUserDefaults.h"
#import "iRate.h"
#import "INEventLogger.h"
#import <sqlite3.h>
#import "UAirship.h"
#import "UAConfig.h"
#import "UAPush.h"
#import "INUserDefaultOperations.h"

@implementation MedicalAppDelegate
@synthesize navigationController = _navigationController;
@synthesize popover;
@synthesize databaseName;
@synthesize databasePath;
@synthesize PRINTEMAILALERT;
@synthesize networkavailable;
@synthesize splashViewController;

#pragma mark -
#pragma mark Application lifecycle

+ (void)initialize
{
    //set the bundle ID. normally you wouldn't need to do this
    //as it is picked up automatically from your Info.plist file
    //but we want to test with an app that's actually on the store
    [iRate sharedInstance].applicationBundleID = @"com.phonethics.oncologyiphone";
    [iRate sharedInstance].appStoreID = 891567879;
    [iRate sharedInstance].verboseLogging = NO;
    
	[iRate sharedInstance].onlyPromptIfLatestVersion = NO;
    
    //enable preview mode
    [iRate sharedInstance].previewMode = NO;
    
    //By default, iRate will use all available languages in the iRate.bundle, even if used in an app that does not support localisation.
    //If you would prefer to restrict iRate to only use the same set of languages that your application already supports,
    //set this property to NO. (Defaults to YES).
    [iRate sharedInstance].useAllAvailableLanguages = NO;
    
//    //overriding the default iRate strings
//    [iRate sharedInstance].messageTitle = NSLocalizedString(@"Rate MyApp", @"iRate message title");
//    [iRate sharedInstance].message = NSLocalizedString(@"If you like MyApp, please take the time, etc", @"iRate message");
//    [iRate sharedInstance].cancelButtonLabel = NSLocalizedString(@"No, Thanks", @"iRate decline button");
//    [iRate sharedInstance].remindButtonLabel = NSLocalizedString(@"Remind Me Later", @"iRate remind button");
//    [iRate sharedInstance].rateButtonLabel = NSLocalizedString(@"Rate It Now", @"iRate accept button");
    
    [iRate sharedInstance].daysUntilPrompt   = 3;
    [iRate sharedInstance].usesUntilPrompt   = 0;
    [iRate sharedInstance].eventsUntilPrompt = 0;
}

- (NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
    NSUInteger orientations = UIInterfaceOrientationMaskAll;
    if (self.window.rootViewController) {
        if ([self.window.rootViewController isKindOfClass:[SplashViewController class]]) {
            orientations = [self.window.rootViewController supportedInterfaceOrientations];
        }else{
            UIViewController* presented = [[(UINavigationController *)self.window.rootViewController viewControllers] lastObject];
            orientations = [presented supportedInterfaceOrientations];
       }
    }
    return orientations;
}

//-(NSString *)urlEncode:(NSString *) urlInput
//{
//    NSMutableString *encodedString = nil;
//    for(int index = 0; index < urlInput.length;index++)
//    {
//        if ([urlInput substringFromIndex:index] =< 'a' && [urlInput substringFromIndex:index] )
//        {
//            <#statements#>
//        }
//    }
//   
//    return nil;
//}

-(void) checkAndCreateDatabase{
    databaseName = @"medicalDatabase.sql";

    // Get the path to the documents directory and append the databaseName
	NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDir = [documentPaths objectAtIndex:0];
	databasePath = [documentsDir stringByAppendingPathComponent:databaseName];
    DebugLog(@"databasePath %@",databasePath);
    
	// Check if the SQL database has already been saved to the users phone, if not then copy it over
	BOOL success;
    
	// Create a FileManager object, we will use this to check the status
	// of the database and to copy it over if required
	NSFileManager *fileManager = [NSFileManager defaultManager];
    
	// Check if the database has already been created in the users filesystem
	success = [fileManager fileExistsAtPath:databasePath];
    BOOL result = NO;
	if(success)
    {
        // If the database already exists
        int oldVersion = [self checkdatabaseversion:databasePath];
        DebugLog(@"old version %d",oldVersion);
        DebugLog(@"DATABASE_VERSION  %d",DATABASE_VERSION);
        if (oldVersion < DATABASE_VERSION) {
            DebugLog(@"old version != DATABASE_VERSION");
            result = YES;
            [self deleteOldDatabaseAndCreateNewOne:databasePath];
        }else{
            DebugLog(@"old version == DATABASE_VERSION");
            return;
        }
    }
    else
    {
        // If not then proceed to copy the database from the application to the users filesystem
        // Get the path to the database in the application package
        NSString *databasePathFromApp = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:databaseName];
        // Copy the database from the package to the users filesystem
        result = [fileManager copyItemAtPath:databasePathFromApp toPath:databasePath error:nil];
    }
    DebugLog(@"result %d",result);
    if(result)
    {
        DebugLog(@"Database created/updated");
        [self setDatabaseSchemaVersion:DATABASE_VERSION];
    }
}

-(void)deleteOldDatabaseAndCreateNewOne:(NSString *)ldbPath
{
    databaseName = @"medicalDatabase.sql";
    BOOL result = NO;
    NSError *error = nil;
    DebugLog(@"path--->%@",ldbPath);
    NSFileManager *fileManager = [NSFileManager defaultManager];
    result = [fileManager removeItemAtPath:ldbPath error:&error];
    DebugLog(@"R-->%d E-->%@", result, error);
    if(result) {
        NSString *databasePathFromApp = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:databaseName];
        // Copy the database from the package to the users filesystem
        result = [fileManager copyItemAtPath:databasePathFromApp toPath:ldbPath error:nil];
    }
}

-(int)checkdatabaseversion:(NSString *)checkdbpath
{
    sqlite3 *databaseP;
    static sqlite3_stmt *stmt_version;
    int databaseVersion = 0;
    
    const char *dbpath = [checkdbpath UTF8String];
    
    if (sqlite3_open(dbpath, &databaseP) == SQLITE_OK)
    {
        if(sqlite3_prepare_v2(databaseP, "PRAGMA user_version;", -1, &stmt_version, NULL) == SQLITE_OK) {
            while(sqlite3_step(stmt_version) == SQLITE_ROW) {
                databaseVersion = sqlite3_column_int(stmt_version, 0);
                DebugLog(@"%s: version %d", __FUNCTION__, databaseVersion);
            }
            DebugLog(@"%s: the databaseVersion is: %d", __FUNCTION__, databaseVersion);
        } else {
            DebugLog(@"%s: ERROR Preparing: , %s", __FUNCTION__, sqlite3_errmsg(databaseP) );
        }
        sqlite3_finalize(stmt_version);
    }
    sqlite3_close(databaseP);
    
    return databaseVersion;
}

- (void)setDatabaseSchemaVersion:(int)version {
    sqlite3 *databaseP;
    const char *dbpath = [databasePath UTF8String];
    
    if (sqlite3_open(dbpath, &databaseP) == SQLITE_OK)
    {
        sqlite3_exec(databaseP, [[NSString stringWithFormat:@"PRAGMA user_version = %d", version] UTF8String], NULL, NULL, NULL);
    }
}

-(void) removeSplashScreen
{
    if (splashViewController) {
        splashViewController = nil;
    }
    if ([MedicalUserDefaults isAcceptedLicenseAgreement]) {
        MedicalViewController *viewController = [[MedicalViewController alloc] initWithNibName:@"MedicalViewController" bundle:nil];
        self.navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
    }else{
        TermsAndPrivacyViewController *webViewController = [[TermsAndPrivacyViewController alloc] initWithNibName:@"TermsAndPrivacyViewController" bundle:nil];
        webViewController.title = @"Terms and Privacy";
        self.navigationController = [[UINavigationController alloc] initWithRootViewController:webViewController];
    }
    
    if ([MedicalUserDefaults isAcceptedLicenseAgreement] && ![MedicalUserDefaults isDoneTermsPrivacyAnimation]) {
        [UIView  transitionWithView:self.window duration:0.8  options:UIViewAnimationOptionTransitionCurlUp
                         animations:^(void) {
                             BOOL oldState = [UIView areAnimationsEnabled];
                             [UIView setAnimationsEnabled:NO];
                             self.window.rootViewController = self.navigationController;
                             [self.window makeKeyAndVisible];
                             [UIView setAnimationsEnabled:oldState];
                         }
                         completion:^(BOOL finished){
                             [MedicalUserDefaults setPlayTermsPrivacyAnimation:YES];
                         }];
    }else{
        self.window.rootViewController = self.navigationController;
        [self.window makeKeyAndVisible];
    }
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    [[UAPush shared] resetBadge];
    // Override point for customization after application launch.
//    for (NSString* family in [UIFont familyNames])
//    {
//        DebugLog(@"%@", family);
//        
//        for (NSString* name in [UIFont fontNamesForFamilyName: family])
//        {
//            DebugLog(@"  %@", name);
//        }
//    }
    
    [self startCheckNetwork];
    
    splashViewController = [[SplashViewController alloc] initWithNibName:@"SplashViewController" bundle:Nil];
    self.window.rootViewController = splashViewController;
    
    PRINTEMAILALERT = 0;

    //Add Flurry
    [Flurry startSession:FLURRY_APPID];
    
    // Create Database
    [self checkAndCreateDatabase];
    
    // Appearance
    self.navigationController.navigationBar.translucent = NO;
    
    //DebugLog(@"FLURRY_APPID %@ ,LOCALYTICS_APPID %@",FLURRY_APPID,LOCALYTICS_APPID);
    
    [[UINavigationBar appearance] setBackgroundImage:[CommonCallback getFlatImage:DEFAULT_COLOR] forBarMetrics:UIBarMetricsDefault];
    NSDictionary *navbarTitleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIColor whiteColor], UITextAttributeTextColor,
                                    [UIColor blackColor],UITextAttributeTextShadowColor,
                                    [NSValue valueWithUIOffset:UIOffsetMake(-1, 0)],UITextAttributeTextShadowOffset,
                                            DEFAULT_BOLD_FONT(18),UITextAttributeFont,
                                    nil];
    [[UINavigationBar appearance] setTitleTextAttributes:navbarTitleTextAttributes];
    
    //[CommonCallback styleNavigationBarWithFontName:DEFAULT_BOLD_FONT(18) andColor:DEFAULT_COLOR];
    
    [CommonCallback styleSegmentedControlWithFontName:DEFAULT_FONT(17) andSelectedColor:DEFAULT_COLOR andUnselectedColor:DEFAULT_COLOR_WITH_ALPHA(0.3) andDidviderColor:[UIColor whiteColor]];
    
    [[UINavigationBar appearanceWhenContainedIn:[MFMailComposeViewController class], nil] setTitleTextAttributes:
     @{
                                                                                            UITextAttributeFont : [UIFont boldSystemFontOfSize:20.0f],
     }];
    
    if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleBlackOpaque];
        
//        [[UINavigationBar appearance] setTintColor:[UIColor blueColor]];
        [[UINavigationBar appearance] setTintColor:[UIColor colorWithRed:8/255.0 green:74/255.0 blue:110/255.0 alpha:1.0]];
        
        [[UISearchBar appearance] setBackgroundImage:[CommonCallback getFlatImage:DEFAULT_COLOR]];

    }else{
        [[UINavigationBar appearance] setBarStyle:UIBarStyleBlackOpaque];
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        
        [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
        [[UIBarButtonItem appearance] setTintColor:[UIColor whiteColor]];
        
        //set back button color
        [[UIBarButtonItem appearanceWhenContainedIn:[UINavigationBar class], nil] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName,nil] forState:UIControlStateNormal];
    }

    //Urban Airship Initialization
    UAConfig *config = [UAConfig defaultConfig];
    config.automaticSetupEnabled = NO;
    [UAirship takeOff:config];
    [UAirship setLogging:FALSE];
    [UAirship setLogLevel:UALogLevelNone];
    [UAPush shared].notificationTypes = (UIRemoteNotificationTypeBadge |
                                         UIRemoteNotificationTypeSound |
                                         UIRemoteNotificationTypeAlert);
    
    [[UAPush shared] registerForRemoteNotifications];
    
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    //UA_LINFO(@"APNS device token: %@", deviceToken);
    
    // Updates the device token and registers the token with UA. This won't occur until
    // push is enabled if the outlined process is followed. This call is required.
    [[UAPush shared] registerDeviceToken:deviceToken];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
    //UA_LINFO(@"Received remote notification: %@", userInfo);
    
    // Fire the handlers for both regular and rich push
    [[UAPush shared] handleNotification:userInfo applicationState:application.applicationState];
  //  [UAInboxPushHandler handleNotification:userInfo];
}

#pragma Reachability Methods
-(void) startCheckNetwork
{
    
	[[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(reachabilityChanged:) name: kReachabilityChangedNotification object: nil];
	
    internetReach = [Reachability reachabilityForInternetConnection];
	[internetReach startNotifer];
    [self updateReachabitlityFlag:internetReach];
    
}

-(void) stopCheckNetwork
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
	internetReach = nil;
}

- (void)reachabilityChanged:(NSNotification *)note
{
	Reachability* curReach = [note object];
	NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
	[self updateReachabilityStatus: curReach];
}

- (void) updateReachabilityStatus: (Reachability*) curReach
{
	if(curReach == internetReach)
	{
		[self updateReachabitlityFlag: curReach];
    }
}

- (void) updateReachabitlityFlag: (Reachability*) curReach
{
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    BOOL connectionRequired=[curReach connectionRequired];
    
    switch (netStatus)
    {
        case NotReachable:
        {
            networkavailable =  NO;
            break;
        }
            
        case ReachableViaWWAN:
        {
			if(connectionRequired==NO)
			{
                networkavailable = YES;
			}
			else
			{
				//this is for invoking internet library
				NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:@"maps.google.com"] cachePolicy:NO timeoutInterval:15.0] ;
				
				NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self];
				if (theConnection) {
					connectionRequired	= NO;
				}
			}
			break;
        }
        case ReachableViaWiFi:
        {
            if(connectionRequired==NO)
            {
                networkavailable = YES;
            }
            break;
		}
    }
    if(connectionRequired)
    {
		networkavailable = NO;
    }
    DebugLog(@"network status = %d",networkavailable);
}


-(void)mailImage:(UIView *)view
{
    DebugLog(@"mailing image");
    view.frame = CGRectMake(0, 0, view.frame.size.width*2,view.frame.size.height);
    // make screenshot
    UIGraphicsBeginImageContext(view.frame.size);
	[view.layer renderInContext:UIGraphicsGetCurrentContext()];
	UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
    NSData *data = UIImagePNGRepresentation(image);
    

    @try
    {
        MFMailComposeViewController *mailComposeViewController=[[MFMailComposeViewController alloc]init];
        mailComposeViewController.mailComposeDelegate = self;
        [mailComposeViewController setToRecipients:[NSArray arrayWithObjects:@"email address",nil]];
        [mailComposeViewController setSubject:@"your subject"];
        [mailComposeViewController setMessageBody:@"Enter your message here." isHTML:YES];
        [mailComposeViewController addAttachmentData:data mimeType:@"image/png" fileName:@"screenshot.png"];
        [mailComposeViewController setModalPresentationStyle:UIModalPresentationFormSheet];     
        [self.navigationController presentViewController:mailComposeViewController animated:YES completion:nil];
    }
    @catch (NSException *exception)
    {
        DebugLog(@"Exception %@",exception.reason);
    }
}

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    DebugLog(@"Mail Result %u",result); 
    [controller dismissViewControllerAnimated:YES completion:nil];
}

//- (void)postNotificationWithString:(NSString *)value //post notification method and logic
//{
//    NSDictionary *dictionary = [NSDictionary dictionaryWithObject:value forKey:titlenotificationKey];
//    [[NSNotificationCenter defaultCenter] postNotificationName:titlenotificationName object:nil userInfo:dictionary];
//}
-(void)printImage:(UIView *)view
{
    
    view.frame = CGRectMake(0, 0, view.frame.size.height,view.frame.size.width+view.frame.size.height);
    // make screenshot
    UIGraphicsBeginImageContext(view.frame.size);
	[view.layer renderInContext:UIGraphicsGetCurrentContext()];
	UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
    
    

    // Obtain the shared UIPrintInteractionController
    UIPrintInteractionController *controller = [UIPrintInteractionController sharedPrintController];
    if(!controller){
        DebugLog(@"Couldn't get shared UIPrintInteractionController!");
        return;
    }
    
    // We need a completion handler block for printing.
    UIPrintInteractionCompletionHandler completionHandler = ^(UIPrintInteractionController *printController, BOOL completed, NSError *error) {
        if(completed && error)
            DebugLog(@"FAILED! due to error in domain %@ with error code %u", error.domain, error.code);
        DebugLog(@"dismiss");
    };
    
    // Obtain a printInfo so that we can set our printing defaults.
    UIPrintInfo *printInfo = [UIPrintInfo printInfo];
    
    // This application prints photos. UIKit will pick a paper size and print
    // quality appropriate for this content type.
    printInfo.outputType = UIPrintInfoOutputGrayscale;// UIPrintInfoOutputPhoto;
    // The path to the image may or may not be a good name for our print job
    // but that's all we've got.
    printInfo.jobName =@"screen.png";
    
    // If we are performing drawing of our image for printing we will print
    // landscape photos in a landscape orientation.
    if(!controller.printingItem && image.size.width > image.size.height)
        printInfo.orientation = UIPrintInfoOrientationLandscape;
    
    // Use this printInfo for this print job.
    controller.printInfo = printInfo;
    
    //  Since the code below relies on printingItem being zero if it hasn't
    //  already been set, this code sets it to nil.
    controller.printingItem = nil;
    
    
#if DIRECT_SUBMISSION
    // Use the URL of the image asset.
    if(self.imageURL && [UIPrintInteractionController canPrintURL:self.imageURL])
        controller.printingItem = self.imageURL;
#endif
    
    // If we aren't doing direct submission of the image or for some reason we don't
    // have an ALAsset or URL for our image, we'll draw it instead.
    if(!controller.printingItem){
        // Create an instance of our PrintPhotoPageRenderer class for use as the
        // printPageRenderer for the print job.
        MedicalPrintImage *pageRenderer = [[MedicalPrintImage alloc]init];
        // The PrintPhotoPageRenderer subclass needs the image to draw. If we were taking
        // this path we use the original image and not the fullScreenImage we obtained from
        // the ALAssetRepresentation.
        pageRenderer.printImage = image;
        controller.printPageRenderer = pageRenderer;
        
    }
    
    // The method we use presenting the printing UI depends on the type of
    // UI idiom that is currently executing. Once we invoke one of these methods
    // to present the printing UI, our application's direct involvement in printing
    // is complete. Our delegate methods (if any) and page renderer methods (if any)
    // are invoked by UIKit.
    [controller presentAnimated:YES completionHandler:completionHandler];  // iPhone
    
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController {
    self.popover = nil;
}
- (NSString *)saveImage: (UIImage*)image
{
    if (image != nil)
    {
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,  NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *filename = [NSString stringWithFormat: @"screen.png"];
        DebugLog(@"filename = %@" , filename);
        NSString* fullPath = [documentsDirectory stringByAppendingPathComponent:filename];
        DebugLog(@"fullpath = %@" , fullPath);
        
        NSData* data = UIImagePNGRepresentation(image);
        //[data writeToFile:fullPath atomically:YES];
        BOOL success = [fileManager createFileAtPath:fullPath contents:data attributes:nil];
        if(success == YES)
            return filename;
        else
            return @"";
    } else {
        return  @"";
    }
}

- (UIImage *)loadImage:(NSString *)limgName
{
    if(limgName != nil) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* fullPath = [documentsDirectory stringByAppendingPathComponent: limgName];
        UIImage* image = [UIImage imageWithContentsOfFile:fullPath];
        return image;
    }
    return nil;
    
}
//removing an image

- (void)removeImage:(NSString *)imageName {
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:imageName];
    BOOL success = [fileManager removeItemAtPath: fullPath error:NULL];
    if(success == YES)
        DebugLog(@"image removed");
    else
        DebugLog(@"image NOT removed");
}

#pragma mark App Version Check
-(void)checkAppVersion {
    NSInteger syncDays = [INUserDefaultOperations getDateDifferenceInDays:[INUserDefaultOperations getRefreshDate]];
    DebugLog(@"sync days count -----------> %d", syncDays);
    if(syncDays != 0) {
        if ([MEDICAL_APP_DELEGATE networkavailable]) {
            NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:ITUNES_URL] cachePolicy:NO timeoutInterval:30.0];
            conn = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
        }
    } else if(syncDays == 0) {
         DebugLog(@"ver --- >%f", [INUserDefaultOperations getLastAppVersion]);
        if([INUserDefaultOperations getLastAppVersion] != 0.000000)
        {
            DebugLog(@"Last Version = %f", [INUserDefaultOperations getLastAppVersion]);
            float bundleVer = [[[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString *)kCFBundleVersionKey] floatValue];
            DebugLog(@"bundle --- >%f", bundleVer);
            float appVer = [INUserDefaultOperations getLastAppVersion];
            if(appVer > bundleVer)
            {
                DebugLog(@"Update Available");
                [self showUpdateAlert];
            } else if (appVer == bundleVer) {
                DebugLog(@"NO Update Available");
            } else {
                //[self showUpdateAlert];
                DebugLog(@"Over Update");
            }
        } else {
            DebugLog(@"NO Last Version");
            if ([MEDICAL_APP_DELEGATE networkavailable]) {
                NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:ITUNES_URL] cachePolicy:NO timeoutInterval:30.0];
                conn = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
            }
        }
    }
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	
	if(responseData==nil)
	{
		responseData = [[NSMutableData alloc] initWithLength:0];
	}
	
	[responseData appendData:data];
    
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    
    if(responseData != NULL)
	{
        [INUserDefaultOperations setRefreshDate:[NSDate date]];

		NSString *output = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
		DebugLog(@"\n result:%@\n\n", output);
        
        NSError *error = nil;
        NSData *jsonData = [output dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error: &error];
        if (!error)
        {
            if([jsonDict objectForKey:@"results"] != nil && [[jsonDict objectForKey:@"results"] count] > 0)
            {
                if([[[jsonDict objectForKey:@"results"] objectAtIndex:0] objectForKey:@"version"] != nil)
                {
                    NSString *version = [[[jsonDict objectForKey:@"results"] objectAtIndex:0] objectForKey:@"version"];
                    DebugLog(@"v---->%@",version);
                    if([version length] > 0)
                    {
                        float appVer = [version floatValue];
                        DebugLog(@"ver---->%f",appVer);
                        float bundleVer = [[[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString *)kCFBundleVersionKey] floatValue];
                        DebugLog(@"bundle --- >%f", bundleVer);
                        [INUserDefaultOperations setLastAppVersion:appVer];
                        if(appVer > bundleVer)
                        {
                            DebugLog(@"Update Available");
                            [self showUpdateAlert];
                        } else if (appVer == bundleVer) {
                            DebugLog(@"NO Update Available");
                        } else {
                            //[self showUpdateAlert];
                             DebugLog(@"Over Update");
                        }
                    }
                }
            }
        }
    }
    
    conn = nil;
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    
    // [self.responseIndicator stopAnimating];
    conn = nil;
}


-(void)showUpdateAlert
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:UPDATE_TITLE
                                                        message:UPDATE_MSG
                                                       delegate:self
                                              cancelButtonTitle:@"Update Now"
                                              otherButtonTitles: nil];
    [alertView show];
}

#pragma UIAlertView delegate methods
- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if ([title isEqualToString:@"Update Now"]) {
        [self openAppStoreLink];
    }
}

#pragma App Store Link
-(void)openAppStoreLink
{
    if(NSClassFromString(@"SKStoreProductViewController")) { // Checks for iOS 6 feature.
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms-apps://itunes.apple.com/app/id891567879?at=10l6dK"]];
        } else {
            // Initialize Product View Controller
            SKStoreProductViewController *storeProductViewController = [[SKStoreProductViewController alloc] init];
            
            // Configure View Controller
            [storeProductViewController setDelegate:self];
            [storeProductViewController loadProductWithParameters:@{SKStoreProductParameterITunesItemIdentifier : @"891567879"} completionBlock:^(BOOL result, NSError *error) {
                if (error) {
                    DebugLog(@"Error %@ with User Info %@.", error, [error userInfo]);
                    
                } else {
                    // Present Store Product View Controller
                    [self.window.rootViewController presentViewController:storeProductViewController animated:YES completion:nil];
                }
            }];
        }
    }
}

#pragma SKStoreProductViewController delegate methods
- (void)productViewControllerDidFinish:(SKStoreProductViewController *)viewController {
    [self.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    [INEventLogger localyticsSessionWillResignActive];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [INEventLogger localyticsSessionDidEnterBackground];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    [[UAPush shared] resetBadge];
    [INEventLogger localyticsSessionWillEnterForeground];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [INEventLogger localyticsSessionDidBecomeActive:LOCALYTICS_APPID];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [INEventLogger localyticsSessionWillTerminate];
}

@end
