//
//  MedicalRSSFeedsViewController.m
//  Medical
//
//  Created by Kirti Nikam on 25/10/13.
//  Copyright (c) 2013 Kirti Nikam. All rights reserved.
//

#import "MedicalRSSFeedsViewController.h"
#import "RssFeedsParseViewController.h"
#import "constants.h"

@interface MedicalRSSFeedsViewController ()

@end

@implementation MedicalRSSFeedsViewController
@synthesize rssFeedTableView;
@synthesize rssFeedDict;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [INEventLogger logEvent:@"Must_Reads_List"];

    rssFeedDict = [[NSMutableDictionary alloc] init];
    [rssFeedDict setObject:@"http://annonc.oxfordjournals.org/rss/current.xml" forKey:@"Annals of Oncology RSS Feeds"];
    [rssFeedDict setObject:@"http://bloodjournal.hematologylibrary.org/rss/current.xml" forKey:@"Blood RSS Feeds"];
    [rssFeedDict setObject:@"http://feeds.nature.com/bjc/rss/current" forKey:@"BJC RSS Feeds"];
    //[rssFeedDict setObject:@"http://www.cancerresearchuk.org/cancer-info/rss/?rsstype=cancer-news" forKey:@"Cancer Research UK"];
    [rssFeedDict setObject:@"http://clincancerres.aacrjournals.org/rss/current.xml" forKey:@"Clinical Cancer Research"];
    [rssFeedDict setObject:@"http://jco.ascopubs.org/rss/current.xml" forKey:@"JCO RSS Feeds"];
    //[rssFeedDict setObject:@"http://jama.jamanetwork.com/rss/site_3/67.xml" forKey:@"JAMA RSS Feeds"];
    [rssFeedDict setObject:@"http://www.thelancet.com/rssfeed/lancet_current.xml" forKey:@"Lancet RSS Feeds"];
    [rssFeedDict setObject:@"http://www.thelancet.com/rssfeed/lanonc_current.xml" forKey:@"Lancet Oncology RSS Feeds"];
    //[rssFeedDict setObject:@"http://feeds.nature.com/nature/rss/current" forKey:@"Nature RSS Feeds"];
    [rssFeedDict setObject:@"http://www.nejm.org/medical-rss/current-issue" forKey:@"NEJM RSS Feeds"];
    //[rssFeedDict setObject:@"http://www.sciencemag.org/rss/current.xml" forKey:@"Science RSS Feeds"];
    //[rssFeedDict setObject:@"http://theoncologist.alphamedpress.org/rss/current.xml" forKey:@"The Oncologist RSS Feeds"];
    //[rssFeedDict setObject:@"http://www.ascopost.com/rss.aspx" forKey:@"The ASCO Post RSS Feeds"];
    [rssFeedTableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setRssFeedTableView:nil];
    [super viewDidUnload];
}
#pragma mark - Rotation
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return STATIC_ROW_HEIGHT;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [[[rssFeedDict allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"RssFeedCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        
        cell.textLabel.font     =   DEFAULT_FONT(FONT_STATIC_ROW_HEIGHT);
        cell.textLabel.numberOfLines = 3;
        
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = DEFAULT_COLOR_WITH_ALPHA(0.5);
        cell.selectedBackgroundView = bgColorView;
    }
    NSString *key = [[[rssFeedDict allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.row];
    NSString *value = [rssFeedDict objectForKey:key];
    if(![value isEqualToString:@""])
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    else
        cell.accessoryType = UITableViewCellAccessoryNone;
    cell.textLabel.text = key;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:WARNING_TITLE
                                                        message:WARNING_MSG_FOR_WEBLINK
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
	[alertView show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	DebugLog(@"buttonIndex = %d",buttonIndex);
    NSIndexPath *indexPath = [rssFeedTableView indexPathForSelectedRow];
    if (indexPath != nil) {
        NSString *key = [[[rssFeedDict allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.row];
        NSString *value = [rssFeedDict objectForKey:key];
        //DebugLog(@"%@",value);
        if(![value isEqualToString:@""])
        {
            RssFeedsParseViewController *feedviewController = [[RssFeedsParseViewController alloc] initWithNibName:@"RssFeedsParseViewController" bundle:nil] ;
            feedviewController.rssUrlLink = value;
            feedviewController.title = key;
            [self.navigationController pushViewController:feedviewController animated:YES];
        }
        [rssFeedTableView deselectRowAtIndexPath:indexPath animated:YES];
    }
}
@end
