//
//  MedicalDrugInteractionsViewController.h
//  Medical
//
//  Created by Kirti Nikam on 05/03/14.
//  Copyright (c) 2014 Kirti Nikam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>

@interface MedicalDrugInteractionsViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>
@property (retain,nonatomic) NSMutableDictionary *drugsDict;
@property (strong, nonatomic) IBOutlet UITableView *drugsTableView;
@end
