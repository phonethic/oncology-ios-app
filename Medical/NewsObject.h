//
//  NewsObject.h
//  Medical
//
//  Created by Kirti Nikam on 08/04/14.
//  Copyright (c) 2014 Kirti Nikam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NewsObject : NSObject
@property(nonatomic,copy) NSString *postId;
@property(nonatomic,copy) NSString *postTitle;
@property(nonatomic,copy) NSString *postContent;
@end
