//
//  MedicalMeetingViewController.h
//  Medical
//
//  Created by Sagar Mody on 05/11/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <EventKit/EventKit.h>
#import "MeetingObject.h"

@interface MedicalMeetingViewController : UIViewController <UIAlertViewDelegate>
{
    MeetingObject *currentObj;
}
@property (strong, nonatomic) IBOutlet UITableView *medicalMeetingTableView;
@property (strong, nonatomic) NSMutableDictionary *medicalMeetingObjdict;
@property (strong, nonatomic) NSMutableArray *monthsArray;

@property (nonatomic, retain) EKEventStore *eventStore;
@end
