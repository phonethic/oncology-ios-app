//
//  MedicalChemoDrugDetailsViewController.m
//  Medical
//
//  Created by Kirti Nikam on 21/11/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//
#import <sqlite3.h>
#import "MedicalChemoDrugDetailsViewController.h"
//#import "MedicalChemoDrugTypeDetailsViewController.h"
#import "MedicalAppDelegate.h"
#import "MedicalWebViewController.h"

@interface MedicalChemoDrugDetailsViewController ()

@end

@implementation MedicalChemoDrugDetailsViewController
@synthesize btnDosing;
@synthesize btnMixing;
@synthesize btnEmesisRisk;
@synthesize btnExtravasation;
@synthesize btnBlackBoxWarn;
@synthesize btnDoseAdjust;
@synthesize btnAdverseReaction;
@synthesize btnClinicalPharm;
@synthesize btnWarnPrecautions;
@synthesize btnClinicalReferences;
@synthesize selectedDrugName,selectedDrugID;
@synthesize chemoDrugsTableView;
@synthesize chemoDrugsColumnArray;
@synthesize imgViewBackground;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    [btnDosing setTitle:Dosing forState:UIControlStateNormal];
//    [btnMixing setTitle:Mixing forState:UIControlStateNormal];
//    [btnEmesisRisk setTitle:EmesisRisk forState:UIControlStateNormal];
//    [btnExtravasation setTitle:Extravasation forState:UIControlStateNormal];
//    [btnBlackBoxWarn setTitle:BlackBoxWarn forState:UIControlStateNormal];
//    [btnDoseAdjust setTitle:DoseAdjust forState:UIControlStateNormal];
//    [btnAdverseReaction setTitle:AdverseReaction forState:UIControlStateNormal];
//    [btnClinicalPharm setTitle:ClinicalPharm forState:UIControlStateNormal];
//    [btnWarnPrecautions setTitle:WarnPrecautions forState:UIControlStateNormal];
//    [btnClinicalReferences setTitle:ClinicalReferences forState:UIControlStateNormal];
//    
//    [self setButtonProperties:btnDosing];
//    [self setButtonProperties:btnMixing];
//    [self setButtonProperties:btnEmesisRisk];
//    [self setButtonProperties:btnExtravasation];
//    [self setButtonProperties:btnBlackBoxWarn];
//    [self setButtonProperties:btnDoseAdjust];
//    [self setButtonProperties:btnAdverseReaction];
//    [self setButtonProperties:btnClinicalPharm];
//    [self setButtonProperties:btnWarnPrecautions];
//    [self setButtonProperties:btnClinicalReferences];
    
    chemoDrugsTableView.backgroundColor = [UIColor clearColor];
    chemoDrugsTableView.separatorStyle  =   UITableViewCellSeparatorStyleNone;
    
    chemoDrugsColumnArray = [[NSMutableArray alloc] initWithObjects:Dosing, ClinicalReferences, Extravasation, ClinicalPharm, nil];
    
    if ([selectedDrugName length] > 0) {
        NSDictionary *params = @{@"drug_name":selectedDrugName};
        [INEventLogger logEvent:@"Oncology_Drugs_Features" withParams:params];
    }
}

-(void)setButtonProperties:(UIButton *)btn
{
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor lightTextColor] forState:UIControlStateHighlighted];
    btn.backgroundColor     = DEFAULT_COLOR;
    btn.layer.borderColor   = [UIColor whiteColor].CGColor;
    btn.layer.borderWidth   = 1.0;
    btn.layer.cornerRadius  = 8;
}

- (void)viewDidUnload
{
    [self setBtnDosing:nil];
    [self setBtnMixing:nil];
    [self setBtnEmesisRisk:nil];
    [self setBtnExtravasation:nil];
    [self setBtnBlackBoxWarn:nil];
    [self setBtnDoseAdjust:nil];
    [self setBtnAdverseReaction:nil];
    [self setBtnClinicalPharm:nil];
    [self setBtnWarnPrecautions:nil];
    [self setBtnClinicalReferences:nil];
    [self setImgViewBackground:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
#pragma mark - Rotation
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
/*
- (IBAction)btnActionMethod:(id)sender
{
    UIButton *selectedButton = (UIButton *)sender;
    NSString *sqlQueryString = nil;
    if ([selectedButton.titleLabel.text isEqualToString:Dosing])
    {
        sqlQueryString = [NSString stringWithFormat:@"SELECT dosing FROM chemodrugs WHERE rowid = \"%@\" AND drug_name =\"%@\" COLLATE NOCASE",selectedDrugID,selectedDrugName];
    }
    else if ([selectedButton.titleLabel.text isEqualToString:Mixing])
    {
        sqlQueryString = [NSString stringWithFormat:@"SELECT mixing FROM chemodrugs WHERE rowid = \"%@\" AND drug_name =\"%@\" COLLATE NOCASE",selectedDrugID,selectedDrugName];
    }
    else if ([selectedButton.titleLabel.text isEqualToString:EmesisRisk])
    {
        sqlQueryString = [NSString stringWithFormat:@"SELECT emesis_risk FROM chemodrugs WHERE rowid = \"%@\" AND drug_name =\"%@\" COLLATE NOCASE",selectedDrugID,selectedDrugName];
    }
    else if ([selectedButton.titleLabel.text isEqualToString:Extravasation])
    {
        sqlQueryString = [NSString stringWithFormat:@"SELECT extravasation FROM chemodrugs WHERE rowid = \"%@\" AND drug_name =\"%@\" COLLATE NOCASE",selectedDrugID,selectedDrugName];
    }
    else if ([selectedButton.titleLabel.text isEqualToString:BlackBoxWarn])
    {
        sqlQueryString = [NSString stringWithFormat:@"SELECT blackbox_warning FROM chemodrugs WHERE rowid = \"%@\" AND drug_name =\"%@\" COLLATE NOCASE",selectedDrugID,selectedDrugName];
    }
    else if ([selectedButton.titleLabel.text isEqualToString:DoseAdjust])
    {
        sqlQueryString = [NSString stringWithFormat:@"SELECT dose_adjustment_Hepatic,dose_adjustment_Renal FROM chemodrugs WHERE rowid = \"%@\" AND drug_name =\"%@\" COLLATE NOCASE",selectedDrugID,selectedDrugName];
    }
    else if ([selectedButton.titleLabel.text isEqualToString:AdverseReaction])
    {
        sqlQueryString = [NSString stringWithFormat:@"SELECT adverse_reactions FROM chemodrugs WHERE rowid = \"%@\" AND drug_name =\"%@\" COLLATE NOCASE",selectedDrugID,selectedDrugName];
    }
    else if ([selectedButton.titleLabel.text isEqualToString:ClinicalPharm])
    {
        sqlQueryString = [NSString stringWithFormat:@"SELECT clinical_pharmacology FROM chemodrugs WHERE rowid = \"%@\" AND drug_name =\"%@\" COLLATE NOCASE",selectedDrugID,selectedDrugName];
    }
    else if ([selectedButton.titleLabel.text isEqualToString:WarnPrecautions])
    {
        sqlQueryString = [NSString stringWithFormat:@"SELECT warnings_precautions FROM chemodrugs WHERE rowid = \"%@\" AND drug_name =\"%@\" COLLATE NOCASE",selectedDrugID,selectedDrugName];
    }
    else if ([selectedButton.titleLabel.text isEqualToString:ClinicalReferences])
    {
        sqlQueryString = [NSString stringWithFormat:@"SELECT clinical_references FROM chemodrugs WHERE rowid = \"%@\" AND drug_name =\"%@\" COLLATE NOCASE",selectedDrugID,selectedDrugName];
    }
    DebugLog(@"sqlQueryString %@ ",sqlQueryString);

    sqlite3 *databaseP = nil;
    NSString *detailsText = @"";
    if (sqlite3_open([[MEDICAL_APP_DELEGATE databasePath] UTF8String], &databaseP) == SQLITE_OK)
    {
        const char *sqlQueryChar = [sqlQueryString UTF8String];
        
        sqlite3_stmt *compiledStatementP = nil;
        if (sqlite3_prepare_v2(databaseP, sqlQueryChar, -1, &compiledStatementP, NULL) == SQLITE_OK)
        {
            if (sqlite3_step(compiledStatementP) == SQLITE_ROW)
            {
                if ([selectedButton.titleLabel.text isEqualToString:DoseAdjust])
                {
                    NSString *Hepatic =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementP, 0)];
                    NSString *Renal =  [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementP, 1)];
                    detailsText = [NSString stringWithFormat:@"<h1>Hepatic:</h1>\n %@\n\n<h1>Renal:</h1>\n%@",Hepatic,Renal];
                }
                else
                {
                    detailsText = [NSString stringWithCString:(char *)sqlite3_column_text(compiledStatementP, 0) encoding:NSASCIIStringEncoding];
                    DebugLog(@"1-detailsText %@ ",detailsText);

                }
            }
        }
        sqlite3_finalize(compiledStatementP);
        sqlite3_close(databaseP);
    }
    DebugLog(@"2-detailsText %@ ",detailsText);
    if (![detailsText isEqualToString:@""])
    {

        - : -
        μ : &#956;
        ⇒ : &#8658;
        ° : &#176;
        ∞ : &#8734;
        α : &#945;
        β : &#946;
        ′ : &#8242;
        ≥ : &#8805;
        ≤ : &#8804;
        ˚ : &#176;
        − : -
        ’ : '
 
 a) Under Brentuximab: The Degree sign doesnot port over: Eg instead of "°C "or "°F" it shows up as _C and _F
 
 b)  Under Busulfan: The " μ " sign under warnings and precautions
 
 c) Under Cabazitaxel: The " ≥" sign under warnings and precautions (elderly patients)
 
 d) Under "capecitabine": Under clinical pharmacology. There are a number of symbols missing: "∞" ,  5′ (not 5 the ' sign),  "α" and "β"
 
 e) Under Carboplatin: "≤ " Under mixing - miscellaneous
 
 f) under dacarbazine: Under adverse reactions: 1 − 12 is showing up as 1_12 (dash is being replaced by a underscore)
 
 g) Trastuzumab (under dosing): AC⇒T⇒H . Comes out as AC_T_  the arrows are replaced by underscores
 

        

        MedicalWebViewController *webviewController = [[MedicalWebViewController alloc] initWithNibName:@"MedicalWebViewController" bundle:nil];
        webviewController.titleString = selectedButton.titleLabel.text;
        webviewController.details = detailsText;
        [self.navigationController pushViewController:webviewController animated:YES];

    }
}
*/
#pragma mark Table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 66;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [chemoDrugsColumnArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ChemoDrugCell";
    UILabel *lblIndex,*lblName;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        
        UIImageView *bgImageView = [[UIImageView alloc] initWithFrame:cell.bounds];
        bgImageView.image        = [UIImage imageNamed:@"cell_bg.png"];
        bgImageView.backgroundColor    = [UIColor clearColor];
        cell.backgroundView            = bgImageView;
        
        lblIndex = [[UILabel alloc] initWithFrame:CGRectMake(20, 18, 30, 30)];
        lblIndex.tag                = 200;
        lblIndex.font               = DEFAULT_BOLD_FONT(22);
        lblIndex.textAlignment      = NSTextAlignmentCenter;
        lblIndex.textColor          = [UIColor whiteColor];
        lblIndex.backgroundColor    = [UIColor clearColor];
        [cell.contentView addSubview:lblIndex];
        
        lblName = [[UILabel alloc] initWithFrame:CGRectMake(64,7, 230, 50)];
        lblName.tag             =   300;
        lblName.font            =   DEFAULT_BOLD_FONT(18);
        lblName.textAlignment   =   NSTextAlignmentLeft;
        lblName.textColor       =   [UIColor whiteColor];
        lblName.backgroundColor =   [UIColor clearColor];
        lblName.numberOfLines   = 0;
        lblName.adjustsFontSizeToFitWidth = YES;
        [cell.contentView addSubview:lblName];
        
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = DEFAULT_COLOR_WITH_ALPHA(0.5);
        cell.selectedBackgroundView = bgColorView;
    }
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    lblIndex        = (UILabel *)[cell viewWithTag:200];
    lblName         = (UILabel *)[cell viewWithTag:300];
    lblIndex.text   = [NSString stringWithFormat:@"%d",indexPath.row+1];
    lblName.text    = [chemoDrugsColumnArray objectAtIndex:indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *textStringToMatch = [chemoDrugsColumnArray objectAtIndex:indexPath.row];
    NSString *sqlQueryString = nil;
    
    NSDictionary *params = @{@"drug_name":selectedDrugName,@"feature_name":textStringToMatch};
    [INEventLogger logEvent:@"Oncology_Drugs_Feature_Details" withParams:params];
    
    if ([textStringToMatch isEqualToString:Dosing])
    {
        sqlQueryString = [NSString stringWithFormat:@"SELECT dosing_warning_reactions FROM chemo_drugs WHERE rowid = \"%@\" AND drug_name =\"%@\" COLLATE NOCASE",selectedDrugID,selectedDrugName];
    }
//    else if ([textStringToMatch isEqualToString:Mixing])
//    {
//        sqlQueryString = [NSString stringWithFormat:@"SELECT mixing,PACKAGE_INSERT FROM chemodrugs WHERE rowid = \"%@\" AND drug_name =\"%@\" COLLATE NOCASE",selectedDrugID,selectedDrugName];
//    }
//    else if ([textStringToMatch isEqualToString:EmesisRisk])
//    {
//        sqlQueryString = [NSString stringWithFormat:@"SELECT emesis_risk,PACKAGE_INSERT FROM chemodrugs WHERE rowid = \"%@\" AND drug_name =\"%@\" COLLATE NOCASE",selectedDrugID,selectedDrugName];
//    }
    else if ([textStringToMatch isEqualToString:Extravasation])
    {
        sqlQueryString = [NSString stringWithFormat:@"SELECT extravasation FROM chemo_drugs WHERE rowid = \"%@\" AND drug_name =\"%@\" COLLATE NOCASE",selectedDrugID,selectedDrugName];
    }
    else if ([textStringToMatch isEqualToString:BlackBoxWarn])
    {
        sqlQueryString = [NSString stringWithFormat:@"SELECT boxed_warning FROM chemo_drugs WHERE rowid = \"%@\" AND drug_name =\"%@\" COLLATE NOCASE",selectedDrugID,selectedDrugName];
    }
    else if ([textStringToMatch isEqualToString:DoseAdjust])
    {
        sqlQueryString = [NSString stringWithFormat:@"SELECT dose_adjustment_Hepatic,dose_adjustment_Renal FROM chemo_drugs WHERE rowid = \"%@\" AND drug_name =\"%@\" COLLATE NOCASE",selectedDrugID,selectedDrugName];
    }
//    else if ([textStringToMatch isEqualToString:AdverseReaction])
//    {
//        sqlQueryString = [NSString stringWithFormat:@"SELECT adverse_reactions,PACKAGE_INSERT FROM chemodrugs WHERE rowid = \"%@\" AND drug_name =\"%@\" COLLATE NOCASE",selectedDrugID,selectedDrugName];
//    }
    else if ([textStringToMatch isEqualToString:ClinicalPharm])
    {
        sqlQueryString = [NSString stringWithFormat:@"SELECT clinical_pharmacology FROM chemo_drugs WHERE rowid = \"%@\" AND drug_name =\"%@\" COLLATE NOCASE",selectedDrugID,selectedDrugName];
    }
//    else if ([textStringToMatch isEqualToString:WarnPrecautions])
//    {
//        sqlQueryString = [NSString stringWithFormat:@"SELECT warnings_precautions,PACKAGE_INSERT FROM chemodrugs WHERE rowid = \"%@\" AND drug_name =\"%@\" COLLATE NOCASE",selectedDrugID,selectedDrugName];
//    }
    else if ([textStringToMatch isEqualToString:ClinicalReferences])
    {
        sqlQueryString = [NSString stringWithFormat:@"SELECT clinical_references,PACKAGE_INSERT FROM chemo_drugs WHERE rowid = \"%@\" AND drug_name =\"%@\" COLLATE NOCASE",selectedDrugID,selectedDrugName];
    }
    DebugLog(@"sqlQueryString %@ ",sqlQueryString);
    
    sqlite3 *databaseP = nil;
    NSString *detailsText = @"";
    NSString *packageInsertLink = @"";
    if (sqlite3_open([[MEDICAL_APP_DELEGATE databasePath] UTF8String], &databaseP) == SQLITE_OK)
    {
        const char *sqlQueryChar = [sqlQueryString UTF8String];
        
        sqlite3_stmt *compiledStatementP = nil;
        if (sqlite3_prepare_v2(databaseP, sqlQueryChar, -1, &compiledStatementP, NULL) == SQLITE_OK)
        {
            if (sqlite3_step(compiledStatementP) == SQLITE_ROW)
            {
                if ([textStringToMatch isEqualToString:DoseAdjust])
                {
                    NSString *Hepatic =  [NSString stringWithUTF8String:(const char*)sqlite3_column_text(compiledStatementP, 0)];
                    NSString *Renal   =  [NSString stringWithUTF8String:(const char*)sqlite3_column_text(compiledStatementP, 1)];
//                    packageInsertLink   =  [NSString stringWithCString:(char *)sqlite3_column_text(compiledStatementP, 2) encoding:NSASCIIStringEncoding];

                    if((Hepatic != (NSString *)[NSNull null] && [Hepatic length] > 0) && (Renal != (NSString *)[NSNull null] && [Renal length] > 0 )){
                        //detailsText = [NSString stringWithFormat:@"<h1>Hepatic</h1>\n %@\n\n<h1>Renal</h1>\n%@",Hepatic,Renal];
                        detailsText = [NSString stringWithFormat:@"%@%@",Hepatic,Renal];
                    }
                    else if((Hepatic != (NSString *)[NSNull null] && [Hepatic length] > 0) && (Renal == (NSString *)[NSNull null] || [Renal length] > 0 )){
//                        detailsText = [NSString stringWithFormat:@"<h1>Hepatic</h1>\n %@\n\n",Hepatic];
                        detailsText = [NSString stringWithFormat:@"%@",Hepatic];
                    }
                    else if((Hepatic == (NSString *)[NSNull null] || [Hepatic length] > 0) && (Renal != (NSString *)[NSNull null] && [Renal length] > 0 )){
//                        detailsText = [NSString stringWithFormat:@"<h1>Renal</h1>\n%@",Renal];
                        detailsText = [NSString stringWithFormat:@"%@",Renal];
                    }
                }
//                else if ([textStringToMatch isEqualToString:ClinicalReferences])
//                {
//                    NSString *clinical_references =  [NSString stringWithCString:(char *)sqlite3_column_text(compiledStatementP, 0) encoding:NSASCIIStringEncoding];
//                    NSString *packageInsertLink   =  [NSString stringWithCString:(char *)sqlite3_column_text(compiledStatementP, 1) encoding:NSASCIIStringEncoding];
//                    
//                    if(packageInsertLink != (NSString *)[NSNull null] && [packageInsertLink length] > 0){
//                        detailsText = [[clinical_references stringByReplacingOccurrencesOfString:@"package insert" withString:[NSString stringWithFormat:@"<a class=\"link\" href=\"%@\">PackageInsert</a>",packageInsertLink]] copy];
//                      DebugLog(@"Text %@ ",detailsText);
//                    }else{
//                        detailsText = clinical_references;
//                    }
//                }
                else
                {
                     if ([textStringToMatch isEqualToString:Dosing] || [textStringToMatch isEqualToString:BlackBoxWarn] || [textStringToMatch isEqualToString:Extravasation]  || [textStringToMatch isEqualToString:ClinicalPharm])
                     {
                    detailsText = [NSString stringWithUTF8String:(const char*)sqlite3_column_text(compiledStatementP, 0)];
                    //packageInsertLink   =  [NSString stringWithCString:(char *)sqlite3_column_text(compiledStatementP, 1) encoding:NSASCIIStringEncoding];
                     } else {
                         detailsText = [NSString stringWithUTF8String:(const char*)sqlite3_column_text(compiledStatementP, 0)];
                         packageInsertLink   =  [NSString stringWithCString:(char *)sqlite3_column_text(compiledStatementP, 1) encoding:NSASCIIStringEncoding];
                     }
                }
                DebugLog(@"\n\n1-detailsText %@ ",detailsText);
                
                if(packageInsertLink != (NSString *)[NSNull null] && [packageInsertLink length] > 0){
                    
                    for (NSString *stringToReplace in @[@"package insert",@"PackageInsert",@"Package Insert"]) {
                        detailsText = [detailsText stringByReplacingOccurrencesOfString:stringToReplace withString:[NSString stringWithFormat:@"<a class=\"link\" href=\"%@\">PackageInsert</a>",packageInsertLink]];
                    }
//                    detailsText = [detailsText stringByReplacingOccurrencesOfString:@"package insert" withString:[NSString stringWithFormat:@"<a class=\"link\" href=\"%@\">PackageInsert</a>",packageInsertLink]];
//                    
//                    detailsText = [detailsText stringByReplacingOccurrencesOfString:@"PackageInsert" withString:[NSString stringWithFormat:@"  <a class=\"link\" href=\"%@\">PackageInsert</a>",packageInsertLink]];
//
//                    detailsText = [detailsText stringByReplacingOccurrencesOfString:@"Package Insert" withString:[NSString stringWithFormat:@"  <a class=\"link\" href=\"%@\">PackageInsert</a>",packageInsertLink]];

                    DebugLog(@"\n\nadded packageInsertLink Text %@ ",detailsText);
                }
            }
        }
        sqlite3_finalize(compiledStatementP);
        sqlite3_close(databaseP);
    }
    DebugLog(@"\n\n2-detailsText %@ ",detailsText);
    DebugLog(@"\n\n params %@ ",params);
    if (detailsText != (NSString *)[NSNull null] && [detailsText length] > 0)
    {
        if ([textStringToMatch isEqualToString:Dosing] || [textStringToMatch isEqualToString:BlackBoxWarn])
        {
            MedicalWebViewController *webviewController = [[MedicalWebViewController alloc] initWithNibName:@"MedicalWebViewController" bundle:nil] ;
            webviewController.medicalLink = detailsText;
            webviewController.title = selectedDrugName;
            [self.navigationController pushViewController:webviewController animated:YES];
        } else {
            MedicalWebViewController *webviewController = [[MedicalWebViewController alloc] initWithNibName:@"MedicalWebViewController" bundle:nil];
            webviewController.titleString   = selectedDrugName;
            webviewController.details       = detailsText;
            webviewController.dictParams    = [params mutableCopy];
            [self.navigationController pushViewController:webviewController animated:YES];
        }
        
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
