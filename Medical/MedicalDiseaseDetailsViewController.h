//
//  MedicalDiseaseDetailsViewController.h
//  Medical
//
//  Created by Kirti Nikam on 03/11/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MedicalDiseaseDetailsViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextView *diseaseTextView;
@property (strong, nonatomic) IBOutlet UILabel *diseaseName;
@property (nonatomic,copy) NSString *diseaseDetailText;
@property (nonatomic,copy) NSString *diseaseNameText;
@end
