//
//  MedicalChemoDrugTypeDetailsViewController.h
//  Medical
//
//  Created by Kirti Nikam on 21/11/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MedicalChemoDrugTypeDetailsViewController : UIViewController
@property (copy,nonatomic) NSString *titleString;
@property (copy,nonatomic) NSString *details;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UIWebView *detailsWebView;
@end
