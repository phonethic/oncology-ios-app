//
//  MedicalCancerCenterDetailViewController.m
//  Medical
//
//  Created by Rishi on 05/11/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import "MedicalCancerCenterDetailViewController.h"
#import "MedicalAppDelegate.h"
#import "DisplayMap.h"

@interface MedicalCancerCenterDetailViewController ()

@end

@implementation MedicalCancerCenterDetailViewController
@synthesize centerMapView;
@synthesize centerName;
@synthesize detailsArray,detailsTableView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSDictionary *params = @{@"center_name": centerName};
    [INEventLogger logEvent:@"Cancer_Centers_Details" withParams:params];

    [self readCancerCentersFromDatabase];
    
    if (latValue == 0 && longValue == 0)
    {
        [centerMapView setHidden:YES];
    }
    else
    {
      //  detailsTableView.tableFooterView = centerMapView;
        
        [centerMapView setMapType:MKMapTypeStandard];
        [centerMapView setZoomEnabled:YES];
        [centerMapView setScrollEnabled:YES];
        [centerMapView setDelegate:self];

        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(latValue,longValue);

        MKCoordinateSpan span;
        span.latitudeDelta  = 0.01;
        span.longitudeDelta = 0.01;
        
        MKCoordinateRegion region;
        region.center = coordinate;
        region.span = span;

        DisplayMap *ann = [[DisplayMap alloc] init];
        ann.title = centerName;
        ann.subtitle = @"";
        ann.coordinate = region.center;
        [centerMapView addAnnotation:ann];
        
        [centerMapView setRegion:region animated:YES];
        [centerMapView regionThatFits:region];
        [centerMapView selectAnnotation:ann animated:NO];
    }
    [detailsTableView reloadData];
    detailsTableView.contentSize = CGSizeMake(detailsTableView.frame.size.width,detailsTableView.contentSize.height+detailsTableView.sectionFooterHeight+100);
    [detailsTableView sizeToFit];
}

-(void) readCancerCentersFromDatabase {
    if (detailsArray == nil) {
        detailsArray = [[NSMutableArray alloc] init];
    }else{
        [detailsArray removeAllObjects];
    }
	// Setup the database object
	sqlite3 *database;
    //university text,director_name text,address
    //text,telphoneno text,faxno text,lattitude text,longitude text
	// Open the database from the users filessytem
	if(sqlite3_open([[MEDICAL_APP_DELEGATE databasePath] UTF8String], &database) == SQLITE_OK) {
		// Setup the SQL Statement and compile it for faster access
        NSString *centerquerySQL = [NSString stringWithFormat:@"select * from cancer_centers where center_name=\"%@\" COLLATE NOCASE", self.centerName];
        const char *sqlStatement = [centerquerySQL UTF8String];
		sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
			// Loop through the results and add them to the feeds array
			while(sqlite3_step(compiledStatement) == SQLITE_ROW)
            {
				// Read the data from the result row
                NSString *cancerName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 0)];
                NSString *cancerUniv = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 1)];
               // NSString *cancerDir = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 2)];
                NSString *cancerAdd = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 3)];
                NSString *cancerTel = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 4)];
                NSString *cancerFax = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 5)];
                latValue = sqlite3_column_double(compiledStatement, 6);
                longValue = sqlite3_column_double(compiledStatement, 7);
               // DebugLog(@"-%@-%@-%@-%@-%@-%@-",cancerName,cancerUniv,cancerDir,cancerAdd,cancerTel,cancerFax);
                if ([cancerName length] > 0) {
                    [detailsArray addObject:cancerName];
                }
                if ([cancerUniv length] > 0) {
                    [detailsArray addObject:[NSString stringWithFormat:@"University: %@",cancerUniv]];
                }
//                if ([cancerDir length] > 0) {
//                    [detailsArray addObject:[NSString stringWithFormat:@"Director: %@",cancerDir]];
//                }
                if ([cancerAdd length] > 0) {
                    [detailsArray addObject:[NSString stringWithFormat:@"Address: %@",cancerAdd]];
                }
                if ([cancerTel length] > 0) {
                    [detailsArray addObject:[NSString stringWithFormat:@"Tel: %@",cancerTel]];
                }
                if ([cancerFax length] > 0) {
                    [detailsArray addObject:[NSString stringWithFormat:@"Fax: %@",cancerFax]];
                }
            }
        }
		// Release the compiled statement from memory
		sqlite3_finalize(compiledStatement);
	}
	sqlite3_close(database);
}

-(MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:(id <MKAnnotation>)annotation {
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    MKPinAnnotationView *pinView = nil;
    if(annotation != centerMapView.userLocation)
    {
        static NSString *defaultPinID = @"MedicalCenterPin";
        pinView = (MKPinAnnotationView *)[centerMapView dequeueReusableAnnotationViewWithIdentifier:defaultPinID];
        if ( pinView == nil )
            pinView = [[MKPinAnnotationView alloc]
                       initWithAnnotation:annotation reuseIdentifier:defaultPinID];
        if ([centerName hasPrefix:@"James Cancer Hospital and Solove Research Institute"]) {
//            pinView.pinColor = MKPinAnnotationColorPurple;
            pinView.image = [UIImage imageNamed:@"PinPurple.png"];
        }else{
//            pinView.pinColor = MKPinAnnotationColorRed;
            pinView.image = [UIImage imageNamed:@"PinBlue.png"];
        }
        pinView.canShowCallout = YES;
       // pinView.animatesDrop = YES;
    }
    else {
        [centerMapView.userLocation setTitle:@"I am here"];
    }
    return pinView;
}

- (void)viewDidUnload
{
    [self setCenterMapView:nil];
    [self setDetailsTableView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark - Rotation
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}


#pragma mark Table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == detailsArray.count) {
        if (!centerMapView.hidden) {
            return centerMapView.frame.size.height;
        }
    }else{
        NSString *stringToMatch = [detailsArray objectAtIndex:indexPath.row];
        UIFont *font;
        if ([stringToMatch hasPrefix:@"University:"]) {
            font     =   DEFAULT_FONT(16);
        }
//        else if ([stringToMatch hasPrefix:@"Director:"]) {
//            font     =   DEFAULT_FONT(14);
//        }
        else if ([stringToMatch hasPrefix:@"Address:"]) {
            font     =   DEFAULT_FONT(14);
        }else if ([stringToMatch hasPrefix:@"Tel:"] || [stringToMatch hasPrefix:@"Fax:"]) {
            font     =   DEFAULT_FONT(14);
        }else {
            font     =   DEFAULT_BOLD_FONT(17);
        }
        CGSize size = [stringToMatch sizeWithFont:font constrainedToSize:CGSizeMake(280, 999) lineBreakMode:NSLineBreakByWordWrapping];
        DebugLog(@"%f",size.height);
        return size.height + 40;
    }
    return 0;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (centerMapView.hidden) {
        return [detailsArray count];
    }
    return [detailsArray count] + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"RssFeedCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.numberOfLines = 0;
    }
    cell.accessoryType = UITableViewCellAccessoryNone;
    if (indexPath.row == detailsArray.count) {
        cell.textLabel.text = @"";
        if (!centerMapView.hidden) {
            [cell.contentView addSubview:centerMapView];
        }
    }else{
        NSString *stringToMatch = [detailsArray objectAtIndex:indexPath.row];
        if ([stringToMatch hasPrefix:@"University:"]) {
            cell.textLabel.font     =   DEFAULT_FONT(16);
            cell.textLabel.textAlignment = NSTextAlignmentLeft;
        }
//        else if ([stringToMatch hasPrefix:@"Director:"]) {
//            cell.textLabel.font     =   DEFAULT_FONT(14);
//            cell.textLabel.textAlignment = NSTextAlignmentLeft;
//        }
        else if ([stringToMatch hasPrefix:@"Address:"]) {
            cell.textLabel.font     =   DEFAULT_FONT(14);
            cell.textLabel.textAlignment = NSTextAlignmentLeft;
        }else if ([stringToMatch hasPrefix:@"Tel:"] || [stringToMatch hasPrefix:@"Fax:"]) {
            cell.textLabel.font     =   DEFAULT_FONT(14);
            cell.textLabel.textAlignment = NSTextAlignmentLeft;
        }else {
            cell.textLabel.font     =   DEFAULT_BOLD_FONT(17);
            cell.textLabel.textAlignment = NSTextAlignmentCenter;
        }
        cell.textLabel.text = stringToMatch;
    }
    return cell;
}
@end
