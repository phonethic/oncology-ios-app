//
//  MedicalPrintImage.h
//  Medical
//
//  Created by Kirti Nikam on 19/10/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface MedicalPrintImage : UIPrintPageRenderer
@property (readwrite, strong) UIImage *printImage;
@end
