//
//  MedicalECOGStatusViewController.h
//  Medical
//
//  Created by Kirti Nikam on 17/10/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface MedicalECOGStatusViewController : UIViewController<UIActionSheetDelegate>
@property(strong,nonatomic) NSMutableArray *ECOGStatusarray;

@property (strong, nonatomic) IBOutlet UITableView *ecogTableView;
@property (strong, nonatomic) IBOutlet UILabel *lblUnderLine;
@property (strong, nonatomic) IBOutlet UILabel *lblGrade;
@property (strong, nonatomic) IBOutlet UILabel *lblDefination;
@property (strong, nonatomic) IBOutlet UIButton *infoBtn;
- (IBAction)infoButtonAction:(id)sender;
@end
