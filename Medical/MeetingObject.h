//
//  MeetingObject.h
//  Medical
//
//  Created by Sagar Mody on 05/11/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MeetingObject : NSObject
@property(nonatomic,copy) NSString *name;
@property(nonatomic,copy) NSString *startDate;
@property(nonatomic,copy) NSString *endDate;
@property(nonatomic,copy) NSString *place;
@property(nonatomic,copy) NSString *weblink;
@property(nonatomic,copy) NSString *eventIdentifier;

-(id)initWithName:(NSString *)leventName  eventStartDate:(NSString *)leventStartDate eventEndDate:(NSString *)leventEndDate eventPlace:(NSString *)leventPlace eventLink:(NSString *)leventLink eventId:(NSString *) leventId;
@end
