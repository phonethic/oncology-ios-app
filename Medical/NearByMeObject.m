//
//  NearByMeObject.m
//  Medical
//
//  Created by Kirti Nikam on 26/11/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import "NearByMeObject.h"

@implementation NearByMeObject
@synthesize nearbymerowid,durationValue,distanceValue,durationText,distanceText;
@synthesize centerName;
@end
