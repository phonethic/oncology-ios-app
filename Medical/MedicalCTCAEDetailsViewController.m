//
//  MedicalCTCAEDetailsViewController.m
//  Medical
//
//  Created by Kirti Nikam on 30/10/13.
//  Copyright (c) 2013 Kirti Nikam. All rights reserved.
//
#import <sqlite3.h>
#import "MedicalAppDelegate.h"

#import "MedicalCTCAEDetailsViewController.h"


#define GRADE1  @"Grade 1"
#define GRADE2  @"Grade 2"
#define GRADE3  @"Grade 3"
#define GRADE4  @"Grade 4"
#define GRADE5  @"Grade 5"
#define TERM_DEFINITION  @"Definition"

@interface MedicalCTCAEDetailsViewController ()

@end

@implementation MedicalCTCAEDetailsViewController
@synthesize selectedTermID,selectedTermName;
@synthesize termObjectDict;
@synthesize ctcaedetailTableView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self loadCTCAEDataFromDatabase];
    NSDictionary *params = @{@"ctcae_name":selectedTermName};
    [INEventLogger logEvent:@"CTCAE_Details" withParams:params];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setCtcaedetailTableView:nil];
    [super viewDidUnload];
}
#pragma mark - Rotation
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

-(void)loadCTCAEDataFromDatabase{
    if (termObjectDict == nil) {
        termObjectDict = [[NSMutableDictionary alloc] init];
    }else{
        [termObjectDict removeAllObjects];
    }
    sqlite3 *databaseP = nil;
    if (sqlite3_open([[MEDICAL_APP_DELEGATE databasePath] UTF8String], &databaseP) == SQLITE_OK) {
        NSString *sqlQueryString = [NSString stringWithFormat:@"SELECT grade1,grade2,grade3,grade4,grade5,term_definition FROM CTCAE WHERE ROWID = %d",selectedTermID];
        
        const char *sqlQueryChar = [sqlQueryString UTF8String];
        sqlite3_stmt *statement = nil;
        if (sqlite3_prepare(databaseP, sqlQueryChar, -1, &statement, NULL)  == SQLITE_OK) {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                NSString *grade1  = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
                NSString *grade2  = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)];
                NSString *grade3  = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 2)];
                NSString *grade4  = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 3)];
                NSString *grade5  = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 4)];
                NSString *termDefinition  = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 5)];
                
//                DebugLog(@"-%@-%@-%@-%@-%@-%@-",grade1,grade2,grade3,grade4,grade5,termDefinition);
                [termObjectDict setObject:grade1 forKey:GRADE1];
                [termObjectDict setObject:grade2 forKey:GRADE2];
                [termObjectDict setObject:grade3 forKey:GRADE3];
                [termObjectDict setObject:grade4 forKey:GRADE4];
                [termObjectDict setObject:grade5 forKey:GRADE5];
                [termObjectDict setObject:termDefinition forKey:TERM_DEFINITION];
            }
            sqlite3_finalize(statement);
        }
        sqlite3_close(databaseP);
    }
    //    DebugLog(@"%@",termObjectDict);
    [ctcaedetailTableView reloadData];
}

#pragma mark Table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[self.termObjectDict allKeys] count];
}

-(CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section
{
    return 35;
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView* headerView          = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 35)];
    headerView.backgroundColor  = DEFAULT_COLOR;
    
    UILabel *headerLabel        = [[UILabel alloc] initWithFrame:CGRectZero];
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.frame           = CGRectMake(10,0,290,35);
    headerLabel.textAlignment   = NSTextAlignmentLeft;
    headerLabel.font            = DEFAULT_BOLD_FONT(17);
    switch (section) {
        case 0:
            headerLabel.text            =   TERM_DEFINITION;
            break;
        case 1:
            headerLabel.text            =   GRADE1;
            break;
        case 2:
            headerLabel.text            =   GRADE2;
            break;
        case 3:
            headerLabel.text            =   GRADE3;
            break;
        case 4:
            headerLabel.text            =   GRADE4;
            break;
        case 5:
            headerLabel.text            =   GRADE5;
            break;
    }
    headerLabel.textColor       = [UIColor whiteColor];
    [headerView addSubview:headerLabel];
    return headerView;
}

//-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//{
//    switch (section) {
//        case 0:
//            return TERM_DEFINITION;
//            break;
//        case 1:
//            return GRADE1;
//            break;
//        case 2:
//            return GRADE2;
//            break;
//        case 3:
//            return GRADE3;
//            break;
//        case 4:
//            return GRADE4;
//            break;
//        case 5:
//            return GRADE5;
//            break;
//    }
//    return nil;
//}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    //  return 120;
    NSString *str;
    switch (indexPath.section) {
        case 0:
            str   = [self.termObjectDict valueForKey:TERM_DEFINITION];
            break;
        case 1:
            str   = [self.termObjectDict valueForKey:GRADE1];
            break;
        case 2:
            str   = [self.termObjectDict valueForKey:GRADE2];
            break;
        case 3:
            str   = [self.termObjectDict valueForKey:GRADE3];
            break;
        case 4:
            str   = [self.termObjectDict valueForKey:GRADE4];
            break;
        case 5:
            str   = [self.termObjectDict valueForKey:GRADE5];
            break;
    }
    
    CGSize size = [str sizeWithFont:DEFAULT_FONT(14) constrainedToSize:CGSizeMake(280, 999) lineBreakMode:NSLineBreakByWordWrapping];
    //    DebugLog(@"%f",size.height);
    return size.height + 40;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    switch (section) {
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
            return 1;
            break;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CACTEDetailCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.textLabel.font             =   DEFAULT_FONT(14);
        cell.textLabel.numberOfLines    =   0;

//        UIView *bgColorView         = [[UIView alloc] init];
//        bgColorView.backgroundColor = DEFAULT_COLOR_WITH_ALPHA(0.5);
//        cell.selectedBackgroundView = bgColorView;
    }
    
    NSString *cName = @"";
    switch (indexPath.section) {
        case 0:
            cName   = [self.termObjectDict valueForKey:TERM_DEFINITION];
            break;
        case 1:
            cName   = [self.termObjectDict valueForKey:GRADE1];
            break;
        case 2:
            cName   = [self.termObjectDict valueForKey:GRADE2];
            break;
        case 3:
            cName   = [self.termObjectDict valueForKey:GRADE3];
            break;
        case 4:
            cName   = [self.termObjectDict valueForKey:GRADE4];
            break;
        case 5:
            cName   = [self.termObjectDict valueForKey:GRADE5];
            break;
    }
    
    if ([cName isEqualToString:@""]) {
         cell.textLabel.text = @"-";
    }else{
        cell.textLabel.text  = [cName stringByReplacingCharactersInRange:NSMakeRange(0,1)
                                                           withString:[[cName  substringToIndex:1] capitalizedString]];
    }
    return cell;
}
@end
