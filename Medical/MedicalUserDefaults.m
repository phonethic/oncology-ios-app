//
//  MedicalUserDefaults.m
//  Medical
//
//  Created by Kirti Nikam on 23/04/14.
//  Copyright (c) 2014 Kirti Nikam. All rights reserved.
//

#import "MedicalUserDefaults.h"

@implementation MedicalUserDefaults
+(void)setPlayTermsPrivacyAnimation:(BOOL)value{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:value forKey:@"TermsPrivacyAnimation"];
    [defaults synchronize];
}

+(BOOL)isDoneTermsPrivacyAnimation{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    //DebugLog(@"TermsPrivacyAnimation %d",[defaults boolForKey:@"TermsPrivacyAnimation"]);
    return [defaults boolForKey:@"TermsPrivacyAnimation"];
}

+(void)setLicenseAgreement:(BOOL)value{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:value forKey:@"LicenseAgreement"];
    [defaults synchronize];
}
+(BOOL)isAcceptedLicenseAgreement{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    //DebugLog(@"LicenseAgreement %d",[defaults boolForKey:@"LicenseAgreement"]);
    return [defaults boolForKey:@"LicenseAgreement"];
}

+(void)addAlertWithPostId:(int)postId title:(NSString *)postTitle content:(NSString *)postContent date:(NSString *)postDate
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setInteger:postId forKey:@"Post_Id"];
    [userDefaults setObject:postTitle forKey:@"Post_Title"];
    [userDefaults setObject:postContent forKey:@"Post_Content"];
    [userDefaults setObject:postDate forKey:@"Post_Date"];
    [userDefaults setObject:[NSDate date] forKey:@"Post_Sync_Date"];
    [userDefaults synchronize];
}
+(void)setPostSyncDate{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:[NSDate date] forKey:@"Post_Sync_Date"];
    [userDefaults synchronize];
}

+(NSDate *)getPostSyncDate{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    return [userDefaults objectForKey:@"Post_Sync_Date"];
}

+(int)getPostId
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    return [userDefaults integerForKey:@"Post_Id"];
}

+(NSString *)getPostDate
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    return [userDefaults objectForKey:@"Post_Date"];
}

+(NSString *)getPostTitle
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    return [userDefaults objectForKey:@"Post_Title"];
}

+(NSString *)getPostContent
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    return [userDefaults objectForKey:@"Post_Content"];
}

+(void)setChemoOrderHelpInfoSceenCount
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    int preValue = [userDefaults integerForKey:@"ChemoOrder_HelpInfo_Sceen_Count"];
    [userDefaults setInteger:++preValue forKey:@"ChemoOrder_HelpInfo_Sceen_Count"];
    [userDefaults synchronize];
}

+(int)getChemoOrderHelpInfoSceenCount
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    return [userDefaults integerForKey:@"ChemoOrder_HelpInfo_Sceen_Count"];

}
@end
