//
//  MedicalChemoDrugsViewController.h
//  Medical
//
//  Created by Kirti Nikam on 21/11/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ChemoDrugObject;

@interface MedicalChemoDrugsViewController : UIViewController<UISearchBarDelegate>
{
    ChemoDrugObject *tempDrugObj;
    BOOL isSearchOn;
    BOOL canSelectRow;
    
    double keyboardHeight;
}
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UITableView *chemoDrugsTableView;

@property (strong,nonatomic) NSMutableArray *chemodrugsNameArray;
@property (retain,nonatomic) NSMutableDictionary *sections;
@property (retain,nonatomic) NSMutableArray *searchArray;
@end
