//
//  MedicalBSAViewController.h
//  Medical
//
//  Created by Kirti Nikam on 08/10/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import <UIKit/UIKit.h>

#define M2 [NSString stringWithFormat:@"m\u00B2"]

@protocol BSAViewControllerDelegate <NSObject>
-(void) setBSResultValue:(NSString*)result weight:(NSString*)weight height:(NSString*)height;
@end

@interface MedicalBSAViewController : UIViewController<UITextFieldDelegate,UIActionSheetDelegate>{
    int TEXTTAG;
    BOOL keyboardIsShown;
    
    int HeightViewFLAG;
    double heightValue;
    double weightValue;
}

@property (nonatomic, weak) id<BSAViewControllerDelegate> bsadelegate;
@property(copy,nonatomic) NSString *BSAText;
@property(copy,nonatomic) NSString *weightText;
@property(copy,nonatomic) NSString *heightText;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) IBOutlet UIView *viewWeight;
@property (strong, nonatomic) IBOutlet UIView *viewHeightInCM;
@property (strong, nonatomic) IBOutlet UIView *viewHeightInFT;

@property (strong, nonatomic) IBOutlet UITextField *weightTextField;
@property (strong, nonatomic) IBOutlet UILabel *weightLabel;

@property (strong, nonatomic) IBOutlet UILabel *heightLabel;
@property (strong, nonatomic) IBOutlet UITextField *heightTextField;
@property (strong, nonatomic) IBOutlet UITextField *ftTextField;
@property (strong, nonatomic) IBOutlet UITextField *inchTextField;

@property (strong, nonatomic) IBOutlet UILabel *lblBSA;
@property (strong, nonatomic) IBOutlet UILabel *labelResult;

@property (strong, nonatomic) IBOutlet UIView *heightkeyboardView;
@property (strong, nonatomic) IBOutlet UIView *weightkeyboardView;

@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentControl;


@property (strong, nonatomic) IBOutlet UILabel *lblErrWeight;
@property (strong, nonatomic) IBOutlet UILabel *lblErrHeight;

@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *viewCollection;
@property (strong, nonatomic) IBOutletCollection(UITextField) NSArray *txtCollection;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *lblCollection;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *lblErrCollection;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *keyBoardBtnCollection;
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *keyBoardViewCollection;


-(void) setBSAValue:(NSString*)bsa weight:(NSString*)weight height:(NSString*)height;


@end
