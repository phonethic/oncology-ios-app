//
//  MedicalCarboplatinViewController.h
//  Medical
//
//  Created by Kirti Nikam on 09/10/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import <UIKit/UIKit.h>

#define UMOLSTRING @"µmol/L"
#define MGDLSTRING @"mg/dL"
#define MMOLSTRING @"mmol/L"

@interface MedicalCarboplatinViewController : UIViewController<UITextFieldDelegate,UIActionSheetDelegate>{
    int TEXTTAG;
    
    BOOL keyboardIsShown;    
    int HeightViewFLAG;
}

@property (readwrite, nonatomic) int comeFromOtherThanCalculator;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) IBOutlet UIView *viewAge;
@property (strong, nonatomic) IBOutlet UIView *viewWeight;
@property (strong, nonatomic) IBOutlet UIView *viewHeightInCM;
@property (strong, nonatomic) IBOutlet UIView *viewHeightInFT;
@property (strong, nonatomic) IBOutlet UIView *viewSerumCreatinine;
@property (strong, nonatomic) IBOutlet UIView *viewTargetAUC;

@property (strong, nonatomic) IBOutlet UITextField *ageTextField;
@property (strong, nonatomic) IBOutlet UITextField *weightTextField;
@property (strong, nonatomic) IBOutlet UITextField *heightTextField;
@property (strong, nonatomic) IBOutlet UITextField *ftTextField;
@property (strong, nonatomic) IBOutlet UITextField *inchTextField;
@property (strong, nonatomic) IBOutlet UITextField *serumTextField;
@property (strong, nonatomic) IBOutlet UITextField *targetAUCTextField;

@property (strong, nonatomic) IBOutlet UILabel *weightLabel;
@property (strong, nonatomic) IBOutlet UILabel *heightLabel;
@property (strong, nonatomic) IBOutlet UILabel *serumLabel;
@property (strong, nonatomic) IBOutlet UILabel *gfrResult;
@property (strong, nonatomic) IBOutlet UILabel *GFRCapped125;

@property (strong, nonatomic) IBOutlet UILabel *labelResult;

@property (strong, nonatomic) IBOutlet UISegmentedControl *genderSegmentControl;

@property (strong, nonatomic) IBOutlet UIView *weightkeyboardView;
@property (strong, nonatomic) IBOutlet UIView *heightkeyboardView;
@property (strong, nonatomic) IBOutlet UIView *serumkeyboardView;

@property (strong, nonatomic) IBOutlet UILabel *lblErrAge;
@property (strong, nonatomic) IBOutlet UILabel *lblErrWeight;
@property (strong, nonatomic) IBOutlet UILabel *lblErrHeight;
@property (strong, nonatomic) IBOutlet UILabel *lblErrSerumC;
@property (strong, nonatomic) IBOutlet UILabel *lblErrTargetAUC;


@property (strong, nonatomic) IBOutlet UILabel *lblGender;
@property (strong, nonatomic) IBOutlet UILabel *lblGFR;
@property (strong, nonatomic) IBOutlet UILabel *lblCarboplatinDose;

@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *viewCollection;
@property (strong, nonatomic) IBOutletCollection(UITextField) NSArray *txtCollection;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *lblCollection;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *lblErrCollection;

@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *keyBoardViewCollection;

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *keyBoardBtnCollection;

@end
