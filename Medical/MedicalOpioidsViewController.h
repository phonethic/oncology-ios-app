//
//  MedicalOpioidsViewController.h
//  Medical
//
//  Created by Kirti Nikam on 08/10/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MedicalOpioidsViewController : UIViewController<UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate,UIActionSheetDelegate>
{
    int BTNTAG;
}
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong,nonatomic) NSArray *opArray;
@property (strong, nonatomic) IBOutlet UITableView *opTableView;

@property (strong, nonatomic) IBOutlet UITextField *convertFromTextField;
@property (strong, nonatomic) IBOutlet UITextField *byPercentageTextField;

@property (strong, nonatomic) IBOutlet UILabel *labelResult;
@property (strong, nonatomic) IBOutlet UILabel *convertFromLabel;
@property (strong, nonatomic) IBOutlet UILabel *convertToLabel;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentControl;

@property (strong, nonatomic) IBOutlet UIView *viewConvertFromTextfield;
@property (strong, nonatomic) IBOutlet UIView *viewConvertFromLabel;
@property (strong, nonatomic) IBOutlet UIView *viewConvertToLabel;
@property (strong, nonatomic) IBOutlet UIView *viewPrecentageTextfield;

@property (strong, nonatomic) IBOutlet UILabel *lblEquivalent;
@property (strong, nonatomic) IBOutlet UILabel *lblBy;
@property (strong, nonatomic) IBOutlet UILabel *lblErrConverFromTextfield;
@property (strong, nonatomic) IBOutlet UILabel *lblErrPercentageTextfield;


@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *viewCollection;
@property (strong, nonatomic) IBOutletCollection(UITextField) NSArray *txtCollection;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *lblCollection;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *lblErrCollection;



@end
