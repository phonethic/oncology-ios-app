//
//  MedicalPrintImage.m
//  Medical
//
//  Created by Kirti Nikam on 19/10/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import "MedicalPrintImage.h"

@implementation MedicalPrintImage
@synthesize printImage;

- (NSInteger)numberOfPages
{
    DebugLog(@"numberPages");   
    return 1;
}

- (void)drawPageAtIndex:(NSInteger)pageIndex inRect:(CGRect)printableRect
{
    if(self.printImage){
        CGRect destRect;
        // When drawPageAtIndex:inRect: paperRect reflects the size of
        // the paper we are printing on and printableRect reflects the rectangle
        // describing the imageable area of the page, that is the portion of the page
        // that the printer can mark without clipping.
        CGSize paperSize =  self.paperRect.size;
        CGSize imageableAreaSize = self.printableRect.size;
        DebugLog(@"papersize %f %f \n image %f %f",paperSize.width,paperSize.height,imageableAreaSize.width,imageableAreaSize.height);
        // If the paperRect and printableRect have the same size, the sheet is borderless and we will use
        // the fill algorithm. Otherwise we will uniformly scale the image to fit the imageable area as close
        // as is possible without clipping.
        BOOL fillSheet = paperSize.width == imageableAreaSize.width && paperSize.height == imageableAreaSize.height;
        CGSize imageSize = [self.printImage size];
        DebugLog(@"old = papersize 288.000000 432.000000 image 264.018890 408.018890");
        DebugLog(@"%f %f",imageSize.width,imageSize.height);
        if(fillSheet){
            destRect = CGRectMake(0, 0, paperSize.width, paperSize.height);
        }
        else
            destRect = self.printableRect;
        
        // Calculate the ratios of the destination rectangle width and height to the image width and height.
        CGFloat width_scale = (CGFloat)destRect.size.width/imageSize.width, height_scale = (CGFloat)destRect.size.height/imageSize.height;
        CGFloat scale;
        if(fillSheet)
            scale = width_scale > height_scale ? width_scale : height_scale;	  // This produces a fill to the entire sheet and clips content.
        else
            scale = width_scale < height_scale ? width_scale : height_scale;	  // This shows all the content at the expense of additional white space.
        
        // Locate destRect so that the scaled image is centered on the sheet.
        destRect = CGRectMake((paperSize.width - imageSize.width*scale)/2+30,
                              (paperSize.height - imageSize.height*scale)/2+100,
                              imageSize.width*scale, imageSize.height*scale);
        // Use UIKit to draw the image to destRect.
        [self.printImage drawInRect:destRect];
        DebugLog(@"dstRect %f %f %f %f",destRect.origin.x,destRect.origin.y,destRect.size.width,destRect.size.height);
    }else {
        DebugLog(@"%s No image to draw!", __func__);
    }
}

@end
