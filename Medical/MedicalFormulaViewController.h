//
//  MedicalFormulaViewController.h
//  Medical
//
//  Created by Kirti Nikam on 08/10/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import <UIKit/UIKit.h>
#define ANC @"Absolute Neutrophil Count"
#define BSA @"Body Surface Area"
#define CARBOPLATIN @"Carboplatin Calvert Calculator"
#define CORTIEQ @"Corticosteroids Equivalence"
#define OPIOIDSEQ @"Opioid Equivalence"
#define RECIST @"RECIST Calculator & Tumor Assessment"
#define HAEMOGLOBIN @"Hemoglobin Iron Deficit"
#define KARNOFSKY @"Karnofsky Performance Status"
#define ECOG @"ECOG Performance Status"

@interface MedicalFormulaViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *formulatableView;
@property(strong,nonatomic) NSArray *formulaArray;
@end
