//
//  RssFeedsParseViewController.h
//  Medical
//
//  Created by Kirti Nikam on 26/10/13.
//  Copyright (c) 2013 Kirti Nikam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface RssFeedsParseViewController : UIViewController<NSURLConnectionDelegate,NSXMLParserDelegate,MBProgressHUDDelegate>{
    NSURLConnection *conn;
    NSMutableData *rssData;
    
    NSXMLParser *parser;
    BOOL elementFound;
    NSString *elementKey;
    
    MBProgressHUD *HUD;
}
@property (copy, nonatomic) NSString *rssUrlLink;
@property (copy, nonatomic) NSMutableString *item_title;
@property (copy, nonatomic) NSMutableString *item_link;

@property (strong, nonatomic) NSMutableDictionary *itemDict;
@property (strong, nonatomic) NSMutableArray *feedArray;

@property (strong, nonatomic) IBOutlet UITableView *feedTableView;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *spinnerIndicator;
@property (strong, nonatomic) IBOutlet UIView *loadingView;
@end
