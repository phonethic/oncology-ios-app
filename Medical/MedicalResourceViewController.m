//
//  MedicalResourceViewController.m
//  Medical
//
//  Created by Sagar Mody on 05/11/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import "MedicalResourceViewController.h"
#import "MedicalUsefulSitesViewController.h"
#import "MedicalCancerSocietiesViewController.h"
#import "MedicalMeetingViewController.h"
#import "MedicalCancerCentersViewController.h"
#import "MedicalRecistViewController.h"
#import "MedicalRSSFeedsViewController.h"

#import "MedicalKarnofskyStatusViewController.h"
#import "MedicalECOGStatusViewController.h"

#import "MedicalJournalsViewController.h"
#import "MedicalBillingViewController.h"

#import "MedicalWebViewController.h"
#import "MedicalSurvivalRatesViewController.h"

#import "constants.h"

#define CANCER_CENTERS @"Cancer Centers"
#define CANCER_SOCIETIES @"Cancer Societies"
#define USEFUL_SITES  @"Web Links"
#define MEETINGS  @"Meetings"

#define RECIST @"RECIST 1.1: Salient features, Challenges and Future directions"
#define RSS @"Must Reads"

#define JOURNALS @"Journals"
#define IDC9Codes @"IDC-9 codes"

#define KARNOFSKY @"Karnofsky Performance Status"
#define ECOG @"ECOG Performance Status"

#define LIVE_VACCINES @"Live vaccines"
#define FIVE_YEAR_SURVIVAL_RATES @"5 year survival rates"
#define CHILD_PUGH_SCORE @"Child-Pugh Score"

@interface MedicalResourceViewController ()

@end

@implementation MedicalResourceViewController
@synthesize resourceTableView;
@synthesize resourceslist;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    resourceslist = [[NSMutableArray alloc] init];
    [resourceslist addObject:CANCER_CENTERS];
    [resourceslist addObject:RECIST];
    //[resourceslist addObject:CANCER_SOCIETIES];
    //[resourceslist addObject:MEETINGS];
    [resourceslist addObject:IDC9Codes];
    //[resourceslist addObject:JOURNALS];
    [resourceslist addObject:LIVE_VACCINES];
    [resourceslist addObject:FIVE_YEAR_SURVIVAL_RATES];
    [resourceslist addObject:CHILD_PUGH_SCORE];
    [resourceslist addObject:KARNOFSKY];
    [resourceslist addObject:ECOG];
//    [resourceslist addObject:USEFUL_SITES];
}

- (void)viewDidUnload
{
    [self setResourceTableView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark Table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return STATIC_ROW_HEIGHT;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [resourceslist count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        
        cell.accessoryType  =   UITableViewCellAccessoryDisclosureIndicator;
        cell.textLabel.font =   DEFAULT_FONT(FONT_STATIC_ROW_HEIGHT);
        cell.textLabel.numberOfLines = 3;

        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = DEFAULT_COLOR_WITH_ALPHA(0.5);
        cell.selectedBackgroundView = bgColorView;
    }
    cell.textLabel.text= [resourceslist objectAtIndex:indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //DebugLog(@"%@", [resourceslist objectAtIndex:indexPath.row]);
    
    NSString *textToMatch = [resourceslist objectAtIndex:indexPath.row];
    if ([textToMatch isEqualToString:CANCER_CENTERS])
    {
        MedicalCancerCentersViewController *cancercenterviewController = [[MedicalCancerCentersViewController alloc] initWithNibName:@"MedicalCancerCentersViewController" bundle:nil];
        cancercenterviewController.title = CANCER_CENTERS;
        [self.navigationController pushViewController:cancercenterviewController animated:YES];
    }
    else if ([textToMatch isEqualToString:CANCER_SOCIETIES])
    {
        MedicalCancerSocietiesViewController *societiesviewController = [[MedicalCancerSocietiesViewController alloc] initWithNibName:@"MedicalCancerSocietiesViewController" bundle:nil];
        societiesviewController.title = CANCER_SOCIETIES;
        [self.navigationController pushViewController:societiesviewController animated:YES];
    }
    else if ([textToMatch isEqualToString:MEETINGS])
    {
        MedicalMeetingViewController *medicalmeetingviewController = [[MedicalMeetingViewController alloc] initWithNibName:@"MedicalMeetingViewController" bundle:nil];
        medicalmeetingviewController.title = MEETINGS;
        [self.navigationController pushViewController:medicalmeetingviewController animated:YES];
    }
    else if ([textToMatch isEqualToString:USEFUL_SITES])
    {
        MedicalUsefulSitesViewController *sitesviewController = [[MedicalUsefulSitesViewController alloc] initWithNibName:@"MedicalUsefulSitesViewController" bundle:nil];
        sitesviewController.title = USEFUL_SITES;
        [self.navigationController pushViewController:sitesviewController animated:YES];
    }
    else if ([textToMatch isEqualToString:RECIST])
    {
        MedicalRecistViewController *recistviewController = [[MedicalRecistViewController alloc] initWithNibName:@"MedicalRecistViewController" bundle:nil];
        recistviewController.title = RECIST;
        [self.navigationController pushViewController:recistviewController animated:YES];
    }
    else if ([textToMatch isEqualToString:RSS])
    {
        MedicalRSSFeedsViewController *rssfeedviewController = [[MedicalRSSFeedsViewController alloc] initWithNibName:@"MedicalRSSFeedsViewController" bundle:nil];
        rssfeedviewController.title = RSS;
        [self.navigationController pushViewController:rssfeedviewController animated:YES];
    }
    else if ([textToMatch isEqualToString:JOURNALS])
    {
        MedicalJournalsViewController *journalsViewController = [[MedicalJournalsViewController alloc] initWithNibName:@"MedicalJournalsViewController" bundle:nil] ;
        journalsViewController.title = JOURNALS;
        [self.navigationController pushViewController:journalsViewController animated:YES];
    }
    else if ([textToMatch isEqualToString:IDC9Codes])
    {
        
        MedicalBillingViewController *billingviewController = [[MedicalBillingViewController alloc] initWithNibName:@"MedicalBillingViewController" bundle:nil] ;
        billingviewController.title = IDC9Codes;
        [self.navigationController pushViewController:billingviewController animated:YES];
    }
    else if ([textToMatch isEqualToString:LIVE_VACCINES])
    {
        [INEventLogger logEvent:@"Live_Vaccines_Details"];
        MedicalWebViewController *webviewController = [[MedicalWebViewController alloc] initWithNibName:@"MedicalWebViewController" bundle:nil];
        webviewController.title         = LIVE_VACCINES;
        webviewController.medicalLink   = LIVE_VACCINES_INFO;
        [self.navigationController pushViewController:webviewController animated:YES];
    }
    else if ([textToMatch isEqualToString:FIVE_YEAR_SURVIVAL_RATES])
    {
        MedicalSurvivalRatesViewController *ratesviewController = [[MedicalSurvivalRatesViewController alloc] initWithNibName:@"MedicalSurvivalRatesViewController" bundle:nil];
        ratesviewController.title         = FIVE_YEAR_SURVIVAL_RATES;
        [self.navigationController pushViewController:ratesviewController animated:YES];
    }
    else if([textToMatch isEqualToString:CHILD_PUGH_SCORE])
    {
        [INEventLogger logEvent:@"Child_Pugh_Score_Details"];

        NSString *text = @"<h1>Bilirubin (mg/dl)</h1><2 (1 point). 2-3 (2 points). >3 (3 points).<h1>Albumin (g/dl)</h1>>3.5 (1 point). 2.8-3.5 (2 points). <2.8 (3 points).<h1>INR</h1>< 1.7 (1 point). 1.71-2.3 (2 points). >2.3 (3 points).<h1>Ascites</h1>None (1 point). Mild (2 points). Moderate (3 points).<h1>Hepatic Encephalopathy</h1>None (1 point). Controlled with medication (2 points). Poorly Controlled (3 points).<h1>Grading</h1>Class A: 5-6 points. Class B: 7-9 points. Class C: 10-15 points<h1>Reference</h1>(1) Child CG, Turcotte JG. Surgery and portal hypertension. The liver and portal hypertension. Philadelphia: Saunders 1964:50-64.<br>(2) Pugh RN, Murray-Lyon IM, Dawson JL, Pietroni MC, Williams R. Transection of the oesophagus for bleeding oesophageal varices. Br J Surg. 1973 Aug; 60(8):646-9.";
        MedicalWebViewController *webviewController = [[MedicalWebViewController alloc] initWithNibName:@"MedicalWebViewController" bundle:nil];
        webviewController.titleString   = CHILD_PUGH_SCORE;
        webviewController.details       = text;
        [self.navigationController pushViewController:webviewController animated:YES];
    }
    else if ([textToMatch isEqualToString: KARNOFSKY])
    {
        [INEventLogger logEvent:@"Karnofsky_Performance_Status_Details"];
        MedicalKarnofskyStatusViewController *karnofskyViewController = [[MedicalKarnofskyStatusViewController alloc] initWithNibName:@"MedicalKarnofskyStatusViewController" bundle:nil] ;
        karnofskyViewController.title = KARNOFSKY;
        [self.navigationController pushViewController:karnofskyViewController animated:YES];
    }
    else if ([textToMatch isEqualToString: ECOG])
    {
        [INEventLogger logEvent:@"ECOG_Performance_Status_Details"];
        MedicalECOGStatusViewController *ecogViewController = [[MedicalECOGStatusViewController alloc] initWithNibName:@"MedicalECOGStatusViewController" bundle:nil] ;
        ecogViewController.title = ECOG;
        [self.navigationController pushViewController:ecogViewController animated:YES];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
@end
