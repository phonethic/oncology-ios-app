//
//  MedicalWebViewController.h
//  Medical
//
//  Created by Sagar Mody on 05/11/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface MedicalWebViewController : UIViewController<MBProgressHUDDelegate> {
	MBProgressHUD *HUD;
}

@property (nonatomic, copy) NSString *medicalLink;
@property (copy,nonatomic) NSString *titleString;
@property (copy,nonatomic) NSString *details;
@property (copy,nonatomic) NSMutableDictionary *dictParams;


@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UIWebView *medicalWebView;
@property (strong, nonatomic) IBOutlet UIToolbar *toolBar;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *goBackBarBtn;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *goForwardBarBtn;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *refreshBarBtn;

- (IBAction)goBackBarBtnPressed:(id)sender;
- (IBAction)goForwardBarBtnPressed:(id)sender;
- (IBAction)refreshBarBtnPressed:(id)sender;

@end
