//
//  MedicalChemoViewController.h
//  Medical
//
//  Created by Rishi on 25/10/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>

@interface MedicalChemoViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITableView *chemotableView;
@property (nonatomic, strong) NSMutableArray *cancerTypes;
@property (nonatomic,retain) NSMutableDictionary *sections;
@end
