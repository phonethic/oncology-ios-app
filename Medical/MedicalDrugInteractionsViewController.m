//
//  MedicalDrugInteractionsViewController.m
//  Medical
//
//  Created by Kirti Nikam on 05/03/14.
//  Copyright (c) 2014 Kirti Nikam. All rights reserved.
//

#import "MedicalDrugInteractionsViewController.h"
#import "MedicalAppDelegate.h"
#import "MedicalWebViewController.h"

@interface MedicalDrugInteractionsViewController ()

@end

@implementation MedicalDrugInteractionsViewController
@synthesize drugsTableView;
@synthesize drugsDict;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self readDrugsFromTable];
    [drugsTableView reloadData];
    [INEventLogger logEvent:@"Drug_Interaction_List"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewDidUnload
{
    
    [self setDrugsTableView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark - Rotation
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

-(void)readDrugsFromTable
{
    if (drugsDict == nil) {
        drugsDict = [[NSMutableDictionary alloc] init];
    }else{
        [drugsDict removeAllObjects];
    }
    sqlite3 *database;
    if (sqlite3_open([[MEDICAL_APP_DELEGATE databasePath] UTF8String], &database) == SQLITE_OK) {
        NSString *query = @"SELECT NAME,DESCRIPTION FROM DRUG_INTERACTIONS";
        const char *queryChar = [query UTF8String];
        sqlite3_stmt *statement = nil;
        if(sqlite3_prepare_v2(database, queryChar, -1, &statement, NULL) == SQLITE_OK){
            while (sqlite3_step(statement) == SQLITE_ROW) {
                NSString *name = [NSString stringWithUTF8String:(const char*)sqlite3_column_text(statement, 0)];
                NSString *description = [NSString stringWithUTF8String:(const char*)sqlite3_column_text(statement, 1)];
                DebugLog(@"%@-%@",name,description);
                [drugsDict setObject:description forKey:name];
            }
        }
        sqlite3_finalize(statement);
    }
    sqlite3_close(database);
}

#pragma - UITableView Datasrource Methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [drugsDict count] ;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return STATIC_ROW_HEIGHT;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"DrugCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

        cell.textLabel.font             = DEFAULT_FONT(FONT_STATIC_ROW_HEIGHT);
        cell.textLabel.textColor        = [UIColor blackColor];
        cell.textLabel.backgroundColor  = [UIColor clearColor];
        cell.textLabel.numberOfLines    = 4;
        
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = DEFAULT_COLOR_WITH_ALPHA(0.5);
        cell.selectedBackgroundView = bgColorView;
    }
    if (indexPath.row < [drugsDict count]) {
        cell.textLabel.text = [[[drugsDict allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:[indexPath row]];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row < [[drugsDict allKeys] count]) {
        NSString *key = [[[drugsDict allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:[indexPath row]];
        
        NSDictionary *params = @{@"drug_name":key};
        [INEventLogger logEvent:@"Drug_Interaction_Details" withParams:params];
        
        MedicalWebViewController *webviewController = [[MedicalWebViewController alloc] initWithNibName:@"MedicalWebViewController" bundle:nil];
        webviewController.titleString   = key;
        webviewController.details       = [drugsDict objectForKey:key];
        [self.navigationController pushViewController:webviewController animated:YES];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
@end
