//
//  MedicalUsefulSitesViewController.h
//  Medical
//
//  Created by Sagar Mody on 05/11/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MedicalUsefulSitesViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITableView *sitesTableView;
@property (strong, nonatomic) NSMutableDictionary *siteObjdict;
@end
