//
//  MedicalCancerSocietiesViewController.h
//  Medical
//
//  Created by Sagar Mody on 05/11/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MedicalCancerSocietiesViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITableView *societiesTableView;
@property (strong, nonatomic) NSMutableDictionary *societiesObjdict;
@end
