//
//  MedicalFormulaViewController.m
//  Medical
//
//  Created by Kirti Nikam on 08/10/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import "MedicalFormulaViewController.h"
#import "MedicalANCViewController.h"
#import "MedicalOpioidsViewController.h"
#import "MedicalCorticosteroidsEqViewController.h"
#import "MedicalBSAViewController.h"
#import "MedicalCarboplatinViewController.h"
#import "MedicalRecistCalcTumorViewController.h"
#import "MedicalHgbirondeficitViewController.h"
#import "MedicalKarnofskyStatusViewController.h"
#import "MedicalECOGStatusViewController.h"
#import "constants.h"
#import "MedicalWebViewController.h"

@interface MedicalFormulaViewController ()

@end

@implementation MedicalFormulaViewController
@synthesize formulatableView;
@synthesize formulaArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self addInfoBarButton];
    
    formulaArray=[[NSArray alloc] initWithObjects:ANC,BSA,CARBOPLATIN ,CORTIEQ,OPIOIDSEQ,RECIST,HAEMOGLOBIN,KARNOFSKY,ECOG,nil];
    self.navigationItem.title=@"Calculator";
    
    self.formulatableView.backgroundColor = [UIColor whiteColor];
//    self.formulatableView.separatorColor  = DEFAULT_COLOR;
    [INEventLogger logEvent:@"MedCalc_Calculator_List"];
}

- (void)viewDidUnload
{
    [self setFormulatableView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
#pragma mark - Rotation
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma internal methods
-(void)addInfoBarButton{
    UIButton *infoBtn = [UIButton buttonWithType:UIButtonTypeInfoLight];
    infoBtn.frame = CGRectMake(200, 2, 30, 30);
    [infoBtn addTarget:self action:@selector(infoBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:infoBtn];
}

-(void)infoBtnPressed
{
    DebugLog(@"infoBtnPressed");
    [INEventLogger logEvent:@"MedCalc_i_Button"];
    [self openWebPage:DISCLAIMER_TITLE url:CALC_DISCLAIMER_INFO];
}

-(void)closeWebView
{
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        DebugLog(@"closeWebView");
    }];
}

-(void)openWebPage:(NSString *)title url:(NSString *)urlLink{
    if ([urlLink length] > 0) {
        MedicalWebViewController *webViewController = [[MedicalWebViewController alloc] initWithNibName:@"MedicalWebViewController" bundle:nil];
        webViewController.title = title;
        webViewController.medicalLink = urlLink;
        
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:webViewController];
        UIButton *closeBtn =  [UIButton buttonWithType:UIButtonTypeCustom];
        [closeBtn setFrame:CGRectMake(276, 0, 44, 44)];
        [closeBtn setTitle:@"" forState:UIControlStateNormal];
        [closeBtn setImage:[UIImage imageNamed:@"cancel_white.png"] forState:UIControlStateNormal];
        [closeBtn addTarget:self action:@selector(closeWebView) forControlEvents:UIControlEventTouchUpInside];
        [navController.navigationBar addSubview:closeBtn];
        [navController.navigationBar setTranslucent:NO];
        [self.navigationController presentViewController:navController animated:YES completion:nil];
    }
}

#pragma mark Table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return STATIC_ROW_HEIGHT;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [formulaArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        
        cell.accessoryType              =   UITableViewCellAccessoryDisclosureIndicator;
        cell.textLabel.font             =   DEFAULT_FONT(FONT_STATIC_ROW_HEIGHT);
        cell.textLabel.numberOfLines    =   2;
        cell.textLabel.textColor        =   [UIColor blackColor];
        
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = DEFAULT_COLOR_WITH_ALPHA(0.7);
        cell.selectedBackgroundView = bgColorView;
    }
    cell.textLabel.text = [formulaArray objectAtIndex:indexPath.row];
    return cell;
}
    
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{   
    NSString *selected=[formulaArray objectAtIndex:indexPath.row];
    if ([selected isEqualToString: ANC])
    {         
        MedicalANCViewController *ancViewController = [[MedicalANCViewController alloc] initWithNibName:@"MedicalANCViewController" bundle:nil] ;
        ancViewController.title = @"ANC";
        [self.navigationController pushViewController:ancViewController animated:YES];
    }
    else if ([selected isEqualToString: BSA])
    {       
        MedicalBSAViewController *bsaViewController = [[MedicalBSAViewController alloc] initWithNibName:@"MedicalBSAViewController" bundle:nil] ;
        bsaViewController.title = selected;
        [self.navigationController pushViewController:bsaViewController animated:YES];
    }
    else if ([selected isEqualToString: CARBOPLATIN])
    {        
        MedicalCarboplatinViewController *carboplatinViewController = [[MedicalCarboplatinViewController alloc] initWithNibName:@"MedicalCarboplatinViewController" bundle:nil] ;
        carboplatinViewController.title = @"Carboplatin";
        [self.navigationController pushViewController:carboplatinViewController animated:YES];
    }
    else if ([selected isEqualToString: CORTIEQ])
    {        
        MedicalCorticosteroidsEqViewController *cortieqViewController = [[MedicalCorticosteroidsEqViewController alloc] initWithNibName:@"MedicalCorticosteroidsEqViewController" bundle:nil] ;
        cortieqViewController.title =@"Corticosteroids Eq";
        [self.navigationController pushViewController:cortieqViewController animated:YES];
    }
    else if ([selected isEqualToString: OPIOIDSEQ])
    {        
        MedicalOpioidsViewController *opioidseqViewController = [[MedicalOpioidsViewController alloc] initWithNibName:@"MedicalOpioidsViewController" bundle:nil] ;
        opioidseqViewController.title = selected;
        [self.navigationController pushViewController:opioidseqViewController animated:YES];
    }
    else if ([selected isEqualToString: RECIST])
    {
        MedicalRecistCalcTumorViewController *recistViewController = [[MedicalRecistCalcTumorViewController alloc] initWithNibName:@"MedicalRecistCalcTumorViewController" bundle:nil] ;
        recistViewController.title = @"RECIST";
        [self.navigationController pushViewController:recistViewController animated:YES];
    }    
    else if ([selected isEqualToString: HAEMOGLOBIN])
    {
        MedicalHgbirondeficitViewController *haemoglobinViewController = [[MedicalHgbirondeficitViewController alloc] initWithNibName:@"MedicalHgbirondeficitViewController" bundle:nil] ;
        haemoglobinViewController.title = @"Hgb Iron Deficit";
        [self.navigationController pushViewController:haemoglobinViewController animated:YES];
    }
    else if ([selected isEqualToString: KARNOFSKY])
    {
        MedicalKarnofskyStatusViewController *karnofskyViewController = [[MedicalKarnofskyStatusViewController alloc] initWithNibName:@"MedicalKarnofskyStatusViewController" bundle:nil] ;
        karnofskyViewController.title = selected;
        [self.navigationController pushViewController:karnofskyViewController animated:YES];
    }
    else if ([selected isEqualToString: ECOG])
    {
        MedicalECOGStatusViewController *ecogViewController = [[MedicalECOGStatusViewController alloc] initWithNibName:@"MedicalECOGStatusViewController" bundle:nil] ;
        ecogViewController.title = selected;
        [self.navigationController pushViewController:ecogViewController animated:YES];
    }
    [formulatableView deselectRowAtIndexPath:[formulatableView indexPathForSelectedRow] animated:NO];
}
@end
