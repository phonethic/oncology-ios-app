//
//  CommonCallback.h
//  ImageOrganiser
//
//  Created by Kirti Nikam on 13/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CommonCallback : NSObject

#pragma UIView
+(UIImage *)getFlatImage:(UIColor *)color;
+(void)roundeWithBorder:(UIView *)view;
+(void)addShadow:(UIView *)view;
+(void)changeButtonFontAndTextColor:(UIButton *)btn;
+(void)setTextFieldProperties:(UITextField *)textField text:(NSString *)textString;
+(void)setTextFieldPropertiesWithBorder:(UITextField *)textField text:(NSString *)textString;
+(void)setTextViewProperties:(UITextView *)textView text:(NSString *)textString;
+(UIView *)setViewPropertiesWithRoundedCorner:(UIView *)view;

+(UILabel *)setGridIconLabelFontandColor:(UILabel *)lbl;

#pragma UIImage
+(UIImage *)separatorImage;
+(UIImage *)fixOrientation:(UIImage *)oldImage;
+(UIImage *)resizeImage:(UIImage*)image newSize:(CGSize)newSize;

+(BOOL)validateEmail: (NSString *) email;
+(void)changeTextFieldColor:(int)type view:(UIView *)lview;

//#pragma animation
//+(void)viewtransitionInCompletion:(UIView *)view completion:(void(^)(void))lcompletion;
//+(void)viewtransitionOutCompletion:(UIView *)view completion:(void(^)(void))completion;
//+(void)teardown:(UIView *)view;

//Flat UI
+(UIImage*)drawImageOfSize:(CGSize)size andColor:(UIColor*)color;
+(void)styleNavigationBarWithFontName:(UIFont *)navigationTitleFont andColor:(UIColor*)color;
+(void)styleSegmentedControlWithFontName:(UIFont *)font andSelectedColor:(UIColor*)selectedColor andUnselectedColor:(UIColor*)unselectedColor andDidviderColor:(UIColor*)dividerColor;

+(void) showOfflineAlert;
@end
