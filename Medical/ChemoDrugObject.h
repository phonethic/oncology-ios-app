//
//  ChemoDrugObject.h
//  Medical
//
//  Created by Kirti Nikam on 14/10/13.
//  Copyright (c) 2013 Kirti Nikam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChemoDrugObject : NSObject
@property (copy,nonatomic) NSString *drugID;
@property (copy,nonatomic) NSString *drugName;
@end
