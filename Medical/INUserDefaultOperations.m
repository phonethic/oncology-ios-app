//
//  INUserDefaultOperations.m
//  inorbit
//
//  Created by Rishi on 03/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "INUserDefaultOperations.h"

@implementation INUserDefaultOperations

+(void)setRefreshDate:(NSDate *)value{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:value forKey:@"Refresh_Date"];
    [defaults synchronize];
}

+(NSDate *)getRefreshDate{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:@"Refresh_Date"] == nil)
        return [NSDate date];
    return [defaults objectForKey:@"Refresh_Date"];
}

+(void)setLastAppVersion:(float)value{
    NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setFloat:value forKey:@"Last_App_Version"];
    [userDefaults synchronize];
}

+(float)getLastAppVersion{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:@"Last_App_Version"] == nil)
        return 0.000000;
    return [defaults floatForKey:@"Last_App_Version"];
}

//----------------------------------------------------------------------------------

+(NSInteger)getDateDifferenceInSeconds:(NSDate *)startDate endDate:(NSDate *)endDate
{
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [gregorianCalendar components:kCFCalendarUnitSecond
                                                        fromDate:startDate
                                                          toDate:endDate
                                                         options:0];
    DebugLog(@"seconds = %ld",(long)[components second]);
    return [components second];
}

+(NSInteger)getDateDifferenceInMinutes:(NSDate *)startDate endDate:(NSDate *)endDate
{
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [gregorianCalendar components:kCFCalendarUnitMinute
                                                        fromDate:endDate
                                                          toDate:startDate
                                                         options:0];
    DebugLog(@"minutes = %d",[components minute]);
    return [components minute];
}

+(NSInteger)getDateDifferenceInHours:(NSDate *)expireDate
{
    NSDate *currentDate = [NSDate date];
    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [gregorianCalendar components:kCFCalendarUnitHour
                                                        fromDate:expireDate
                                                          toDate:currentDate
                                                         options:0];
    //DebugLog(@"hours = %d",[components hour]);
    return [components hour];
}

+(NSInteger)getDateDifferenceInDays:(NSDate *)expireDate
{
    NSDate *currentDate = [NSDate date];
    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [gregorianCalendar components:kCFCalendarUnitDay
                                                        fromDate:expireDate
                                                          toDate:currentDate
                                                         options:0];
    //DebugLog(@"days = %d",[components day]);
    return [components day];
}

+(NSInteger)getAgeInYears:(NSDate *)birthDate
{
    NSDate *currentDate = [NSDate date];
    
    NSDateComponents* ageComponents = [[NSCalendar currentCalendar]
                                       components:NSYearCalendarUnit
                                       fromDate:birthDate
                                       toDate:currentDate
                                       options:0];
    DebugLog(@"years = %d",[ageComponents year]);
    return [ageComponents year];
}
/*****************************************************************************/

@end
