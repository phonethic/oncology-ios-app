//
//  MedicalChemoViewController.m
//  Medical
//
//  Created by Rishi on 25/10/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import "MedicalChemoViewController.h"
#import "MedicalRegimenViewController.h"
#import "MedicalAppDelegate.h"
#import "MedicalWebViewController.h"

@interface MedicalChemoViewController ()

@end

@implementation MedicalChemoViewController
@synthesize chemotableView;
@synthesize cancerTypes;
@synthesize sections;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UIBarButtonItem *back = [[UIBarButtonItem alloc] initWithTitle:@"Back"
                                                             style:UIBarButtonItemStylePlain
                                                            target:nil
                                                            action:nil];
    [[self navigationItem] setBackBarButtonItem:back];
    
    [self addInfoBarButton];
    
    cancerTypes = [[NSMutableArray alloc] init];
    [self readcancerFromDatabase];
    [INEventLogger logEvent:@"Chemo_Orders_Cancer_List"];
}

- (void)viewDidUnload
{
    [self setChemotableView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark - Rotation
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma internal methods
-(void)addInfoBarButton{
    UIButton *infoBtn = [UIButton buttonWithType:UIButtonTypeInfoLight];
    infoBtn.frame = CGRectMake(200, 2, 30, 30);
    [infoBtn addTarget:self action:@selector(infoBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:infoBtn];
}

-(void)infoBtnPressed
{
    DebugLog(@"infoBtnPressed");
    [INEventLogger logEvent:@"Chemo_Orders_i_Button"];
    [self openWebPage:DISCLAIMER_TITLE url:CHEMOORDERS_DISCLAIMER_INFO];
}

-(void)closeWebView
{
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        DebugLog(@"closeWebView");
    }];
}

-(void)openWebPage:(NSString *)title url:(NSString *)urlLink{
    if ([urlLink length] > 0) {
        MedicalWebViewController *webViewController = [[MedicalWebViewController alloc] initWithNibName:@"MedicalWebViewController" bundle:nil];
        webViewController.title = title;
        webViewController.medicalLink = urlLink;
        
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:webViewController];
        UIButton *closeBtn =  [UIButton buttonWithType:UIButtonTypeCustom];
        [closeBtn setFrame:CGRectMake(276, 0, 44, 44)];
        [closeBtn setTitle:@"" forState:UIControlStateNormal];
        [closeBtn setImage:[UIImage imageNamed:@"cancel_white.png"] forState:UIControlStateNormal];
        [closeBtn addTarget:self action:@selector(closeWebView) forControlEvents:UIControlEventTouchUpInside];
        [navController.navigationBar addSubview:closeBtn];
        [navController.navigationBar setTranslucent:NO];
        [self.navigationController presentViewController:navController animated:YES completion:nil];
    }
}

-(void) readcancerFromDatabase {
	// Setup the database object
	sqlite3 *database;
    self.sections = [[NSMutableDictionary alloc] init];
    BOOL found;
	// Open the database from the users filessytem
	if(sqlite3_open([[MEDICAL_APP_DELEGATE databasePath] UTF8String], &database) == SQLITE_OK) {
		// Setup the SQL Statement and compile it for faster access
		const char *sqlStatement = "select cancer from regions group by cancer";
		sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
			// Loop through the results and add them to the feeds array
			while(sqlite3_step(compiledStatement) == SQLITE_ROW)
            {
				// Read the data from the result row
                NSString *cancerName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 0)];
                NSString *prefixChar = [[cancerName substringToIndex:1] uppercaseString];
                found = NO;
                
                for (NSString *str in [self.sections allKeys])
                {
                    if ([str isEqualToString:prefixChar])
                    {
                        found = YES;
                        break;
                    }
                }
                if (!found)
                {
                    [self.sections setValue:[[NSMutableArray alloc] init] forKey:prefixChar];
                }
                [cancerTypes addObject:cancerName];
                [[self.sections objectForKey:prefixChar] addObject:cancerName];
                cancerName =  nil;
                
            }
        }
		// Release the compiled statement from memory
		sqlite3_finalize(compiledStatement);
	}
	sqlite3_close(database);
}


#pragma mark Table view methods

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    return [[self.sections allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return [[self.sections allKeys] count];

}

-(CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section
{
    return 35;
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView* headerView          = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 35)];
    headerView.backgroundColor  = DEFAULT_COLOR;
    
    UILabel *headerLabel        = [[UILabel alloc] initWithFrame:CGRectZero];
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.frame           = CGRectMake(10,0,290,35);
    headerLabel.textAlignment   = NSTextAlignmentLeft;
    headerLabel.font            = DEFAULT_BOLD_FONT(17);
    headerLabel.text            = [[[self.sections allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:section];
    headerLabel.textColor       = [UIColor whiteColor];
    [headerView addSubview:headerLabel];
    return headerView;
}

//-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//{
//
//    return [[[self.sections allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:section];
//}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return STATIC_ROW_HEIGHT;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
   return [[self.sections valueForKey:[[[self.sections allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:section]] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        
        cell.textLabel.font =   DEFAULT_FONT(FONT_STATIC_ROW_HEIGHT);
        cell.textLabel.numberOfLines = 3;

        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = DEFAULT_COLOR_WITH_ALPHA(0.5);
        cell.selectedBackgroundView = bgColorView;
    }
    
    NSString *key = [[[self.sections allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section];
    NSArray *sortedArray = [[self.sections valueForKey:key] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    cell.textLabel.text = [sortedArray objectAtIndex:indexPath.row];
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *key = [[[self.sections allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section];
    NSArray *sortedArray = [[self.sections valueForKey:key] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    NSString *cancerName = [sortedArray objectAtIndex:indexPath.row];
    
    MedicalRegimenViewController *regimendetailController = [[MedicalRegimenViewController alloc] initWithNibName:@"MedicalRegimenViewController" bundle:nil] ;
    regimendetailController.cancerName  = cancerName;
    regimendetailController.title       = cancerName;
    [self.navigationController pushViewController:regimendetailController animated:YES];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
@end
