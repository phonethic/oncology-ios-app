//
//  MedicalSignUpViewController.m
//  Medical
//
//  Created by Kirti Nikam on 16/04/14.
//  Copyright (c) 2014 Kirti Nikam. All rights reserved.
//

#import "MedicalSignUpViewController.h"
#import "constants.h"
#import "ActionSheetPicker.h"
#import "CommonCallback.h"
#import "MedicalAppDelegate.h"
#import "MedicalUserDefaults.h"

#define SUBMIT_LINK @"http://www.cancerebook.org/form-submit.php"

@interface MedicalSignUpViewController ()

@end

@implementation MedicalSignUpViewController
@synthesize signUpScrollView;
@synthesize txtEmailId,txtFirstname,txtLastname,txtAge,txtCountry,txtOccupation,txtZipCode,txtCollection;
@synthesize lblEmailId,lblFirstname,lblLastname,lblOccupation,lblAge,lblCountry,lblZipCode,lblCollection;
@synthesize btnSendEmailAlerts,btnSubmit,btnLater,btnCollection;
@synthesize viewCollection;
@synthesize arryAge,arryCountry,arryOccupation;
@synthesize imgViewAge,imgViewCountry,imgViewOccupation;
@synthesize vAge,vCountry,vEmailId,vOccupation,vLastname,vFirstname,vZipCode;
@synthesize reponseData;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setUI];
    [self addHUD];
    
    signUpScrollView.contentSize = CGSizeMake(signUpScrollView.frame.size.width, CGRectGetMaxY(btnSubmit.frame)+30);
    
    arryOccupation  = [[NSMutableArray alloc]initWithObjects:@"Oncology Physician (MD,DO)",@"Oncology Pharmacist",@"Oncology Trainees (Fellow,Resident)",@"Oncology Nursing",@"NP/PA",@"Radiation Oncology",@"Other", nil];
    arryAge         = [[NSMutableArray alloc]initWithObjects:@"< 30",@"31-40",@"41-50",@"51-60",@"> 60", nil];
    
    arryCountry     = [[NSMutableArray alloc]init];
    
    NSError* error;
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"country_code" ofType:@"txt"];
    if (filePath) {
        NSString *dataString = [NSString stringWithContentsOfFile:filePath encoding:NSASCIIStringEncoding error:&error];
        DebugLog(@"dataString %@",dataString);
        NSDictionary* jsonDict = [NSJSONSerialization  JSONObjectWithData:[dataString dataUsingEncoding:NSUTF8StringEncoding]  options:kNilOptions error:&error];
        DebugLog(@"jsonDict %@",jsonDict);
        if (jsonDict != nil) {
            for(NSDictionary *obj in jsonDict) {
                NSString *countryName =[obj objectForKey:@"name"];
                [arryCountry addObject:countryName];
            }
        }
    }
    [arryCountry addObject:@"Other"];

    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected:)];
    tapGesture.numberOfTapsRequired = 1;
    tapGesture.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapGesture];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewDidUnload
{
    [self setSignUpScrollView:nil];
    [self setTxtEmailId:nil];
    [self setTxtFirstname:nil];
    [self setTxtLastname:nil];
    [self setTxtAge:nil];
    [self setTxtOccupation:nil];
    [self setTxtCountry:nil];
    [self setTxtZipCode:nil];
    [self setLblEmailId:nil];
    [self setLblFirstname:nil];
    [self setLblLastname:nil];
    [self setLblAge:nil];
    [self setLblOccupation:nil];
    [self setLblCountry:nil];
    [self setLblZipCode:nil];
    [self setBtnSendEmailAlerts:nil];
    [self setBtnSubmit:nil];
    [self setBtnLater:nil];
    [self setTxtCollection:nil];
    [self setLblCollection:nil];
    [self setBtnCollection:nil];
    [self setViewCollection:nil];
    [self setImgViewAge:nil];
    [self setImgViewCountry:nil];
    [self setImgViewOccupation:nil];
    [self setVEmailId:nil];
    [self setVFirstname:nil];
    [self setVLastname:nil];
    [self setVAge:nil];
    [self setVCountry:nil];
    [self setVZipCode:nil];
    [self setVOccupation:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

#pragma mark - Rotation
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)addHUD{
	HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
	[self.navigationController.view addSubview:HUD];
	HUD.delegate    = self;
	HUD.labelText   = @"Processing";
    HUD.labelFont   = DEFAULT_FONT(16.0);
    HUD.color       = DEFAULT_COLOR_WITH_ALPHA(0.9);
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureTapDetacted:)];
    [HUD addGestureRecognizer:gesture];
}

#pragma UIGestureRecognizer selector methods
-(void)gestureTapDetacted:(UIGestureRecognizer *)sender{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:@"Do you want to cancel processing?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
    [alert show];
}

#pragma mark -
#pragma mark MBProgressHUDDelegate methods
- (void)hudWasHidden:(MBProgressHUD *)hud {
	// Remove HUD from screen when the HUD was hidded
	[HUD removeFromSuperview];
	HUD = nil;
}

#pragma internal methods
-(void) setUI{
    for (UIView *lview in viewCollection) {
        lview.backgroundColor     = [UIColor whiteColor];
        lview.layer.borderColor   = [UIColor lightGrayColor].CGColor;
        lview.layer.borderWidth   = 1.0;
    }
    
    for (UITextField *txt in txtCollection) {
        txt.delegate            = self;
        txt.backgroundColor     = [UIColor clearColor];
        txt.layer.borderColor   = [UIColor clearColor].CGColor;
        txt.textAlignment       = NSTextAlignmentCenter;
        txt.textColor           = [UIColor blackColor];
        txt.font                = DEFAULT_FONT(20);
        txt.minimumFontSize     = 15;
    }
    for (UILabel *lbl in lblCollection) {
        lbl.backgroundColor     = [UIColor clearColor];
        lbl.textColor           = [UIColor redColor];
        lbl.font                = DEFAULT_FONT(12);
        lbl.hidden              = YES;
    }
    for (UIButton *btn in btnCollection) {
        if ([btn isEqual:btnSubmit])
            btn.backgroundColor     = DEFAULT_COLOR;
        else
            btn.backgroundColor     = [UIColor grayColor];
        
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor colorWithWhite:1.0 alpha:0.5] forState:UIControlStateHighlighted];
        btn.titleLabel.font     = DEFAULT_BOLD_FONT(20);
        btn.layer.cornerRadius  = 5.0;
    }
    
    btnSendEmailAlerts.backgroundColor     = [UIColor clearColor];
    [btnSendEmailAlerts setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [btnSendEmailAlerts setTitleColor:[UIColor colorWithWhite:0 alpha:0.3] forState:UIControlStateHighlighted];
    btnSendEmailAlerts.titleLabel.font     = DEFAULT_FONT(14);
}

-(void)tapDetected:(UIGestureRecognizer *)sender{
    [self.view endEditing:YES];
    CGPoint tapLocation = [sender locationInView:signUpScrollView];
    UIView *view = [signUpScrollView hitTest:tapLocation withEvent:nil];
    if (![view isKindOfClass:[UIButton class]]) {
        [self scrollTobottom];
    }
}

-(void)showAgePicketView
{
    [signUpScrollView setContentOffset:CGPointMake(0,vAge.frame.origin.y-100) animated:YES];
    [CommonCallback changeTextFieldColor:TEXT_EDITING view:vAge];
    [imgViewAge setHidden:YES];
    
    int initialIndex = 0;
    if ([txtAge.text length] > 0) {
        initialIndex = [arryAge indexOfObject:txtAge.text];
    }
    [ActionSheetStringPicker showPickerWithTitle:@"Select Age" rows:arryAge initialSelection:initialIndex target:self successAction:@selector(selectAge:element:) cancelAction:@selector(actionPickerCancelled:) origin:self.txtAge];
}
#pragma mark - Actionsheet Implementation
- (void)selectAge:(NSNumber *)lselectedIndex element:(id)element {
    NSInteger selectedIndex = [lselectedIndex integerValue];
    NSString *age = [arryAge objectAtIndex:selectedIndex];
    DebugLog(@"%@ ",age);
    txtAge.text = age;
    [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:vAge];
    [imgViewAge setHidden:NO];
    [self showOccupationPicketView];
}

-(void)showOccupationPicketView
{
    [signUpScrollView setContentOffset:CGPointMake(0,vOccupation.frame.origin.y-100) animated:YES];
    [CommonCallback changeTextFieldColor:TEXT_EDITING view:vOccupation];
    [imgViewOccupation setHidden:YES];
    int initialIndex = 0;
    if ([txtOccupation.text length] > 0) {
        initialIndex = [arryOccupation indexOfObject:txtOccupation.text];
    }
    [ActionSheetStringPicker showPickerWithTitle:@"Select Occupation" rows:arryOccupation initialSelection:initialIndex target:self successAction:@selector(selectOccupation:element:) cancelAction:@selector(actionPickerCancelled:) origin:self.txtOccupation];
}

#pragma mark - Actionsheet Implementation
- (void)selectOccupation:(NSNumber *)lselectedIndex element:(id)element {
    NSInteger selectedIndex = [lselectedIndex integerValue];
    NSString *occupation = [arryOccupation objectAtIndex:selectedIndex];
    DebugLog(@"%@ ",occupation);
    txtOccupation.text = occupation;
    [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:vOccupation];
    [imgViewOccupation setHidden:NO];
    [self showCountryPicketView];
}

-(void)showCountryPicketView
{
    [signUpScrollView setContentOffset:CGPointMake(0,vCountry.frame.origin.y-100) animated:YES];
    [CommonCallback changeTextFieldColor:TEXT_EDITING view:vCountry];
    [imgViewCountry setHidden:YES];
    int initialIndex = 0;
    if ([txtCountry.text length] > 0) {
        initialIndex = [arryCountry indexOfObject:txtCountry.text];
    }
    [ActionSheetStringPicker showPickerWithTitle:@"Select Country" rows:arryCountry initialSelection:initialIndex target:self successAction:@selector(selectCountry:element:) cancelAction:@selector(actionPickerCancelled:) origin:self.txtCountry];
}
#pragma mark - Actionsheet Implementation
- (void)selectCountry:(NSNumber *)lselectedIndex element:(id)element {
    NSInteger selectedIndex = [lselectedIndex integerValue];
    NSString *country = [arryCountry objectAtIndex:selectedIndex];
    DebugLog(@"%@ ",country);
    txtCountry.text = country;
    [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:vCountry];
    [imgViewCountry setHidden:NO];
    //[txtZipCode becomeFirstResponder];
    [self scrollTobottom];
}

- (void)actionPickerCancelled:(id)sender {
    if ([sender isEqual: txtAge]) {
        [imgViewAge setHidden:NO];
        [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:vAge];
        [self showOccupationPicketView];
    }
    if ([sender isEqual: txtOccupation]) {
        [imgViewOccupation setHidden:NO];
        [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:vOccupation];
        [self showCountryPicketView];
    }
    if ([sender isEqual: txtCountry]) {
        [imgViewCountry setHidden:NO];
        [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:vCountry];
        //[txtZipCode becomeFirstResponder];
        [self scrollTobottom];
    }
}

#pragma UITextFieldDelegate methods
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if([textField isEqual:txtEmailId])
    {
        [self scrollTobottom];
        [CommonCallback changeTextFieldColor:TEXT_EDITING view:vEmailId];
    }
    else if([textField isEqual:txtFirstname])
    {
        [self scrollTobottom];
        [CommonCallback changeTextFieldColor:TEXT_EDITING view:vFirstname];
    }
    else if([textField isEqual:txtLastname])
    {
        [CommonCallback changeTextFieldColor:TEXT_EDITING view:vLastname];
        [signUpScrollView setContentOffset:CGPointMake(0,vLastname.frame.origin.y-100) animated:YES];
    }
    else if([textField isEqual:txtAge])
    {
        [txtEmailId resignFirstResponder];
        [txtAge resignFirstResponder];
        [self showAgePicketView];
        return NO;
    }
    else if([textField isEqual:txtOccupation])
    {
        [txtOccupation resignFirstResponder];
        [self showOccupationPicketView];
        return NO;
    }
    else if([textField isEqual:txtCountry])
    {
        [txtCountry resignFirstResponder];
        [self showCountryPicketView];
        return NO;
    }
//    else if([textField isEqual:txtZipCode])
//    {
//        [CommonCallback changeTextFieldColor:TEXT_EDITING view:vZipCode];
//        [signUpScrollView setContentOffset:CGPointMake(0,vZipCode.frame.origin.y-100) animated:YES];
//    }
    signUpScrollView.contentSize = CGSizeMake(signUpScrollView.frame.size.width, CGRectGetMaxY(btnSubmit.frame)+300);
    return YES;
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if([textField isEqual:txtEmailId])
    {
        [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:vEmailId];
    }
    else if([textField isEqual:txtFirstname])
    {
        [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:vFirstname];
    }
    else if([textField isEqual:txtLastname])
    {
        [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:vLastname];
    }
    else if([textField isEqual:txtZipCode])
    {
        [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:vZipCode];
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == txtEmailId) {
     //   [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:vEmailId];
        [txtAge becomeFirstResponder];
	}else if (textField == txtFirstname) {
     //   [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:vFirstname];
        [txtLastname becomeFirstResponder];
	}else if (textField == txtLastname) {
      //  [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:vLastname];
        [txtLastname resignFirstResponder];
        [self showAgePicketView];
	}
    else if (textField == txtZipCode) {
    //    [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:vZipCode];
        [txtZipCode resignFirstResponder];
        [self scrollTobottom];
	}
   	return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if([textField isEqual:txtFirstname]) {
        NSString *firstNameString = [txtFirstname.text stringByReplacingCharactersInRange:range withString:string];
        return !([firstNameString length] > 50);
    }
    if([textField isEqual:txtLastname]) {
        NSString *lastNameString = [txtLastname.text stringByReplacingCharactersInRange:range withString:string];
        return !([lastNameString length] > 50);
    }
//    if([textField isEqual:txtZipCode]) {
//        NSString *zipCodeString = [txtZipCode.text stringByReplacingCharactersInRange:range withString:string];
//        return !([zipCodeString length] > 10);
//    } else {
//        return YES;
//    }
    return YES;
}

-(void)scrollTobottom
{
    [UIView animateWithDuration:0.2 animations:^{
        signUpScrollView.contentSize = CGSizeMake(signUpScrollView.frame.size.width,CGRectGetMaxY(btnSubmit.frame)+30);
        CGPoint bottomOffset = CGPointMake(0, 0);
        [signUpScrollView setContentOffset:bottomOffset animated:YES];
    }];
}

-(void)setErrorMessageOnLabel:(NSString *)errorMessage label:(UILabel *)lbl{
    lbl.hidden = NO;
    lbl.text = errorMessage;
    if ([lbl isEqual:lblEmailId]) {
        [CommonCallback changeTextFieldColor:TEXT_ERROR view:vEmailId];
    }else if ([lbl isEqual:lblFirstname]) {
        [CommonCallback changeTextFieldColor:TEXT_ERROR view:vFirstname];
    }else if ([lbl isEqual:lblLastname]) {
        [CommonCallback changeTextFieldColor:TEXT_ERROR view:vLastname];
    }else if ([lbl isEqual:lblAge]) {
        [CommonCallback changeTextFieldColor:TEXT_ERROR view:vAge];
    }else if ([lbl isEqual:lblOccupation]) {
        [CommonCallback changeTextFieldColor:TEXT_ERROR view:vOccupation];
    }else if ([lbl isEqual:lblCountry]) {
        [CommonCallback changeTextFieldColor:TEXT_ERROR view:vCountry];
    }else if ([lbl isEqual:lblZipCode]) {
        [CommonCallback changeTextFieldColor:TEXT_ERROR view:vZipCode];
    }
}

-(void)removeErrorMessageFromLabel:(UILabel *)lbl{
    lbl.hidden = YES;
    if ([lbl isEqual:lblEmailId]) {
        [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:vEmailId];
    }else if ([lbl isEqual:lblFirstname]) {
        [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:vFirstname];
    }else if ([lbl isEqual:lblLastname]) {
        [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:vLastname];
    }else if ([lbl isEqual:lblAge]) {
        [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:vAge];
    }else if ([lbl isEqual:lblOccupation]) {
        [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:vOccupation];
    }else if ([lbl isEqual:lblCountry]) {
        [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:vCountry];
    }else if ([lbl isEqual:lblZipCode]) {
        [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:vZipCode];
    }
}

-(BOOL)validate{
    BOOL success = YES;
    if ([txtEmailId.text length] == 0) {
        [self setErrorMessageOnLabel:@"Enter email address" label:lblEmailId];
        success = NO;
    }else if ([txtEmailId.text length] > 0 ) {
        if ([CommonCallback validateEmail:txtEmailId.text]) {
            [self removeErrorMessageFromLabel:lblEmailId];
        }else{
            [self setErrorMessageOnLabel:@"Enter valid email address" label:lblEmailId];
            success = NO;
        }
    }
    
//    if ([txtFirstname.text length] == 0) {
//        [self setErrorMessageOnLabel:@"Enter your first name" label:lblFirstname];
//        success = NO;
//    }else if ([txtFirstname.text length] > 0) {
//        [self removeErrorMessageFromLabel:lblFirstname];
//
//    }
//    
//    if ([txtLastname.text length] == 0) {
//        [self setErrorMessageOnLabel:@"Enter your last name" label:lblLastname];
//        success = NO;
//    }else if ([txtLastname.text length] > 0) {
//        [self removeErrorMessageFromLabel:lblLastname];
//        
//    }
//    else if ([txtLastname.text length] > 0) {
//        if ([txtLastname.text length] < 4) {
//            [self setErrorMessageOnLabel:@"Password must be at least 4 characters minimum." label:lblLastname];
//            success = NO;
//        }else{
//            [self removeErrorMessageFromLabel:lblLastname];
//        }
//    }
    
    if ([txtAge.text length] == 0) {
        [self setErrorMessageOnLabel:@"Select your age" label:lblAge];
        success = NO;
    }else if ([txtAge.text length] > 0) {
        [self removeErrorMessageFromLabel:lblAge];
    }
    
    if ([txtOccupation.text length] == 0) {
        [self setErrorMessageOnLabel:@"Select your occupation" label:lblOccupation];
        success = NO;
    }else if ([txtOccupation.text length] > 0) {
        [self removeErrorMessageFromLabel:lblOccupation];
    }
    
    if ([txtCountry.text length] == 0) {
        [self setErrorMessageOnLabel:@"Select your country" label:lblCountry];
        success = NO;
    }else if ([txtCountry.text length] > 0) {
        [self removeErrorMessageFromLabel:lblCountry];
    }
    
//    if ([txtZipCode.text length] == 0) {
//        [self setErrorMessageOnLabel:@"Enter your Zip/Postal code" label:lblZipCode];
//        success = NO;
//    }else if ([txtZipCode.text length] > 0) {
//        if ([txtZipCode.text length] < 6) {
//            [self setErrorMessageOnLabel:@"Zip/Postal code must be at least 6 characters." label:lblZipCode];
//            success = NO;
//        }else{
//            [self removeErrorMessageFromLabel:lblZipCode];
//        }
//    }
    return success;
}

- (IBAction)btnSendEmailAlertsPressed:(id)sender {
    btnSendEmailAlerts.selected = !btnSendEmailAlerts.selected;
    if (btnSendEmailAlerts.selected) {
        [btnSendEmailAlerts setTitleColor:DEFAULT_COLOR forState:UIControlStateNormal];
        [btnSendEmailAlerts setTitleColor:DEFAULT_COLOR_WITH_ALPHA(0.5) forState:UIControlStateHighlighted];
    }else{
        [btnSendEmailAlerts setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [btnSendEmailAlerts setTitleColor:[UIColor colorWithWhite:0 alpha:0.3] forState:UIControlStateHighlighted];
    }
}

- (IBAction)btnSubmitPressed:(id)sender {
    [self scrollTobottom];
    if ([self validate]) {
        DebugLog(@"Got validation success: Send to server");
        btnLater.selected = NO;
       // [self showTermsAndPrivacyView];
        if (btnSendEmailAlerts.selected) {
            NSDictionary *params = @{@"email_subscribe":@"yes"};
            [INEventLogger logEvent:@"SignUp_Email_Subscription" withParams:params];
        }else{
            NSDictionary *params = @{@"email_subscribe":@"no"};
            [INEventLogger logEvent:@"SignUp_Email_Subscription" withParams:params];
        }
        [INEventLogger logEvent:@"SignUp_Submit"];
        [self sendSignUpRequest];
    }
}

- (IBAction)btnLaterPressed:(id)sender {
    [INEventLogger logEvent:@"SignUp_Cancel"];
    [self scrollTobottom];
    btnLater.selected = YES;
  //  [self showTermsAndPrivacyView];
    [self.navigationController dismissViewControllerAnimated:YES completion:^(void){
        DebugLog(@"btnLaterPressed");
    }];
}

-(void)showTermsAndPrivacyView{
    if ([MedicalUserDefaults isAcceptedLicenseAgreement]) {
        return;
    }
    TermsAndPrivacyViewController *webViewController = [[TermsAndPrivacyViewController alloc] initWithNibName:@"TermsAndPrivacyViewController" bundle:nil];
    webViewController.title = @"Terms and Privacy";
    webViewController.termsDelegate = self;
    
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:webViewController];
//    UIButton *closeBtn =  [UIButton buttonWithType:UIButtonTypeCustom];
//    [closeBtn setFrame:CGRectMake(276, 0, 44, 44)];
//    [closeBtn setTitle:@"" forState:UIControlStateNormal];
//    [closeBtn setImage:[UIImage imageNamed:@"cancel_white.png"] forState:UIControlStateNormal];
//    [closeBtn addTarget:self action:@selector(closeTermsAndPrivacyView) forControlEvents:UIControlEventTouchUpInside];
//    [navController.navigationBar addSubview:closeBtn];
    [navController.navigationBar setTranslucent:NO];
    [self.navigationController presentViewController:navController animated:YES completion:nil];
}

-(void)closeTermsAndPrivacyView
{
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        DebugLog(@"closeTermsAndPrivacyView");
        if (btnLater.selected) {
            [self.navigationController dismissViewControllerAnimated:YES completion:^(void){
                DebugLog(@"btnLaterPressed");
            }];
        }else{
            [self sendSignUpRequest];
        }
    }];
}

-(void)sendSignUpRequest{
    DebugLog(@"sendSignUpRequest");
    if (![MEDICAL_APP_DELEGATE networkavailable]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:ALERT_MESSAGE_NO_NETWORK delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return;
    }
    [HUD show:YES];
//    The api endpoint is http://www.cancerebook.org/form-submit.php
//    
//    variables to be sent are
//    
//    first_name
//    last_name
//    email
//    age
//    occupation
//    pin_code
//    country
//    email_alerts (1/0)
    
    NSMutableDictionary *postDict = [[NSMutableDictionary alloc]init];
    
    if ([txtEmailId.text length] > 0)
        [postDict setObject:txtEmailId.text forKey:@"email"];
    else
        [postDict setObject:@"" forKey:@"email"];

    if ([txtFirstname.text length] > 0)
        [postDict setObject:txtFirstname.text forKey:@"first_name"];
    else
        [postDict setObject:@"" forKey:@"first_name"];
    
    if ([txtLastname.text length] > 0)
        [postDict setObject:txtLastname.text forKey:@"last_name"];
    else
        [postDict setObject:@"" forKey:@"last_name"];
    
    if ([txtAge.text length] > 0)
        [postDict setObject:txtAge.text forKey:@"age"];
    else
        [postDict setObject:@"" forKey:@"age"];
    
    if ([txtOccupation.text length] > 0)
        [postDict setObject:txtOccupation.text forKey:@"occupation"];
    else
        [postDict setObject:@"" forKey:@"occupation"];
    
    if ([txtZipCode.text length] > 0)
        [postDict setObject:txtZipCode.text forKey:@"pin_code"];
    else
        [postDict setObject:@"" forKey:@"pin_code"];
    
    if ([txtCountry.text length] > 0)
        [postDict setObject:txtCountry.text forKey:@"country"];
    else
        [postDict setObject:@"" forKey:@"country"];
    
    if (btnSendEmailAlerts.selected)
        [postDict setObject:[NSNumber numberWithInt:1] forKey:@"email_alerts"];
    else
        [postDict setObject:[NSNumber numberWithInt:0] forKey:@"email_alerts"];
    
    DebugLog(@"postDict %@",postDict);

    NSError *error;
    NSData *postData = [NSJSONSerialization dataWithJSONObject:postDict options:NSJSONWritingPrettyPrinted error:&error];
    if (!postData) {
        DebugLog(@"Got an error: %@", error);
    }
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:SUBMIT_LINK] cachePolicy:NO timeoutInterval:30.0];
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [urlRequest addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:postData];
    
    conn = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

#pragma NSURLConnection Delegate
-(void) connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *) response
{
    if ([response isKindOfClass:[NSHTTPURLResponse class]])
	{
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
		status = [httpResponse statusCode];
	    DebugLog(@"status %d",status);
    }
}

-(void) connection:(NSURLConnection *)connection didReceiveData:(NSData *) data
{
    if(reponseData==nil)
	{
		reponseData = [[NSMutableData alloc] initWithLength:0];
	}
    [reponseData appendData:data];
}

-(void) connection:(NSURLConnection *)connection didFailWithError:(NSError *) error
{
    DebugLog(@"didFailWithError error %@",error);
    [conn cancel];
    conn = nil;
   [HUD hide:YES];
}

-(void) connectionDidFinishLoading:(NSURLConnection *) connection
{
    DebugLog(@"connectionDidFinishLoading reponseData.length %d",[reponseData length]);
    [HUD hide:YES];
    if (reponseData)
    {
//        NSString *result = [[NSString alloc] initWithData:reponseData encoding:NSASCIIStringEncoding];
//		DebugLog(@"\n result:%@\n\n", result);
        NSError *error;
        NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:reponseData options:NSJSONReadingAllowFragments error:&error];
        DebugLog(@"jsonDict %@",jsonDict);
        if (jsonDict != nil) {
            if ([jsonDict objectForKey:@"success"] != nil && [[jsonDict objectForKey:@"success"] isEqualToString:@"true"])
            {
                NSDictionary *params = @{@"success":@"true"};
                [INEventLogger logEvent:@"SignUp_Submit_Response" withParams:params];
            }else{
                NSDictionary *params = @{@"success":@"false"};
                [INEventLogger logEvent:@"SignUp_Submit_Response" withParams:params];
            }
            NSString *message  = [jsonDict objectForKey:@"message"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }

        reponseData = nil;
        conn = nil;
    }
}

#pragma UIAlertView delegate methods
- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if ([title isEqualToString:@"OK"]) {
        if (conn) {
            [conn cancel];
            conn = nil;
        }
        if (![HUD isHidden]) {
            [HUD hide:YES];
        }
        [self.navigationController dismissViewControllerAnimated:YES completion:^(void){
            DebugLog(@"connectionDidFinishLoading : dismissViewControllerAnimated");
        }];
    }else if ([title isEqualToString:@"YES"]) {
        if (conn) {
            [conn cancel];
            conn = nil;
        }
        if (![HUD isHidden]) {
            [HUD hide:YES];
        }
    }
}
@end
