//
//  MedicalAppDelegate.h
//  Medical
//
//  Created by Kirti Nikam on 08/10/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "Reachability.h"
#import "constants.h"
#import <StoreKit/StoreKit.h>

#define MEDICAL_APP_DELEGATE (MedicalAppDelegate *)[[UIApplication sharedApplication] delegate]
//#define titlenotificationName  @"ChemoOrdersNotification"
//#define titlenotificationKey  @"UnhideEvent"

@class MedicalViewController;
@class SplashViewController;
@interface MedicalAppDelegate : UIResponder <UIApplicationDelegate,UIPopoverControllerDelegate,MFMailComposeViewControllerDelegate,SKStoreProductViewControllerDelegate>
{
    Reachability* internetReach;
    Boolean networkavailable;
    NSURLConnection *conn;
    NSMutableData *responseData;
}
@property(nonatomic) Boolean networkavailable;
@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) MedicalViewController *viewController;
@property (strong, nonatomic) SplashViewController *splashViewController;

@property (strong, nonatomic) UINavigationController *navigationController;
@property (strong, readwrite) UIPopoverController *popover;

@property(copy,nonatomic) NSString *databaseName;
@property(copy,nonatomic) NSString *databasePath;
@property (nonatomic, readwrite) int PRINTEMAILALERT;

-(void)mailImage:(UIView *)view;
-(void)printImage:(UIView *)view;

-(void) removeSplashScreen;
-(void)checkAppVersion;
@end
