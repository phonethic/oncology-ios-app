//
//  MedicalNewsDetailsViewController.h
//  Medical
//
//  Created by Kirti Nikam on 08/04/14.
//  Copyright (c) 2014 Kirti Nikam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

#define FDA @"FDA Alert"
#define JOB @"Job Alert"
#define LOCUM @"Locum Alert"
#define STUDY @"Study Alert"
#define MED @"Med Alert"

//FDA Alert, Clinical Trial Alert, Drug Approval Alert and Must Read Article Alert.

@class NewsObject;
@interface MedicalNewsDetailsViewController : UIViewController <NSURLConnectionDelegate,MBProgressHUDDelegate>
{
    NSURLConnection *conn;
    
    NewsObject *newsObj;
    
    MBProgressHUD *HUD;
}
@property (copy,nonatomic) NSString *selectedAlertName;
@property (copy,nonatomic) NSString *selectedSlugName;
@property (strong,nonatomic) NSMutableArray *postArray;
@property (strong,nonatomic) NSMutableData *webData;

@property (strong, nonatomic) IBOutlet UITableView *detailTableView;
@end
