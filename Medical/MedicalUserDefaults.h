//
//  MedicalUserDefaults.h
//  Medical
//
//  Created by Kirti Nikam on 23/04/14.
//  Copyright (c) 2014 Kirti Nikam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MedicalUserDefaults : NSObject
+(void)setPlayTermsPrivacyAnimation:(BOOL)value;
+(BOOL)isDoneTermsPrivacyAnimation;

+(void)setLicenseAgreement:(BOOL)value;
+(BOOL)isAcceptedLicenseAgreement;

+(void)addAlertWithPostId:(int)postId title:(NSString *)postTitle content:(NSString *)postContent date:(NSString *)postDate;
+(void)setPostSyncDate;
+(NSDate *)getPostSyncDate;
+(int)getPostId;
+(NSString *)getPostDate;
+(NSString *)getPostTitle;
+(NSString *)getPostContent;

+(void)setChemoOrderHelpInfoSceenCount;
+(int)getChemoOrderHelpInfoSceenCount;
@end
