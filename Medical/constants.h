
#ifndef _ALERT_MESSAGES_H_
#define _ALERT_MESSAGES_H_
#import "Reachability.h"


#define SYSTEM_VERSION_LESS_THAN(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define ALERT_TITLE @"Cancer eBook"
#define ALERT_MESSAGE_NO_NETWORK @"No internet. Please connect to the internet and try again."

#define NETWORK_NOTIFICATION @"NetWorkchangeNotification"

//#define ONCOLOGY_APPSTORE_URL @"https://itunes.apple.com/us/app/"
//#define CRITTERCISM_APPID @"5269206be432f53d32000001"
//#define PARSE_APPID @"UD0PNYHMlbsxliXGBVn6IUJe9KTQ8mz93385tb8k"
//#define PARSE_CLIENTKEY @"849ZNsheq3xTNHSw120qO1xDeWjWDz8CcdslMwn5"

#define DATABASE_VERSION 5

//#ifdef DEBUG
//    #define FLURRY_APPID @"JDM7MR3TXVRW8TCJRHR8"
//    #define LOCALYTICS_APPID @"fb919b6fb4f4b93d56ea935-7b7f7fd2-783f-11e3-94f2-005cf8cbabd8"
//#else
    #define FLURRY_APPID @"ZDD2MVJWMKVWCQSGHZDF"
    #define LOCALYTICS_APPID @"2e172610b4beda2a3bfe7ba-f7159ee0-f557-11e3-200a-004a77f8b47f"  // Live APP LOCALYTICS ID
//#endif


#define ITUNES_URL @"http://itunes.apple.com/lookup?id=891567879"

//==============HUD TEXT=================
#define HUD_TITLE @"Loading"
#define HUD_SUBTITLE @"Please wait ..."


//==============FONT=================
//#define DEFAULT_FONT(SIZE) [UIFont systemFontOfSize:SIZE]
//#define DEFAULT_BOLD_FONT(SIZE) [UIFont boldSystemFontOfSize:SIZE]

#define DEFAULT_FONT(SIZE) [UIFont fontWithName:@"OpenSans" size:SIZE]
#define DEFAULT_ITALIC_FONT(SIZE) [UIFont fontWithName:@"OpenSans-Italic" size:SIZE]
#define DEFAULT_BOLD_FONT(SIZE) [UIFont fontWithName:@"OpenSans-Bold" size:SIZE]
#define DEFAULT_BOLD_ITALIC_FONT(SIZE) [UIFont fontWithName:@"OpenSans-BoldItalic" size:SIZE]
#define DEFAULT_SEMIBOLD_FONT(SIZE) [UIFont fontWithName:@"OpenSans-Semibold" size:SIZE]
#define DEFAULT_SEMIBOLD_ITALIC_FONT(SIZE) [UIFont fontWithName:@"OpenSans-SemiboldItalic" size:SIZE]

//Open Sans
//OpenSansLight-Italic
//OpenSans-Bold
//OpenSans-SemiboldItalic
//OpenSans
//OpenSans-BoldItalic
//OpenSans-Light
//OpenSans-Italic
//OpenSans-Semibold

//==============COLOR CODES===============
//#define BACKGROUND_COLOR [UIColor colorWithRed:0.505 green:0.745 blue:0.968 alpha:1.0]
//#define BACKGROUND_COLOR1 [UIColor colorWithRed:0.662 green:0.815 blue:0.960 alpha:1.0]
//#define BACKGROUND_COLOR2 [UIColor colorWithRed:0.741 green:0.741 blue:0.741 alpha:1.0]
//#define BACKGROUND_COLOR3 [UIColor colorWithRed:0.643 green:0.643 blue:0.643 alpha:1.0]

//Dark
#define DEFAULT_COLOR [UIColor colorWithRed:23/255.0 green:121/255.0 blue:172/255.0 alpha:1.0]
#define DEFAULT_COLOR_WITH_ALPHA(ALPHA) [UIColor colorWithRed:23/255.0 green:121/255.0 blue:172/255.0 alpha:ALPHA]

//Medium
//#define DEFAULT_COLOR [UIColor colorWithRed:51/255.0 green:152/255.0 blue:204/255.0 alpha:1.0]
//#define DEFAULT_COLOR_WITH_ALPHA(ALPHA) [UIColor colorWithRed:51/255.0 green:152/255.0 blue:204/255.0 alpha:ALPHA]

//Light
//#define LIGHT_DEFAULT_COLOR [UIColor colorWithRed:88/255.0 green:161/255.0 blue:197/255.0 alpha:1.0]
//#define LIGHT_DEFAULT_COLOR [UIColor colorWithRed:149/255.0 green:219/255.0 blue:255/255.0 alpha:1.0]

#define NAVIGATION_TINTCOLOR [UIColor colorWithRed:0.0 green:0.501 blue:1.0 alpha:1.0]
#define NAVIGATION_THEMELIGHTLIGHTCOLOR [UIColor colorWithRed:0.439 green:0.525 blue:0.643 alpha:1.0]
#define NAVIGATION_THEMELIGHTCOLOR [UIColor colorWithRed:0.450 green:0.537 blue:0.647 alpha:1.0]
#define NAVIGATION_THEMEDARKCOLOR [UIColor colorWithRed:0.286 green:0.419 blue:0.607 alpha:1.0]

//UILABEL
//RESULTLABEL
#define RESULTLABEL_TEXTCOLOR [UIColor blackColor]
#define RESULTLABEL_FONT [UIFont boldSystemFontOfSize:18]

//UITEXTFIELD
#define TEXTFIELDVIEW_BORDERWIDTH 1.8
#define TEXTFIELDVIEW_CORNERRADIUS 7

#define TEXTFIELDVIEW_DEFAULT_BORDERCOLOR [UIColor lightGrayColor].CGColor
#define TEXTFIELDVIEW_DEFAULT_BACKGROUNDCOLOR [UIColor whiteColor]

#define TEXTFIELDVIEW_ERROR_BORDERCOLOR [UIColor redColor].CGColor
#define TEXTFIELDVIEW_ERROR_BACKGROUNDCOLOR [UIColor colorWithRed:1.0 green:0.788 blue:0.788 alpha:1.0]

//UITABLEVIEW
#define TABLEVIEW_BORDERWIDTH 2.0
#define TABLEVIEW_CORNERRADIUS 10
#define TABLEVIEW_BORDERCOLOR [UIColor lightGrayColor].CGColor
#define TABLEVIEW_BACKGROUNDCOLOR [UIColor blackColor]

#define TABLEVIEW_HEADERLABEL_TEXTCOLOR [UIColor lightGrayColor]
#define TABLEVIEW_HEADERLABEL_FONT [UIFont boldSystemFontOfSize:14]

#define TABLEVIEW_CELLLABEL_TEXTCOLOR [UIColor whiteColor]
#define TABLEVIEW_CELLLABEL_FONT [UIFont fontWithName:@"ArialMT" size:16]

#define CUSTOMVIEW_CORNERRADIUS 15
#define CUSTOMVIEW_BORDERWIDTH 2.0
#define INDICATOR_COLOR [UIColor whiteColor];

#define ONE_POUND_IN_KG 0.45359237
#define ONE_MGdL_IN_UMOL 88.4
#define ONE_UMOL_IN_MGdL 0.011312217194570135
#define ONE_MMOL_IN_MGdL 11.3122171945701 //18.0182
#define CARBO_MALE 1
#define CARBO_FEMALE 0.85
#define ONE_INCHES_IN_CM 2.54
#define ONE_CM_IN_INCHES 0.393701

#define KEYBORAD_APPEARANCE UIKeyboardAppearanceDefault //UIKeyboardAppearanceAlert


#define STATIC_ROW_HEIGHT 70
#define FONT_STATIC_ROW_HEIGHT 17

#define UPDATE_TITLE @"Warning"
#define UPDATE_MSG @"A new version of Cancer eBook is available on the App store. Please download this version as it contains important updates. The user bears full responsibility for using an out of date version."


//Warning or info or disclaimer messages
#define WARNING_TITLE @"Please Note"
#define WARNING_MSG_FOR_WEBLINK @"The selected link will take you to a site not owned, controlled or affiliated with Ansh,LLC. Your use of third party websites is at your own risk and is subject to the terms and conditions of such websites."

#define WARNING_MSG_FOR_PRINT_EMAIL @"WARNING \n\nEmail/print only on a secure and encrypted network. Use minimal essential personal health information. User has the sole resposibility to be fully aware of the current HIPAA laws and to be  compliant with HIPAA.\nAnsh,LLC does not take any responsibility and will not be liable for any confidentialty or HIPAA violations.\n"

#define DISCLAIMER_TITLE @"Disclaimer"
#define CALC_DISCLAIMER_INFO @"calculator_disclaimer"
#define CHEMOTHERAPY_DISCLAIMER_INFO @"chemotherapy_disclaimer"
#define CHEMOORDERS_DISCLAIMER_INFO @"chemoorders_disclaimer"
#define CTCAE_DISCLAIMER_INFO @"ctcae_disclaimer"
#define CHEMOORDERS_HELP_INFO @"chemorders_helpinfo"
#define CHEMOORDERS_HELP_INFO_iOS7 @"chemorders_helpinfoiOS7"

// App information
#define ABOUT_INFO @"about"
#define CONTRIBUTERS_INFO @"contributors"
#define TERMS_CONDITIONS_INFO @"terms-and-condition"
#define PRIVATE_POLICY_INFO @"privacy-policy"
#define CONTACTUS_INFO @"info@cancerebook.org"

#define LIVE_VACCINES_INFO @"live-vaccines"
#define TERMS_AND_PRIVATE_POLICY_INFO @"terms-and-privacy"


#define TEXT_EDITING 1
#define TEXT_ERROR 2
#define TEXT_ENDEDITITNG 3
#endif
