//
//  MedicalCancerSocietiesViewController.m
//  Medical
//
//  Created by Sagar Mody on 05/11/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import "MedicalCancerSocietiesViewController.h"
#import "MedicalWebViewController.h"
#import "constants.h"

@interface MedicalCancerSocietiesViewController ()

@end

@implementation MedicalCancerSocietiesViewController
@synthesize societiesTableView;
@synthesize societiesObjdict;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [INEventLogger logEvent:@"Cancer_Societies_Link_List"];

    societiesObjdict = [[NSMutableDictionary alloc] init];
	[societiesObjdict setObject:@"http://www.aacr.org/" forKey:@"American Association for Cancer Research"];
	[societiesObjdict setObject:@"http://www.abta.org/" forKey:@"American Brain Tumor Association"];
	[societiesObjdict setObject:@"http://www.cancer.org/" forKey:@"American Cancer Society"];
    [societiesObjdict setObject:@"http://www.headandneckcancer.org/" forKey:@"American Head and Neck Society"];
    [societiesObjdict setObject:@"https://www.astro.org/" forKey:@"American Society for Therapeutic Radiology and Oncology"];
    [societiesObjdict setObject:@"http://www.asco.org/" forKey:@"American Society of Clinical Oncology"];
    [societiesObjdict setObject:@"" forKey:@"American Society of Hematology"];
    [societiesObjdict setObject:@"http://www.aspho.org/" forKey:@"American Society of Pediatric Hematology/Oncology"];
    [societiesObjdict setObject:@"http://www.thoracic.org/" forKey:@"American Thoracic Society"];
    [societiesObjdict setObject:@"http://www.ampweb.org/" forKey:@"Association for Molecular Pathology"];
    [societiesObjdict setObject:@"http://apao.cc" forKey:@"Association of Physician Assistants in Oncology"];
    [societiesObjdict setObject:@"http://www.childrensoncologygroup.org" forKey:@"Children’s Oncology group"];
    //[societiesObjdict setObject:@"" forKey:@"Cancer and Leukemia Group B (CALGB)"];
    [societiesObjdict setObject:@"http://ecog.dfci.harvard.edu/" forKey:@"Eastern Cooperative Oncology Group (ECOG)"];
    [societiesObjdict setObject:@"http://www.eortc.org" forKey:@"European Organisation for Research and Treatment of Cancer (EORTC)"];
    [societiesObjdict setObject:@"http://www.esmo.org" forKey:@"European Society of Medical Oncology"];
    [societiesObjdict setObject:@"http://www.fda.gov/" forKey:@"Food and Drug Administration"];
    [societiesObjdict setObject:@"http://www.iaslc.org" forKey:@"International Association For the Study of Lung Cancer"];
    [societiesObjdict setObject:@"http://www.lls.org/" forKey:@"Leukemia and Lymphoma Society"];
    [societiesObjdict setObject:@"http://www.cancer.gov/" forKey:@"National Cancer Institute"];
    //[societiesObjdict setObject:@"http://www.nccn.org/index.asp" forKey:@"National Comprehensive Cancer Center Network"];
    [societiesObjdict setObject:@"http://marrow.org/Home.aspx" forKey:@"National Marrow Donor Program"];
    //[societiesObjdict setObject:@"http://ncctg.mayo.edu/" forKey:@"North Central Cancer Treatment Group"];
    [societiesObjdict setObject:@"http://www.ons.org/" forKey:@"Oncology Nursing Society"];
    //[societiesObjdict setObject:@"http://www.rtog.org/" forKey:@"Radiation Therapy Oncology Group"];
    [societiesObjdict setObject:@"http://www.sgo.org/" forKey:@"Society of Gynecologic Oncologists"];
    [societiesObjdict setObject:@"http://www.surgonc.org/" forKey:@"Society of Surgical Oncology"];
    [societiesObjdict setObject:@"http://www.swog.org/" forKey:@"Southwest Oncology Group"];

    [societiesTableView reloadData];
}

- (void)viewDidUnload
{
    [self setSocietiesTableView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark Table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *key       = [[[societiesObjdict allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.row];
    CGSize size         = [key sizeWithFont:DEFAULT_FONT(FONT_STATIC_ROW_HEIGHT) constrainedToSize:CGSizeMake(280, 999) lineBreakMode:NSLineBreakByWordWrapping];
    DebugLog(@"%f",size.height);
    return size.height + 40;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[societiesObjdict allKeys] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        
        cell.textLabel.font=DEFAULT_FONT(FONT_STATIC_ROW_HEIGHT);
        cell.textLabel.numberOfLines = 3;
        
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = DEFAULT_COLOR_WITH_ALPHA(0.5);
        cell.selectedBackgroundView = bgColorView;
    }
    NSString *key       = [[[societiesObjdict allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.row];
    NSString *value     = [societiesObjdict objectForKey:key];
    if(![value isEqualToString:@""])
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    else
        cell.accessoryType = UITableViewCellAccessoryNone;
    cell.textLabel.text = key;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:WARNING_TITLE
                                                        message:WARNING_MSG_FOR_WEBLINK
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
	[alertView show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	DebugLog(@"buttonIndex = %d",buttonIndex);
    NSIndexPath *indexPath = [societiesTableView indexPathForSelectedRow];
    if (indexPath != nil) {
        NSString *key       = [[[societiesObjdict allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.row];
        NSString *value     = [societiesObjdict objectForKey:key];
        //DebugLog(@"%@",value);
        NSDictionary *params = @{@"society_name":key,@"link":value};
        [INEventLogger logEvent:@"Cancer_Societies_Website" withParams:params];
        
        if(![value isEqualToString:@""])
        {
            MedicalWebViewController *webviewController = [[MedicalWebViewController alloc] initWithNibName:@"MedicalWebViewController" bundle:nil] ;
            webviewController.medicalLink = value;
            webviewController.title = key;
            [self.navigationController pushViewController:webviewController animated:YES];
        }
        [societiesTableView deselectRowAtIndexPath:indexPath animated:YES];
    }
}
@end
