//
//  INUserDefaultOperations.h
//  inorbit
//
//  Created by Rishi on 03/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface INUserDefaultOperations : NSObject

+(void)setRefreshDate:(NSDate *)value;
+(NSDate *)getRefreshDate;
+(void)setLastAppVersion:(float)value;
+(float)getLastAppVersion;
    
+(NSInteger)getDateDifferenceInSeconds:(NSDate *)startDate endDate:(NSDate *)endDate;
+(NSInteger)getDateDifferenceInMinutes:(NSDate *)startDate endDate:(NSDate *)endDate;
+(NSInteger)getDateDifferenceInHours:(NSDate *)expireDate;
+(NSInteger)getDateDifferenceInDays:(NSDate *)expireDate;
+(NSInteger)getAgeInYears:(NSDate *)birthDate;

@end
