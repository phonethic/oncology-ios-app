//
//  MeetingObject.m
//  Medical
//
//  Created by Sagar Mody on 05/11/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import "MeetingObject.h"

@implementation MeetingObject
@synthesize name,startDate,endDate,place,weblink,eventIdentifier;

-(id)initWithName:(NSString *)leventName  eventStartDate:(NSString *)leventStartDate eventEndDate:(NSString *)leventEndDate eventPlace:(NSString *)leventPlace eventLink:(NSString *)leventLink  eventId:(NSString *) leventId
{
    self = [super init];
    
    if(self) {
        self.name = leventName;
        self.startDate = leventStartDate;
        self.endDate = leventEndDate;
        self.place = leventPlace;
        self.weblink = leventLink;
        self.eventIdentifier = leventId;
    }
	return self;
}
@end
