//
//  MedicalCarboplatinViewController.m
//  Medical
//
//  Created by Kirti Nikam on 09/10/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import "MedicalCarboplatinViewController.h"
#import "MedicalInfoImageViewController.h"
#import "MedicalAppDelegate.h"
#import "CommonCallback.h"

@interface MedicalCarboplatinViewController ()
@end

@implementation MedicalCarboplatinViewController
@synthesize scrollView;
@synthesize labelResult;
@synthesize weightLabel;
@synthesize serumLabel;
@synthesize gfrResult;
@synthesize GFRCapped125;
@synthesize ageTextField;
@synthesize weightTextField;
@synthesize heightTextField;
@synthesize ftTextField;
@synthesize inchTextField;
@synthesize serumTextField;
@synthesize targetAUCTextField;
@synthesize genderSegmentControl;
@synthesize weightkeyboardView;
@synthesize heightkeyboardView;
@synthesize serumkeyboardView;
@synthesize viewAge,viewWeight,viewHeightInCM,viewHeightInFT,viewSerumCreatinine,viewTargetAUC;
@synthesize lblErrAge,lblErrWeight,lblErrHeight,lblErrSerumC,lblErrTargetAUC;
@synthesize lblGender,lblGFR,lblCarboplatinDose;
@synthesize viewCollection,txtCollection,lblCollection,lblErrCollection,keyBoardViewCollection,keyBoardBtnCollection;
@synthesize heightLabel;
@synthesize comeFromOtherThanCalculator;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void) viewWillAppear:(BOOL)animated {
    //—-registers the notifications for keyboard—-
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:)
//                                                 name:UIKeyboardDidShowNotification object:self.view.window];
//    
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification
//                                               object:nil];
    
    [super viewWillAppear:animated];
//    if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation))
//    {
//        DebugLog(@"already in Portrait");
//    }
//    else
//    {
//        UIViewController *c = [[UIViewController alloc]init];
//        [self presentModalViewController:c animated:NO];
//        [self dismissModalViewControllerAnimated:NO];
//    }
}

-(void)touchesBegan:(NSSet*)touches withEvent:(UIEvent*)event
{
    UITouch *touch = [touches anyObject];
    if(touch.view == scrollView) {
        DebugLog(@"you touched scrollview");
        [self.view endEditing:YES];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSDictionary *params = @{@"calculator_name":self.title};
    [INEventLogger logEvent:@"MedCalc_Calculator_Details" withParams:params];
  //  [self roundViewWithBorder];
    if(comeFromOtherThanCalculator) {
        //Regimen BSA View
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]
                                                  initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                  target:self action:@selector(doneBarBtnClicked:)];
        self.navigationItem.hidesBackButton = YES;
    }else{
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]
                                                  initWithBarButtonSystemItem:UIBarButtonSystemItemAction
                                                  target:self action:@selector(shareBarBtnClicked:)];
    }

    [self setUI];
    scrollView.contentSize = CGSizeMake(scrollView.frame.size.width, CGRectGetMaxY(lblCarboplatinDose.frame)+30);
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected:)];
    tapGesture.numberOfTapsRequired = 1;
    tapGesture.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapGesture];
}

- (void)viewDidUnload
{
    [self setWeightkeyboardView:nil];
    [self setHeightkeyboardView:nil];
    [self setSerumkeyboardView:nil];
    [self setAgeTextField:nil];
    [self setWeightTextField:nil];
    [self setHeightTextField:nil];
    [self setFtTextField:nil];
    [self setInchTextField:nil];
    [self setSerumTextField:nil];
    [self setGenderSegmentControl:nil];
    [self setLabelResult:nil];
    [self setWeightLabel:nil];
    [self setSerumLabel:nil];
    [self setTargetAUCTextField:nil];
    [self setScrollView:nil];
    [self setGfrResult:nil];
    [self setGFRCapped125:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
#pragma mark - Rotation
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    DebugLog(@"shouldAutorotateToInterfaceOrientation %u",toInterfaceOrientation);
    if(comeFromOtherThanCalculator) {
        return (toInterfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    }else{
        return (toInterfaceOrientation == UIInterfaceOrientationPortrait);
    }
}
- (BOOL)shouldAutorotate
{
    if(comeFromOtherThanCalculator) {
        return YES;
    }else{
        return NO;
    }
}

- (NSUInteger)supportedInterfaceOrientations
{
    if(comeFromOtherThanCalculator) {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    }else{
        return UIInterfaceOrientationMaskPortrait;
    }
}

#pragma internal methods
-(void)setUI{
    
    lblGender.backgroundColor               = [UIColor clearColor];
    lblGender.textColor                     = DEFAULT_COLOR;
    lblGender.font                          = DEFAULT_FONT(20);
    
    lblGFR.backgroundColor                  = [UIColor clearColor];
    lblGFR.textColor                        = DEFAULT_COLOR;
    lblGFR.font                             = DEFAULT_SEMIBOLD_FONT(20);
    
    gfrResult.backgroundColor               = [UIColor clearColor];
    gfrResult.textColor                     = DEFAULT_COLOR;
    gfrResult.font                          = DEFAULT_SEMIBOLD_FONT(20);
    
    lblCarboplatinDose.backgroundColor      = [UIColor clearColor];
    lblCarboplatinDose.textColor            = DEFAULT_COLOR;
    lblCarboplatinDose.font                 = DEFAULT_BOLD_FONT(25);
    
    labelResult.backgroundColor             = [UIColor clearColor];
    labelResult.textColor                   = DEFAULT_COLOR;
    labelResult.font                        = DEFAULT_BOLD_FONT(25);
    
    for (UIView *lview in viewCollection) {
        lview.backgroundColor     = [UIColor whiteColor];
        lview.layer.borderColor   = [UIColor lightGrayColor].CGColor;
        lview.layer.borderWidth   = 1.0;
    }
    
    for (UITextField *txt in txtCollection) {
        txt.delegate            = self;
        txt.backgroundColor     = [UIColor clearColor];
        txt.textColor           = [UIColor blackColor];
        txt.font                = DEFAULT_FONT(22);
        txt.keyboardType        = UIKeyboardTypeDecimalPad;
        txt.textAlignment       = NSTextAlignmentCenter;
        txt.layer.borderColor   = [UIColor clearColor].CGColor;
        txt.text                = @"";
    }
    
    weightTextField.inputAccessoryView  = weightkeyboardView;
    heightTextField.inputAccessoryView  = heightkeyboardView;
    ftTextField.inputAccessoryView      = heightkeyboardView;
    inchTextField.inputAccessoryView    = heightkeyboardView;
    serumTextField.inputAccessoryView   = serumkeyboardView;
    
    for (UILabel *lbl in lblCollection) {
        lbl.backgroundColor     = [UIColor clearColor];
        lbl.textColor           = DEFAULT_COLOR;
        lbl.font                = DEFAULT_SEMIBOLD_FONT(20);
        lbl.textAlignment       = NSTextAlignmentCenter;
    }
    
    for (UILabel *lbl in lblErrCollection) {
        lbl.backgroundColor     = [UIColor clearColor];
        lbl.textColor           = [UIColor redColor];
        lbl.font                = DEFAULT_SEMIBOLD_FONT(12);
        lbl.hidden              = YES;
    }
    
    for (UIView *keyboardView in keyBoardViewCollection) {
        keyboardView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.4];
    }
    
    for (UIButton *btn in keyBoardBtnCollection) {
        btn.backgroundColor     = DEFAULT_COLOR;
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor colorWithWhite:1.0 alpha:0.5] forState:UIControlStateHighlighted];
        btn.titleLabel.font     = DEFAULT_FONT(20);
        
    }
}

-(void)tapDetected:(UIGestureRecognizer *)sender{
    [self.view endEditing:YES];
    CGPoint tapLocation = [sender locationInView:scrollView];
    UIView *view = [scrollView hitTest:tapLocation withEvent:nil];
    if (![view isKindOfClass:[UIButton class]]) {
        [self scrollTobottom];
    }
}

- (void) shareBarBtnClicked:(id)sender {
    UIActionSheet *uiActionSheetP= [[UIActionSheet alloc]
                                    initWithTitle: @""
                                    delegate:self
                                    cancelButtonTitle:@"CANCEL"
                                    destructiveButtonTitle:nil
                                    otherButtonTitles:@"Print",@"Email", nil];
	
	uiActionSheetP.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
	[uiActionSheetP showInView:self.navigationController.view];
    
}

#pragma UIActionSheet Delegate Methods
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    DebugLog(@"buttonIndex = %d",buttonIndex);
	[self.view endEditing:YES];
    CGRect oldFrame = scrollView.frame;
    scrollView.frame=CGRectMake(scrollView.frame.origin.x, scrollView.frame.origin.y, scrollView.contentSize.width, scrollView.contentSize.height);
	if (buttonIndex == 0)
    {
        NSDictionary *params = @{@"calculator_name":self.title, @"share_via" : @"print"};
        [INEventLogger logEvent:@"MedCalc_Calculator_Share" withParams:params];
        [MEDICAL_APP_DELEGATE printImage:scrollView];
    }
    else if (buttonIndex == 1)
    {
        NSDictionary *params = @{@"calculator_name":self.title, @"share_via" : @"email"};
        [INEventLogger logEvent:@"MedCalc_Calculator_Share" withParams:params];
        [MEDICAL_APP_DELEGATE mailImage:scrollView];
    }
	scrollView.frame=oldFrame;
    [self scrollTobottom];
}

- (void) doneBarBtnClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) roundViewWithBorder
{
    for (int i=100; i<106; i++)
    {
        UIView *text=(UIView *)[self.view viewWithTag:i];
        text.layer.cornerRadius = TEXTFIELDVIEW_CORNERRADIUS;
        text.clipsToBounds = YES;
        text.layer.borderColor = TEXTFIELDVIEW_DEFAULT_BORDERCOLOR;
        text.layer.borderWidth = TEXTFIELDVIEW_BORDERWIDTH;
    }
}


- (IBAction)keyboardButtonAction:(id)sender {
    
    UIButton *btn=(UIButton *)sender;
    switch (btn.tag) {
        case 301:
            {
                weightLabel.text    =   @"kg";
                [self validate:weightTextField];
            }
            break;
        case 302:
            {
                weightLabel.text    =   @"lb";
                [self validate:weightTextField];
            }
            break;
        case 303: // cm
        {
            HeightViewFLAG  = 0;
            heightLabel.text      = @"cm";
            [viewHeightInFT setHidden:TRUE];
            [viewHeightInCM setHidden:FALSE];
            [self validate:heightTextField];
            [heightTextField becomeFirstResponder];
        }
            break;
        case 304: // feet/inch
        {
            HeightViewFLAG  = 1;
            [viewHeightInCM setHidden:TRUE];
            [viewHeightInFT setHidden:FALSE];
            [ftTextField becomeFirstResponder];
        }
            break;
        case 305: // inches
        {
            HeightViewFLAG  = 2;
            heightLabel.text      = @"inches";
            [viewHeightInFT setHidden:TRUE];
            [viewHeightInCM setHidden:FALSE];
            [self validate:heightTextField];
            [heightTextField becomeFirstResponder];
        }
            break;
        case 306:
        {
            serumLabel.text     =   MGDLSTRING;
            [self validate:serumTextField];
        }
            break;
        case 307:
        {
            serumLabel.text     =   MMOLSTRING;
            [self validate:serumTextField];
        }
            break;
        case 308:
        {
            serumLabel.text     =   UMOLSTRING;
            [self validate:serumTextField];
        }
            break;
        default:
            break;
    }
    [self compute];
}

- (IBAction)segmentControlChanged:(id)sender {
    [self compute];
}

- (IBAction)editingChanged:(UITextField *)textField {
    [self validate:textField];
    [self compute];
}


-(BOOL)validate:(UITextField *)textField{
    BOOL success = YES;
    double doubleValue = [textField.text doubleValue];
    if([textField isEqual:ageTextField])
    {
        if ((doubleValue >= 0 && doubleValue <= 120)) {
            [self removeErrorMessageFromLabel:lblErrAge];
        }else{
            [self setErrorMessageOnLabel:@"Enter age value in between 0 - 120." label:lblErrAge];
            success = NO;
        }
    }else if([textField isEqual:weightTextField])
    {
        if ([weightLabel.text isEqualToString:@"kg"])
        {
            if ((doubleValue >= 10 && doubleValue <= 200)) {
                [self removeErrorMessageFromLabel:lblErrWeight];
            }else{
                [self setErrorMessageOnLabel:@"Enter weight value in between 10 - 200." label:lblErrWeight];
                success = NO;
            }
        }else if ([weightLabel.text isEqualToString:@"lb"])
        {
            if ((doubleValue >= 22 && doubleValue <= 441)) {
                [self removeErrorMessageFromLabel:lblErrWeight];
            }else{
                [self setErrorMessageOnLabel:@"Enter weight value in between 22 - 441." label:lblErrWeight];
                success = NO;
            }
        }
    }else if([textField isEqual:heightTextField])
    {
        if ([heightLabel.text isEqualToString:@"cm"])
        {
            if ((doubleValue >= 30 && doubleValue <= 241.3)) {
                [self removeErrorMessageFromLabel:lblErrHeight];
            }else{
                [self setErrorMessageOnLabel:@"Enter height value in between 30 - 241." label:lblErrHeight];
                success = NO;
            }
        }else if ([heightLabel.text isEqualToString:@"inches"])
        {
            if ((doubleValue >= 11.8 && doubleValue <= 95)) {
                [self removeErrorMessageFromLabel:lblErrHeight];
            }else{
                [self setErrorMessageOnLabel:@"Enter height value in between 11.8 - 95." label:lblErrHeight];
                success = NO;
            }
        }
    }else if([textField isEqual:ftTextField])
    {
        if ((doubleValue >= 1 && doubleValue <= 7)) {
            [self removeErrorMessageFromLabel:lblErrHeight];
        }else{
            [self setErrorMessageOnLabel:@"Enter feet value in between 1 - 7." label:lblErrHeight];
            success = NO;
        }
    }
    else if([textField isEqual:inchTextField])
    {
        if ((doubleValue >= 0 && doubleValue <= 11)) {
            [self removeErrorMessageFromLabel:lblErrHeight];
        }else{
            [self setErrorMessageOnLabel:@"Enter inch value in between 0 - 11." label:lblErrHeight];
            success = NO;
        }
    }else if([textField isEqual:serumTextField])
    {
        if ([serumLabel.text isEqualToString:MGDLSTRING])
        {
            if ((doubleValue >= 0 && doubleValue <= 10)) {
                [self removeErrorMessageFromLabel:lblErrSerumC];
            }else{
                [self setErrorMessageOnLabel:@"Enter serum creatinine value in between 0 - 10." label:lblErrSerumC];
                success = NO;
            }
        }else if ([serumLabel.text isEqualToString:UMOLSTRING])
        {
            if ((doubleValue >= 5 && doubleValue <= 500)) {
                [self removeErrorMessageFromLabel:lblErrSerumC];
            }else{
                [self setErrorMessageOnLabel:@"Enter serum creatinine value in between 5 - 500." label:lblErrSerumC];
                success = NO;
            }
        }
        else if ([serumLabel.text isEqualToString:MMOLSTRING])
        {
            if ((doubleValue >= 22 && doubleValue <= 441)) {
                [self removeErrorMessageFromLabel:lblErrSerumC];
            }else{
                [self setErrorMessageOnLabel:@"Enter serum creatinine value in between 22 - 441." label:lblErrSerumC];
                success = NO;
            }
        }
    }else if([textField isEqual:targetAUCTextField])
    {
        if ((doubleValue >= 1 && doubleValue <= 10)) {
            [self removeErrorMessageFromLabel:lblErrTargetAUC];
        }else{
            [self setErrorMessageOnLabel:@"Enter target AUC value in between 1 - 10." label:lblErrTargetAUC];
            success = NO;
        }
    }
    return success;
}

-(void)compute{
    
    if(![ageTextField.text isEqualToString:@""] && ![weightTextField.text isEqualToString:@""] &&
       (![heightTextField.text isEqualToString:@""] || (![ftTextField.text isEqualToString:@""] && ![inchTextField.text isEqualToString:@""])) &&
       ![serumTextField.text isEqualToString:@""])
    {
        
        DebugLog(@"computing ....");
        
        
        double ageValue     = [ageTextField.text doubleValue];
        double weightValue  = [weightTextField.text doubleValue];
        double heightValue  = [heightTextField.text doubleValue];
        double serumValue   = [serumTextField.text doubleValue];
        
        int count = 0;
        if ([self validate:ageTextField])
            count++;
        
        
        if ([self validate:weightTextField])
        {
            if ([weightLabel.text isEqualToString:@"lb"])
                weightValue=weightValue * ONE_POUND_IN_KG;
            
            count++;
        }
        
        switch (HeightViewFLAG) {
            case 0: //cm
            {
                if ([self validate:heightTextField])
                {
                    count ++;
                }
            }
                break;
            case 1:
            {
                if ([self validate:ftTextField] && [self validate:inchTextField])
                {
                    heightValue =  [self convertintocm:[ftTextField.text doubleValue] inch:[inchTextField.text doubleValue]];
                    count ++;
                }
            }
                break;
            case 2:
            {
                if ([self validate:heightTextField])
                {
                    heightValue =    heightValue * ONE_INCHES_IN_CM;
                    count ++;
                }
            }
                break;
            default:
                break;
        }
        
        heightValue = heightValue * ONE_CM_IN_INCHES;
        
        if ([self validate:serumTextField])
        {
            if ([serumLabel.text isEqualToString:UMOLSTRING])
                serumValue=serumValue * ONE_UMOL_IN_MGdL;
            else if ([serumLabel.text isEqualToString:MMOLSTRING])
                serumValue=serumValue * ONE_MMOL_IN_MGdL;
            count ++;
        }
        
        double GFR=0;
        DebugLog(@"serum Value %f",serumValue);
        if (count == 4)
        {
            //new formula: GFR = Sex * (140 - age) x wt (kg) / (serum creatinine x 72)
            // old formula: GFR = Sex * ((140 - Age) / (SerumCreat)) * (Weight / 72)
            //CarboplatinDose = TargetAUC * (GFR + 25)
            switch (self.genderSegmentControl.selectedSegmentIndex)
            {
                case 0:
                    GFR = (CARBO_MALE * (140 - ageValue) * weightValue) / (serumValue * 72);
                    break;
                case 1:
                    GFR = (CARBO_FEMALE * (140 - ageValue) * weightValue) / (serumValue * 72);
                    break;
            }
            DebugLog(@"GFR %.2f",GFR);
            [self show:GFR label:gfrResult];
            if (GFR > 125)
            {
                GFR=125;
                GFRCapped125.text=@"(GFR capped at 125)";
            }
        }
        else
        {
            gfrResult.text=@"....";
        }
        
        if (![targetAUCTextField.text isEqualToString:@""] && [self validate:targetAUCTextField] && count == 4)
        {
            [self show:([targetAUCTextField.text doubleValue] * (GFR + 25)) label:labelResult];
        }
        else
        {
            labelResult.text=@"....";
        }
        
    }
    else
    {
        labelResult.text=@"....";
    }
}

//-(BOOL)validate:(double)doublevalue checktype:(int)type viewTag:(int)tag
//{
//    switch (type) {
//        case 0: //age
//            if (doublevalue >=0 && doublevalue<=120)
//            {
//                [self changeTextFieldColor:TRUE viewTag:tag];
//                return TRUE;
//            }
//            else
//            {
//                [self changeTextFieldColor:FALSE viewTag:tag];
//                return FALSE;
//            }
//            break;
//        case 1: // kg or lb
//            if ((doublevalue >=10 && doublevalue<=200) && [weightLabel.text isEqualToString:@"kg"])
//            {
//                [self changeTextFieldColor:TRUE viewTag:tag];
//                return TRUE;
//            }
//            else if ((doublevalue >=22 && doublevalue<=441) && [weightLabel.text isEqualToString:@"lb"])
//            {
//                [self changeTextFieldColor:TRUE viewTag:tag];
//                return TRUE;
//            }
//            else
//            {
//                [self changeTextFieldColor:FALSE viewTag:tag];
//                return FALSE;
//            }
//            break;
//        case 2: //cm
//            if (doublevalue >=33 && doublevalue<=241)
//            {
//                [self changeTextFieldColor:TRUE viewTag:tag];
//                return TRUE;
//                
//            }
//            else
//            {
//                [self changeTextFieldColor:FALSE viewTag:tag];
//                return FALSE;
//            }
//            break;
//        case 3: //ft
//            if (doublevalue >= 1 && doublevalue<=7)
//            {
//                [self changeTextFieldColor:TRUE viewTag:tag];
//                return TRUE;
//                
//            }
//            else
//            {
//                [self changeTextFieldColor:FALSE viewTag:tag];
//                return FALSE;
//            }
//            break;
//        case 4: // inch
//            if (doublevalue >= 0 && doublevalue<=11)
//            {
//                [self changeTextFieldColor:TRUE viewTag:tag];
//                return TRUE;
//                
//            }
//            else
//            {
//                [self changeTextFieldColor:FALSE viewTag:tag];
//                return FALSE;
//            }
//            break;
//        case 5: // serum
//            if ((doublevalue >=0 && doublevalue<=10) && [serumLabel.text isEqualToString:MGDLSTRING])
//            {
//                [self changeTextFieldColor:TRUE viewTag:tag];
//                return TRUE;
//            }
//            else if ((doublevalue >=5 && doublevalue<=500) && [serumLabel.text isEqualToString:UMOLSTRING])
//            {
//                [self changeTextFieldColor:TRUE viewTag:tag];
//                return TRUE;
//            }
//            else if ((doublevalue >=22 && doublevalue<=441) && [serumLabel.text isEqualToString:@"mmol/L"])
//            {
//                [self changeTextFieldColor:TRUE viewTag:tag];
//                return TRUE;
//            }
//            else
//            {
//                [self changeTextFieldColor:FALSE viewTag:tag];
//                return FALSE;
//            }
//            break;
//        case 6: //target AUC
//            if (doublevalue >=1 && doublevalue<=10)
//            {
//                [self changeTextFieldColor:TRUE viewTag:tag];
//                return TRUE;
//            }
//            else
//            {
//                [self changeTextFieldColor:FALSE viewTag:tag];
//                return FALSE;
//            }
//            break;
//        default:
//            break;
//    }
//    return FALSE;
//}
//-(void)changeTextFieldColor:(BOOL)type viewTag:(int)tag
//{
//    UIView *view=(UIView *)[self.view viewWithTag:tag];
//    if (type)
//    {
//        view.backgroundColor = TEXTFIELDVIEW_DEFAULT_BACKGROUNDCOLOR;
//        view.layer.borderColor = TEXTFIELDVIEW_DEFAULT_BORDERCOLOR;
//        
//    }
//    else
//    {
//        view.backgroundColor = TEXTFIELDVIEW_ERROR_BACKGROUNDCOLOR;
//        view.layer.borderColor = TEXTFIELDVIEW_ERROR_BORDERCOLOR;
//        labelResult.text = @"";
//    }
//}
//-(void)compute{
//    
//    if(![ageTextField.text isEqualToString:@""] && ![weightTextField.text isEqualToString:@""] &&
//       (![heightTextField.text isEqualToString:@""] || (![ftTextField.text isEqualToString:@""] && ![inchTextField.text isEqualToString:@""])) &&
//       ![serumTextField.text isEqualToString:@""])
//    {
//        
//        DebugLog(@"computing ....");
//
//        
//        double ageValue = [ageTextField.text doubleValue];
//        double weightValue = [weightTextField.text doubleValue];
//        double heightValue = [heightTextField.text doubleValue];
//        double serumValue = [serumTextField.text doubleValue];        
//        
//        int count = 0;
//        if ([self validate:ageValue checktype:0 viewTag:100])                    
//            count++;
//      
//        
//        if ([self validate:weightValue checktype:1 viewTag:101])
//        {
//            if ([weightLabel.text isEqualToString:@"lb"])
//                weightValue=weightValue * ONE_POUND_IN_KG;
//            
//            count++;
//        }
//        
//        if (HeightViewFLAG &&  [self validate:heightValue checktype:2 viewTag:102]) //cm
//        {
//            //heightValue = [heightTextField.text doubleValue];
//            count ++;
//        }
//        else if((!HeightViewFLAG) && ([self validate:[ftTextField.text doubleValue] checktype:3 viewTag:103] && [self validate:[inchTextField.text doubleValue] checktype:4 viewTag:103])) // ft inch
//        {
//            //heightValue=[self convertintocm:[ftTextField.text doubleValue] inch:[inchTextField.text doubleValue]];
//            count ++;
//        }
//        
//        if ([self validate:serumValue checktype:5 viewTag:104])
//        {
//            if ([serumLabel.text isEqualToString:UMOLSTRING])
//                    serumValue=serumValue * ONE_UMOL_IN_MGdL;
//            else if ([serumLabel.text isEqualToString:@"mmol/L"])
//                serumValue=serumValue * ONE_MMOL_IN_MGdL;
//            
//            count ++;
//        }
//        
//        double GFR=0;
//        DebugLog(@"serum Value %f",serumValue);
//        if (count == 4)
//        {
//            //new formula: GFR = Sex * (140 - age) x wt (kg) / (serum creatinine x 72)        
//            // old formula: GFR = Sex * ((140 - Age) / (SerumCreat)) * (Weight / 72)
//            //CarboplatinDose = TargetAUC * (GFR + 25)
//            switch (self.genderSegmentControl.selectedSegmentIndex)
//                {
//                    case 0:
//                        GFR = (CARBO_MALE * (140 - ageValue) * weightValue) / (serumValue * 72);
//                        break;
//                    case 1:
//                        GFR = (CARBO_FEMALE * (140 - ageValue) * weightValue) / (serumValue * 72);
//                        break;                   
//                }
//            DebugLog(@"GFR %.2f",GFR);
//            [self show:GFR label:gfrResult];
//            if (GFR > 125)
//            {
//                GFR=125;
//                GFRCapped125.text=@"(GFR capped at 125)";
//            }
//                             
//        }
//        else
//        {
//            gfrResult.text=@"....";
//        }
//            
//        if (![targetAUCTextField.text isEqualToString:@""] && [self validate:[targetAUCTextField.text doubleValue] checktype:6 viewTag:105] && count == 4)
//        {
//            [self show:([targetAUCTextField.text doubleValue] * (GFR + 25)) label:labelResult];
//        }
//        else
//        {
//            labelResult.text=@"....";
//        }
//       
//    }
//    else
//    {
//        labelResult.text=@"....";
//    }
//}

-(void)show:(double)result label:(UILabel *)labelName
{
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [formatter setMaximumFractionDigits:2];
    [formatter setRoundingMode: NSNumberFormatterRoundCeiling];
    DebugLog(@"result %f",result);
    if (labelName == gfrResult)
    {
         //labelName.text=[NSString stringWithFormat:@"%@ ml/min",[formatter stringFromNumber:[NSNumber numberWithFloat:result]]];
        labelName.text=[NSString stringWithFormat:@"%.2f ml/min",result];
    }
    else
    {
      //  labelName.text=[NSString stringWithFormat:@"%@ mg",[formatter stringFromNumber:[NSNumber numberWithFloat:result]]];
        labelName.text=[NSString stringWithFormat:@"%.2f mg",result];
    }
}
-(double)convertintocm:(double) feet inch:(double)inchvalue {
    
    double temp=(feet*12)+inchvalue;
    return (temp*2.54);
}

#pragma UITextField Delegate methods
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if([textField isEqual:ageTextField])
    {
        if (UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation)) {
            DebugLog(@"Potrait");
            [self scrollTobottom];
        }else{
            DebugLog(@"Landscape");
            [scrollView setContentOffset:CGPointMake(0,viewAge.frame.origin.y-5) animated:YES];
        }
        [CommonCallback changeTextFieldColor:TEXT_EDITING view:viewAge];
    }
    else if([textField isEqual:weightTextField])
    {
        if (UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation)) {
            DebugLog(@"Potrait");
            [scrollView setContentOffset:CGPointMake(0,viewWeight.frame.origin.y-100) animated:YES];
        }else{
            DebugLog(@"Landscape");
            [scrollView setContentOffset:CGPointMake(0,viewWeight.frame.origin.y-5) animated:YES];
        }
        [CommonCallback changeTextFieldColor:TEXT_EDITING view:viewWeight];
    }
    else if([textField isEqual:heightTextField])
    {
        if (UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation)) {
            DebugLog(@"Potrait");
            [scrollView setContentOffset:CGPointMake(0,viewHeightInCM.frame.origin.y-100) animated:YES];
        }else{
            DebugLog(@"Landscape");
            [scrollView setContentOffset:CGPointMake(0,viewHeightInCM.frame.origin.y-5) animated:YES];
        }
        [CommonCallback changeTextFieldColor:TEXT_EDITING view:viewHeightInCM];
    }
    else if([textField isEqual:ftTextField] || [textField isEqual:inchTextField])
    {
        if (UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation)) {
            DebugLog(@"Potrait");
            [scrollView setContentOffset:CGPointMake(0,viewHeightInFT.frame.origin.y-100) animated:YES];
        }else{
            DebugLog(@"Landscape");
            [scrollView setContentOffset:CGPointMake(0,viewHeightInFT.frame.origin.y-5) animated:YES];
        }
        [CommonCallback changeTextFieldColor:TEXT_EDITING view:viewHeightInFT];
    }
    else if([textField isEqual:serumTextField])
    {
        if (UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation)) {
            DebugLog(@"Potrait");
            [scrollView setContentOffset:CGPointMake(0,viewSerumCreatinine.frame.origin.y-100) animated:YES];
        }else{
            DebugLog(@"Landscape");
            [scrollView setContentOffset:CGPointMake(0,viewSerumCreatinine.frame.origin.y-5) animated:YES];
        }
        [CommonCallback changeTextFieldColor:TEXT_EDITING view:viewSerumCreatinine];
    }
    else if([textField isEqual:targetAUCTextField])
    {
        if (UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation)) {
            DebugLog(@"Potrait");
            [scrollView setContentOffset:CGPointMake(0,viewTargetAUC.frame.origin.y-100) animated:YES];
        }else{
            DebugLog(@"Landscape");
            [scrollView setContentOffset:CGPointMake(0,viewTargetAUC.frame.origin.y-5) animated:YES];
        }
        [CommonCallback changeTextFieldColor:TEXT_EDITING view:viewTargetAUC];
    }
    scrollView.contentSize = CGSizeMake(scrollView.frame.size.width, CGRectGetMaxY(lblCarboplatinDose.frame)+300);
    return YES;
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if([textField isEqual:weightTextField])
    {
        [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:viewWeight];
    }
    else if([textField isEqual:heightTextField])
    {
        [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:viewHeightInCM];
    }
    else if([textField isEqual:ftTextField] || [textField isEqual:inchTextField])
    {
        [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:viewHeightInFT];
    }
    else if([textField isEqual:serumTextField])
    {
        [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:viewSerumCreatinine];
    }
    else if([textField isEqual:targetAUCTextField])
    {
        [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:viewTargetAUC];
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    // allow backspace
    if (range.length > 0 && [string length] == 0) {
        return YES;
    }
    // do not allow . at the beggining
    //    if (range.location == 0 && [string isEqualToString:@"."]) {
    //        return NO;
    //    }
    
    NSRange temprange = [textField.text rangeOfString:@"."];
    if ((temprange.location != NSNotFound) && [string isEqualToString:@"."])
    {
        return NO;
    }
    else if (textField == ftTextField && (range.location == 0 && [string isEqualToString:@"0"]))
    {
        return NO;
    }
    else if ((textField == ftTextField ||  textField == inchTextField) && ([string isEqualToString:@"."]))
    {
        return NO;
    }
    else
    {
        return YES;
    }
}

-(void)scrollTobottom
{
    [UIView animateWithDuration:0.2 animations:^{
        scrollView.contentSize = CGSizeMake(scrollView.frame.size.width, CGRectGetMaxY(lblCarboplatinDose.frame)+30);
        CGPoint bottomOffset = CGPointMake(0, 0);
        [scrollView setContentOffset:bottomOffset animated:YES];
    }];
}


- (IBAction)infoButtonAction:(id)sender {
    NSDictionary *params = @{@"calculator_name":self.title};
    [INEventLogger logEvent:@"MedCalc_Calculator_Info_Image" withParams:params];
    
    MedicalInfoImageViewController *typedetailController = [[MedicalInfoImageViewController alloc] initWithimageName:@"carboplatin_info.png"];
    [self.navigationController pushViewController:typedetailController animated:YES];
}


-(void)setErrorMessageOnLabel:(NSString *)errorMessage label:(UILabel *)lbl{
    labelResult.text=@"....";
    lbl.hidden = NO;
    lbl.text = errorMessage;
    if ([lbl isEqual:lblErrWeight]) {
        [CommonCallback changeTextFieldColor:TEXT_ERROR view:viewWeight];
    }else if ([lbl isEqual:lblErrHeight]) {
        if (viewHeightInCM.hidden) {
            [CommonCallback changeTextFieldColor:TEXT_ERROR view:viewHeightInFT];
        }else{
            [CommonCallback changeTextFieldColor:TEXT_ERROR view:viewHeightInCM];
        }
    }else if ([lbl isEqual:lblErrSerumC]) {
        [CommonCallback changeTextFieldColor:TEXT_ERROR view:viewSerumCreatinine];
    }else if ([lbl isEqual:lblErrTargetAUC]) {
        [CommonCallback changeTextFieldColor:TEXT_ERROR view:viewTargetAUC];
    }
}

-(void)removeErrorMessageFromLabel:(UILabel *)lbl{
    lbl.hidden = YES;
    if ([lbl isEqual:lblErrWeight]) {
        [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:viewWeight];
    }else if ([lbl isEqual:lblErrHeight]) {
        if (viewHeightInCM.hidden) {
            [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:viewHeightInFT];
        }else{
            [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:viewHeightInCM];
        }
    }else if ([lbl isEqual:lblErrSerumC]) {
        [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:viewSerumCreatinine];
    }else if ([lbl isEqual:lblErrTargetAUC]) {
        [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:viewTargetAUC];
    }
}
@end
