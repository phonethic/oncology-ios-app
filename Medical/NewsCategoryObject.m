//
//  NewsCategoryObject.m
//  Medical
//
//  Created by Kirti Nikam on 09/04/14.
//  Copyright (c) 2014 Kirti Nikam. All rights reserved.
//

#import "NewsCategoryObject.h"

@implementation NewsCategoryObject
@synthesize categoryId;
@synthesize categoryName;
@synthesize categorySlug;
@end
