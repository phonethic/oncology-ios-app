//
//  MedicalRSSFeedsViewController.h
//  Medical
//
//  Created by Kirti Nikam on 25/10/13.
//  Copyright (c) 2013 Kirti Nikam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MedicalRSSFeedsViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITableView *rssFeedTableView;
@property (strong, nonatomic) NSMutableDictionary *rssFeedDict;
@end
