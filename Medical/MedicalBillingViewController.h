//
//  MedicalBillingViewController.h
//  Medical
//
//  Created by Kirti Nikam on 03/11/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>

@interface MedicalBillingViewController : UIViewController<UISearchBarDelegate>{
    BOOL isSearchOn;
    BOOL canSelectRow;
    double keyboardHeight;
}
@property (nonatomic,retain) NSMutableDictionary *sections;
@property (retain,nonatomic) NSMutableArray *searchArray;

@property (strong, nonatomic) IBOutlet UITableView *billingTableView;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@end
