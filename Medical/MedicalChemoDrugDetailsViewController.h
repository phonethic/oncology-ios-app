//
//  MedicalChemoDrugDetailsViewController.h
//  Medical
//
//  Created by Kirti Nikam on 21/11/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import <UIKit/UIKit.h>
//#define Dosing @"Dosing" 
//#define Mixing @"Mixing and Administration"
//#define EmesisRisk @"Emesis Risk"
//#define Extravasation @"Extravasation"
//#define BlackBoxWarn @"Black Box Warning"
//#define DoseAdjust @"Dose Adjustment"
//#define AdverseReaction @"Selected Adverse Reactions"
//#define ClinicalPharm @"Clinical Pharmacology"
//#define WarnPrecautions @"Warnings and Precautions"
//#define ClinicalReferences @"Clinical Considerations and References"


#define Dosing @"Package Insert"
#define BlackBoxWarn @"Boxed Warning"
#define ClinicalReferences @"Clinical Considerations and Drug-Drug Interactions"
#define DoseAdjust @"Dose Adjustment"
#define Extravasation @"Extravasation"
#define ClinicalPharm @"Clinical Pharmacology"


@interface MedicalChemoDrugDetailsViewController : UIViewController

@property (nonatomic, strong) NSMutableArray *chemoDrugsColumnArray;

@property (copy,nonatomic) NSString *selectedDrugID;
@property (copy,nonatomic) NSString *selectedDrugName;
@property (strong, nonatomic) IBOutlet UIButton *btnDosing;
@property (strong, nonatomic) IBOutlet UIButton *btnMixing;
@property (strong, nonatomic) IBOutlet UIButton *btnEmesisRisk;
@property (strong, nonatomic) IBOutlet UIButton *btnExtravasation;
@property (strong, nonatomic) IBOutlet UIButton *btnBlackBoxWarn;
@property (strong, nonatomic) IBOutlet UIButton *btnDoseAdjust;
@property (strong, nonatomic) IBOutlet UIButton *btnAdverseReaction;
@property (strong, nonatomic) IBOutlet UIButton *btnClinicalPharm;
@property (strong, nonatomic) IBOutlet UIButton *btnWarnPrecautions;
@property (strong, nonatomic) IBOutlet UIButton *btnClinicalReferences;

@property (strong, nonatomic) IBOutlet UITableView *chemoDrugsTableView;
@property (strong, nonatomic) IBOutlet UIImageView *imgViewBackground;




@end
