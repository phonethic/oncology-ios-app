//
//  MedicalInfoImageViewController.h
//  Medical
//
//  Created by Kirti Nikam on 09/10/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MedicalInfoImageViewController : UIViewController<UIScrollViewDelegate>
{
    NSString *imageNameString;
}
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIImageView *infoImageView;
- (id)initWithimageName:(NSString *)image;

@end
