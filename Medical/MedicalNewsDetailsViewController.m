//
//  MedicalNewsDetailsViewController.m
//  Medical
//
//  Created by Kirti Nikam on 08/04/14.
//  Copyright (c) 2014 Kirti Nikam. All rights reserved.
//

#import "MedicalNewsDetailsViewController.h"
#import "MedicalAppDelegate.h"
#import "MedicalWebViewController.h"

#import "NewsObject.h"

#define FDA_ALERT_POST_LINK @"http://cancerebook.org/?json=get_category_posts&slug=fda-alert"
#define JOB_ALERT_POST_LINK @"http://cancerebook.org/?json=get_category_posts&slug=job-alert"
#define LOCUM_ALERT_POST_LINK @"http://cancerebook.org/?json=get_category_posts&slug=locum-alert"
#define STUDY_ALERT_POST_LINK @"http://cancerebook.org/?json=get_category_posts&slug=study-alert"
#define MED_ALERT_POST_LINK @"http://cancerebook.org/?json=get_category_posts&slug=med-alert"

#define ALERT_POST_LINK(SLUG_NAME) [NSString stringWithFormat:@"http://cancerebook.org/?json=get_category_posts&slug=%@",SLUG_NAME]
@interface MedicalNewsDetailsViewController ()

@end

@implementation MedicalNewsDetailsViewController
@synthesize postArray;
@synthesize selectedAlertName,selectedSlugName;
@synthesize webData;
@synthesize detailTableView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if (HUD) {
        [HUD removeFromSuperview];
        HUD = nil;
    }
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if ([selectedSlugName length] > 0) {
        NSDictionary *params = @{@"category_name":selectedSlugName};
        [INEventLogger logEvent:@"Alerts_Post_List" withParams:params];
    }
    
    [self addHUD];
    [detailTableView setHidden:YES];
    [self sendPostRequest:ALERT_POST_LINK(selectedSlugName)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewDidUnload
{
    [self setDetailTableView:nil];
    [super viewDidUnload];
}

#pragma mark - Rotation
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)addHUD{
	HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
	[self.navigationController.view addSubview:HUD];
	HUD.delegate    = self;
	HUD.labelText   = @"Loading";
    HUD.labelFont   = DEFAULT_FONT(16.0);
    HUD.color       = DEFAULT_COLOR_WITH_ALPHA(0.9);
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureTapDetacted:)];
    [HUD addGestureRecognizer:gesture];
}

//#pragma mark -
//#pragma mark MBProgressHUDDelegate methods
//- (void)hudWasHidden:(MBProgressHUD *)hud {
//	// Remove HUD from screen when the HUD was hidded
//	[HUD removeFromSuperview];
//	HUD = nil;
//}

#pragma UIGestureRecognizer selector methods
-(void)gestureTapDetacted:(UIGestureRecognizer *)sender{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:@"Do you want to cancel loading?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
    [alert show];
}

#pragma UIAlertView delegate methods
- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if ([title isEqualToString:@"YES"]) {
        if (conn) {
            [conn cancel];
            conn = nil;
        }
        if (![HUD isHidden]) {
            [HUD hide:YES];
        }
    }
}

-(void)sendPostRequest:(NSString *)urlString{
    if (![MEDICAL_APP_DELEGATE networkavailable]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:ALERT_MESSAGE_NO_NETWORK delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return;
    }
    [HUD show:YES];
    
    DebugLog(@"URL : %@",urlString);
   // urlString = @"http://192.168.254.193/pods_shift/?json=get_category_posts&custom_fields=gallery_thumb&slug=news_doc";
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString] cachePolicy:NO timeoutInterval:30.0];

    conn = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
    if (conn)
    {
        webData = [NSMutableData data];
    }
}

#pragma NSURLConnection Delegate
-(void) connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *) response
{
    [webData setLength: 0];
}

-(void) connection:(NSURLConnection *)connection didReceiveData:(NSData *) data
{
    [webData appendData:data];
}

-(void) connection:(NSURLConnection *)connection didFailWithError:(NSError *) error
{
    DebugLog(@"didFailWithError error %@",error);
    [conn cancel];
    conn = nil;
    [HUD hide:YES];
}

-(void) connectionDidFinishLoading:(NSURLConnection *) connection
{
    if (webData)
    {
        NSError *error;
        NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:webData options:NSJSONReadingAllowFragments error:&error];
        DebugLog(@"jsonDict %@",jsonDict);
        if (jsonDict != nil) {
            if (postArray == nil) {
                postArray = [[NSMutableArray alloc] init];
            }
            else{
                [postArray removeAllObjects];
            }
            NSArray *lpostArray = [jsonDict objectForKey:@"posts"];
            for (NSDictionary *postDict in lpostArray) {
                NSString *postId = [postDict objectForKey:@"id"];
                NSString *title = [postDict objectForKey:@"title"];
                NSString *content = [postDict objectForKey:@"content"];
                DebugLog(@"postId %@ title %@ content %@",postId,title,content);
                
                newsObj = [[NewsObject alloc] init];
                newsObj.postId      = postId;
                newsObj.postTitle   = title;
                newsObj.postContent = content;
                [postArray addObject:newsObj];
                newsObj = nil;
            }
            
            if ([postArray count] > 0) {
                [detailTableView setHidden:NO];
            }else{
                [detailTableView setHidden:YES];
                [HUD hide:YES];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:[NSString stringWithFormat:@"Sorry! No posts found for %@.",selectedAlertName] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
            }
            [detailTableView reloadData];
        }else{
            DebugLog(@"Got json nil");
            [detailTableView setHidden:YES];
            [HUD hide:YES];
        }
        webData = nil;
        conn = nil;
    }
    if (![HUD isHidden]) {
        [HUD hide:YES afterDelay:0.2];
    }
}

#pragma mark Table view methods
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NewsObject *tempObj = (NewsObject *)[postArray objectAtIndex:indexPath.row];
    NSString *str       = [tempObj.postTitle capitalizedString];
    CGSize size         = [str sizeWithFont:DEFAULT_FONT(FONT_STATIC_ROW_HEIGHT) constrainedToSize:CGSizeMake(280, 999) lineBreakMode:NSLineBreakByWordWrapping];
    DebugLog(@"%f",size.height);
    return size.height + 40;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [postArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"PostCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.accessoryType  =   UITableViewCellAccessoryDisclosureIndicator;
        
        cell.textLabel.font =   DEFAULT_BOLD_FONT(FONT_STATIC_ROW_HEIGHT);
        cell.textLabel.numberOfLines = 3;
        cell.textLabel.textColor = DEFAULT_COLOR;
        
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = DEFAULT_COLOR_WITH_ALPHA(0.5);
        cell.selectedBackgroundView = bgColorView;
    }
    NewsObject *tempObj = (NewsObject *)[postArray objectAtIndex:indexPath.row];
    cell.textLabel.text= tempObj.postTitle;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NewsObject *tempObj = (NewsObject *)[postArray objectAtIndex:indexPath.row];
    if ([tempObj.postTitle length] > 0) {
        NSDictionary *params = @{@"category_name":selectedSlugName,@"post_title":tempObj.postTitle};
        [INEventLogger logEvent:@"Alerts_Post_Details" withParams:params];
    }
    MedicalWebViewController *webviewController = [[MedicalWebViewController alloc] initWithNibName:@"MedicalWebViewController" bundle:nil];
    webviewController.titleString = tempObj.postTitle;
    webviewController.details     = tempObj.postContent;
    [self.navigationController pushViewController:webviewController animated:YES];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
@end
