//
//  MedicalDiseaseDetailsViewController.m
//  Medical
//
//  Created by Kirti Nikam on 03/11/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import "MedicalDiseaseDetailsViewController.h"
#import "constants.h"

@interface MedicalDiseaseDetailsViewController ()

@end

@implementation MedicalDiseaseDetailsViewController
@synthesize diseaseTextView;
@synthesize diseaseDetailText,diseaseNameText;
@synthesize diseaseName;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self roundViewWithBorder];
    diseaseName.text        =   diseaseNameText;
    diseaseTextView.text    =   diseaseDetailText;
}

- (void)viewDidUnload
{
    [self setDiseaseTextView:nil];
    [self setDiseaseName:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

-(void) roundViewWithBorder
{
    diseaseTextView.layer.cornerRadius  = TEXTFIELDVIEW_CORNERRADIUS;
    diseaseTextView.clipsToBounds       = YES;
    diseaseTextView.layer.borderColor   = TEXTFIELDVIEW_DEFAULT_BORDERCOLOR;
    diseaseTextView.layer.borderWidth   = TEXTFIELDVIEW_BORDERWIDTH;
}
@end
