//
//  Journal.m
//  Medical
//
//  Created by Kirti Nikam on 11/10/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import "Journal.h"

@implementation Journal
@synthesize name,abbreviation,pubmedid,imapactfactor,weblink;

- (id)init {
    self = [super init]; if (self) {
        // Initialization code here.
    }
        return self;
   
}

- (NSComparisonResult)compare:(Journal *)otherObject {
    return [self.name compare:otherObject.name];
}

//-(id)init:(NSString *)namevalue abbreviaiton:(NSString *)abbreviaitonvalue pubmedid:(NSString *)pubmedidvalue imapactfactor:(NSString *)imapactfactorvalue weblink:(NSString *)weblinkvalue{
//    self.name=namevalue;
//    self.abbreviaiton=abbreviaitonvalue;
//    self.pubmedid=pubmedidvalue;
//    self.imapactfactor=imapactfactorvalue;
//    self.weblink=weblinkvalue;
//    return self;
//}
    
    
@end
