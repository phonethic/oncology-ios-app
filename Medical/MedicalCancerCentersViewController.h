//
//  MedicalCancerCentersViewController.h
//  Medical
//
//  Created by Rishi on 05/11/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "NearByMeObject.h"
#import "MBProgressHUD.h"

#define CenterList @"List"
#define NearByMe @"NearByMe"
#define RADIANS(degrees) ((degrees * (float)M_PI) / 180.0f)
#define EARTH_RADIUS_IN_KM 6371
/*
 #define INVALID_REQUEST @"INVALID_REQUEST"
 #define OK @"OK"
 #define INVALID_REQUEST @"INVALID_REQUEST"
 #define MAX_ELEMENTS_EXCEEDED @"MAX_ELEMENTS_EXCEEDED"
 #define OVER_QUERY_LIMIT @"OVER_QUERY_LIMIT"
 #define REQUEST_DENIED @""
 #define UNKNOWN_ERROR @""
 #define NOT_FOUND @"Origin and/or destination of this pairing could not be geocoded."
 #define ZERO_RESULTS @"No route could be found between the origin and destination."
 */

@interface MedicalCancerCentersViewController : UIViewController<UISearchBarDelegate,MKMapViewDelegate,CLLocationManagerDelegate,NSURLConnectionDelegate,NSXMLParserDelegate,MBProgressHUDDelegate> {
    BOOL isSearchOn;
    BOOL canSelectRow;
    CLLocationManager *locationManager;
    
    NSMutableData *webData;
    
	NSURLConnection *conn;
    NSXMLParser *xmlParser;
    BOOL elementFound;

    NearByMeObject *tempNearByMeObj;
    
    MBProgressHUD *HUD;
    
    BOOL locationNotFound;
}
@property (nonatomic, readwrite) NSInteger nearbymerowid;
@property (readwrite,nonatomic) double currentLatitude;
@property (readwrite,nonatomic) double currentLongitude;
@property (nonatomic,retain) NSMutableDictionary *sections;
@property (nonatomic, copy) NSMutableArray *nearCenterListArray;
@property (nonatomic, copy) NSMutableArray *centerNamesArray;
@property (nonatomic, copy) NSString *elementKey;
@property (nonatomic, copy) NSString *elementParent;
@property (nonatomic, copy) NSString *elementStatus;

@property (strong, nonatomic) IBOutlet MKMapView *showcenterMapView;
@property (strong, nonatomic) IBOutlet UIButton *btnCenterList;
@property (strong, nonatomic) IBOutlet UIButton *btnNearBy;

- (IBAction)btnActionMethod:(id)sender;
@end
