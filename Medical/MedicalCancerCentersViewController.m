//
//  MedicalCancerCentersViewController.m
//  Medical
//
//  Created by Rishi on 05/11/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//
#import "MedicalCancerCentersViewController.h"
#import "MedicalAppDelegate.h"
#import "MedicalCancerCentersListViewController.h"
#import "MedicalCancerCenterDetailViewController.h"
#import "DisplayMap.h"
#import "CommonCallback.h"

@interface MedicalCancerCentersViewController ()

@end

@implementation MedicalCancerCentersViewController
@synthesize sections;
@synthesize showcenterMapView;
@synthesize btnCenterList;
@synthesize btnNearBy;
@synthesize currentLatitude,currentLongitude;
@synthesize nearCenterListArray;
@synthesize elementKey,elementStatus,nearbymerowid;
@synthesize elementParent;
@synthesize centerNamesArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if (HUD) {
        [HUD removeFromSuperview];
        HUD = nil;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
   /* isSearchOn = NO;
    canSelectRow = YES;
    [self readCancerCentersFromDatabase];*/
    NSDictionary *params = @{@"view_from":@"map"};
    [INEventLogger logEvent:@"Cancer_Centers_List" withParams:params];
    
    [self addHUD];
    locationNotFound = NO;
    
    centerNamesArray = [[NSMutableArray alloc] init];
    
    btnCenterList.layer.shadowColor = DEFAULT_COLOR.CGColor;
    btnCenterList.layer.shadowOffset = CGSizeMake(3, 3);
    btnCenterList.layer.shadowOpacity = 0.6;
    btnCenterList.layer.shadowRadius = 0.5;
    
    btnNearBy.layer.shadowColor = DEFAULT_COLOR.CGColor;
    btnNearBy.layer.shadowOffset = CGSizeMake(3, 3);
    btnNearBy.layer.shadowOpacity = 0.6;
    btnNearBy.layer.shadowRadius = 0.5;
    
    [btnCenterList setTitle:CenterList forState:UIControlStateNormal];
    [btnNearBy setTitle:NearByMe forState:UIControlStateNormal];
    
    [showcenterMapView setMapType:MKMapTypeStandard];
    [showcenterMapView setZoomEnabled:YES];
    [showcenterMapView setScrollEnabled:YES];
    [showcenterMapView setDelegate:self];
    showcenterMapView.showsUserLocation = YES;
    [self readCancerCentersFromDatabase];
    
    [self getLocationValues];
}

- (void)viewDidUnload
{
    [self setShowcenterMapView:nil];
    [self setBtnCenterList:nil];
    [self setBtnNearBy:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
#pragma mark - Rotation
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)addHUD{
	HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
	[self.navigationController.view addSubview:HUD];
	HUD.delegate    = self;
	HUD.labelText   = @"Loading";
    HUD.labelFont   = DEFAULT_FONT(16.0);
    HUD.color       = DEFAULT_COLOR_WITH_ALPHA(0.9);
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureTapDetacted:)];
    [HUD addGestureRecognizer:gesture];
}

//#pragma mark -
//#pragma mark MBProgressHUDDelegate methods
//- (void)hudWasHidden:(MBProgressHUD *)hud {
//	// Remove HUD from screen when the HUD was hidded
//	[HUD removeFromSuperview];
//	HUD = nil;
//}

#pragma UIGestureRecognizer selector methods
-(void)gestureTapDetacted:(UIGestureRecognizer *)sender{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:@"Do you want to cancel loading?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
    [alert show];
}

#pragma UIAlertView delegate methods
- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if ([title isEqualToString:@"YES"]) {
        if (conn) {
            [conn cancel];
            conn = nil;
        }
        if (![HUD isHidden]) {
            [HUD hide:YES];
        }
    }
}


-(void) readCancerCentersFromDatabase {
	// Setup the database object
	sqlite3 *database;
    //university text,director_name text,address
    //text,telphoneno text,faxno text,lattitude text,longitude text
	// Open the database from the users filessytem
	if(sqlite3_open([[MEDICAL_APP_DELEGATE databasePath] UTF8String], &database) == SQLITE_OK) {
		// Setup the SQL Statement and compile it for faster access
        NSString *centerquerySQL = @"select center_name,lattitude,longitude from cancer_centers";
        const char *sqlStatement = [centerquerySQL UTF8String];
		sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
			// Loop through the results and add them to the feeds array
			while(sqlite3_step(compiledStatement) == SQLITE_ROW)
            {
				// Read the data from the result row
                NSString *centerName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 0)];
                double latValue = sqlite3_column_double(compiledStatement, 1);
                double longValue = sqlite3_column_double(compiledStatement,2);
                // DebugLog(@"-%@-%f-%f-",centerName,latValue,longValue);
                if(latValue != 0)
                {
                    MKCoordinateRegion region = { {0.0, 0.0 }, { 0.0, 0.0 } };
                    
                    region.center.latitude = latValue;
                    region.center.longitude = longValue;
                    region.span.latitudeDelta = 80.0f;
                    region.span.longitudeDelta = 80.0f;
                    [showcenterMapView setRegion:region animated:YES];
                    
                    DisplayMap *ann = [[DisplayMap alloc] init];
                    ann.title = centerName;
                    ann.subtitle = @"";
                    ann.coordinate = region.center;
                    [showcenterMapView addAnnotation:ann];
                }
            }
        }
		// Release the compiled statement from memory
		sqlite3_finalize(compiledStatement);
	}
	sqlite3_close(database);
    
}

-(void)getLocationValues{
    if ([CLLocationManager locationServicesEnabled])
    {
        DebugLog(@"Location services are enabled.");
        HUD.labelText = @"Location Search";
        [HUD show:YES];
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        locationManager.distanceFilter  = kCLDistanceFilterNone;
        [self performSelector:@selector(stopUpdatingCoreLocation:) withObject:@"Timed Out" afterDelay:15];
        [locationManager startUpdatingLocation];
    }
    else
    {
        DebugLog(@"Location services not enabled.");
        UIAlertView *servicesDisabledAlert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:@"Location serviecs are disabled on your device, 'Cancer eBook' needs your location to bring you information from nearby, please enable location services." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [servicesDisabledAlert show];
    }
}

#pragma CLLocationManager Methods
- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations
{
    
}

- (void) locationManager:(CLLocationManager *) manager didFailWithError:(NSError *) error
{
    [locationManager stopUpdatingLocation];
    [self stopUpdatingCoreLocation:nil];
}

- (void)stopUpdatingCoreLocation:(NSString *)state
{
	[locationManager stopUpdatingLocation];
    [self saveCurrentocation:[locationManager location]];
}
-(void)saveCurrentocation:(CLLocation *)lLocation
{
     [HUD hide:YES];
    if (lLocation) {
        currentLatitude  = lLocation.coordinate.latitude;
        currentLongitude = lLocation.coordinate.longitude;
    }else{
        if (locationNotFound) {
            return;
        }
        locationNotFound = YES;
        // If it's not possible to get a location, then return.
        UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                             message:@"Sorry we could not get your current location accurately at this point."
                                                            delegate:nil
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil];
        [errorAlert show];
    }
}

#pragma MapView Methods
-(MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:(id <MKAnnotation>)annotation {
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    MKPinAnnotationView *pinView = nil;
    if(annotation != showcenterMapView.userLocation)
    {
        static NSString *defaultPinID = @"MedicalCenterPin";
        pinView = (MKPinAnnotationView *)[showcenterMapView dequeueReusableAnnotationViewWithIdentifier:defaultPinID];
        if ( pinView == nil )
        {
            pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:defaultPinID];
            DisplayMap *Obj = (DisplayMap *)annotation;
            DebugLog(@"name %@",Obj.title);
            if ([Obj.title hasPrefix:@"James Cancer Hospital and Solove Research Institute"]) {
                pinView.pinColor = MKPinAnnotationColorPurple;
                pinView.image = [UIImage imageNamed:@"PinPurple.png"];
            }else{
                //pinView.pinColor = MKPinAnnotationColorRed;
                pinView.image = [UIImage imageNamed:@"PinBlue.png"];
            }
            pinView.canShowCallout = YES;
           // pinView.animatesDrop = YES;
            
            
            UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
      //      UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
       //     [rightButton setImage:[UIImage imageNamed:@"Info.ong"] forState:UIControlStateNormal];
            pinView.rightCalloutAccessoryView = rightButton;
        }
        else
        {
            pinView.annotation = annotation;
        }               
    }
    else
    {
        [showcenterMapView.userLocation setTitle:@"I am here"];
    }
    return pinView;        
}

- (void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)views {
    CGRect visibleRect = [mapView annotationVisibleRect];
    for (MKAnnotationView *annView in views) {
        
        // Don't pin drop if annotation is user location
        if ([annView.annotation isKindOfClass:[MKUserLocation class]]) {
            continue;
        }
        
        // Check if current annotation is inside visible map rect, else go to next one
//        MKMapPoint point =  MKMapPointForCoordinate(annView.annotation.coordinate);
//        if (!MKMapRectContainsPoint(mapView.visibleMapRect, point)) {
//            continue;
//        }
        
        CGRect endFrame = annView.frame;
        CGRect startFrame = endFrame;
        startFrame.origin.y = visibleRect.origin.y - startFrame.size.height;
        annView.frame = startFrame;
        
        // Move annotation out of view
        //annView.frame = CGRectMake(annView.frame.origin.x, annView.frame.origin.y - self.view.frame.size.height, annView.frame.size.width, annView.frame.size.height);
        
        [UIView beginAnimations:@"drop" context:NULL];
        [UIView setAnimationDuration:1];
        annView.frame = endFrame;
        [UIView commitAnimations];
    }
}
- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    MKPinAnnotationView *pinView = (MKPinAnnotationView *)[showcenterMapView viewForAnnotation:view.annotation];
    UIImage *pinImage = pinView.image;
//    if(pinView.pinColor == MKPinAnnotationColorRed || pinView.pinColor == MKPinAnnotationColorPurple)
    if([pinImage isEqual:[UIImage imageNamed:@"PinBlue.png"]] || [pinImage isEqual:[UIImage imageNamed:@"PinPurple.png"]])
    {
        NSDictionary *params = @{@"center_name": view.annotation.title,@"clicked_from":@"map"};
        [INEventLogger logEvent:@"Cancer_Centers_Details_Click" withParams:params];
        
        MedicalCancerCenterDetailViewController *centerdetailviewController = [[MedicalCancerCenterDetailViewController alloc] initWithNibName:@"MedicalCancerCenterDetailViewController" bundle:nil] ;
        centerdetailviewController.centerName = view.annotation.title;
        [self.navigationController pushViewController:centerdetailviewController animated:YES];
    }
    else if ([pinImage isEqual:[UIImage imageNamed:@"PinGreen.png"]])//(pinView.pinColor == MKPinAnnotationColorGreen)
    {
        sqlite3 *database = nil;
        if(sqlite3_open([[MEDICAL_APP_DELEGATE databasePath] UTF8String], &database) == SQLITE_OK)
        {            
            NSString *querySQL = [NSString stringWithFormat:@"select center_name,lattitude,longitude from cancer_centers where center_name like \"%@%%\"", view.annotation.title];
            const char *sqlStatement = [querySQL UTF8String];
            sqlite3_stmt *compiledStatement;
            if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
            {
                while(sqlite3_step(compiledStatement) == SQLITE_ROW)
                {
                    NSString *centerName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 0)];
                    double compareTolatValue = sqlite3_column_double(compiledStatement, 1);
                    double compareTolongValue = sqlite3_column_double(compiledStatement,2);
                    DebugLog(@"centerName: %@  --> %f,%f",centerName,compareTolatValue,compareTolongValue);
                    if (compareTolatValue != 0)
                    {
                        UIApplication *app = [UIApplication sharedApplication];                       
                        //NSString *urlString = [NSString stringWithFormat:@"http://maps.google.com/maps?saddr=Current+Location&daddr=%lf,%lf",compareTolatValue,compareTolongValue];
                        NSString *urlString = [NSString stringWithFormat:@"http://maps.google.com/maps?saddr=%lf,%lf&daddr=%lf,%lf",currentLatitude,currentLongitude,compareTolatValue,compareTolongValue];
                       // DebugLog(@"url %@",urlString);
                        [app openURL:[NSURL URLWithString:urlString]];
                    }
                    
                }
            }
            sqlite3_finalize(compiledStatement);
        }
        sqlite3_close(database);
    }
}

#pragma UIButton action method
- (IBAction)btnActionMethod:(UIButton *)sender {
    if ([sender.titleLabel.text isEqualToString:CenterList])
    {
        MedicalCancerCentersListViewController *centerListviewController = [[MedicalCancerCentersListViewController alloc] initWithNibName:@"MedicalCancerCentersListViewController" bundle:nil] ;
        centerListviewController.title = @"Cancer Centers List";
        [self.navigationController pushViewController:centerListviewController animated:YES];
    }
    else if ([sender.titleLabel.text isEqualToString:NearByMe])
    {
        [INEventLogger logEvent:@"Cancer_Centers_Near_By_Me"];

        if (![MEDICAL_APP_DELEGATE networkavailable]) {
            [CommonCallback showOfflineAlert];
            return;
        }
        DebugLog(@"currentLatitude %f %f",currentLatitude,currentLongitude);
        
        if (currentLatitude == 0 || currentLongitude == 0) {
            DebugLog(@"Found currentLatitude == currentLongitude == 0");
            [self getLocationValues];
        }
        HUD.labelText = @"Processing";
        [HUD show:YES];
        [centerNamesArray removeAllObjects];
        // Current Center     
        //University of Colorado Cancer Center| 39.74502870000001|-104.83757259999999
        // Huntsman Cancer Institute  rowId 20  --> 40.772565,-111.834343
        //Fred Hutchinson/ University of Washington Cancer Consortium rowId 15  --> 47.628590,-122.330798
        //OHSU Knight Cancer Institute  rowId 40  --> 45.497595,-122.685976
        /*currentLatitude = 39.74502870000001;
        currentLongitude = -104.83757259999999;
        for (int index = 0; index < [showcenterMapView annotations].count; index++)
        {
            DisplayMap *ann = (DisplayMap *)[[showcenterMapView annotations] objectAtIndex:index];
            if ([ann.title isEqualToString:@"University of Colorado Cancer Center "])
            {                
                MKPinAnnotationView *av = (MKPinAnnotationView *)[showcenterMapView viewForAnnotation:ann];
                av.pinColor = MKPinAnnotationColorPurple;
                break;
            }
        }*/
//        currentLatitude  =  39.74502870000001;
//        currentLongitude = -104.83757259999999;
        
        NSMutableString *destinationString= [[NSMutableString alloc] initWithCapacity:0];
        sqlite3 *database = nil;
        if(sqlite3_open([[MEDICAL_APP_DELEGATE databasePath] UTF8String], &database) == SQLITE_OK)
        {
            NSString *querySQL = @"select center_name,lattitude,longitude from cancer_centers";
            const char *sqlStatement = [querySQL UTF8String];
            sqlite3_stmt *compiledStatement;
            if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
            {
                while(sqlite3_step(compiledStatement) == SQLITE_ROW)
                {
                    NSString *centerName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 0)];
                    double compareTolatValue = sqlite3_column_double(compiledStatement, 1);
                    double compareTolongValue = sqlite3_column_double(compiledStatement,2);
                    if (compareTolatValue != 0)
                    {
                        [destinationString appendString:[NSString stringWithFormat:@"%f,%f%%7C",compareTolatValue,compareTolongValue]];
                        [centerNamesArray addObject:centerName];
                    }
                }
            }
            sqlite3_finalize(compiledStatement);
        }
        sqlite3_close(database);       
        
        NSString *newDestinationString = [destinationString substringToIndex:[destinationString length]-3];
        NSString *urlString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/distancematrix/xml?origins=%f,%f&destinations=%@&mode=driving&sensor=false",currentLatitude,currentLongitude,newDestinationString];
   
        DebugLog(@"url %@",urlString);
        NSURL *url = [NSURL URLWithString:urlString];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:url cachePolicy:YES timeoutInterval:30.0];
        
        conn = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:YES];
        if (conn)
        {
            webData = [NSMutableData data];
        }
    }
}
-(void) connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *) response
{    
    [webData setLength: 0];
}

-(void) connection:(NSURLConnection *)connection didReceiveData:(NSData *) data
{
    [webData appendData:data];
}

-(void) connection:(NSURLConnection *)connection didFailWithError:(NSError *) error
{
     DebugLog(@"didFailWithError error %@",error);
    [conn cancel];
    [HUD hide:YES];
}

-(void) connectionDidFinishLoading:(NSURLConnection *) connection
{
    if (webData)
    {
        NSString *result = [[NSString alloc] initWithData:webData encoding:NSASCIIStringEncoding];
		DebugLog(@"\n result:%@\n\n", result);   
        
        NSString *xmlDataFromChannelSchemes = [[NSString alloc] initWithString:result];
		NSData *xmlDataInNSData = [xmlDataFromChannelSchemes dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
		xmlParser = [[NSXMLParser alloc] initWithData:xmlDataInNSData];
		[xmlParser setDelegate:self];
		[xmlParser parse];        
        
//        NSSortDescriptor *sortDescriptor =  [[NSSortDescriptor alloc] initWithKey:@"distanceValue" ascending:YES];
//        [nearCenterListArray sortUsingDescriptors:[NSMutableArray arrayWithObject:sortDescriptor]];
//                
        // result of sorting----------
//        for (int index = 0; index < [nearCenterListArray count]; index++)
//        {
//            NearByMeObject *temp1 = (NearByMeObject *)[nearCenterListArray objectAtIndex:index];
//            DebugLog(@"Near Array Rowid %d distance %@ duration %@ ",temp1.nearbymerowid,temp1.distanceText,temp1.durationText);
//        }
        [self reloadMapData];
    }    
}
//xml parsing----------
-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"row"])
    {
        nearbymerowid = 0;
        if(nearCenterListArray == nil)
            nearCenterListArray = [[NSMutableArray alloc] init];
    }
    else if ([elementName isEqualToString:@"element"])
    {
        tempNearByMeObj = [[NearByMeObject alloc] init];   
    }
    else if ([elementName isEqualToString:@"status"])
    {
        elementFound = YES;
        elementKey = elementName;
    }
    else if ([elementName isEqualToString:@"duration"])
    {
        elementKey = elementName;
    }
    else if ([elementName isEqualToString:@"distance"])
    {
        elementKey = elementName;
    }
    else if ([elementName isEqualToString:@"value"])
    {
        elementFound = YES;
        elementParent = elementName;
    }
    else if ([elementName isEqualToString:@"text"])
    {
        elementFound = YES;
        elementParent = elementName;
    }
}

-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string{
    
    if (elementFound && [elementKey isEqualToString:@"status"])
    {
        elementStatus = string;        
    }
    else if (elementFound && [elementKey isEqualToString:@"duration"] && [elementStatus isEqualToString:@"OK"] && [elementParent isEqualToString:@"value"])
    {
        tempNearByMeObj.durationValue = [string doubleValue];
    }
    else if (elementFound && [elementKey isEqualToString:@"duration"] && [elementStatus isEqualToString:@"OK"] && [elementParent isEqualToString:@"text"])
    {
        tempNearByMeObj.durationText = string;
    }
    else if (elementFound && [elementKey isEqualToString:@"distance"] && [elementStatus isEqualToString:@"OK"] && [elementParent isEqualToString:@"value"])
    {
        tempNearByMeObj.distanceValue = [string doubleValue];
    }
    else if (elementFound && [elementKey isEqualToString:@"distance"] && [elementStatus isEqualToString:@"OK"] && [elementParent isEqualToString:@"text"])
    {
        tempNearByMeObj.distanceText = string;
    }
    elementFound = FALSE;   
}

-(void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if ([elementName isEqualToString:@"DistanceMatrixResponse"])
    {
        [centerNamesArray removeAllObjects];
    }
    else if ([elementName isEqualToString:@"element"] && tempNearByMeObj != nil)
    {
        tempNearByMeObj.nearbymerowid = nearbymerowid;
        tempNearByMeObj.centerName = [centerNamesArray objectAtIndex:nearbymerowid];
        [nearCenterListArray addObject:tempNearByMeObj];
        elementStatus = nil;
        tempNearByMeObj = nil;
        nearbymerowid +=1;
    }
    else if ([elementName isEqualToString:@"duration"])
    {
        elementKey = nil;
    }
    else if ([elementName isEqualToString:@"distance"])
    {
        elementKey = nil;
    }
}

-(void)parser:(NSXMLParser *)parser validationErrorOccurred:(NSError *)validationError{
}

-(void)parserDidEndDocument:(NSXMLParser *)parser{
}

-(void)reloadMapData
{
    DebugLog(@"Names Array %@",centerNamesArray);
    DebugLog(@"nearCenterListArray Array %@",nearCenterListArray);
    
    // 1 mm is equivalent to 0.621371 miles.
    // 1 meter == 0.000621371 miles
    // distance should be less than 200 miles. i.e. 321869 meters
    // distance should be less than 250 miles. i.e. 402336 meters
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"distanceValue <= 402336"];
    NSArray *filteredArray = [nearCenterListArray filteredArrayUsingPredicate:predicate];
    DebugLog(@"%@", filteredArray);
    
    for (int fitlerIndex = 0; fitlerIndex < [filteredArray count]; fitlerIndex++) {
        NearByMeObject *nearestObject = (NearByMeObject *)[filteredArray objectAtIndex:fitlerIndex];
        DebugLog(@"nearestObject : %d nearest center as  %@ : (%@,%@)",fitlerIndex,nearestObject.centerName,nearestObject.distanceText,nearestObject.durationText);
        if (nearestObject.distanceText != nil)
        {
            for (int annIndex = 0; annIndex < [showcenterMapView annotations].count; annIndex++)
            {
                DisplayMap *ann = (DisplayMap *)[[showcenterMapView annotations] objectAtIndex:annIndex];
             //   DebugLog(@"annotation : %d nearest center as  %@",annIndex,ann.title);
                if ([ann.title isEqualToString:nearestObject.centerName])
                {
             //       DebugLog(@"Got nearest center as %@",ann.title);
             //       ann.subtitle = [NSString stringWithFormat:@"Distance : %@ \nDuration : %@",nearestObject.distanceText,nearestObject.durationText];
                    
                    double distanceInMiles = nearestObject.distanceValue * 0.000621371;
                    ann.subtitle = [NSString stringWithFormat:@"Distance : %.2f miles \nDuration : %@",distanceInMiles,nearestObject.durationText];
                    MKPinAnnotationView *av = (MKPinAnnotationView *)[showcenterMapView viewForAnnotation:ann];
                    // av.pinColor = MKPinAnnotationColorGreen;
                    av.image = [UIImage imageNamed:@"PinGreen.png"];
                    break;
                }
            }
        }
    }
    
    //    NearByMeObject *nearestObject = (NearByMeObject *)[nearCenterListArray objectAtIndex:0];
    //    DebugLog(@"nearest rowid %d ---> distanceValue %f ",nearestObject.nearbymerowid ,nearestObject.distanceValue);
    //    if (nearestObject.distanceText != nil)
    //    {
    //        for (int index = 0; index < [showcenterMapView annotations].count; index++)
    //        {
    //            DisplayMap *ann = (DisplayMap *)[[showcenterMapView annotations] objectAtIndex:index];
    //            if ([ann.title isEqualToString:[centerNamesArray objectAtIndex:nearestObject.nearbymerowid]])
    //            {
    //                DebugLog(@"At rowid : %d in array Got nearest center as  %@",nearestObject.nearbymerowid,ann.title);
    //                ann.subtitle = [NSString stringWithFormat:@"Distance : %@ \nDuration : %@",nearestObject.distanceText,nearestObject.durationText];
    //                MKPinAnnotationView *av = (MKPinAnnotationView *)[showcenterMapView viewForAnnotation:ann];
    //               // av.pinColor = MKPinAnnotationColorGreen;
    //                 av.image = [UIImage imageNamed:@"PinGreen.png"];
    //                break;
    //            }
    //        }        
    //    }
    [HUD hide:YES];
}
@end
