//
//  MedicalHgbirondeficitViewController.h
//  Medical
//
//  Created by Kirti Nikam on 16/10/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MedicalHgbirondeficitViewController : UIViewController<UITextFieldDelegate,UIActionSheetDelegate>
{
    int TEXTTAG;
    BOOL keyboardIsShown;
    int HeightViewFLAG;

}

@property (strong, nonatomic) IBOutlet UIScrollView *scrollview;
@property (strong, nonatomic) IBOutlet UILabel *lbGender;
@property (strong, nonatomic) IBOutlet UISegmentedControl *genderSegment;

@property (strong, nonatomic) IBOutlet UIView *viewWeight;
@property (strong, nonatomic) IBOutlet UIView *viewHeightInCM;
@property (strong, nonatomic) IBOutlet UIView *viewHeightInFT;
@property (strong, nonatomic) IBOutlet UIView *viewTargetHemoglobin;
@property (strong, nonatomic) IBOutlet UIView *viewCurrentHemoglobin;

@property (strong, nonatomic) IBOutlet UITextField *weightTextField;
@property (strong, nonatomic) IBOutlet UITextField *heightTextField;
@property (strong, nonatomic) IBOutlet UITextField *ftTextField;
@property (strong, nonatomic) IBOutlet UITextField *inchTextField;
@property (strong, nonatomic) IBOutlet UITextField *targerHGBTextfield;
@property (strong, nonatomic) IBOutlet UITextField *currentHGBTextfield;


@property (strong, nonatomic) IBOutlet UILabel *lblWeight;
//@property (strong, nonatomic) IBOutlet UILabel *heightLabel;
@property (strong, nonatomic) IBOutlet UILabel *lblCM;
@property (strong, nonatomic) IBOutlet UILabel *lblFT;
@property (strong, nonatomic) IBOutlet UILabel *lblInch;
@property (strong, nonatomic) IBOutlet UILabel *lblTargetHGB;
@property (strong, nonatomic) IBOutlet UILabel *lblCurrentHGB;




@property (strong, nonatomic) IBOutlet UILabel *lblDosingWeight;
@property (strong, nonatomic) IBOutlet UILabel *lblTotalDoseIronDextran;



//Make Bold
@property (strong, nonatomic) IBOutlet UILabel *lbwLabel;
@property (strong, nonatomic) IBOutlet UILabel *totalDoseLabel;
@property (strong, nonatomic) IBOutlet UILabel *lblIronDeficit;
@property (strong, nonatomic) IBOutlet UILabel *HGBIrondeficitLabel;


//KeyboardViews
@property (strong, nonatomic) IBOutlet UIView *keyBoardViewWeight;
@property (strong, nonatomic) IBOutlet UIView *keyBoardViewHeight;

@property (strong, nonatomic) IBOutlet UIButton *infoButton;

@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *viewCollection;
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *keyboardViewCollection;
@property (strong, nonatomic) IBOutletCollection(UITextField) NSArray *txtCollection;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *lblCollection;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *lblErrCollection;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *keyBoardBtnCollection;

@property (strong, nonatomic) IBOutlet UILabel *lblErrWeight;
@property (strong, nonatomic) IBOutlet UILabel *lblErrHeight;
@property (strong, nonatomic) IBOutlet UILabel *lblErrTargetHGB;
@property (strong, nonatomic) IBOutlet UILabel *lblErrCurrentHGB;

- (IBAction)segmentControlChanged:(id)sender;
@end
