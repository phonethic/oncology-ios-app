//
//  MedicalChemoDrugsViewController.m
//  Medical
//
//  Created by Kirti Nikam on 21/11/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//
#import <sqlite3.h>
#import "MedicalChemoDrugsViewController.h"
#import "MedicalAppDelegate.h"
#import "MedicalChemoDrugDetailsViewController.h"
#import "ChemoDrugObject.h"
#import "MedicalWebViewController.h"

@interface MedicalChemoDrugsViewController ()

@end

@implementation MedicalChemoDrugsViewController
@synthesize chemoDrugsTableView;
@synthesize chemodrugsNameArray;
@synthesize sections;
@synthesize searchBar;
@synthesize searchArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (isSearchOn) {
        [searchBar becomeFirstResponder];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self addInfoBarButton];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        searchBar.barTintColor = DEFAULT_COLOR;
        [searchBar setTranslucent:NO];
    }else{
       // searchBar.tintColor = DEFAULT_COLOR;
        for (UIView *searchBarSubView in searchBar.subviews) {
            if ([searchBarSubView conformsToProtocol:@protocol(UITextInputTraits)]) {
                @try {
                    [(UITextField *)searchBarSubView setBorderStyle:UITextBorderStyleRoundedRect];
                }
                @catch (NSException *exception) {
                    //  exception
                    DebugLog(@"Got exception : %@",exception);
                }
            }
        }
    }
    searchBar.autocorrectionType    = UITextAutocorrectionTypeNo;
    searchBar.spellCheckingType     = UITextSpellCheckingTypeNo;
    isSearchOn = NO;
    canSelectRow = YES;
    keyboardHeight = 216;
    searchArray = [[NSMutableArray alloc] init];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:)
                                                 name:@"UIKeyboardWillShowNotification"
                                               object:nil];
    [self loadChemodrugsNameArray];
    [INEventLogger logEvent:@"Oncology_Drugs_Drug_List"];
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:WARNING_TITLE
                                                        message:WARNING_MSG_FOR_WEBLINK
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
    [alertView show];
}

- (void)viewDidUnload
{
    [self setChemoDrugsTableView:nil];
    [self setSearchBar:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark - Rotation
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma internal methods
-(void)addInfoBarButton{
    UIButton *infoBtn = [UIButton buttonWithType:UIButtonTypeInfoLight];
    infoBtn.frame = CGRectMake(200, 2, 30, 30);
    [infoBtn addTarget:self action:@selector(infoBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:infoBtn];
}

-(void)infoBtnPressed
{
    DebugLog(@"infoBtnPressed");
    [INEventLogger logEvent:@"Oncology_Drugs_i_Button"];
    [self openWebPage:DISCLAIMER_TITLE url:CHEMOTHERAPY_DISCLAIMER_INFO];
}

-(void)closeWebView
{
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        DebugLog(@"closeWebView");
    }];
}

-(void)openWebPage:(NSString *)title url:(NSString *)urlLink{
    if ([urlLink length] > 0) {
        MedicalWebViewController *webViewController = [[MedicalWebViewController alloc] initWithNibName:@"MedicalWebViewController" bundle:nil];
        webViewController.title = title;
        webViewController.medicalLink = urlLink;
        
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:webViewController];
        UIButton *closeBtn =  [UIButton buttonWithType:UIButtonTypeCustom];
        [closeBtn setFrame:CGRectMake(276, 0, 44, 44)];
        [closeBtn setTitle:@"" forState:UIControlStateNormal];
        [closeBtn setImage:[UIImage imageNamed:@"cancel_white.png"] forState:UIControlStateNormal];
        [closeBtn addTarget:self action:@selector(closeWebView) forControlEvents:UIControlEventTouchUpInside];
        [navController.navigationBar addSubview:closeBtn];
        [navController.navigationBar setTranslucent:NO];
        [self.navigationController presentViewController:navController animated:YES completion:nil];
    }
}

#pragma mark Table view methods
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    if (isSearchOn)
    {
        return nil;
    }
    return [[self.sections allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (isSearchOn) {
        return 1;
    }
    return [[self.sections allKeys] count];
}

-(CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section
{
    return 35;
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView* headerView          = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 35)];
    headerView.backgroundColor  = DEFAULT_COLOR;
    
    UILabel *headerLabel        = [[UILabel alloc] initWithFrame:CGRectZero];
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.frame           = CGRectMake(10,0,290,35);
    if (isSearchOn) {
            headerLabel.text    = @"";
    }else{
        headerLabel.text        = [[[self.sections allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:section];
        headerLabel.textAlignment   = NSTextAlignmentLeft;
        headerLabel.font            = DEFAULT_BOLD_FONT(17);
        headerLabel.textColor       = [UIColor whiteColor];
    }
    [headerView addSubview:headerLabel];
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return STATIC_ROW_HEIGHT;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (isSearchOn) {
        return [searchArray count];
    }
    return [[self.sections valueForKey:[[[self.sections allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:section]] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ChemoDrugCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        
        cell.accessoryType  =   UITableViewCellAccessoryDisclosureIndicator;
        cell.textLabel.font =   DEFAULT_FONT(FONT_STATIC_ROW_HEIGHT);
        
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = DEFAULT_COLOR_WITH_ALPHA(0.5);
        cell.selectedBackgroundView = bgColorView;
    }
    ChemoDrugObject *tempObj;
    if (isSearchOn && [searchArray count] > 0) {
        tempObj = (ChemoDrugObject *)[searchArray objectAtIndex:indexPath.row];
    }else{
        NSString *key = [[[self.sections allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section];
        NSArray *sortedArray = [[self.sections valueForKey:key] sortedArrayUsingDescriptors:[NSArray arrayWithObject:[[NSSortDescriptor alloc] initWithKey:@"drugName" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)]]];
        tempObj = (ChemoDrugObject *)[sortedArray objectAtIndex:indexPath.row];
    }
    cell.textLabel.text= [tempObj.drugName capitalizedString];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ChemoDrugObject *tempObj = nil;
    if (isSearchOn && [searchArray count] > 0) {
        tempObj = (ChemoDrugObject *)[searchArray objectAtIndex:indexPath.row];
    }else{
        NSString *key = [[[self.sections allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section];
        NSArray *sortedArray = [[self.sections valueForKey:key] sortedArrayUsingDescriptors:[NSArray arrayWithObject:[[NSSortDescriptor alloc] initWithKey:@"drugName" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)]]];
        tempObj = (ChemoDrugObject *)[sortedArray objectAtIndex:indexPath.row];
    }
    MedicalChemoDrugDetailsViewController *chemodrugsdetailsViewController = [[MedicalChemoDrugDetailsViewController alloc] initWithNibName:@"MedicalChemoDrugDetailsViewController" bundle:nil] ;
    chemodrugsdetailsViewController.title            = [tempObj.drugName capitalizedString];
    chemodrugsdetailsViewController.selectedDrugID   = tempObj.drugID;
    chemodrugsdetailsViewController.selectedDrugName = tempObj.drugName;
    DebugLog(@"%@ %@",  tempObj.drugID, tempObj.drugName);
    [self.navigationController pushViewController:chemodrugsdetailsViewController animated:YES];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (canSelectRow)
        return indexPath;
    else
        return nil;
}

- (void) keyboardWillShow:(NSNotification *)note {
    if ([self.view window]) //means is visible
    {    //do something
        NSDictionary *userInfo = [note userInfo];
        CGSize kbSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        
        DebugLog(@"userInfo %@", userInfo);
        
        DebugLog(@"Keyboard Height: %f Width: %f", kbSize.height, kbSize.width);
        
        keyboardHeight = kbSize.height;
        if (isSearchOn) {
            [UIView animateWithDuration:0.3 animations:^{
                [chemoDrugsTableView setFrame:CGRectMake(chemoDrugsTableView.frame.origin.x, chemoDrugsTableView.frame.origin.y, chemoDrugsTableView.frame.size.width, self.view.frame.size.height-(searchBar.frame.size.height+keyboardHeight))];
            }];
        }
    }else{
        //return
    }
}

#pragma UISearchBar Delegate methods
-(void)searchBarTextDidBeginEditing:(UISearchBar *)lsearchBar{

    isSearchOn = YES;
    if ([searchBar.text length] > 0) {
        canSelectRow = YES;
        self.chemoDrugsTableView.scrollEnabled = YES;
    }else{
        canSelectRow = NO;
        self.chemoDrugsTableView.scrollEnabled = NO;
    }
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneSearching:)];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if ([searchText length] > 0) {
        canSelectRow = YES;
        self.chemoDrugsTableView.scrollEnabled = YES;
    }else{
        canSelectRow = NO;
        self.chemoDrugsTableView.scrollEnabled = NO;
    }
    [self updateSearchArray];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
      [self updateSearchArray];
}

-(void)doneSearching:(id)sender{
    DebugLog(@"doneSearching");
    
    isSearchOn=NO;
    canSelectRow=YES;
    self.chemoDrugsTableView.scrollEnabled = YES;
    
    self.navigationItem.rightBarButtonItem = nil;
    [searchBar resignFirstResponder];
    searchBar.text=@"";
    [searchArray removeAllObjects];
    [self.chemoDrugsTableView reloadData];
    
    [UIView animateWithDuration:0.3 animations:^{
        [chemoDrugsTableView setFrame:CGRectMake(chemoDrugsTableView.frame.origin.x, chemoDrugsTableView.frame.origin.y, chemoDrugsTableView.frame.size.width, self.view.frame.size.height-searchBar.frame.size.height)];
    }];
    [self addInfoBarButton];
}

-(void)updateSearchArray{
    if (searchArray == nil) {
        searchArray = [[NSMutableArray alloc] init];
    }else{
        [searchArray removeAllObjects];
    }
    sqlite3 *databaseP = nil;
    if(sqlite3_open([[MEDICAL_APP_DELEGATE databasePath] UTF8String], &databaseP) == SQLITE_OK) {
		// Setup the SQL Statement and compile it for faster access
        NSString *regimenquerySQL = [NSString stringWithFormat:@"select rowid,drug_name from chemo_drugs where drug_name like\"%%%@%%\" COLLATE NOCASE", self.searchBar.text];
        const char *sqlStatement = [regimenquerySQL UTF8String];
		sqlite3_stmt *statement;
        if(sqlite3_prepare_v2(databaseP, sqlStatement, -1, &statement, NULL) == SQLITE_OK) {
			// Loop through the results and add them to the feeds array
			while(sqlite3_step(statement) == SQLITE_ROW)
            {
                NSString *drugID = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
                NSString *drugName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)];
//                DebugLog(@"-%@-%@-",drugID,drugName);
                if (drugName != nil && ![drugName isEqualToString:@""]) {
                    tempDrugObj = [[ChemoDrugObject alloc] init];
                    tempDrugObj.drugID   = drugID;
                    tempDrugObj.drugName = drugName;
                    [searchArray addObject:tempDrugObj];
                    tempDrugObj = nil;
                }
            }
        }
		sqlite3_finalize(statement);
	}
	sqlite3_close(databaseP);
//    DebugLog(@"%d",searchArray.count);
    [chemoDrugsTableView reloadData];
}

#pragma internal methods
-(void)loadChemodrugsNameArray{
    //    if (chemodrugsNameArray == nil) {
    //        chemodrugsNameArray = [[NSMutableArray alloc] init];
    //    }else{
    //        [chemodrugsNameArray removeAllObjects];
    //    }
    if (sections == nil) {
        sections = [[NSMutableDictionary alloc] init];
    }else{
        [sections removeAllObjects];
    }
    BOOL found;
    sqlite3 *databaseP = nil;
    if (sqlite3_open([[MEDICAL_APP_DELEGATE databasePath] UTF8String], &databaseP) == SQLITE_OK) {
        NSString *sqlQueryString = @"select rowid,drug_name from chemo_drugs";
        
        const char *sqlQueryChar = [sqlQueryString UTF8String];
        sqlite3_stmt *statement = nil;
        if (sqlite3_prepare(databaseP, sqlQueryChar, -1, &statement, NULL)  == SQLITE_OK) {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                NSString *drugID = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
                NSString *drugName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)];
//                DebugLog(@"-%@-%@-",drugID,drugName);
                if (drugName != nil && ![drugName isEqualToString:@""]) {
                    tempDrugObj = [[ChemoDrugObject alloc] init];
                    tempDrugObj.drugID   = drugID;
                    tempDrugObj.drugName = drugName;
                    // [chemodrugsNameArray addObject:tempDrugObj];
                    
                    
                    NSString *prefixChar = [[drugName substringToIndex:1] uppercaseString];
                    found = NO;
                    
                    for(NSString *str in sections.allKeys){
                        if ([str isEqualToString:prefixChar])
                        {
                            found = YES;
                            break;
                        }
                    }
                    if (!found) {
                        [sections setValue:[[NSMutableArray alloc] init] forKey:prefixChar];
                    }
                    [[self.sections objectForKey:prefixChar] addObject:tempDrugObj];
                    tempDrugObj = nil;
                }
            }
            sqlite3_finalize(statement);
        }
        sqlite3_close(databaseP);
    }
//    DebugLog(@"%@",sections);
    [chemoDrugsTableView reloadData];
}
@end
