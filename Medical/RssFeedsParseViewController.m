//
//  RssFeedsParseViewController.m
//  Medical
//
//  Created by Kirti Nikam on 26/10/13.
//  Copyright (c) 2013 Kirti Nikam. All rights reserved.
//

#import "RssFeedsParseViewController.h"
#import "MedicalWebViewController.h"
#import "MedicalAppDelegate.h"

@interface RssFeedsParseViewController ()

@end

@implementation RssFeedsParseViewController
@synthesize rssUrlLink;
@synthesize feedTableView;
@synthesize feedArray,itemDict,item_title,item_link;
@synthesize spinnerIndicator,loadingView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if (HUD) {
        [HUD removeFromSuperview];
        HUD = nil;
    }
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    if (conn != nil) {
        [conn cancel];
    }
    if (parser != nil) {
        [parser abortParsing];
        parser = nil;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSDictionary *params = @{@"RSS_feed_name":self.title};
    [INEventLogger logEvent:@"Must_Reads_RSS_Feed_List" withParams:params];
    
    DebugLog(@"-%@-",rssUrlLink);
    loadingView.backgroundColor = NAVIGATION_THEMELIGHTCOLOR;
    loadingView.layer.borderColor = NAVIGATION_THEMEDARKCOLOR.CGColor;
    loadingView.layer.cornerRadius = CUSTOMVIEW_CORNERRADIUS;
    loadingView.layer.borderWidth = CUSTOMVIEW_BORDERWIDTH;
//    [loadingView setHidden:NO];
//    [spinnerIndicator startAnimating];

    [self addHUD];
    [self.feedTableView setHidden:TRUE];
    
    if (rssUrlLink != nil && ![rssUrlLink isEqualToString:@""]) {
        [self sendHTTPRequest];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setFeedTableView:nil];
    [self setSpinnerIndicator:nil];
    [self setLoadingView:nil];
    [super viewDidUnload];
}
#pragma mark - Rotation
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)addHUD{
	HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
	[self.navigationController.view addSubview:HUD];
	HUD.delegate    = self;
	HUD.labelText   = @"Loading";
    HUD.labelFont   = DEFAULT_FONT(16.0);
    HUD.color       = DEFAULT_COLOR_WITH_ALPHA(0.9);
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureTapDetacted:)];
    [HUD addGestureRecognizer:gesture];
}

//#pragma mark -
//#pragma mark MBProgressHUDDelegate methods
//- (void)hudWasHidden:(MBProgressHUD *)hud {
//	// Remove HUD from screen when the HUD was hidded
//	[HUD removeFromSuperview];
//	HUD = nil;
//}

#pragma UIGestureRecognizer selector methods
-(void)gestureTapDetacted:(UIGestureRecognizer *)sender{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:@"Do you want to cancel loading?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
    [alert show];
}

#pragma UIAlertView delegate methods
- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if ([title isEqualToString:@"YES"]) {
        if (conn) {
            [conn cancel];
            conn = nil;
        }
        if (![HUD isHidden]) {
            [HUD hide:YES];
        }
    }else if([title isEqualToString:@"OK"]){
        DebugLog(@"buttonIndex = %d",buttonIndex);
        NSIndexPath *indexPath = [feedTableView indexPathForSelectedRow];
        if (indexPath != nil) {
            NSArray *sortedArray = [feedArray sortedArrayUsingDescriptors:[NSArray arrayWithObject:[[NSSortDescriptor alloc] initWithKey:@"title" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)]]];
            NSString *ltitle = [[sortedArray objectAtIndex:indexPath.row] objectForKey:@"title"];
            NSString *link   = [[sortedArray objectAtIndex:indexPath.row] objectForKey:@"link"];
            DebugLog(@"ltitle-%@-",ltitle);
            DebugLog(@"link-%@-",link);
            if(![link isEqualToString:@""])
            {
                NSDictionary *params = @{@"RSS_feed_name":self.title,@"title":ltitle,@"link":link};
                [INEventLogger logEvent:@"Must_Reads_RSS_Feed_Website" withParams:params];
                
                MedicalWebViewController *webviewController = [[MedicalWebViewController alloc] initWithNibName:@"MedicalWebViewController" bundle:nil] ;
                webviewController.medicalLink = link;
                webviewController.title = ltitle;
                [self.navigationController pushViewController:webviewController animated:YES];
            }
            [feedTableView deselectRowAtIndexPath:indexPath animated:YES];
        }
    }
}

-(void)sendHTTPRequest{
    if (![MEDICAL_APP_DELEGATE networkavailable]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:ALERT_MESSAGE_NO_NETWORK delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return;
    }
    [HUD show:YES];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:rssUrlLink] cachePolicy:NO timeoutInterval:30.0];
    conn = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

#pragma NSURLConnection Delegate methods
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{

}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    if(rssData==nil)
	{
		rssData = [[NSMutableData alloc] initWithLength:0];
	}
	[rssData appendData:data];
}
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    DebugLog(@"didFailWithError Error: %@", [error localizedDescription]);
    //[spinnerIndicator stopAnimating];
   // [loadingView setHidden:YES];
    [HUD hide:YES];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    if (rssData) {
//        NSString *rssstring = [[NSString alloc] initWithData:rssData encoding:NSUTF8StringEncoding];
//        DebugLog(@"rssstring : %@", rssstring);
        feedArray = [[NSMutableArray alloc] init];
        parser = [[NSXMLParser alloc] initWithData:rssData];
        parser.delegate = self;
        parser.shouldResolveExternalEntities = NO;
        [parser parse];
        
        rssData = nil;
        conn = nil;
    }else{
        DebugLog(@"rssData == nil");
       // [spinnerIndicator stopAnimating];
       // [loadingView setHidden:YES];
        [HUD hide:YES];
    }
}

#pragma NSXMLParser Delegate methods
-(void)parserDidStartDocument:(NSXMLParser *)parser{
    DebugLog(@"1)parserDidStartDocument");
}

-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
//    DebugLog(@"2)didStartElement");
    elementKey = elementName;
    if ([elementName isEqualToString:@"item"]) {
        itemDict        = [[NSMutableDictionary alloc] init];
        item_title      = [[NSMutableString alloc] init];
        item_link       = [[NSMutableString alloc] init];
        elementFound    = YES;
    }
}

-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string{
//    DebugLog(@"3)foundCharacters -%@-",string);
    if (elementFound) {
        if ([elementKey isEqualToString:@"title"]) {
            [item_title appendString:[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
        } else if ([elementKey isEqualToString:@"link"]) {
            [item_link appendString:[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
        }
    }
}

-(void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName{
//    DebugLog(@"4)didEndElement");
    if ([elementName isEqualToString:@"item"]) {
        [itemDict setObject:item_title forKey:@"title"];
        [itemDict setObject:item_link forKey:@"link"];
        [feedArray addObject:[itemDict copy]];
        itemDict = nil;
        item_title = nil;
        item_link = nil;
        elementFound = NO;
    }
}

-(void)parserDidEndDocument:(NSXMLParser *)parser{
    DebugLog(@"5)parserDidEndDocument");
    [spinnerIndicator stopAnimating];
//    [loadingView setHidden:YES];
//    DebugLog(@"feedArray %@",feedArray);
    [self.feedTableView setHidden:FALSE];
    [self.feedTableView reloadData];
    [HUD hide:YES];
}

-(void)parser:(NSXMLParser *)parser validationErrorOccurred:(NSError *)validationError{
    DebugLog(@"validationErrorOccurred : %@", validationError);
    [HUD hide:YES];
}

#pragma UITableView DataSource and Delegate methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSArray *sortedArray = [feedArray sortedArrayUsingDescriptors:[NSArray arrayWithObject:[[NSSortDescriptor alloc] initWithKey:@"title" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)]]];
    NSString *str = [[sortedArray objectAtIndex:indexPath.row] objectForKey: @"title"];
    CGSize size = [str sizeWithFont:DEFAULT_FONT(FONT_STATIC_ROW_HEIGHT) constrainedToSize:CGSizeMake(280, 999) lineBreakMode:NSLineBreakByWordWrapping];
    DebugLog(@"%f",size.height);
    return size.height + 40;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return feedArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"FeedCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

        cell.textLabel.font = DEFAULT_FONT(FONT_STATIC_ROW_HEIGHT);
        cell.textLabel.numberOfLines = 0;
        
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = DEFAULT_COLOR_WITH_ALPHA(0.5);
        cell.selectedBackgroundView = bgColorView;
    }
    NSArray *sortedArray = [feedArray sortedArrayUsingDescriptors:[NSArray arrayWithObject:[[NSSortDescriptor alloc] initWithKey:@"title" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)]]];
    cell.textLabel.text = [[sortedArray objectAtIndex:indexPath.row] objectForKey: @"title"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:WARNING_TITLE
                                                        message:WARNING_MSG_FOR_WEBLINK
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
	[alertView show];
}
@end
