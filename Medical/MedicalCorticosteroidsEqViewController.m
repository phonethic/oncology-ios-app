//
//  MedicalCorticosteroidsEqViewController.m
//  Medical
//
//  Created by Kirti Nikam on 08/10/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import "MedicalCorticosteroidsEqViewController.h"
#import "MedicalInfoImageViewController.h"
#import "MedicalAppDelegate.h"
#import "CommonCallback.h"

@interface MedicalCorticosteroidsEqViewController ()

@end

@implementation MedicalCorticosteroidsEqViewController
@synthesize convertFromTextField;
@synthesize byPercentageTextField;
@synthesize labelResult;
@synthesize convertFromLabel;
@synthesize convertToLabel;
@synthesize segmentControl;
@synthesize viewConvertFromTextfield,viewConvertFromLabel,viewConvertToLabel,viewPrecentageTextfield;
@synthesize lblEquivalent,lblBy;
@synthesize viewCollection,txtCollection,lblCollection,lblErrCollection;
@synthesize lblErrConverFromTextfield,lblErrPercentageTextfield;
@synthesize scrollView;
@synthesize cortArray;
@synthesize cortTableView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSDictionary *params = @{@"calculator_name":self.title};
    [INEventLogger logEvent:@"MedCalc_Calculator_Details" withParams:params];
    
   // [self roundViewWithBorder];
    cortArray=[[NSArray alloc] initWithObjects:
               @"Betamethasone",
               @"Cortisone",
               @"Dexamethasone",
              // @"Fludrocortisone",
               @"Hydrocortisone",
               @"Methylprednisolone",
               @"Prednisolone",
               @"Prednisone",
               @"Triamcinolone",nil ];
    cortTableView.backgroundColor=TABLEVIEW_BACKGROUNDCOLOR;
    cortTableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]
                                              initWithBarButtonSystemItem:UIBarButtonSystemItemAction
                                              target:self action:@selector(shareBarBtnClicked:)];

    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.backBarButtonItem = backButton;

    [self setUI];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected:)];
    tapGesture.numberOfTapsRequired = 1;
    tapGesture.cancelsTouchesInView = NO;
    [scrollView addGestureRecognizer:tapGesture];
    
    scrollView.contentSize = CGSizeMake(scrollView.frame.size.width,scrollView.frame.size.height);
}

- (void)viewDidUnload
{
    [self setConvertFromTextField:nil];
    [self setConvertFromLabel:nil];
    [self setConvertToLabel:nil];
    [self setByPercentageTextField:nil];
    [self setLabelResult:nil];
    [self setSegmentControl:nil];
    [self setCortTableView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
#pragma mark - Rotation
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma internal methods
-(void)setUI{
    
    lblEquivalent.backgroundColor     = [UIColor clearColor];
    lblEquivalent.textColor           = DEFAULT_COLOR;
    lblEquivalent.font                = DEFAULT_BOLD_FONT(20);
    
    lblBy.backgroundColor     = [UIColor clearColor];
    lblBy.textColor           = DEFAULT_COLOR;
    lblBy.font                = DEFAULT_BOLD_FONT(20);
    
    labelResult.backgroundColor     = [UIColor clearColor];
    labelResult.textColor           = DEFAULT_COLOR;
    labelResult.font                = DEFAULT_BOLD_FONT(20);
    
    convertFromLabel.backgroundColor     = [UIColor clearColor];
    convertFromLabel.textColor           = DEFAULT_COLOR;
    convertFromLabel.font                = DEFAULT_FONT(17);
    
    convertToLabel.backgroundColor     = [UIColor clearColor];
    convertToLabel.textColor           = DEFAULT_COLOR;
    convertToLabel.font                = DEFAULT_FONT(17);
    
    for (UIView *lview in viewCollection) {
        lview.backgroundColor     = [UIColor whiteColor];
        lview.layer.borderColor   = [UIColor lightGrayColor].CGColor;
        lview.layer.borderWidth   = 1.0;
    }
    
    for (UITextField *txt in txtCollection) {
        txt.delegate            = self;
        txt.backgroundColor     = [UIColor clearColor];
        txt.textColor           = [UIColor blackColor];
        txt.font                = DEFAULT_FONT(22);
        txt.keyboardType        = UIKeyboardTypeDecimalPad;
        txt.textAlignment       = NSTextAlignmentCenter;
        txt.layer.borderColor   = [UIColor clearColor].CGColor;
        txt.text 			    = @"";
    }
    
    for (UILabel *lbl in lblCollection) {
        lbl.backgroundColor     = [UIColor clearColor];
        lbl.textColor           = DEFAULT_COLOR;
        lbl.font                = DEFAULT_SEMIBOLD_FONT(20);
    }
    
    for (UILabel *lbl in lblErrCollection) {
        lbl.backgroundColor     = [UIColor clearColor];
        lbl.textColor           = [UIColor redColor];
        lbl.font                = DEFAULT_SEMIBOLD_FONT(12);
        lbl.hidden              = YES;
    }
    
    cortTableView.backgroundColor =   DEFAULT_COLOR;
    cortTableView.separatorStyle  =   UITableViewCellSeparatorStyleNone;
    
    cortTableView.layer.shadowColor = [UIColor blackColor].CGColor;
    cortTableView.layer.shadowOpacity = 0.6;
    cortTableView.layer.shadowOffset = CGSizeMake(3, 3);
    [cortTableView setHidden:TRUE];
}

-(void)tapDetected:(UIGestureRecognizer *)sender{
    [self.view endEditing:YES];
    [cortTableView setHidden:TRUE];
    [self scrollTobottom];
}


- (void) shareBarBtnClicked:(id)sender {
    UIActionSheet *uiActionSheetP= [[UIActionSheet alloc]
                                    initWithTitle: @""
                                    delegate:self
                                    cancelButtonTitle:@"CANCEL"
                                    destructiveButtonTitle:nil
                                    otherButtonTitles:@"Print",@"Email", nil];
	
	uiActionSheetP.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
	[uiActionSheetP showInView:self.navigationController.view];
    
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self.cortTableView setHidden:YES];
    [self.view endEditing:YES];
    CGRect oldFrame = scrollView.frame;
    scrollView.frame=CGRectMake(scrollView.frame.origin.x, scrollView.frame.origin.y, scrollView.contentSize.width, scrollView.contentSize.height);
	if (buttonIndex == 0)
    {
        NSDictionary *params = @{@"calculator_name":self.title, @"share_via" : @"print"};
        [INEventLogger logEvent:@"MedCalc_Calculator_Share" withParams:params];
        [MEDICAL_APP_DELEGATE printImage:scrollView];
    }
    else if (buttonIndex == 1)
    {
        NSDictionary *params = @{@"calculator_name":self.title, @"share_via" : @"email"};
        [INEventLogger logEvent:@"MedCalc_Calculator_Share" withParams:params];
        [MEDICAL_APP_DELEGATE mailImage:scrollView];
    }
	scrollView.frame=oldFrame;
    [self scrollTobottom];
}

-(void) roundViewWithBorder
{
    for (int i=100; i<104; i++)
    {
        UIView *text=(UIView *)[self.view viewWithTag:i];
        text.layer.cornerRadius = TEXTFIELDVIEW_CORNERRADIUS;
        text.clipsToBounds = YES;
        text.layer.borderColor = TEXTFIELDVIEW_DEFAULT_BORDERCOLOR;
        text.layer.borderWidth = TEXTFIELDVIEW_BORDERWIDTH;
    }
    UIView *text=(UIView *)[self.view viewWithTag:104];
    text.layer.cornerRadius = TABLEVIEW_CORNERRADIUS;
    text.clipsToBounds = YES;
    text.layer.borderColor = TABLEVIEW_BORDERCOLOR;
    text.layer.borderWidth = TABLEVIEW_BORDERWIDTH;
}

#pragma mark Table view methods
-(CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
   UIView* customView = [[UIView alloc] initWithFrame:CGRectMake(10,0,300,30)];
   UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.frame = CGRectMake(5,5,290,20);
    headerLabel.textAlignment = NSTextAlignmentCenter;
    headerLabel.font = DEFAULT_BOLD_FONT(20);
     switch (BTNTAG) {
          case 301:
              headerLabel.text = @"Convert from";
              break;
        case 302:
               headerLabel.text = @"Convert to";
              break;
              
          default:           
              break;
      }
    headerLabel.textColor = [UIColor whiteColor];
    [customView addSubview:headerLabel];
    return customView;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 42;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [cortArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UILabel *lblTitle;
    static NSString *CellIdentifier = @"CortiCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        
        lblTitle       = [[UILabel alloc] initWithFrame:CGRectMake(cell.frame.origin.x + 10,2, 220, 40)];
        lblTitle.tag   = 1111;
        lblTitle.font  = DEFAULT_FONT(17);
        lblTitle.textAlignment      = NSTextAlignmentLeft;
        lblTitle.textColor          = [UIColor whiteColor];
        lblTitle.backgroundColor    = [UIColor clearColor];
        [cell.contentView addSubview:lblTitle];
        
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
        cell.selectedBackgroundView = bgColorView;
    }
    cell.selectionStyle=UITableViewCellSelectionStyleGray;
    
    lblTitle = (UILabel *)[cell viewWithTag:1111];
    lblTitle.text = [cortArray objectAtIndex:indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  
    NSString *selected=[cortArray objectAtIndex:indexPath.row];
    switch (BTNTAG) {
        case 301:
            convertFromLabel.text=selected;
            break;
        case 302:
            convertToLabel.text=selected;
            break;    
        default:           
            break;
    }
    [cortTableView deselectRowAtIndexPath:[cortTableView indexPathForSelectedRow] animated:NO];
    [cortTableView setHidden:TRUE];
    [self compute];
}

- (IBAction)convertButtonAction:(UIButton *)btn{
    BTNTAG  = btn.tag;
    [self.view endEditing:YES];
    [cortTableView reloadData];
    [cortTableView setHidden:FALSE];
}

- (IBAction)segmentControlChanged:(id)sender {
    [self compute];
}

- (IBAction)editingChanged:(UITextField *)textField {
    [self validate:textField];
    [self compute];
}

-(BOOL)validate:(UITextField *)textField{
    BOOL success = YES;
    double doubleValue = [textField.text doubleValue];
    if([textField isEqual:convertFromTextField])
    {
        if ((doubleValue >= 0 && doubleValue <= 9999)) {
            [self removeErrorMessageFromLabel:lblErrConverFromTextfield];
        }else{
            [self setErrorMessageOnLabel:@"Enter mg value less than 9999" label:lblErrConverFromTextfield];
            success = NO;
        }
    }else if([textField isEqual:byPercentageTextField])
    {
        if ((doubleValue >= 0 && doubleValue <= 100)) {
            [self removeErrorMessageFromLabel:lblErrPercentageTextfield];
        }else{
            [self setErrorMessageOnLabel:@"Enter percentage value less than 100" label:lblErrPercentageTextfield];
            success = NO;
        }
    }
    return success;
}

-(void)compute{
    if ((![convertFromTextField.text isEqualToString:@""] && ![convertFromLabel.text isEqualToString:@"Convert from"] && ![convertToLabel.text isEqualToString:@"Convert to"] ) && [self validate:convertFromTextField])
    {
        double convertFrom  =   [self getPotency:convertFromLabel.text];
        double convertTo    =   [self getPotency:convertToLabel.text];
        double result       =   [convertFromTextField.text doubleValue] * (convertFrom /convertTo);
        if (![byPercentageTextField.text isEqualToString:@""] && [self validate:byPercentageTextField])
        {
            double bypercentage     =   [byPercentageTextField.text doubleValue];
            switch (self.segmentControl.selectedSegmentIndex) {
                case 0:
                    {
                        [self show:(result+(result*(bypercentage/100)))];
                    }
                    break;
                case 1:
                    {
                        [self show:(result-(result*(bypercentage/100)))];
                    }
                    break;
                default:
                    break;
            }

        }
        else if ([byPercentageTextField.text isEqualToString:@""])
        {
            [self show:result];
        }
        else
        {
            labelResult.text=@"....";
        }

    }
    else
    {
        labelResult.text=@"....";
    }
}

//-(BOOL)validate:(double)doublevalue checktype:(int)type viewTag:(int)tag
//{
//    switch (type) {
//       case 0:
//            if ((doublevalue >=0 && doublevalue<=9999))
//            {
//                [self changeTextFieldColor:TRUE viewTag:tag];
//                return TRUE;
//            }
//             else
//            {
//                [self changeTextFieldColor:FALSE viewTag:tag];
//                return FALSE;
//            }
//            break;
//        case 1:
//            if (doublevalue >=0 && doublevalue<=100)
//            {
//                [self changeTextFieldColor:TRUE viewTag:tag];
//                return TRUE;
//                
//            }
//            else
//            {
//                [self changeTextFieldColor:FALSE viewTag:tag];
//                return FALSE;
//            }
//            break;
//        default:
//            break;
//    }
//    return FALSE;
//}
//-(void)changeTextFieldColor:(BOOL)type viewTag:(int)tag
//{
//    UIView *view=(UIView *)[self.view viewWithTag:tag];
//    if (type)
//    {
//        view.backgroundColor=TEXTFIELDVIEW_DEFAULT_BACKGROUNDCOLOR;
//        view.layer.borderColor=TEXTFIELDVIEW_DEFAULT_BORDERCOLOR;
//        
//    }
//    else
//    {
//        view.backgroundColor=TEXTFIELDVIEW_ERROR_BACKGROUNDCOLOR;
//        view.layer.borderColor=TEXTFIELDVIEW_ERROR_BORDERCOLOR;
//        labelResult.text=@"";
//    }
//}
//-(void)compute{
//    if ((![convertFromTextField.text isEqualToString:@""] && ![convertFromLabel.text isEqualToString:@"Convert from"] && ![convertToLabel.text isEqualToString:@"Convert to"] ) && [self validate:[convertFromTextField.text doubleValue] checktype:0 viewTag:100])
//    {
//        double convertFrom=[self getPotency:convertFromLabel.text];
//        double convertTo=[self getPotency:convertToLabel.text];
//        double result= [convertFromTextField.text doubleValue] * (convertFrom /convertTo);
//        if (![byPercentageTextField.text isEqualToString:@""] && [self validate:[byPercentageTextField.text doubleValue] checktype:1 viewTag:103])
//        {
//            double bypercentage=[byPercentageTextField.text doubleValue];
//            switch (self.segmentControl.selectedSegmentIndex) {
//                case 0:
//                    {
//                        [self show:(result+(result*(bypercentage/100)))];
//                       // labelResult.text=[NSString stringWithFormat:@"%.1f mg",result+(result*(bypercentage/100))];
//                    }
//                    break;
//                case 1:
//                    {
//                        [self show:(result-(result*(bypercentage/100)))];
//                        // labelResult.text=[NSString stringWithFormat:@"%.1f mg",result-(result*(bypercentage/100))];
//                    }
//                    break;
//                default:
//                    break;
//            }         
//
//        }
//        else if ([byPercentageTextField.text isEqualToString:@""])
//        {
//          //  labelResult.text=[NSString stringWithFormat:@"%.1f mg",result];
//            [self show:result];
//        }
//        else
//        {
//            labelResult.text=@"....";
//        }
//       
//    }
//    else
//    {
//        labelResult.text=@"....";
//    }
//}

-(void)show:(double)result{
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [formatter setMaximumFractionDigits:2];
    [formatter setRoundingMode: NSNumberFormatterRoundCeiling];
    DebugLog(@"result %f",result);
  //  labelResult.text=[NSString stringWithFormat:@"%@ mg",[formatter stringFromNumber:[NSNumber numberWithFloat:result]]];
    labelResult.text=[NSString stringWithFormat:@"%.2f mg",result];
}
-(double)getPotency:(NSString *)steroidText{
    double potency=0;
    if ([steroidText isEqualToString:@"Betamethasone"])
        potency=30;
    if ([steroidText isEqualToString:@"Cortisone"])
        potency=0.8;
    if ([steroidText isEqualToString:@"Dexamethasone"])
        potency=25;
    if ([steroidText isEqualToString:@"Fludrocortisone"])
        potency=15;
    if ([steroidText isEqualToString:@"Hydrocortisone"])
        potency=1;
    if ([steroidText isEqualToString:@"Methylprednisolone"])
        potency=5;
    if ([steroidText isEqualToString:@"Prednisolone"])
        potency=4;
    if ([steroidText isEqualToString:@"Prednisone"])
        potency=4;
    if ([steroidText isEqualToString:@"Triamcinolone"])
        potency=5;
    return potency;
}

#pragma UITextField Delegate methods
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if([textField isEqual:convertFromTextField])
    {
        [self scrollTobottom];
        [CommonCallback changeTextFieldColor:TEXT_EDITING view:viewConvertFromTextfield];
    }
    else if([textField isEqual:byPercentageTextField])
    {
        [scrollView setContentOffset:CGPointMake(0,viewPrecentageTextfield.frame.origin.y-100) animated:YES];
        [CommonCallback changeTextFieldColor:TEXT_EDITING view:viewPrecentageTextfield];
    }
    scrollView.contentSize = CGSizeMake(scrollView.frame.size.width, scrollView.frame.size.height+100);
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if([textField isEqual:convertFromTextField])
    {
        [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:viewConvertFromTextfield];
    }
    else if([textField isEqual:byPercentageTextField])
    {
        [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:viewPrecentageTextfield];
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    // allow backspace
    if (range.length > 0 && [string length] == 0) {
        return YES;
    }
    // do not allow . at the beggining
    //    if (range.location == 0 && [string isEqualToString:@"."]) {
    //        return NO;
    //    }
    /*if (textField == convertFromTextField && (![convertFromTextField.text isEqualToString:@""] && convertFromTextField.text.length > 9))
     {
     return NO;
     }
     else
     {
     return YES;
     }*/
    
    NSRange temprange = [textField.text rangeOfString:@"."];
    if ((temprange.location != NSNotFound) && [string isEqualToString:@"."])
    {
        return NO;
    }
    else
        return YES;
}

-(void)scrollTobottom
{
    [UIView animateWithDuration:0.2 animations:^{
        scrollView.contentSize = CGSizeMake(scrollView.frame.size.width, scrollView.frame.size.height);
        CGPoint bottomOffset = CGPointMake(0, 0);
        [scrollView setContentOffset:bottomOffset animated:YES];
    }];
}

- (IBAction)infoButtonAction:(id)sender {
    NSDictionary *params = @{@"calculator_name":self.title};
    [INEventLogger logEvent:@"MedCalc_Calculator_Info_Image" withParams:params];
    
    MedicalInfoImageViewController *typedetailController = [[MedicalInfoImageViewController alloc] initWithimageName:@"cortisone_info.png"] ;
    [self.navigationController pushViewController:typedetailController animated:YES];
}

-(void)setErrorMessageOnLabel:(NSString *)errorMessage label:(UILabel *)lbl{
    labelResult.text    =   @"....";
    lbl.hidden = NO;
    lbl.text = errorMessage;
    if ([lbl isEqual:lblErrConverFromTextfield]) {
        [CommonCallback changeTextFieldColor:TEXT_ERROR view:viewConvertFromTextfield];
    }else if ([lbl isEqual:lblErrPercentageTextfield]) {
        [CommonCallback changeTextFieldColor:TEXT_ERROR view:viewPrecentageTextfield];
    }
}

-(void)removeErrorMessageFromLabel:(UILabel *)lbl{
    lbl.hidden = YES;
    if ([lbl isEqual:lblErrConverFromTextfield]) {
        [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:viewConvertFromTextfield];
    }else if ([lbl isEqual:lblErrPercentageTextfield]) {
        [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:viewPrecentageTextfield];
    }
}
@end
