//
//  MedicalANCViewController.m
//  Medical
//
//  Created by Kirti Nikam on 08/10/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import "MedicalANCViewController.h"
#import "MedicalInfoImageViewController.h"
#import "MedicalAppDelegate.h"
#import "CommonCallback.h"

@interface MedicalANCViewController ()

@end

@implementation MedicalANCViewController
@synthesize cmmLabel;
@synthesize labelResult;
@synthesize wbcTextField;
@synthesize polysTextField;
@synthesize bandsTextField;
@synthesize keyboardView;
@synthesize viewWBC,viewPolys,viewBands,viewCollection;
@synthesize txtCollection,lblCollection,lblErrorCollection;
@synthesize lblNCount;
@synthesize lblErrWBC,lblErrPolys,lblErrBands;
@synthesize ancScrollView;
@synthesize keyBoardBtnCollection;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from it`s nib.
    NSDictionary *params = @{@"calculator_name":self.title};
    [INEventLogger logEvent:@"MedCalc_Calculator_Details" withParams:params];
    
    //self.view.backgroundColor = BACKGROUND_COLOR;
    [self setUI];
    
    wbcTextField.inputAccessoryView = keyboardView;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]
                                              initWithBarButtonSystemItem:UIBarButtonSystemItemAction
                                              target:self action:@selector(shareBarBtnClicked:)];

    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.backBarButtonItem = backButton;
    
    ancScrollView.contentSize = CGSizeMake(ancScrollView.frame.size.width, CGRectGetMaxY(lblNCount.frame)+30);
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected:)];
    tapGesture.numberOfTapsRequired = 1;
    tapGesture.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapGesture];
}

- (void)viewDidUnload
{
    [self setLabelResult:nil];
    [self setWbcTextField:nil];
    [self setPolysTextField:nil];
    [self setBandsTextField:nil];
    [self setCmmLabel:nil];
    [self setKeyboardView:nil];
    [self setLblNCount:nil];
    [self setLblCollection:nil];
    [self setTxtCollection:nil];
    [self setViewCollection:nil];
    [self setViewWBC:nil];
    [self setViewPolys:nil];
    [self setViewBands:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
#pragma mark - Rotation
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

-(void)setUI{
    
    cmmLabel.text   =  CMM3;
    UIButton *btn   =   (UIButton *)[self.view  viewWithTag:40];
    [btn setTitle:CMM3 forState:UIControlStateNormal];
    
    lblNCount.backgroundColor     = [UIColor clearColor];
    lblNCount.textColor           = DEFAULT_COLOR;
    lblNCount.font                = DEFAULT_BOLD_FONT(25);
    
    labelResult.backgroundColor     = [UIColor clearColor];
    labelResult.textColor           = DEFAULT_COLOR;
    labelResult.font                = DEFAULT_BOLD_FONT(25);
    
    for (UIView *lview in viewCollection) {
        lview.backgroundColor     = [UIColor whiteColor];
        lview.layer.borderColor   = [UIColor lightGrayColor].CGColor;
        lview.layer.borderWidth   = 1.0;
    }
    
    for (UITextField *txt in txtCollection) {
        txt.delegate            = self;
        txt.backgroundColor     = [UIColor clearColor];
        txt.textColor           = [UIColor blackColor];
        txt.font                = DEFAULT_FONT(22);
        txt.keyboardType        = UIKeyboardTypeDecimalPad;
        txt.textAlignment       = NSTextAlignmentCenter;
        txt.layer.borderColor   = [UIColor clearColor].CGColor;
    }
    
    for (UILabel *lbl in lblCollection) {
        lbl.backgroundColor     = [UIColor clearColor];
        lbl.textColor           = DEFAULT_COLOR;
        lbl.font                = DEFAULT_SEMIBOLD_FONT(20);
    }
    
    for (UILabel *lbl in lblErrorCollection) {
        lbl.backgroundColor     = [UIColor clearColor];
        lbl.textColor           = [UIColor redColor];
        lbl.font                = DEFAULT_SEMIBOLD_FONT(12);
        lbl.hidden              = YES;
    }
    
    keyboardView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.4];
    for (UIButton *btn in keyBoardBtnCollection) {
        btn.backgroundColor     = DEFAULT_COLOR;
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor colorWithWhite:1.0 alpha:0.5] forState:UIControlStateHighlighted];
        btn.titleLabel.font     = DEFAULT_FONT(20);

    }
}

-(void)tapDetected:(UIGestureRecognizer *)sender{
    [self.view endEditing:YES];
    CGPoint tapLocation = [sender locationInView:ancScrollView];
    UIView *view = [ancScrollView hitTest:tapLocation withEvent:nil];
    if (![view isKindOfClass:[UIButton class]]) {
        [self scrollTobottom];
    }
}

//-(void)touchesBegan:(NSSet*)touches withEvent:(UIEvent*)event
//{
//    UITouch *touch = [touches anyObject];
//    if(touch.view.tag == 10) {
//        [self.view endEditing:YES];
//    }
//}

- (void) shareBarBtnClicked:(id)sender {
    UIActionSheet *uiActionSheetP= [[UIActionSheet alloc]
                                    initWithTitle: @""
                                    delegate:self
                                    cancelButtonTitle:@"CANCEL"
                                    destructiveButtonTitle:nil
                                    otherButtonTitles:@"Print",@"Email", nil];
	
	uiActionSheetP.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
	[uiActionSheetP showInView:self.navigationController.view];
}

#pragma UIActionSheet Deletegate methods
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [keyboardView setHidden:YES];
    [self.view endEditing:YES];
    CGRect oldFrame = ancScrollView.frame;
    ancScrollView.frame=CGRectMake(ancScrollView.frame.origin.x, ancScrollView.frame.origin.y, ancScrollView.contentSize.width, ancScrollView.contentSize.height);
	if (buttonIndex == 0)
    {
        NSDictionary *params = @{@"calculator_name":self.title, @"share_via" : @"print"};
        [INEventLogger logEvent:@"MedCalc_Calculator_Share" withParams:params];
        [MEDICAL_APP_DELEGATE printImage:ancScrollView];
    }
    else if (buttonIndex == 1)
    {
        NSDictionary *params = @{@"calculator_name":self.title, @"share_via" : @"email"};
        [INEventLogger logEvent:@"MedCalc_Calculator_Share" withParams:params];
        [MEDICAL_APP_DELEGATE mailImage:ancScrollView];
    }
	ancScrollView.frame=oldFrame;
    [self scrollTobottom];
}

-(void) roundViewWithBorder
{
    for (int i=100; i<103; i++)
    {
        UIView *text=(UIView *)[self.view viewWithTag:i];
        text.layer.cornerRadius = TEXTFIELDVIEW_CORNERRADIUS;
        text.clipsToBounds = YES;
        text.layer.borderColor = TEXTFIELDVIEW_DEFAULT_BORDERCOLOR;
        text.layer.borderWidth = TEXTFIELDVIEW_BORDERWIDTH;
    }
}

- (IBAction)keyboardButtonAction:(id)sender {
    
    UIButton *btn=(UIButton *)sender;
    cmmLabel.text=btn.titleLabel.text;
    //[self validate:[wbcTextField.text doubleValue] checktype:0 viewTag:100];
    [self validate:wbcTextField];
    [self compute];
}

- (IBAction)editingChanged:(UITextField *)textField {
    [self validate:textField];
    [self compute];
}

-(BOOL)validate:(UITextField *)textField{
    BOOL success = YES;
    double doubleValue = [textField.text doubleValue];
    if([textField isEqual:wbcTextField])
    {
        
        if ([cmmLabel.text isEqualToString:CMM3])
        {
            if ((doubleValue >= 0 && doubleValue <= 100000)) {
                [self removeErrorMessageFromLabel:lblErrWBC];
            }else{
                [self setErrorMessageOnLabel:@"Enter WBC value less than 100000" label:lblErrWBC];
                success = NO;
            }
        }
        if ([cmmLabel.text isEqualToString:@"G/L"])
        {
                
                if ((doubleValue >= 0 && doubleValue <= 200)) {
                    [self removeErrorMessageFromLabel:lblErrWBC];
                }else{
                    [self setErrorMessageOnLabel:@"Enter WBC value less than 200" label:lblErrWBC];
                    success = NO;
                }
        }
    }else{
        if (doubleValue >= 0 && doubleValue <= 100)
        {
            if([textField isEqual:polysTextField])
            {
                [self removeErrorMessageFromLabel:lblErrPolys];
            }
            else if([textField isEqual:bandsTextField])
            {
               [self removeErrorMessageFromLabel:lblErrBands];
            }
        }
        else
        {
            if([textField isEqual:polysTextField])
            {
                [self setErrorMessageOnLabel:@"Polys value must be less than 100" label:lblErrPolys];
            }
            else if([textField isEqual:bandsTextField])
            {
                [self setErrorMessageOnLabel:@"Bands value must be less than 100" label:lblErrBands];
            }
            success = NO;
        }
    }
    return success;
}

//-(BOOL)validate:(double)doublevalue checktype:(int)type viewTag:(int)tag
//{
//    switch (type) {
//        case 0:
//            if ((doublevalue >=0 && doublevalue<=100000) && [cmmLabel.text isEqualToString:CMM3])
//            {
//                [self changeTextFieldColor:TRUE viewTag:tag];
//                return TRUE;
//            }
//            if ((doublevalue >=0 && doublevalue<=200) && [cmmLabel.text isEqualToString:@"G/L"])
//            {
//                [self changeTextFieldColor:TRUE viewTag:tag];
//                return TRUE;
//            }
//            else
//            {
//                [self changeTextFieldColor:FALSE viewTag:tag];
//                return FALSE;
//            }
//            break;
//        case 1:
//            if (doublevalue >=0 && doublevalue<=100)
//            {
//                [self changeTextFieldColor:TRUE viewTag:tag];
//                return TRUE;
//
//            }
//            else
//            {
//                [self changeTextFieldColor:FALSE viewTag:tag];
//                 return FALSE;
//            }
//            break;
//        default:
//            break;
//    }
//    return FALSE;
//}
//
//-(void)changeTextFieldColor:(BOOL)type viewTag:(int)tag
//{
//     UIView *view=(UIView *)[self.view viewWithTag:tag];
//    if (type)
//    {
//        view.backgroundColor    =   TEXTFIELDVIEW_DEFAULT_BACKGROUNDCOLOR;
//        view.layer.borderColor  =   TEXTFIELDVIEW_DEFAULT_BORDERCOLOR;
//
//    }
//    else
//    {
//        view.backgroundColor    =   TEXTFIELDVIEW_ERROR_BACKGROUNDCOLOR;
//        view.layer.borderColor  =   TEXTFIELDVIEW_ERROR_BORDERCOLOR;
//        labelResult.text=@"";
//    }
//}

-(void)compute{
    DebugLog(@"%@ %f",wbcTextField.text,[wbcTextField.text doubleValue]);
    if (![wbcTextField.text isEqualToString:@""] && ![polysTextField.text isEqualToString:@""] && ![bandsTextField.text  isEqualToString:@""] > 0)
    {
        int count=0;
        if ([self validate:wbcTextField])
            count++;
        if ([self validate:polysTextField])
            count++;
        if ([self validate:bandsTextField])
            count++;
        
        double cmmvalue;
        if ([cmmLabel.text isEqualToString:CMM3])
            cmmvalue=[wbcTextField.text doubleValue];
        else
            cmmvalue=[wbcTextField.text doubleValue] * 1000 ;
        
        
        if (count==3)
        {
            
            labelResult.text=[NSString stringWithFormat:@"%.2f %@",(cmmvalue * ([polysTextField.text doubleValue] + [bandsTextField.text doubleValue])/100),CMM3];
            // [self show:(cmmvalue * ([polysTextField.text doubleValue] + [bandsTextField.text doubleValue])/100)];
        }
        else
            labelResult.text=@"....";
    }
    else
    {
        labelResult.text=@"....";
    }
}

//-(void)compute{
//    DebugLog(@"%@ %f",wbcTextField.text,[wbcTextField.text doubleValue]);
//    if ([wbcTextField.text length] > 0 && [polysTextField.text length] > 0 && ![bandsTextField.text length] > 0)
//    {
//        int count=0;
//        if ([self validate:[wbcTextField.text doubleValue] checktype:0 viewTag:100])
//            count++;
//        if ([self validate:[polysTextField.text doubleValue] checktype:1 viewTag:101])
//            count++;
//        if ([self validate:[bandsTextField.text doubleValue] checktype:1 viewTag:102])
//            count++;
//        double cmmvalue;
//        if ([cmmLabel.text isEqualToString:CMM3])
//            cmmvalue=[wbcTextField.text doubleValue];
//        else
//            cmmvalue=[wbcTextField.text doubleValue] * 1000 ;
//
//            
//        if (count==3)
//        {
//        
//           labelResult.text=[NSString stringWithFormat:@"%.2f %@",(cmmvalue * ([polysTextField.text doubleValue] + [bandsTextField.text doubleValue])/100),CMM3];
//           // [self show:(cmmvalue * ([polysTextField.text doubleValue] + [bandsTextField.text doubleValue])/100)];
//        }
//        else
//           labelResult.text=@"...."; 
//    }
//    else
//    {
//        labelResult.text=@"....";
//    }
//}

-(void)show:(double)result{
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [formatter setMaximumFractionDigits:2];
    [formatter setRoundingMode: NSNumberFormatterRoundCeiling];
    DebugLog(@"result %f",result);
    labelResult.text=[NSString stringWithFormat:@"%@ %@",[formatter stringFromNumber:[NSNumber numberWithFloat:result]],CMM3];    
}

#pragma UITextField Delegate methods
#pragma UITextFieldDelegate methods
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if([textField isEqual:wbcTextField])
    {
        [self scrollTobottom];
        [CommonCallback changeTextFieldColor:TEXT_EDITING view:viewWBC];
    }
    else if([textField isEqual:polysTextField])
    {
        [ancScrollView setContentOffset:CGPointMake(0,viewPolys.frame.origin.y-100) animated:YES];
        [CommonCallback changeTextFieldColor:TEXT_EDITING view:viewPolys];
    }
    else if([textField isEqual:bandsTextField])
    {
        [ancScrollView setContentOffset:CGPointMake(0,viewBands.frame.origin.y-100) animated:YES];
        [CommonCallback changeTextFieldColor:TEXT_EDITING view:viewBands];
    }
    ancScrollView.contentSize = CGSizeMake(ancScrollView.frame.size.width, CGRectGetMaxY(lblNCount.frame)+300);
    return YES;
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if([textField isEqual:wbcTextField])
    {
        [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:viewWBC];
    }
    else if([textField isEqual:polysTextField])
    {
        [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:viewPolys];
    }
    else if([textField isEqual:bandsTextField])
    {
        [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:viewBands];
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    // allow backspace
    if (range.length > 0 && [string length] == 0) {
        return YES;
    }
    // do not allow . at the beggining
    //    if (range.location == 0 && [string isEqualToString:@"."]) {
    //        return NO;
    //    }
    
    NSRange temprange = [textField.text rangeOfString:@"."];
    if ((temprange.location != NSNotFound) && [string isEqualToString:@"."])
    {
        return NO;
    }
    else
        return YES;
}

-(void)scrollTobottom
{
    [UIView animateWithDuration:0.2 animations:^{
        ancScrollView.contentSize = CGSizeMake(ancScrollView.frame.size.width, CGRectGetMaxY(lblNCount.frame)+30);
        CGPoint bottomOffset = CGPointMake(0, 0);
        [ancScrollView setContentOffset:bottomOffset animated:YES];
    }];
}

- (IBAction)infoButtonAction:(id)sender {
    NSDictionary *params = @{@"calculator_name":self.title};
    [INEventLogger logEvent:@"MedCalc_Calculator_Info_Image" withParams:params];
    
    MedicalInfoImageViewController *typedetailController = [[MedicalInfoImageViewController alloc] initWithimageName:@"anc_info.png"] ;
    [self.navigationController pushViewController:typedetailController animated:YES];
}


-(void)setErrorMessageOnLabel:(NSString *)errorMessage label:(UILabel *)lbl{
    labelResult.text    =   @"....";
    lbl.hidden = NO;
    lbl.text = errorMessage;
    if ([lbl isEqual:lblErrWBC]) {
        [CommonCallback changeTextFieldColor:TEXT_ERROR view:viewWBC];
    }else if ([lbl isEqual:lblErrPolys]) {
        [CommonCallback changeTextFieldColor:TEXT_ERROR view:viewPolys];
    }else if ([lbl isEqual:lblErrBands]) {
        [CommonCallback changeTextFieldColor:TEXT_ERROR view:viewBands];
    }
}

-(void)removeErrorMessageFromLabel:(UILabel *)lbl{
    lbl.hidden = YES;
    if ([lbl isEqual:lblErrWBC]) {
        [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:viewWBC];
    }else if ([lbl isEqual:lblErrPolys]) {
        [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:viewPolys];
    }else if ([lbl isEqual:lblErrBands]) {
        [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:viewBands];
    }
}
@end
