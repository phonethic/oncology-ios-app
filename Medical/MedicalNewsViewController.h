//
//  MedicalNewsViewController.h
//  Medical
//
//  Created by Kirti Nikam on 08/04/14.
//  Copyright (c) 2014 Kirti Nikam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@class NewsCategoryObject;
@interface MedicalNewsViewController : UIViewController<NSURLConnectionDelegate,MBProgressHUDDelegate>
{
   
    NSURLConnection *conn;
    
    NewsCategoryObject *newsCatgObj;
    
     MBProgressHUD *HUD;
}

@property (strong,nonatomic) NSMutableData *webData;
@property (strong,nonatomic) NSMutableArray *newsAlertCategoryArray;

@property (strong, nonatomic) IBOutlet UITableView *newsTableView;
@end
