//
//  MedicalKarnofskyStatusViewController.m
//  Medical
//
//  Created by Kirti Nikam on 17/10/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import "MedicalKarnofskyStatusViewController.h"
#import "MedicalAppDelegate.h"
#import "MedicalInfoImageViewController.h"

@interface MedicalKarnofskyStatusViewController ()

@end

@implementation MedicalKarnofskyStatusViewController
@synthesize karnofskyStatusArry;
@synthesize kartableView;
@synthesize infoBtn;
@synthesize lblPercentage,lblFunctionaCapacity;
@synthesize lblUnderLine;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)showActionsheet
{
	UIActionSheet *uiActionSheetP= [[UIActionSheet alloc]
                                    initWithTitle: @""
                                    delegate:self
                                    cancelButtonTitle:@"CANCEL"
                                    destructiveButtonTitle:nil
                                    otherButtonTitles:@"Print",@"Email", nil];
	
	uiActionSheetP.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
	[uiActionSheetP showInView:self.navigationController.view];
    
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    DebugLog(@"buttonIndex = %d",buttonIndex);
	
	if (buttonIndex == 0) {
        [self takescreenshot:nil];
    } else if (buttonIndex == 1) {
        [self sendMail];
    }
	
}
- (IBAction)takescreenshot:(id)sender
{
   
    CGRect oldFrame = kartableView.frame;
    kartableView.frame=CGRectMake(kartableView.frame.origin.x, kartableView.frame.origin.y, kartableView.contentSize.width, kartableView.contentSize.height);
    [MEDICAL_APP_DELEGATE printImage:kartableView];
    //save into photolibrary
	//UIImageWriteToSavedPhotosAlbum(viewImage, nil, nil, nil);
	kartableView.frame=oldFrame;
    
}
- (void)sendMail
{
    [MEDICAL_APP_DELEGATE mailImage:self.view];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSDictionary *params = @{@"calculator_name":self.title};
    [INEventLogger logEvent:@"MedCalc_Calculator_Details" withParams:params];
   // self.view.backgroundColor = BACKGROUND_COLOR;

    karnofskyStatusArry=[[NSMutableArray alloc] init];
    [karnofskyStatusArry addObject:[NSArray arrayWithObjects:@"Normal, no complaints, no signs of disease",@"100",nil]];
    [karnofskyStatusArry addObject:[NSArray arrayWithObjects:@"Able to carry on normal activity, minor signs or symptoms of disease",@"90",nil]];
    [karnofskyStatusArry addObject:[NSArray arrayWithObjects:@"Normal activity with effort, some signs or symptoms of disease",@"80",nil]];
    [karnofskyStatusArry addObject:[NSArray arrayWithObjects:@"Cares for self, unable to carry on normal activity or to do active work",@"70",nil]];
    [karnofskyStatusArry addObject:[NSArray arrayWithObjects:@"Requires occasional assistance, but is able to care for most needs",@"60",nil]];
    [karnofskyStatusArry addObject:[NSArray arrayWithObjects:@"Requires considerable assistance and frequent medical care",@"50",nil]];
    [karnofskyStatusArry addObject:[NSArray arrayWithObjects:@"Disabled, requires special care and assistance",@"40",nil]];
    [karnofskyStatusArry addObject:[NSArray arrayWithObjects:@"Severely disabled, hospitalization is indicated although death is not imminent",@"30",nil]];
    [karnofskyStatusArry addObject:[NSArray arrayWithObjects:@"Hospitalization is necessary, very sick, active supportive treatment necessary",@"20",nil]];
    [karnofskyStatusArry addObject:[NSArray arrayWithObjects:@"Moribund, fatal processes progressing rapidly",@"10",nil]];
    [karnofskyStatusArry addObject:[NSArray arrayWithObjects:@"Dead",@"0",nil]];
    
    kartableView.separatorColor = [UIColor blackColor];
    
    lblPercentage.backgroundColor           = [UIColor clearColor];
    lblPercentage.textColor                 = DEFAULT_COLOR;
    lblPercentage.font                      = DEFAULT_BOLD_FONT(25);
    
    lblFunctionaCapacity.backgroundColor     = [UIColor clearColor];
    lblFunctionaCapacity.textColor           = DEFAULT_COLOR;
    lblFunctionaCapacity.font                = DEFAULT_BOLD_FONT(20);
    
    lblUnderLine.backgroundColor= DEFAULT_COLOR;
    lblUnderLine.text = @"";
    
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]
//                                              initWithBarButtonSystemItem:UIBarButtonSystemItemAction
//                                              target:self action:@selector(save_Clicked:)];
}
- (void) save_Clicked:(id)sender {
    [self showActionsheet];
}

- (void)viewDidUnload
{
    [self setKartableView:nil];
    [self setInfoBtn:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
#pragma mark - Rotation
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
#pragma mark Table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [karnofskyStatusArry count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UILabel *lblValue,*lblLevel,*lblUnderline;

    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        
        lblValue = [[UILabel alloc] initWithFrame:CGRectMake(5,0 , 85,68)];
        lblValue.tag            = 200;
        lblValue.font           = DEFAULT_BOLD_FONT(30);
        lblValue.textAlignment  = NSTextAlignmentCenter;
        lblValue.textColor      = DEFAULT_COLOR;
        lblValue.backgroundColor= [UIColor clearColor];
        [cell.contentView addSubview:lblValue];
        
        lblLevel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(lblValue.frame),0, 225, 68)];
        lblLevel.tag            = 300;
        lblLevel.font           = DEFAULT_FONT(14);
        lblLevel.textAlignment  = NSTextAlignmentLeft;
        lblLevel.textColor      = DEFAULT_COLOR;
        lblLevel.backgroundColor= [UIColor clearColor];
        lblLevel.numberOfLines  = 4;
        [cell.contentView addSubview:lblLevel];
        
        lblUnderline= [[UILabel alloc] initWithFrame:CGRectMake(20,CGRectGetMaxY(lblValue.frame), 280, 2)];
        lblUnderline.tag = 400;
        lblUnderline.backgroundColor= DEFAULT_COLOR;
        lblUnderline.text = @"";
        [cell.contentView addSubview:lblUnderline];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    NSArray *arr=[self.karnofskyStatusArry objectAtIndex:indexPath.row];
    lblValue = (UILabel *)[cell viewWithTag:200];
    lblValue.text = [arr objectAtIndex:1];
    lblLevel = (UILabel *)[cell viewWithTag:300];
    lblLevel.text = [arr objectAtIndex:0];
    
    lblUnderline = (UILabel *)[cell viewWithTag:400];
    DebugLog(@"indexPath.row %d %d",indexPath.row, karnofskyStatusArry.count - 1);
    if (indexPath.row == karnofskyStatusArry.count - 1) {
        lblUnderline.hidden = YES;
    }else{
        lblUnderline.hidden = NO;
    }
    return cell;
}


- (IBAction)infoButtonAction:(id)sender {
    NSDictionary *params = @{@"calculator_name":self.title};
    [INEventLogger logEvent:@"MedCalc_Calculator_Info_Image" withParams:params];
    
    MedicalInfoImageViewController *typedetailController = [[MedicalInfoImageViewController alloc] initWithimageName:@"karnofsky_info.png"] ;
    [self.navigationController pushViewController:typedetailController animated:YES];
}
@end
