//
//  MedicalRegimenViewController.h
//  Medical
//
//  Created by Rishi on 26/10/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>

@class RegimenObject;

@interface MedicalRegimenViewController : UIViewController
{
    RegimenObject *tempRegimentObj;
}
@property (strong, nonatomic) IBOutlet UITableView *regimenTableview;
@property (nonatomic,copy) NSString *cancerName;
@property (nonatomic, strong) NSMutableDictionary *regimenDict;
@end
