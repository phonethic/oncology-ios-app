//
//  MedicalRegimenViewController.m
//  Medical
//
//  Created by Rishi on 26/10/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import "MedicalRegimenViewController.h"
#import "MedicalRegimenDetailsViewController.h"
#import "MedicalAppDelegate.h"
#import "RegimenObject.h"

@interface MedicalRegimenViewController ()

@end

@implementation MedicalRegimenViewController
@synthesize regimenTableview;
@synthesize cancerName;
@synthesize regimenDict;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UIBarButtonItem *back = [[UIBarButtonItem alloc] initWithTitle:@"Back"
                                                             style:UIBarButtonItemStylePlain
                                                            target:nil
                                                            action:nil];
    [[self navigationItem] setBackBarButtonItem:back];
    
    regimenDict  = [[NSMutableDictionary alloc] init];
    // Get the path to the documents directory and append the databaseName
    [self readregimenDataFromDatabase];
    
    if ([cancerName length] > 0) {
        NSDictionary *params = @{@"cancer_name":cancerName};
        [INEventLogger logEvent:@"Chemo_Orders_Regimen_List" withParams:params];
    }
}

- (void)viewDidUnload
{
    [self setRegimenTableview:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
#pragma mark - Rotation
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

-(void) readregimenDataFromDatabase {
	// Setup the database object
	sqlite3 *database;
    int regimenId = 0;
    NSString * regimenName = @"";
    int warn = 0;
    NSString * stageName = @"";
    
	// Open the database from the users filessytem
	if(sqlite3_open([[MEDICAL_APP_DELEGATE databasePath] UTF8String], &database) == SQLITE_OK) {
		// Setup the SQL Statement and compile it for faster access
        NSString *regimenquerySQL = [NSString stringWithFormat:@"select stage,region_id,region_name,regimen_warn from regions where cancer=\"%@\"", self.cancerName];
        const char *sqlStatement = [regimenquerySQL UTF8String];
		sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
			// Loop through the results and add them to the feeds array
			while(sqlite3_step(compiledStatement) == SQLITE_ROW)
            {
                stageName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 0)];
                regimenId = sqlite3_column_int(compiledStatement, 1);
                regimenName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 2)];
                warn = sqlite3_column_int(compiledStatement, 3);
                
                DebugLog(@"regimen:cancer %@ stage : %@ regionId %d regimenName %@ isWarn %d",cancerName,stageName,regimenId,regimenName,warn);
                
                tempRegimentObj = [[RegimenObject alloc] init];
                tempRegimentObj.regimenID   = regimenId;
                tempRegimentObj.regimenName = [regimenName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                tempRegimentObj.regimenWarn = warn;
                
                if (![regimenDict.allKeys containsObject:stageName]) {
                    [regimenDict setValue:[[NSMutableArray alloc] init] forKey:stageName];
                }
                [[regimenDict objectForKey:stageName] addObject:tempRegimentObj];
                tempRegimentObj = nil;
            }
        }
		// Release the compiled statement from memory
		sqlite3_finalize(compiledStatement);
	}
	sqlite3_close(database);
}

#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [regimenDict.allKeys count];
}

-(CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section
{
    return 35;
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView* headerView          = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 35)];
    headerView.backgroundColor  = DEFAULT_COLOR;
    
    UILabel *headerLabel        = [[UILabel alloc] initWithFrame:CGRectZero];
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.frame           = CGRectMake(10,0,290,35);
    headerLabel.textAlignment   = NSTextAlignmentLeft;
    headerLabel.font            = DEFAULT_BOLD_FONT(17);
    headerLabel.text            =  [[[regimenDict allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:section];
    headerLabel.textColor       = [UIColor whiteColor];
    [headerView addSubview:headerLabel];
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return STATIC_ROW_HEIGHT;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[regimenDict valueForKey:[[[regimenDict allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:section]] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.textLabel.font =   DEFAULT_FONT(FONT_STATIC_ROW_HEIGHT);
        cell.textLabel.numberOfLines = 3;

        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = DEFAULT_COLOR_WITH_ALPHA(0.5);
        cell.selectedBackgroundView = bgColorView;
    }
    NSString *key = [[[regimenDict allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section];
    NSArray *sortedArray = [[regimenDict valueForKey:key] sortedArrayUsingDescriptors:[NSArray arrayWithObject:[[NSSortDescriptor alloc] initWithKey:@"regimenName" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)]]];
    RegimenObject *tempObj = (RegimenObject *)[sortedArray objectAtIndex:indexPath.row];
    cell.textLabel.text = tempObj.regimenName;
    cell.textLabel.numberOfLines = 3;
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *key = [[[regimenDict allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section];
    NSArray *sortedArray = [[regimenDict valueForKey:key] sortedArrayUsingDescriptors:[NSArray arrayWithObject:[[NSSortDescriptor alloc] initWithKey:@"regimenName" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)]]];
    RegimenObject *tempObj = (RegimenObject *)[sortedArray objectAtIndex:indexPath.row];

    MedicalRegimenDetailsViewController *regimendetailController = [[MedicalRegimenDetailsViewController alloc] initWithNibName:@"MedicalRegimenDetailsViewController" bundle:nil] ;
    regimendetailController.regimenName = tempObj.regimenName; //cell.textLabel.text;
    regimendetailController.regimenId =   [NSString stringWithFormat:@"%d",tempObj.regimenID];//[NSString stringWithFormat:@"%d",cell.tag];
    regimendetailController.title = @"";
    regimendetailController.cancerName = cancerName;
    regimendetailController.regionWarnResult = tempObj.regimenWarn;//[[regimenWarns objectAtIndex:indexPath.row] intValue];
    [self.navigationController pushViewController:regimendetailController animated:YES];
}


-(NSInteger) getrowsCount : (NSString *) sectionName
{
    // Setup the database object
	sqlite3 *database;
    NSInteger value = 0;
	// Open the database from the users filessytem
	if(sqlite3_open([[MEDICAL_APP_DELEGATE databasePath] UTF8String], &database) == SQLITE_OK) {
		// Setup the SQL Statement and compile it for faster access
        NSString *regimenquerySQL = [NSString stringWithFormat:@"select count(*) from regions where cancer=\"%@\"  and stage=\"%@\"", self.cancerName, sectionName];
        const char *sqlStatement = [regimenquerySQL UTF8String];
		sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
			// Loop through the results and add them to the feeds array
			while(sqlite3_step(compiledStatement) == SQLITE_ROW)
            {
                value = sqlite3_column_int(compiledStatement, 0);
            }
        }
		// Release the compiled statement from memory
		sqlite3_finalize(compiledStatement);
	}
	sqlite3_close(database);
    //DebugLog(@"count = %d value = %d",[regimenNames count], value);
    return value;
}
@end
