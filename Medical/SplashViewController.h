//
//  SplashViewController.h
//  Medical
//
//  Created by Kirti Nikam on 21/04/14.
//  Copyright (c) 2014 Kirti Nikam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SplashViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIImageView *imageView;

@end
