//
//  MedicalRecistCalcTumorViewController.m
//  Medical
//
//  Created by Kirti Nikam on 09/10/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import "MedicalRecistCalcTumorViewController.h"
#import "MedicalInfoImageViewController.h"
#import "MedicalAppDelegate.h"
#import "CommonCallback.h"

@interface MedicalRecistCalcTumorViewController ()

@end

@implementation MedicalRecistCalcTumorViewController

@synthesize pickerValue,baseSLDValue,currentSLDValue;
@synthesize labelResult,lblResponse;
@synthesize scrollView;
@synthesize pickerView;
@synthesize viewBaseSLD,viewCurrentSLD;
@synthesize baseSLDTextField,currentSLDTextField;
@synthesize lblCollection,txtCollection,viewCollection,lblErrCollection;
@synthesize lblErrBaseSLD,lblErrCurrentSLD;
@synthesize imgViewBaseSLD,imgViewCurrentSLD;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    NSDictionary *params = @{@"calculator_name":self.title};
//    [INEventLogger logEvent:@"MedCalc_Calculator_Details" withParams:params];
    
   // [self roundViewWithBorder];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]
                                              initWithBarButtonSystemItem:UIBarButtonSystemItemAction
                                              target:self action:@selector(shareBarBtnClicked:)];
    
    [self addLabels];
    [self addPickerWithDoneButton];
    [self setUI];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected:)];
    tapGesture.numberOfTapsRequired = 1;
    tapGesture.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapGesture];
}

- (void)viewDidUnload
{
    [self setLabelResult:nil];
    [self setScrollView:nil];
    [self setPickerView:nil];
    [self setBaseSLDTextField:nil];
    [self setCurrentSLDTextField:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
#pragma mark - Rotation
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma internal methods
-(void)setUI{
    
    lblResponse.backgroundColor     = [UIColor clearColor];
    lblResponse.textColor           = DEFAULT_COLOR;
    lblResponse.font                = DEFAULT_BOLD_FONT(20);
    
    labelResult.backgroundColor     = [UIColor clearColor];
    labelResult.textColor           = DEFAULT_COLOR;
    labelResult.font                = DEFAULT_BOLD_FONT(20);
    
    for (UIView *lview in viewCollection) {
        lview.backgroundColor     = [UIColor whiteColor];
        lview.layer.borderColor   = [UIColor lightGrayColor].CGColor;
        lview.layer.borderWidth   = 1.0;
    }
    
    for (UITextField *txt in txtCollection) {
        txt.delegate            = self;
        txt.backgroundColor     = [UIColor clearColor];
        txt.textColor           = [UIColor blackColor];
        txt.font                = DEFAULT_FONT(14);
        txt.keyboardType        = UIKeyboardTypeDecimalPad;
        txt.textAlignment       = NSTextAlignmentCenter;
        txt.layer.borderColor   = [UIColor clearColor].CGColor;
        txt.text 			    = @"";
    }
    
    for (UILabel *lbl in lblCollection) {
        lbl.backgroundColor     = [UIColor clearColor];
        lbl.textColor           = DEFAULT_COLOR;
        lbl.font                = DEFAULT_BOLD_FONT(18);
    }
    
    for (UILabel *lbl in lblErrCollection) {
        lbl.backgroundColor     = [UIColor clearColor];
        lbl.textColor           = [UIColor redColor];
        lbl.font                = DEFAULT_SEMIBOLD_FONT(12);
        lbl.hidden              = YES;
    }
}

-(void)tapDetected:(UIGestureRecognizer *)sender{
    [self.view endEditing:YES];
    CGPoint tapLocation = [sender locationInView:scrollView];
    UIView *view = [scrollView hitTest:tapLocation withEvent:nil];
    if (![view isKindOfClass:[UIButton class]]) {
        [self scrollTobottom];
    }
}

- (void) shareBarBtnClicked:(id)sender {
    UIActionSheet *uiActionSheetP= [[UIActionSheet alloc]
                                    initWithTitle: @""
                                    delegate:self
                                    cancelButtonTitle:@"CANCEL"
                                    destructiveButtonTitle:nil
                                    otherButtonTitles:@"Print",@"Email", nil];
	
	uiActionSheetP.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
	[uiActionSheetP showInView:self.navigationController.view];
    
}

#pragma UIActionSheet Delegate Methods
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    DebugLog(@"buttonIndex = %d",buttonIndex);
	[self.view endEditing:YES];
    CGRect oldFrame = scrollView.frame;
    scrollView.frame=CGRectMake(scrollView.frame.origin.x, scrollView.frame.origin.y, scrollView.contentSize.width, scrollView.contentSize.height);
	if (buttonIndex == 0)
    {
        NSDictionary *params = @{@"calculator_name":self.title, @"share_via" : @"print"};
        [INEventLogger logEvent:@"MedCalc_Calculator_Share" withParams:params];
        [MEDICAL_APP_DELEGATE printImage:scrollView];
    }
    else if (buttonIndex == 1)
    {
        NSDictionary *params = @{@"calculator_name":self.title, @"share_via" : @"email"};
        [INEventLogger logEvent:@"MedCalc_Calculator_Share" withParams:params];
        [MEDICAL_APP_DELEGATE mailImage:scrollView];
    }
	scrollView.frame=oldFrame;
    [self scrollTobottom];
}

-(void) roundViewWithBorder
{
    for (int i=100; i<104; i++)
    {
        UIView *text=(UIView *)[self.view viewWithTag:i];
        text.layer.cornerRadius = TEXTFIELDVIEW_CORNERRADIUS;
        text.clipsToBounds = YES;
        text.layer.borderColor = TEXTFIELDVIEW_DEFAULT_BORDERCOLOR;
        text.layer.borderWidth = TEXTFIELDVIEW_BORDERWIDTH;
    }
    for (int i=4; i<=5; i++)
    {
        UIView *text=(UIView *)[self.view viewWithTag:i];
        text.layer.cornerRadius = 2;//TEXTFIELDVIEW_CORNERRADIUS;
        text.clipsToBounds = YES;
        text.layer.borderColor = [UIColor colorWithRed:113/255.0 green:182/255.0 blue:235/255.0 alpha:1.0].CGColor;//[UIColor darkGrayColor].CGColor;// TEXTFIELDVIEW_DEFAULT_BORDERCOLOR;
        text.layer.borderWidth = 2;//TEXTFIELDVIEW_BORDERWIDTH;
    }
}

-(void)addLabels
{
    NSArray *labelsArray = [[NSArray alloc] initWithObjects:@"1) Partial Response (PR): ≥ 30% reduction",@"Baseline measurements taken as reference.",@"2) Progressive Disease (PD): ≥ 20% increase",@"Sum of smallest diameter during study is taken as reference.",@"3) Stable Disease: Neither PR nor PD",@"Sum of smallest diameter during study is taken as reference.",nil];
    yaxis = labelResult.frame.origin.y+labelResult.frame.size.height+20;
    
    UILabel *lbl;
    for (int index = 0; index < labelsArray.count; index++) {
        lbl = [[UILabel alloc] initWithFrame:CGRectMake(10,yaxis, 300, 40)];
        if (index%2 == 0)
        {
            lbl.frame = [self setLabelProperties:lbl text:[labelsArray objectAtIndex:index] type:0];
        }
        else
        {
            lbl.frame = [self setLabelProperties:lbl text:[labelsArray objectAtIndex:index] type:1];
        }
        [self.scrollView addSubview:lbl];
    }
    scrollView.contentSize = CGSizeMake(scrollView.frame.size.width,yaxis);
}

-(CGRect)setLabelProperties:(UILabel *)lLabel text:(NSString *)lText type:(int)lType
{
    lLabel.textColor     = DEFAULT_COLOR;
    lLabel.lineBreakMode = NSLineBreakByWordWrapping;
    lLabel.numberOfLines = 0;//Dynamic
    lLabel.backgroundColor = [UIColor clearColor];
//    lLabel.adjustsFontSizeToFitWidth = YES;
//    lLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    lLabel.textAlignment = NSTextAlignmentLeft;
    //yasix+label height
    switch (lType) {
        case 0: // heading
        {
            lLabel.font = DEFAULT_BOLD_FONT(16);
            lLabel.text = [NSString stringWithFormat:@" %@",lText];
        }
            break;
        case 1:
        {
            lLabel.font = DEFAULT_FONT(14);
            lLabel.text = [NSString stringWithFormat:@"%@",lText];
        }
            break;
        default:
            break;
    }
    [lLabel sizeToFit];
    // Set the height
    CGSize maximumLabelSize = CGSizeMake(300,9999);
    CGSize titleSize = [lLabel.text sizeWithFont:lLabel.font constrainedToSize:maximumLabelSize lineBreakMode:lLabel.lineBreakMode];
    // DebugLog(@"REgimenDetails: Height: %.f  Width: %.f", titleSize.height, titleSize.width);
    switch (lType) {
        case 0:
        {
            yaxis = yaxis + titleSize.height+ 5;
        }
            break;
        case 1: // content
        {
            yaxis = yaxis + titleSize.height+20;
        }
            break;
        default:
            break;
    }
    //Adjust the label the the new height
    CGRect newFrame = lLabel.frame;
    if(lType == 0)
    {
        newFrame.size.width = 300;
    }
    else
    {
        newFrame.size.width = titleSize.width;
    }
    newFrame.size.height = titleSize.height;
    return newFrame;
}

-(void) addPickerWithDoneButton
{
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        
        
        DebugLog(@"%f", self.view.frame.size.height);
        
        _customView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width , 256)];
        _customView.backgroundColor = [UIColor whiteColor];
        
        
        UILabel *lbl=[[UILabel alloc]initWithFrame:CGRectMake(50, 6, 200, 30)];
        [lbl setText:@"Select mall"];
        lbl.backgroundColor     = [UIColor clearColor];
        lbl.textAlignment       = NSTextAlignmentCenter;
        lbl.textColor           = [UIColor whiteColor];
        lbl.font                = DEFAULT_FONT(18);
        [_customView addSubview:lbl];
        
        UISegmentedControl *doneButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"Done"]];
        doneButton.momentary    = YES;
        doneButton.frame        = CGRectMake(240, 6.0f, 70.0f, 30.0f);
        doneButton.tintColor    = DEFAULT_COLOR;
        doneButton.segmentedControlStyle = UISegmentedControlStyleBar;
        [doneButton addTarget:self action:@selector(donePickerBtnClicked:) forControlEvents:UIControlEventValueChanged];
        [_customView addSubview:doneButton];
        
        UISegmentedControl *cancelButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"Cancel"]];
        cancelButton.momentary    = YES;
        cancelButton.frame        = CGRectMake(10, 6.0f, 70.0f, 30.0f);
        cancelButton.tintColor    = DEFAULT_COLOR;
        cancelButton.segmentedControlStyle = UISegmentedControlStyleBar;
        [cancelButton addTarget:self action:@selector(cancelPickerBtnClicked:) forControlEvents:UIControlEventValueChanged];
        [_customView addSubview:cancelButton];
        
        pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 40, 320, 400)];
        pickerView.delegate = self;
        [_customView addSubview:pickerView];
        
        [self.view addSubview:_customView];
        
        [_customView setHidden:TRUE];
        
    } else {
    
    actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                              delegate:nil
                                     cancelButtonTitle:nil
                                destructiveButtonTitle:nil
                                     otherButtonTitles:nil];
    
    [actionSheet setActionSheetStyle:UIActionSheetStyleBlackOpaque];
    
    CGRect pickerFrame = CGRectMake(0, 40, 0, 0);
    
    pickerView = [[UIPickerView alloc] initWithFrame:pickerFrame];
    pickerView.showsSelectionIndicator = YES;
    pickerView.dataSource = self;
    pickerView.delegate = self;
    [actionSheet addSubview:pickerView];
    
    UISegmentedControl *doneButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"Done"]];
    doneButton.momentary = YES;
    doneButton.frame = CGRectMake(240, 7.0f, 70.0f, 30.0f);
    doneButton.segmentedControlStyle = UISegmentedControlStyleBar;
    doneButton.tintColor = DEFAULT_COLOR;
    [doneButton addTarget:self action:@selector(donePickerBtnClicked:) forControlEvents:UIControlEventValueChanged];
    [actionSheet addSubview:doneButton];
    
    UISegmentedControl *cancelButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"Cancel"]];
    cancelButton.momentary = YES;
    cancelButton.frame = CGRectMake(10, 7.0f, 90.0f, 30.0f);
    cancelButton.segmentedControlStyle = UISegmentedControlStyleBar;
    cancelButton.tintColor = [UIColor blackColor];
    [cancelButton addTarget:self action:@selector(cancelPickerBtnClicked:) forControlEvents:UIControlEventValueChanged];
    [actionSheet addSubview:cancelButton];
        
    }
    
   [pickerView reloadAllComponents];
    if([pickerView numberOfRowsInComponent:0] != -1 && [pickerView numberOfRowsInComponent:2] != -1)
    {
        [pickerView selectRow:0 inComponent:0 animated:YES];
        [pickerView selectRow:0 inComponent:2 animated:YES];
    }
    pickerValue = [NSMutableString stringWithString: @"0 cm 0 mm"];
}

-(void)showPicker
{
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        [_customView setHidden:FALSE];
        [UIView animateWithDuration:0.3
                         animations:^(void){
                             _customView.frame=CGRectMake(0, self.view.frame.size.height - 250, _customView.frame.size.width, _customView.frame.size.height);
                         } completion:^(BOOL finished){
                             
                         }];
        
    } else {
        [actionSheet showInView:self.view];
        [actionSheet setBounds:CGRectMake(0, 0, 320, 485)];
    }
}
- (void) donePickerBtnClicked:(id)sender
{
    DebugLog(@ "currentcase %d pickervalue %@",CURRENTCASE,pickerValue);
    switch (CURRENTCASE) {
        case 4:
        {
            baseSLDTextField.text       = pickerValue;
            baseSLDTextField.font       = DEFAULT_FONT(20);
        }
            break;
        case 5:
        {
            currentSLDTextField.text    = pickerValue;
            currentSLDTextField.font    = DEFAULT_FONT(20);
        }
            break;
        default:
            break;
    }
    [self compute];
    [self cancelPickerBtnClicked:nil];
}
- (void) cancelPickerBtnClicked:(id)sender
{
    switch (CURRENTCASE) {
        case 4:
        {
            [imgViewBaseSLD setHidden:NO];
            [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:viewBaseSLD];
        }
            break;
        case 5:
        {
            [imgViewCurrentSLD setHidden:NO];
            [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:viewCurrentSLD];
        }
            break;
        default:
            break;
    }
    CURRENTCASE = 0;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        
        [UIView animateWithDuration:0.3
                         animations:^(void){
                             _customView.frame=CGRectMake(0, self.view.frame.size.height, _customView.frame.size.width, _customView.frame.size.height);
                         } completion:^(BOOL finished){
                             [_customView setHidden:TRUE];
                         }];
    } else {
    
        [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
    }
    [self scrollTobottom];
}

#pragma mark -
#pragma mark Picker Data Source Methods
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
	return 4;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    switch (component) {
        case 1:
        case 3:
            return 1;
            break;
        case 0:
            return 26;
        case 2:
            return 10;
            break;
        default:
            break;
    }
    return 0;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    switch (component){
        case 0:
        case 2 :
            return 80.0f;
        case 1:
        case 3:
            return 70.0f;
    }
    return 0;
}

#pragma mark Picker Delegate Methods

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row   forComponent:(NSInteger)component
{
    switch (component) {
        case 0:
            return [NSString stringWithFormat:@"%d",row];
            break;
        case 1:
            return @"cm";
            break;
        case 2:
            return [NSString stringWithFormat:@"%d",row];
        case 3:
            return @"mm";
            break;
            break;
        default:
            break;
    }
    return @"";
}

- (void)pickerView:(UIPickerView *)thePickerView  didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    NSInteger row_one = [thePickerView selectedRowInComponent:0];
	NSInteger row_two = [thePickerView selectedRowInComponent:2];
    
    pickerValue = [NSMutableString stringWithString:[NSString stringWithFormat:@"%d cm %d mm",row_one,row_two]];
    switch (CURRENTCASE)
    {
        case 4:
        {
            baseSLDValue = [[NSString stringWithFormat:@"%d.%d",row_one,row_two] doubleValue];
        }
            break;
        case 5:
        {
            currentSLDValue = [[NSString stringWithFormat:@"%d.%d",row_one,row_two] doubleValue];
        }
            break;
        default:
            break;
    }
}

#pragma UITextFieldDelegate methods
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if([textField isEqual:baseSLDTextField])
    {
        [baseSLDTextField resignFirstResponder];
        [imgViewBaseSLD setHidden:YES];
        [scrollView setContentOffset:CGPointMake(0,viewBaseSLD.frame.origin.y-100) animated:YES];
        [CommonCallback changeTextFieldColor:TEXT_EDITING view:viewBaseSLD];
    }
    else if([textField isEqual:currentSLDTextField])
    {
        [baseSLDTextField resignFirstResponder];
        [imgViewCurrentSLD setHidden:YES];
        [scrollView setContentOffset:CGPointMake(0,viewCurrentSLD.frame.origin.y-100) animated:YES];
        [CommonCallback changeTextFieldColor:TEXT_EDITING view:viewCurrentSLD];
    }
    scrollView.contentSize = CGSizeMake(scrollView.frame.size.width, yaxis+300);
    CURRENTCASE = textField.tag;
    [self showPicker];
    return NO;
}

-(void)scrollTobottom
{
    [UIView animateWithDuration:0.2 animations:^{
        scrollView.contentSize = CGSizeMake(scrollView.frame.size.width, yaxis);
        CGPoint bottomOffset = CGPointMake(0, 0);
        [scrollView setContentOffset:bottomOffset animated:YES];
    }];
}


-(void)compute{
    DebugLog(@"%f , %f",baseSLDValue,currentSLDValue);
    if (![baseSLDTextField.text isEqualToString:@""] && ![currentSLDTextField.text isEqualToString:@""])
    {
        [self removeErrorMessageFromLabel:lblErrBaseSLD];
        [self removeErrorMessageFromLabel:lblErrCurrentSLD];
        
        if (baseSLDValue == currentSLDValue)
        {
            labelResult.text=@"0 %";
        }
        else if (baseSLDValue == 0)
        {
            labelResult.text=[NSString stringWithFormat:@"%.2f %%",currentSLDValue];
            
        }else
        {
            labelResult.text=[NSString stringWithFormat:@"%.2f %%",((currentSLDValue-baseSLDValue)/baseSLDValue)*100];
        }
    }else if(![baseSLDTextField.text isEqualToString:@""] && [currentSLDTextField.text isEqualToString:@""])
    {
        [self setErrorMessageOnLabel:@"Select current sum of target lesions" label:lblErrCurrentSLD];
    }
    else if([baseSLDTextField.text isEqualToString:@""] && ![currentSLDTextField.text isEqualToString:@""])
    {
        [self setErrorMessageOnLabel:@"Select baseline measurement" label:lblErrBaseSLD];
    }
    else
    {
        labelResult.text = @"....";
    }
}

-(void)show:(double)result{
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [formatter setMaximumFractionDigits:2];
    [formatter setRoundingMode: NSNumberFormatterRoundCeiling];
    DebugLog(@"result %f",result);
  //  labelResult.text=[NSString stringWithFormat:@"%@ %%",[formatter stringFromNumber:[NSNumber numberWithFloat:result]]];
    labelResult.text=[NSString stringWithFormat:@"%.2f %%",result];
}

- (IBAction)infoButtonAction:(id)sender {
    NSDictionary *params = @{@"calculator_name":self.title};
    [INEventLogger logEvent:@"MedCalc_Calculator_Info_Image" withParams:params];
    
    MedicalInfoImageViewController *typedetailController = [[MedicalInfoImageViewController alloc] initWithimageName:@"recist_info.png"];
    [self.navigationController pushViewController:typedetailController animated:YES];
}

-(void)setErrorMessageOnLabel:(NSString *)errorMessage label:(UILabel *)lbl{
    labelResult.text    =   @"....";
    lbl.hidden = NO;
    lbl.text = errorMessage;
    if ([lbl isEqual:lblErrBaseSLD]) {
        [CommonCallback changeTextFieldColor:TEXT_ERROR view:viewBaseSLD];
    }else if ([lbl isEqual:lblErrCurrentSLD]) {
        [CommonCallback changeTextFieldColor:TEXT_ERROR view:viewCurrentSLD];
    }
}

-(void)removeErrorMessageFromLabel:(UILabel *)lbl{
    lbl.hidden = YES;
    if ([lbl isEqual:lblErrBaseSLD]) {
        [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:viewBaseSLD];
    }else if ([lbl isEqual:lblErrCurrentSLD]) {
        [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:viewCurrentSLD];
    }
}
@end
