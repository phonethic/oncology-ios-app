//
//  MedicalJournalsViewController.h
//  Medical
//
//  Created by Kirti Nikam on 11/10/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MedicalJournalsViewController : UIViewController
@property (strong,nonatomic) NSMutableArray *journalCatgArray;
@property (strong, nonatomic) IBOutlet UITableView *journalsTableView;
@end
