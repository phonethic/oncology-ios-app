//
//  MedicalJournalOncologyViewController.h
//  Medical
//
//  Created by Kirti Nikam on 17/11/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MedicalJournalCategorylistViewController : UIViewController<UITableViewDataSource,UISearchBarDelegate>
{
    BOOL isSearchOn;
    BOOL canSelectRow;
    double keyboardHeight;
}
@property(copy,nonatomic) NSString *journal_category;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UITableView *jtableView;
@end
