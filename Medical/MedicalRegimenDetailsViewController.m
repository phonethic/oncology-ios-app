//
//  MedicalRegimenDetailsViewController.m
//  Medical
//
//  Created by Rishi on 26/10/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import "MedicalRegimenDetailsViewController.h"
#import "MedicalAppDelegate.h"
#import "MedicalCarboplatinViewController.h"
#import "MedicalWebViewController.h"
#import "MedicalUserDefaults.h"
#import "CommonCallback.h"
@interface MedicalRegimenDetailsViewController ()

@end

@implementation MedicalRegimenDetailsViewController
@synthesize lblKg,lblcm;
@synthesize regimenId,lblRegimenNameHeader,txtPatientName,txtPatientAge,txtPatientBSA,txtPatientWeight,txtPatientHeight;
@synthesize BSABtn;
@synthesize regimenDetailScrollview;
@synthesize preDayArray,BSAArray;  //To store BSA results
@synthesize detailClinicalPearls,detailsReferences,detailsReferencesLink;
@synthesize regionWarnResult;
@synthesize lblCollection,txtCollection,viewCollection;
@synthesize viewPatientAge,transviewPatientBSA,viewPatientBSA,viewPatientHeight,viewPatientName,viewPatientWeight;
@synthesize detailView,detailTextView,detailHeader,detailOK,referencebtn;
@synthesize backView;
@synthesize regimenName;
@synthesize cancerName;
@synthesize lblErrCollection,lblErrBSA,lblErrWeight,lblErrHeight;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
//    viewPatientBSA.frame = CGRectMake(viewPatientAge.frame.origin.x, viewPatientBSA.frame.origin.y, viewPatientBSA.frame.size.width, viewPatientBSA.frame.size.height);
//    BSABtn.frame = CGRectMake(CGRectGetMaxX(viewPatientBSA.frame)+3, viewPatientBSA.frame.origin.y, BSABtn.frame.size.width, BSABtn.frame.size.height);
//    DebugLog(@"RegimenDetail viewWillAppear");
//    if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation))
//    {
//        DebugLog(@"RegimenDetail already in Portrait");
//    }
//    else
//    {
//        UIViewController *c = [[UIViewController alloc]init];
//        [self presentModalViewController:c animated:NO];
//        [self dismissModalViewControllerAnimated:NO];
////            [self presentViewController:c animated:NO completion:^{
////                [self dismissViewControllerAnimated:NO completion:nil];
////            }];
//    }
//    UIViewController *c = [[UIViewController alloc]init];
////    [self presentModalViewController:c animated:NO];
////    [self dismissModalViewControllerAnimated:NO];
//    [self presentViewController:c animated:NO completion:^{
//        [self dismissViewControllerAnimated:NO completion:nil];
//    }];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UIBarButtonItem *back = [[UIBarButtonItem alloc] initWithTitle:@"Back"
                                                             style:UIBarButtonItemStylePlain
                                                            target:nil
                                                            action:nil];
    [[self navigationItem] setBackBarButtonItem:back];
    
    DebugLog(@"RegimenDetails : regimenId %@ warnValue %d" ,regimenId,regionWarnResult);
    if ([cancerName length] > 0 || [regimenName length] > 0) {
        NSDictionary *params = @{@"cancer_name":cancerName, @"regimen_name" : regimenName};
        [INEventLogger logEvent:@"Chemo_Orders_Regimen_Details" withParams:params];
    }
    
//    if ([MEDICAL_APP_DELEGATE PRINTEMAILALERT] == 0) //Take out by mail 12th May 2014
//    {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Print/Email Message" message:@"To Print/Email turn the device horizontal." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil,nil];
//        [alert show];
//        
//        [MEDICAL_APP_DELEGATE setPRINTEMAILALERT:1];
//    }
    
//    if(regionWarnResult)
//    {
//        UIAlertView *regimenalert = [[UIAlertView alloc] initWithTitle:CARBOPLATIN_WARN message:[ NSString stringWithFormat:@"The doses of drugs are calculated automatically according to body surface area (%@). For drugs using alternate dosing (mg/kg, unit, carboplatin, fixed dosing), please RECALCULATE the dose.",MGM2] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//        [regimenalert show];
//    }
    
    preDayArray = [[NSMutableArray alloc] init];
    
    lblRegimenNameHeader.text = regimenName;

    UIBarButtonItem *actionBarBtn = [[UIBarButtonItem alloc]
                                  initWithBarButtonSystemItem:UIBarButtonSystemItemAction
                                  target:self action:@selector(shareBarBtnClicked:)];
    
    UIButton *infoBtn = [UIButton buttonWithType:UIButtonTypeInfoLight];
    infoBtn.frame = CGRectMake(200, 2, 30, 30);
    [infoBtn addTarget:self action:@selector(infoBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *infoBarBtn = [[UIBarButtonItem alloc] initWithCustomView:infoBtn];
    
    self.navigationItem.rightBarButtonItems = @[actionBarBtn,infoBarBtn];
    
    UITapGestureRecognizer *viewTap = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapDetected:)];
    viewTap.numberOfTapsRequired = 1;
    viewTap.cancelsTouchesInView = NO;
    [self.regimenDetailScrollview  addGestureRecognizer:viewTap];    
    
    [self setUI];
    [self readcancerFromDatabase];
    
    DebugLog(@"[MedicalUserDefaults getChemoOrderHelpInfoSceenCount] %d",[MedicalUserDefaults getChemoOrderHelpInfoSceenCount]);
    if ([MedicalUserDefaults getChemoOrderHelpInfoSceenCount] < 3) {
//        [self performSelector:@selector(infoBtnPressed) withObject:nil afterDelay:2.0];
        [MedicalUserDefaults setChemoOrderHelpInfoSceenCount];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Information" message:@"Click on 'i' button for tips to navigate Chemotherapy Orders" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil,nil];
        [alert show];
    }
}

- (void)viewDidUnload
{
    [self setLblRegimenNameHeader:nil];
    [self setTxtPatientName:nil];
    [self setTxtPatientAge:nil];
    [self setTxtPatientBSA:nil];
    [self setBSABtn:nil];
    [self setRegimenDetailScrollview:nil];
    [self setTxtPatientWeight:nil];
    [self setTxtPatientHeight:nil];
    [self setDetailView:nil];
    [self setDetailTextView:nil];
    [self setDetailHeader:nil];
    [self setDetailOK:nil];
    [self setLblKg:nil];
    [self setLblcm:nil];
    [self setReferencebtn:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark - Rotation
//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
//    DebugLog(@"shouldAutorotateToInterfaceOrientation %u",toInterfaceOrientation);
//    
//    return (toInterfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
//}
//- (BOOL)shouldAutorotate
//{
//    return YES;
//}
//
//- (NSUInteger)supportedInterfaceOrientations
//{
//    return UIInterfaceOrientationMaskAllButUpsideDown;
//}
#pragma mark - Rotation
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}


- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
//    viewPatientBSA.frame = CGRectMake(viewPatientAge.frame.origin.x, viewPatientBSA.frame.origin.y, viewPatientBSA.frame.size.width, viewPatientBSA.frame.size.height);
//    BSABtn.frame = CGRectMake(CGRectGetMaxX(viewPatientBSA.frame)+3, viewPatientBSA.frame.origin.y, BSABtn.frame.size.width, BSABtn.frame.size.height);
}

//- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
//{
//    
//    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
//    if( UIInterfaceOrientationIsLandscape(fromInterfaceOrientation) && (self.interfaceOrientation == UIInterfaceOrientationPortrait))
//    {
//        // Adjust your text field...
//        DebugLog(@"from UIInterfaceOrientationIsLandscape");
//
//        //   lbltxttxttxtPatientName.frame=CGRectMake(lbltxttxtPatientName.frame.origin.x, lbltxttxtPatientName.frame.origin.y, lbltxttxtPatientName.frame.size.width, lbltxttxtPatientName.frame.size.height);
//        
//        txtPatientName.frame=CGRectMake(txtPatientName.frame.origin.x-20, txtPatientName.frame.origin.y, self.view.frame.size.width-self.view.frame.size.width*0.29, txtPatientName.frame.size.height);
//        
//        //  lbltxtPatientAge.frame=CGRectMake(lbltxtPatientAge.frame.origin.x-50, lbltxtPatientAge.frame.origin.y, lbltxtPatientAge.frame.size.width, lbltxtPatientAge.frame.size.height);
//        
//        txtPatientAge.frame=CGRectMake(txtPatientAge.frame.origin.x-20, txtPatientAge.frame.origin.y,self.view.frame.size.width-self.view.frame.size.width*0.87, txtPatientAge.frame.size.height);
//        
//        BSABtn.frame=CGRectMake(txtPatientAge.frame.origin.x+txtPatientAge.frame.size.width+10, BSABtn.frame.origin.y, self.view.frame.size.width-self.view.frame.size.width*0.73, BSABtn.frame.size.height);
//        
//        txtPatientBSA.frame=CGRectMake(BSABtn.frame.origin.x+BSABtn.frame.size.width+10, txtPatientBSA.frame.origin.y, self.view.frame.size.width-self.view.frame.size.width*0.75, txtPatientBSA.frame.size.height);
//        
//        
//        txtPatientWeight.frame=CGRectMake(txtPatientWeight.frame.origin.x-20, txtPatientWeight.frame.origin.y, txtPatientWeight.frame.size.width, txtPatientWeight.frame.size.height);
//        
//        txtPatientHeight.frame=CGRectMake(txtPatientHeight.frame.origin.x-40, txtPatientHeight.frame.origin.y, txtPatientHeight.frame.size.width, txtPatientHeight.frame.size.height);
//        
//        lblKg.frame=CGRectMake(lblKg.frame.origin.x-20, lblKg.frame.origin.y, lblKg.frame.size.width, lblKg.frame.size.height);
//        lblcm.frame=CGRectMake(lblcm.frame.origin.x-40, lblcm.frame.origin.y, lblcm.frame.size.width, lblcm.frame.size.height);
//        
//        //   detailView.frame=CGRectMake(detailView.frame.origin.x-70, detailView.frame.origin.y+100, detailView.frame.size.width, detailView.frame.size.height);
//        
//        for(NSString *tagid in BSAArray) {
//            
//            UITextField *doseText = (UITextField *)[regimenDetailScrollview viewWithTag:[tagid intValue]+1000];
//            //DebugLog(@"---%f-----",[self.txtPatientBSA.text doubleValue]);
//            doseText.frame=CGRectMake(doseText.frame.origin.x-40, doseText.frame.origin.y, doseText.frame.size.width, doseText.frame.size.height);
//            
//            UILabel *lblunits = (UILabel *)[regimenDetailScrollview viewWithTag:[tagid intValue]+500];
//            //DebugLog(@"---%f-----",[self.txtPatientBSA.text doubleValue]);
//            lblunits.frame=CGRectMake(lblunits.frame.origin.x-40, lblunits.frame.origin.y, lblunits.frame.size.width, lblunits.frame.size.height);
//            
//            UILabel *lblequalto = (UILabel *)[regimenDetailScrollview viewWithTag:[tagid intValue]+600];
//            //DebugLog(@"---%f-----",[self.txtPatientBSA.text doubleValue]);
//            lblequalto.frame=CGRectMake(lblequalto.frame.origin.x-80, lblequalto.frame.origin.y, lblequalto.frame.size.width, lblequalto.frame.size.height);
//            
//            UITextField *bsaCalcText = (UITextField *)[regimenDetailScrollview viewWithTag:[tagid intValue]];
//            //DebugLog(@"---%f-----",[self.txtPatientBSA.text doubleValue]);
//            bsaCalcText.frame=CGRectMake(bsaCalcText.frame.origin.x-80, bsaCalcText.frame.origin.y, bsaCalcText.frame.size.width, bsaCalcText.frame.size.height);
//            
//            UILabel *lblmg = (UILabel *)[regimenDetailScrollview viewWithTag:[tagid intValue]+5000];
//            //DebugLog(@"---%f-----",[self.txtPatientBSA.text doubleValue]);
//            lblmg.frame=CGRectMake(lblmg.frame.origin.x-80, lblmg.frame.origin.y, lblmg.frame.size.width, lblmg.frame.size.height);
//            
//        }
//        
//        for (int i=4000; i<4004; i++) {
//            UITextView *tempTextView = (UITextView *)[regimenDetailScrollview viewWithTag:i];
//            //DebugLog(@"---%f-----",[self.txtPatientBSA.text doubleValue]);
//            tempTextView.frame=CGRectMake(tempTextView.frame.origin.x, tempTextView.frame.origin.y,self.view.frame.size.width-self.view.frame.size.width*0.1, tempTextView.frame.size.height);
//            
//        }
//        
//    } else if( UIInterfaceOrientationIsPortrait(fromInterfaceOrientation))
//    {
//        // Adjust your text field...
//        DebugLog(@"from UIInterfaceOrientationIsPortrait");
//        //   lbltxttxtPatientName.frame=CGRectMake(lbltxttxtPatientName.frame.origin.x, lbltxttxtPatientName.frame.origin.y, lbltxttxtPatientName.frame.size.width, lbltxtPatientName.frame.size.height);
//        
//        txtPatientName.frame=CGRectMake(txtPatientName.frame.origin.x+20, txtPatientName.frame.origin.y,self.view.frame.size.width-self.view.frame.size.width*0.29, txtPatientName.frame.size.height);
//        
//        //    lbltxtPatientAge.frame=CGRectMake(lbltxtPatientAge.frame.origin.x+50, lbltxtPatientAge.frame.origin.y, lbltxtPatientAge.frame.size.width, lbltxtPatientAge.frame.size.height);
//        
//        txtPatientAge.frame=CGRectMake(txtPatientAge.frame.origin.x+20, txtPatientAge.frame.origin.y, self.view.frame.size.width-self.view.frame.size.width*0.87, txtPatientAge.frame.size.height);
//        
//        //DebugLog(@"txtPatientNametext %f %f %f %f",txtPatientName.frame.origin.x,txtPatientName.frame.origin.y, txtPatientName.frame.size.width, txtPatientName.frame.size.height);
//        BSABtn.frame=CGRectMake(txtPatientAge.frame.origin.x+txtPatientAge.frame.size.width+10, BSABtn.frame.origin.y, self.view.frame.size.width-self.view.frame.size.width*0.73, BSABtn.frame.size.height);
//        
//        txtPatientBSA.frame=CGRectMake(BSABtn.frame.origin.x+BSABtn.frame.size.width+10, txtPatientBSA.frame.origin.y, self.view.frame.size.width-self.view.frame.size.width*0.74, txtPatientBSA.frame.size.height);
//        //DebugLog(@"txtPatientBSA %f %f %f %f",txtPatientBSA.frame.origin.x,txtPatientBSA.frame.origin.y, txtPatientBSA.frame.size.width, txtPatientBSA.frame.size.height);
//        
//        //   lblWeight.frame=CGRectMake(lblWeight.frame.origin.x, lblWeight.frame.origin.y, lblWeight.frame.size.width, lblWeight.frame.size.height);
//        
//        txtPatientWeight.frame=CGRectMake(txtPatientWeight.frame.origin.x+20, txtPatientWeight.frame.origin.y, txtPatientWeight.frame.size.width, txtPatientWeight.frame.size.height);
//        
//        txtPatientHeight.frame=CGRectMake(txtPatientHeight.frame.origin.x+40, txtPatientHeight.frame.origin.y, txtPatientHeight.frame.size.width, txtPatientHeight.frame.size.height);
//        
//        lblKg.frame=CGRectMake(lblKg.frame.origin.x+20, lblKg.frame.origin.y, lblKg.frame.size.width, lblKg.frame.size.height);
//        lblcm.frame=CGRectMake(lblcm.frame.origin.x+40, lblcm.frame.origin.y, lblcm.frame.size.width, lblcm.frame.size.height);
//        
//        //  detailView.frame=CGRectMake(detailView.frame.origin.x+70, detailView.frame.origin.y-100, detailView.frame.size.width, detailView.frame.size.height);
//        
//        for(NSString *tagid in BSAArray) {
//            
//            UITextField *doseText = (UITextField *)[regimenDetailScrollview viewWithTag:[tagid intValue]+1000];
//            //DebugLog(@"---%f-----",[self.txtPatientBSA.text doubleValue]);
//            doseText.frame=CGRectMake(doseText.frame.origin.x+40, doseText.frame.origin.y, doseText.frame.size.width, doseText.frame.size.height);
//            
//            UILabel *lblunits = (UILabel *)[regimenDetailScrollview viewWithTag:[tagid intValue]+500];
//            //DebugLog(@"---%f-----",[self.txtPatientBSA.text doubleValue]);
//            lblunits.frame=CGRectMake(lblunits.frame.origin.x+40, lblunits.frame.origin.y, lblunits.frame.size.width, lblunits.frame.size.height);
//            
//            UILabel *lblequalto = (UILabel *)[regimenDetailScrollview viewWithTag:[tagid intValue]+600];
//            //DebugLog(@"---%f-----",[self.txtPatientBSA.text doubleValue]);
//            lblequalto.frame=CGRectMake(lblequalto.frame.origin.x+80, lblequalto.frame.origin.y, lblequalto.frame.size.width, lblequalto.frame.size.height);
//            
//            UITextField *bsaCalcText = (UITextField *)[regimenDetailScrollview viewWithTag:[tagid intValue]];
//            //DebugLog(@"---%f-----",[self.txtPatientBSA.text doubleValue]);
//            bsaCalcText.frame=CGRectMake(bsaCalcText.frame.origin.x+80, bsaCalcText.frame.origin.y, bsaCalcText.frame.size.width, bsaCalcText.frame.size.height);
//            
//            UILabel *lblmg = (UILabel *)[regimenDetailScrollview viewWithTag:[tagid intValue]+5000];
//            //DebugLog(@"---%f-----",[self.txtPatientBSA.text doubleValue]);
//            lblmg.frame=CGRectMake(lblmg.frame.origin.x+80, lblmg.frame.origin.y, lblmg.frame.size.width, lblmg.frame.size.height);
//        }
//        
//        for (int i=4000; i<4004; i++) {
//            UITextView *commentsTextView = (UITextView *)[regimenDetailScrollview viewWithTag:i];
//            //DebugLog(@"---%f-----",[self.txtPatientBSA.text doubleValue]);
//            commentsTextView.frame=CGRectMake(commentsTextView.frame.origin.x, commentsTextView.frame.origin.y,self.view.frame.size.width-self.view.frame.size.width*0.1, commentsTextView.frame.size.height);
//            
//        }
//    }
//}


-(void)infoBtnPressed
{
    DebugLog(@"infoBtnPressed");
    [INEventLogger logEvent:@"Chemo_Orders_Details_i_Button"];
    if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
        [self openWebPage:@"How to navigate Chemotherapy Orders" url:CHEMOORDERS_HELP_INFO];
    }else{
        [self openWebPage:@"How to navigate Chemotherapy Orders" url:CHEMOORDERS_HELP_INFO_iOS7];
    }
    
}

-(void)closeWebView
{
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        DebugLog(@"closeWebView");
    }];
}

-(void)openWebPage:(NSString *)title url:(NSString *)urlLink{
    if ([urlLink length] > 0) {
        MedicalWebViewController *webViewController = [[MedicalWebViewController alloc] initWithNibName:@"MedicalWebViewController" bundle:nil];
        webViewController.title = @"";
        webViewController.medicalLink = urlLink;
        [self.navigationController pushViewController:webViewController animated:YES];
        
//        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:webViewController];
//        UIButton *closeBtn =  [UIButton buttonWithType:UIButtonTypeCustom];
//        [closeBtn setFrame:CGRectMake(276, 0, 44, 44)];
//        [closeBtn setTitle:@"" forState:UIControlStateNormal];
//        [closeBtn setImage:[UIImage imageNamed:@"cancel_white.png"] forState:UIControlStateNormal];
//        [closeBtn addTarget:self action:@selector(closeWebView) forControlEvents:UIControlEventTouchUpInside];
//        closeBtn.autoresizingMask =  UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin;
//        [navController.navigationBar addSubview:closeBtn];
//        [navController.navigationBar setTranslucent:NO];
//        [self.navigationController presentViewController:navController animated:YES completion:nil];
    }
}

-(void)setUI{

    lblRegimenNameHeader.backgroundColor     = [UIColor clearColor];
    lblRegimenNameHeader.textColor           = DEFAULT_COLOR;
    lblRegimenNameHeader.font                = DEFAULT_BOLD_FONT(22);
    lblRegimenNameHeader.numberOfLines       = 3;
    
    for (UIView *lview in viewCollection) {
        lview.backgroundColor     = [UIColor whiteColor];
        lview.layer.borderColor   = [UIColor lightGrayColor].CGColor;
        lview.layer.borderWidth   = 1.0;
    }
    
    for (UITextField *txt in txtCollection) {
        txt.delegate            = self;
        txt.backgroundColor     = [UIColor clearColor];
        txt.textColor           = [UIColor blackColor];
        txt.font                = DEFAULT_FONT(22);
        txt.keyboardType        = UIKeyboardTypeDecimalPad;
        txt.textAlignment       = NSTextAlignmentCenter;
        txt.layer.borderColor   = [UIColor clearColor].CGColor;
    }
    
    for (UILabel *lbl in lblCollection) {
        lbl.backgroundColor     = [UIColor clearColor];
        lbl.textColor           = DEFAULT_COLOR;
        lbl.font                = DEFAULT_SEMIBOLD_FONT(20);
    }
    
    for (UILabel *lbl in lblErrCollection) {
        lbl.backgroundColor     = [UIColor clearColor];
        lbl.textColor           = [UIColor redColor];
        lbl.font                = DEFAULT_SEMIBOLD_FONT(12);
        lbl.hidden              = YES;
    }
    
    BSABtn.backgroundColor     = DEFAULT_COLOR;
    BSABtn.titleLabel.font     = DEFAULT_BOLD_FONT(18);
    BSABtn.layer.cornerRadius  = 5.0;
    [BSABtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [BSABtn setTitleColor:[UIColor colorWithWhite:1.0 alpha:0.5] forState:UIControlStateHighlighted];
    
    detailView.backgroundColor      = [UIColor whiteColor];
    detailView.layer.shadowColor      = [UIColor blackColor].CGColor;
    detailView.layer.shadowOffset     = CGSizeMake(1, 1);
    detailView.layer.shadowOpacity    = 1.0;
    detailView.layer.shadowRadius     = 10.0;
    [detailView setHidden:YES];
    [backView setHidden:YES];
    
    detailHeader.textAlignment     = NSTextAlignmentCenter;
    detailHeader.backgroundColor   = [UIColor clearColor];
    detailHeader.font              = DEFAULT_BOLD_FONT(20);
    detailHeader.textColor         = DEFAULT_COLOR;
    detailHeader.adjustsFontSizeToFitWidth = YES;
    
    detailTextView.backgroundColor     = [UIColor clearColor];
    detailTextView.textColor           = [UIColor blackColor];
    detailTextView.font                = DEFAULT_FONT(15);
    detailTextView.textAlignment       = NSTextAlignmentCenter;
    detailTextView.editable            = FALSE;
    
    referencebtn.backgroundColor     = [UIColor clearColor];
    referencebtn.titleLabel.font     = DEFAULT_BOLD_FONT(18);
    [referencebtn setTitleColor:DEFAULT_COLOR forState:UIControlStateNormal];
    [referencebtn setTitleColor:DEFAULT_COLOR_WITH_ALPHA(0.5) forState:UIControlStateHighlighted];
    
    detailOK.backgroundColor     = DEFAULT_COLOR;
    detailOK.titleLabel.font     = DEFAULT_BOLD_FONT(18);
    detailOK.layer.cornerRadius  = 5.0;
    [detailOK setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [detailOK setTitleColor:[UIColor colorWithWhite:1.0 alpha:0.5] forState:UIControlStateHighlighted];
}

-(void)tapDetected:(UIGestureRecognizer *)sender{
    [self.view endEditing:YES];
    [regimenDetailScrollview setContentSize:CGSizeMake(self.view.frame.size.width, yaxis)];

//    CGPoint tapLocation = [sender locationInView:regimenDetailScrollview];
//    UIView *view = [regimenDetailScrollview hitTest:tapLocation withEvent:nil];
//    if (![view isKindOfClass:[UIButton class]] && ![view isKindOfClass:[UITextField class]] && ![view isKindOfClass:[UITextView class]]) {
//        [self scrollTobottom];
//    }
}

- (void) shareBarBtnClicked:(id)sender
{
    for (UIButton *but in [self.regimenDetailScrollview subviews])
    {
        if ([but isKindOfClass:[UIButton class]])
        {
            if (but.selected)
            {
                if (but.tag != AddPreMedicationCheckBoxTag)
                {
                    but.backgroundColor = [UIColor whiteColor];
                    [but setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                }
            }
            else
            {
                [but setHidden:TRUE];                
                if (but.tag == AddPreMedicationCheckBoxTag)
                {
                    UITextView *commentsTextView = (UITextView *)[regimenDetailScrollview viewWithTag:AddPreMedicationTextViewTag];
                    [commentsTextView setHidden:TRUE];
                }
            }
        }
    }
    [self showAlert];
}
- (void)showAlert {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:WARNING_TITLE
                                                        message:WARNING_MSG_FOR_PRINT_EMAIL
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
	[alertView show];
}

#pragma UIAlertView Delegate methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	DebugLog(@"buttonIndex = %d",buttonIndex);
    [self performSelector:@selector(showActionsheet) withObject:nil afterDelay:0.2];
}

-(void)showActionsheet
{
	UIActionSheet *uiActionSheetP= [[UIActionSheet alloc]
                                    initWithTitle: @""
                                    delegate:self
                                    cancelButtonTitle:@"CANCEL"
                                    destructiveButtonTitle:nil
                                    otherButtonTitles:@"Print",@"Email", nil];
	
	uiActionSheetP.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
	[uiActionSheetP showInView:self.navigationController.view];
    
}

#pragma UIActionSheet Delegate methods
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    DebugLog(@"buttonIndex = %d",buttonIndex);
    [self.view endEditing:YES];
    CGRect oldFrame = regimenDetailScrollview.frame;
    regimenDetailScrollview.frame=CGRectMake(regimenDetailScrollview.frame.origin.x, regimenDetailScrollview.frame.origin.y, regimenDetailScrollview.contentSize.width, regimenDetailScrollview.contentSize.height);
    [self changeLabelHeaderColor:@"hide"];
    
	if (buttonIndex == 0)
    {
        NSDictionary *params = @{@"cancer_name":cancerName, @"regimen_name" : regimenName, @"share_via" : @"print"};
        [INEventLogger logEvent:@"Chemo_Orders_Share" withParams:params];
        [MEDICAL_APP_DELEGATE printImage:regimenDetailScrollview];
    } else if (buttonIndex == 1)
    {
        NSDictionary *params = @{@"cancer_name":cancerName, @"regimen_name" : regimenName,@"share_via" : @"email"};
        [INEventLogger logEvent:@"Chemo_Orders_Share" withParams:params];
        [MEDICAL_APP_DELEGATE mailImage:regimenDetailScrollview];
    }
    
	regimenDetailScrollview.frame=oldFrame;
    [self unhideAllRadioButtons];
    [self changeLabelHeaderColor:@"unhide"];
}

-(void)unhideAllRadioButtons
{
    for (UIButton *but in [self.regimenDetailScrollview subviews]) {
        if ([but isKindOfClass:[UIButton class]]) {
            if (but.selected)
            {
                if (but.tag != AddPreMedicationCheckBoxTag)
                {
                    but.backgroundColor = DEFAULT_COLOR;
                    [but setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                }
            }
            else
            {
                [but setHidden:FALSE];
                if (but.tag == AddPreMedicationCheckBoxTag)
                {
                    UITextView *commentsTextView = (UITextView *)[regimenDetailScrollview viewWithTag:AddPreMedicationTextViewTag];
                    [commentsTextView setHidden:FALSE];
                }
            }
        }
    }
}
-(void)changeLabelHeaderColor:(NSString *)duringValue
{
    UILabel *tempLabel;
    for (int headerindex = PREMEDICATIONHEADERTAG; headerindex <= COMMENTSHEADERTAG; headerindex ++)
    {
        tempLabel = (UILabel *)[regimenDetailScrollview viewWithTag:headerindex];
        if([duringValue isEqualToString:@"hide"])
        {
            tempLabel.backgroundColor = [UIColor whiteColor];
            tempLabel.textColor = [UIColor blackColor];
        }
        else
        {
            tempLabel.backgroundColor = DEFAULT_COLOR;
            tempLabel.textColor = [UIColor whiteColor];
        }        
    }
}

- (void)setPlaceholder
{
    UITextView *tempTextView = (UITextView *)[regimenDetailScrollview viewWithTag:textViewTag];
    if (textViewTag == 4002)
    {
        tempTextView.text = NSLocalizedString(@"Enter your premedication comments here. [Optional]", @"placeholder");
    }
    else
    {
        tempTextView.text = NSLocalizedString(@"Enter your comments here.", @"placeholder");
    }
    tempTextView.textColor = [UIColor lightGrayColor];
}

#pragma Database callbacks
-(void) readcancerFromDatabase {
	sqlite3 *database;
    
	if(sqlite3_open([[MEDICAL_APP_DELEGATE databasePath] UTF8String], &database) == SQLITE_OK)
    {
        NSString *regimenquerySQL = [NSString stringWithFormat:@"select premedication_day from premedication where region_id=\"%@\" group by premedication_day", self.regimenId];
        const char *sqlStatement = [regimenquerySQL UTF8String];
		sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
        {
			while(sqlite3_step(compiledStatement) == SQLITE_ROW)
            {
                NSString *preMedicationDay = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 0)];
                [preDayArray addObject:preMedicationDay];                
            }
        }
		sqlite3_finalize(compiledStatement);
	}
	sqlite3_close(database);
    [self readpremedicationFromDatabase];
}

-(void) readpremedicationFromDatabase {
	sqlite3 *database;
	if(sqlite3_open([[MEDICAL_APP_DELEGATE databasePath] UTF8String], &database) == SQLITE_OK)
    {
        sqlite3_stmt *compiledStatement;
        yaxis = CGRectGetMaxY(lblErrBSA.frame) + SECTIONSPACE;
        UILabel *lblPremedicationHead   = [[UILabel alloc] initWithFrame:CGRectMake(10,yaxis, 300, 50)];
        lblPremedicationHead.frame      = [self setLabelProperties:lblPremedicationHead text:@"Premedication : " type:0];
        lblPremedicationHead.tag        = PREMEDICATIONHEADERTAG;
        lblPremedicationHead.textAlignment = NSTextAlignmentLeft;
        [self.regimenDetailScrollview addSubview:lblPremedicationHead];
        
        DebugLog(@"RegimenDetails : preDayArray %@ ",preDayArray);
        for (NSString *str in preDayArray)
        {
            UILabel *lblPremedicationDay = [[UILabel alloc] initWithFrame:CGRectMake(10,yaxis, 300, 30)];
            lblPremedicationDay.frame    = [self setLabelProperties:lblPremedicationDay text:str type:3];
            [self.regimenDetailScrollview addSubview:lblPremedicationDay];
            
            NSString *previousGroup = [[NSString alloc] init];
            NSString *regimenquerySQL = [NSString stringWithFormat:@"select premedication,optiongroup from premedication where premedication_day=\"%@\" and region_id=\"%@\"", str, self.regimenId];
            const char *sqlStatement = [regimenquerySQL UTF8String];
            
            if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
            {
                while(sqlite3_step(compiledStatement) == SQLITE_ROW)
                {
                    NSString *premedicationName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 0)];
                    NSString *optiongroup = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 1)];
                    if(![optiongroup isEqualToString:previousGroup])
                        yaxis = yaxis + 10;
                    
                    UIButton *radiobutton   = [UIButton buttonWithType:UIButtonTypeCustom];
                    [radiobutton setFrame:CGRectMake(10, yaxis, 300, 40)];
                     radiobutton.tag        = [optiongroup intValue]+2000;
                     radiobutton.frame      = [self setButtonProperties:radiobutton text:premedicationName type:@"Radio"];
                    [self.regimenDetailScrollview addSubview:radiobutton];
                    previousGroup = optiongroup;
                }
            }
            yaxis = yaxis + 15;
            sqlite3_finalize(compiledStatement);
        }	
	}
	sqlite3_close(database);
    
    
    //-------------add comments for premedication
    UIButton *checkbutton   = [UIButton buttonWithType:UIButtonTypeCustom];
    [checkbutton setFrame:CGRectMake(10, yaxis, 300, 40)];
    checkbutton.tag         = AddPreMedicationCheckBoxTag;
    checkbutton.frame       = [self setButtonProperties:checkbutton text:@"  Additional premedication :" type:@"CheckBox"];
    [self.regimenDetailScrollview addSubview:checkbutton];
    
    UITextView *commentsTextView    =   [[UITextView alloc] initWithFrame:CGRectMake(10,yaxis+5, 300, 80)];
    commentsTextView.tag            =   AddPreMedicationTextViewTag;
    commentsTextView.editable       =   FALSE;
    commentsTextView.frame          =   [self setTextViewProperties:commentsTextView text:@"Enter your premedication here. [Optional]"];
    [regimenDetailScrollview addSubview:commentsTextView];
    
    //-----------------
    yaxis = CGRectGetMaxY(commentsTextView.frame);
    [self readchemotherapyFromDatabase];
}

-(void)readchemotherapyFromDatabase {
	sqlite3 *database;
	if(sqlite3_open([[MEDICAL_APP_DELEGATE databasePath] UTF8String], &database) == SQLITE_OK)
    {
        sqlite3_stmt *compiledStatement;
        NSString *regimenquerySQL = [NSString stringWithFormat:@"select rowid,name,dose,units,administration_infusion,days from chemotherapy where region_id=\"%@\"", self.regimenId];
        const char *sqlStatement = [regimenquerySQL UTF8String];
        
        yaxis = yaxis + SECTIONSPACE;
        UILabel *lblChemoHead   = [[UILabel alloc] initWithFrame:CGRectMake(10,yaxis, 300, 50)];
        lblChemoHead.frame      = [self setLabelProperties:lblChemoHead text:@"Chemotherapy : " type:0];
        lblChemoHead.tag        = CHEMOTHERAPYHEADERTAG;
        [lblChemoHead setHidden:YES];
        [self.regimenDetailScrollview addSubview:lblChemoHead];
                
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
        {
            BSAArray = [[NSMutableDictionary alloc] init];
            while(sqlite3_step(compiledStatement) == SQLITE_ROW)
            {
                NSString *idCol             = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 0)];
                NSString *chemoNameString   = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 1)];
                chemoDose                   = sqlite3_column_double(compiledStatement, 2);
                NSString *unitsString       = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 3)];
                NSString *admininfusionString = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 4)];
                NSString *daysString          = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 5)];

                DebugLog(@"RegimenDetails: chemoNameString %@ dose = %f  unitsString: --%@--",chemoNameString,chemoDose,unitsString);

                //Template :
                //chemoName
                //chemoDose units = value units
                //admin-infusion
                //days
                
                //     case 1:
                if (chemoNameString.length != 0)
                {
                    
                    [lblChemoHead setHidden:NO];
                    yaxis   +=  10;
                    DebugLog(@"1) yaxis %f",yaxis);

                    //ChemoName-----
                    UILabel *lblChemotherapy         = [[UILabel alloc] initWithFrame:CGRectMake(10,yaxis, 300, 30)];
                    lblChemotherapy.frame            = [self setLabelProperties:lblChemotherapy text:chemoNameString type:5];
                    [regimenDetailScrollview addSubview:lblChemotherapy];
                    
                    //DoseText-----
                    if (unitsString.length != 0 )
                    {
                        DebugLog(@"2)yaxis %f",yaxis);

                        [BSAArray setObject:[NSString stringWithFormat:@"%f",chemoDose] forKey:idCol];
                        
                        UIView *viewDoseField = [[UIView alloc] initWithFrame:CGRectMake(10, yaxis, 140, 50)];
                        viewDoseField.backgroundColor     = [UIColor whiteColor];
                        viewDoseField.layer.borderColor   = [UIColor lightGrayColor].CGColor;
                        viewDoseField.layer.borderWidth   = 1.0;
                        //viewDoseField.tag = CHEMOTHERAPY_VIEW_TAG *
                        [regimenDetailScrollview addSubview:viewDoseField];

                        // adding equal to sign
                        UILabel *lblequalSign = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(viewDoseField.frame)+5,yaxis, 10, 50)];
                        lblequalSign.tag              = [idCol intValue]+600;
                        lblequalSign.backgroundColor  = [UIColor clearColor];
                        lblequalSign.textColor        = [UIColor blackColor];
                        lblequalSign.font             = DEFAULT_SEMIBOLD_FONT(17);
                        lblequalSign.text             = @"=";
                        [regimenDetailScrollview addSubview:lblequalSign];
                        
                        UIView *viewBSAField = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(lblequalSign.frame)+5, yaxis, 140, 50)];
                        viewBSAField.backgroundColor     = [UIColor whiteColor];
                        viewBSAField.layer.borderColor   = [UIColor lightGrayColor].CGColor;
                        viewBSAField.layer.borderWidth   = 1.0;
                        [regimenDetailScrollview addSubview:viewBSAField];
                        
                        // viewDoseField subviews
                        UITextField *dosetextField       = [[UITextField alloc] initWithFrame:CGRectMake(7,0, 65, 50)];
                        dosetextField.text  = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 2)];
                        dosetextField.tag                 = [idCol intValue]+1000;
                        dosetextField.delegate            = self;
                        dosetextField.backgroundColor     = [UIColor clearColor];
                        dosetextField.textColor           = [UIColor blackColor];
                        dosetextField.font                = DEFAULT_FONT(17);
                        dosetextField.keyboardType        = UIKeyboardTypeDecimalPad;
                        dosetextField.textAlignment       = NSTextAlignmentCenter;
                        dosetextField.layer.borderColor   = [UIColor clearColor].CGColor;
                        [viewDoseField addSubview:dosetextField];
                        
                        //units-----
                        UILabel *lbldoseUnits = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(dosetextField.frame),0, 63, 50)];
                        lbldoseUnits.backgroundColor     = [UIColor clearColor];
                        lbldoseUnits.textColor           = [UIColor blackColor];
                        lbldoseUnits.font                = DEFAULT_FONT(14);
                        lbldoseUnits.textAlignment       = NSTextAlignmentRight;
                        //lbldoseUnits.frame   = [self setLabelProperties:lbldoseUnits text:unitsString type:2];
                        lbldoseUnits.tag     = [idCol intValue]+500;
                        lbldoseUnits.adjustsFontSizeToFitWidth = YES;
                        [viewDoseField addSubview:lbldoseUnits];
                        if ([[unitsString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@"mg/m2"])
                        {
                            lbldoseUnits.text = MGM2;
                        }
                        else if ([[unitsString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@"mg/m2/day"] || [[unitsString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@"mg/m2/Day"])
                        {
                            lbldoseUnits.text = MGM2DAY;
                        }
                        else
                        {
                            lbldoseUnits.text = unitsString;
                        }
                        
                        
                        //txtPatientBSA-----
                        UITextField *bsaMulttextField = [[UITextField alloc] initWithFrame:CGRectMake(7,0, 100, 50)];
                        DebugLog(@"unitsString %@",unitsString);
                        bsaMulttextField.text = @"";
//                        if ([unitsString  hasPrefix:@"mg/m"])
//                        {
//                            //[BSAArray setObject:[NSString stringWithFormat:@"%f",chemoDose] forKey:idCol];
//                            bsaMulttextField.text = [self show:[self.txtPatientBSA.text doubleValue]*chemoDose];
//                        }
//                        else
//                        {
//                            bsaMulttextField.text = @"0";
//                        }
                        
                        bsaMulttextField.tag = [idCol intValue];
                        bsaMulttextField.delegate            = self;
                        bsaMulttextField.backgroundColor     = [UIColor clearColor];
                        bsaMulttextField.textColor           = [UIColor blackColor];
                        bsaMulttextField.font                = DEFAULT_SEMIBOLD_FONT(17);
                        bsaMulttextField.keyboardType        = UIKeyboardTypeDecimalPad;
                        bsaMulttextField.textAlignment       = NSTextAlignmentCenter;
                        bsaMulttextField.layer.borderColor   = [UIColor clearColor].CGColor;
                        [viewBSAField addSubview:bsaMulttextField];
                        
                        //bsa unit-----
                       UILabel *lblbsaUnit = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(bsaMulttextField.frame),0, 30, 50)];
                        lblbsaUnit.backgroundColor  = [UIColor clearColor];
                        lblbsaUnit.textColor        = [UIColor blackColor];
                        lblbsaUnit.font             = DEFAULT_SEMIBOLD_FONT(14);
                        lblbsaUnit.text             = @"mg";
                        lblbsaUnit.textAlignment    = NSTextAlignmentRight;
                        lblbsaUnit.tag              = [idCol intValue]+5000;
                        lblbsaUnit.adjustsFontSizeToFitWidth = YES;
                       // lblbsaUnit.frame       =   [self setLabelProperties:lblbsaUnit text:@"mg" type:2];
                        [viewBSAField addSubview:lblbsaUnit];
                        
                        yaxis = CGRectGetMaxY(viewDoseField.frame) + 2;
                    }
                    else
                    {
                        yaxis += 20;
                    }
                    
                    //administration_infusion-----                            
                    if (admininfusionString.length != 0)
                    {
                        DebugLog(@"3)yaxis %f",yaxis);
                        UILabel *lbladmininfusion = [[UILabel alloc] initWithFrame:CGRectMake(10,yaxis, 300, 20)];
                        lbladmininfusion.frame   = [self setLabelProperties:lbladmininfusion text:admininfusionString type:1];
                        [regimenDetailScrollview addSubview:lbladmininfusion];
                    }
                    
                   //days-----
                    if (daysString.length != 0)
                    {
                        DebugLog(@"4)yaxis %f",yaxis);
                        UILabel *lbldays         = [[UILabel alloc] initWithFrame:CGRectMake(10,yaxis, 300, 20)];
                        lbldays.frame   = [self setLabelProperties:lbldays text:daysString type:1];
                        [regimenDetailScrollview addSubview:lbldays];
                    }
                }
            }
        }
		sqlite3_finalize(compiledStatement);
	}
	sqlite3_close(database);
    [self readregimenFromDatabase];
}

-(void)readregimenFromDatabase
{    
    yaxis = yaxis + SECTIONSPACE-10;   
    // Setup the database object
	sqlite3 *database;
	// Open the database from the users filessytem
	if(sqlite3_open([[MEDICAL_APP_DELEGATE databasePath] UTF8String], &database) == SQLITE_OK) {
		// Setup the SQL Statement and compile it for faster access
        sqlite3_stmt *compiledStatement;
  
        NSString *regimenquerySQL = [NSString stringWithFormat:@"select labs,frequency,comments,clinical_pearls,ref,ref_link from regions where region_id=\"%@\"", self.regimenId];
        DebugLog(@"regimenquerySQL %@",regimenquerySQL);
        const char *sqlStatement = [regimenquerySQL UTF8String];
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK)
        {
            // Loop through the results and add them to the feeds array
            if(sqlite3_step(compiledStatement) == SQLITE_ROW)
            {
                
                NSString *labs      = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 0)];
                NSString *frequency = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 1)];
                NSString *commentsString  = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 2)];
                NSString *clinical_pearls = [NSString stringWithCString:(char *)sqlite3_column_text(compiledStatement, 3) encoding:NSASCIIStringEncoding];
                NSString *ref       = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 4)];
                NSString *ref_link  = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 5)];

                
                UILabel *lblLabs    = [[UILabel alloc] initWithFrame:CGRectMake(10,yaxis, 300, 50)];
                lblLabs.frame       = [self setLabelProperties:lblLabs text:@"Labs : " type:0];
                lblLabs.tag         = LABSHEADERTAG;
                [regimenDetailScrollview addSubview:lblLabs];
                
                UITextView *labsTextView =  [[UITextView alloc] initWithFrame:CGRectMake(10,yaxis, 300, 80)];
                labsTextView.tag         =  4001;
               // labsTextView.frame       =  [self setTextViewProperties:labsTextView text:labs];
                labsTextView.frame       =  [self setTextViewProperties:labsTextView text:@""];
                DebugLog(@"labs %@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 0)]);
                [regimenDetailScrollview addSubview:labsTextView];
                
                yaxis   += labsTextView.frame.size.height+ SECTIONSPACE;
                
                UILabel *lblFrequency   = [[UILabel alloc] initWithFrame:CGRectMake(10,yaxis, 300, 50)];
                lblFrequency.frame      = [self setLabelProperties:lblFrequency text:@"Frequency : " type:0];
                lblFrequency.tag        = FREQUENCYHEADERTAG;
                [regimenDetailScrollview addSubview:lblFrequency];
                
                UITextView *frequencyTextView = [[UITextView alloc] initWithFrame:CGRectMake(10,yaxis, 300, 80)];
                frequencyTextView.tag         = 4003;
                frequencyTextView.frame       = [self setTextViewProperties:frequencyTextView text:frequency];
                [regimenDetailScrollview addSubview:frequencyTextView];
                
                yaxis += frequencyTextView.frame.size.height+ SECTIONSPACE;
                
                UILabel *lblComments    = [[UILabel alloc] initWithFrame:CGRectMake(10,yaxis, 300, 50)];
                lblComments.frame       = [self setLabelProperties:lblComments text:@"Comments : " type:0];
                lblComments.tag         = COMMENTSHEADERTAG;
                [regimenDetailScrollview addSubview:lblComments];
                
                UITextView *commentsTextView = [[UITextView alloc] initWithFrame:CGRectMake(10,yaxis, 300, 80)];
                commentsTextView.tag         = 4000;
                if ([commentsString length] > 0)
                    commentsTextView.frame     =  [self setTextViewProperties:commentsTextView text:commentsString];
                else
                    commentsTextView.frame      = [self setTextViewProperties:commentsTextView text:@"Enter your comments here."];
                [regimenDetailScrollview addSubview:commentsTextView];
                
                yaxis += commentsTextView.frame.size.height+ SECTIONSPACE;

                detailClinicalPearls =   [[clinical_pearls stringByReplacingOccurrencesOfString:@"&#956;" withString:@"μ"] copy];
                DebugLog(@"clinical_pearls %@",clinical_pearls);
                DebugLog(@"detailClinicalPearls %@",detailClinicalPearls);

                UIButton *buttonClinicalPearls = [UIButton buttonWithType:UIButtonTypeCustom];
                buttonClinicalPearls.frame = CGRectMake(10,yaxis, 145, 50);
                [buttonClinicalPearls setTitle:@"Clinical Pearls" forState:UIControlStateNormal];
                [buttonClinicalPearls addTarget:self action:@selector(detailBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
                [self setButtonProperties:buttonClinicalPearls];
                [regimenDetailScrollview addSubview:buttonClinicalPearls];
                
                detailsReferences   =   [ref copy];
                UIButton *buttonReferences = [UIButton buttonWithType:UIButtonTypeCustom];
                buttonReferences.frame = CGRectMake(165,yaxis, 145, 50);
                [buttonReferences setTitle:@"References" forState:UIControlStateNormal];
                [buttonReferences addTarget:self action:@selector(detailBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
                [self setButtonProperties:buttonReferences];
                [regimenDetailScrollview addSubview:buttonReferences];
                
                detailsReferencesLink = [ref_link copy];
                
                yaxis =   CGRectGetMaxY(buttonReferences.frame) + 10;
                
//                UIButton *buttonCarboplatin = [UIButton buttonWithType:UIButtonTypeCustom];
//                buttonCarboplatin.frame = CGRectMake(10,yaxis,300, 50);
//                [buttonCarboplatin setTitle:@"Carboplatin" forState:UIControlStateNormal];
//                [buttonCarboplatin addTarget:self action:@selector(detailBtnPressed:)  forControlEvents:UIControlEventTouchUpInside];
//                [self setButtonProperties:buttonCarboplatin];
//                [regimenDetailScrollview addSubview:buttonCarboplatin];
//                
//                yaxis =   CGRectGetMaxY(buttonCarboplatin.frame) + 30;
            }
        }
		sqlite3_finalize(compiledStatement);
	}
	sqlite3_close(database);
    
    [regimenDetailScrollview setContentSize:CGSizeMake(self.view.frame.size.width, yaxis)];
}


#pragma UITextField Delegate methods
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if([textField isEqual:txtPatientBSA])
    {
        if (UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation)) {
            DebugLog(@"Potrait");
            [regimenDetailScrollview setContentOffset:CGPointMake(0,transviewPatientBSA.frame.origin.y-100) animated:YES];
        }else{
            DebugLog(@"Landscape");
            [regimenDetailScrollview setContentOffset:CGPointMake(0,transviewPatientBSA.frame.origin.y-5) animated:YES];
        }
        [CommonCallback changeTextFieldColor:TEXT_EDITING view:viewPatientBSA];
    }
    else if([textField isEqual:txtPatientWeight])
    {
        if (UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation)) {
            DebugLog(@"Potrait");
            [regimenDetailScrollview setContentOffset:CGPointMake(0,viewPatientWeight.frame.origin.y-100) animated:YES];
        }else{
            DebugLog(@"Landscape");
            [regimenDetailScrollview setContentOffset:CGPointMake(0,viewPatientWeight.frame.origin.y-5) animated:YES];
        }
        [CommonCallback changeTextFieldColor:TEXT_EDITING view:viewPatientWeight];
    }
    else if([textField isEqual:txtPatientHeight])
    {
        if (UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation)) {
            DebugLog(@"Potrait");
            [regimenDetailScrollview setContentOffset:CGPointMake(0,viewPatientHeight.frame.origin.y-100) animated:YES];
        }else{
            DebugLog(@"Landscape");
            [regimenDetailScrollview setContentOffset:CGPointMake(0,viewPatientHeight.frame.origin.y-5) animated:YES];
        }
        [CommonCallback changeTextFieldColor:TEXT_EDITING view:viewPatientHeight];
    }else{
        if ([textField.superview isKindOfClass:[UIView class]]) {
            if (UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation)) {
                DebugLog(@"Potrait");
                [regimenDetailScrollview setContentOffset:CGPointMake(0,textField.superview.frame.origin.y-100) animated:YES];
            }else{
                DebugLog(@"Landscape");
                [regimenDetailScrollview setContentOffset:CGPointMake(0,textField.superview.frame.origin.y-5) animated:YES];
            }
            [CommonCallback changeTextFieldColor:TEXT_EDITING view:textField.superview];
        }
    }
    [regimenDetailScrollview setContentSize:CGSizeMake(self.view.frame.size.width, yaxis+200)];
    return YES;
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if([textField isEqual:txtPatientBSA])
    {
        [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:viewPatientBSA];
    }
    else if([textField isEqual:txtPatientWeight])
    {
        [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:viewPatientWeight];
    }
    else if([textField isEqual:txtPatientHeight])
    {
        [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:viewPatientHeight];
    }
    else{
        if ([textField.superview isKindOfClass:[UIView class]]) {
            [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:textField.superview];
        }
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (textField == txtPatientAge)
    {
        return !([txtPatientAge.text length]> 2 && [string length] > range.length);
    }
    else if (textField == txtPatientName)
    {
        return !([txtPatientName.text length]> 30 && [string length] > range.length);
    }    
    else
    {    
//        UITextField *tempText = (UITextField *)[regimenDetailScrollview viewWithTag:textField.tag-1000];
//        tempText.text = [self show:[self.txtPatientBSA.text doubleValue]*[textField.text doubleValue]];     
//        [BSAArray setObject:[NSString stringWithFormat:@"%f",[textField.text doubleValue]] forKey:[NSString stringWithFormat:@"%d",textField.tag-1000]];        
//        return !([textField.text length]> 5 && [string length] > range.length);
        
        return YES;
    }
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField
{    
    if (textField == txtPatientName)
    {
        [txtPatientAge becomeFirstResponder];
    }
    else
    {
         [textField resignFirstResponder];
    }
    return NO;
}

#pragma UITextView Delegate methods
- (void)textViewDidBeginEditing:(UITextView *)txtView
{
    textViewTag =    txtView.tag;
    if(txtView.frame.origin.y > regimenDetailScrollview.contentOffset.y)
        [regimenDetailScrollview setContentOffset:CGPointMake(0,txtView.frame.origin.y-27) animated:YES];
    
    if ([txtView.text hasPrefix:@"Enter your"])
    {
        txtView.textColor = [UIColor blackColor];
        txtView.text = @"";
    }
}
- (void)textViewDidEndEditing:(UITextView *)textView{
    if (textView.text.length == 0) {
        [self setPlaceholder];
    }
}

- (void)textViewDidChange:(UITextView *)textView{
    
    NSInteger restrictedLength  =   120;
    NSString *temp              =   textView.text;
    if([[textView text] length] > restrictedLength){
        textView.text   =   [temp substringToIndex:[temp length]-1];
    }
}

-(void)scrollTobottom
{
    [UIView animateWithDuration:0.2 animations:^{
      //  regimenDetailScrollview.contentSize = CGSizeMake(regimenDetailScrollview.frame.size.width, CGRectGetMaxY(lblNCount.frame)+30);
        CGPoint bottomOffset = CGPointMake(0, 0);
        [regimenDetailScrollview setContentOffset:bottomOffset animated:YES];
    }];
}


#pragma UIButton Action callbacks
- (void)radioboxButton:(UIButton *)button
{
    for (UIButton *but in [self.regimenDetailScrollview subviews]) {
        if ([but isKindOfClass:[UIButton class]] && but.tag==button.tag) {
            [but setSelected:NO];
        }
    }
    if (!button.selected) {
        button.selected = !button.selected;
        //DebugLog(@"but pressed ---> %d",button.tag);
    }
    if(button.frame.origin.y > regimenDetailScrollview.contentOffset.y)
        [regimenDetailScrollview setContentOffset:CGPointMake(0,button.frame.origin.y-20) animated:YES];
    [self.regimenDetailScrollview endEditing:YES];
}

- (void)checkboxButton:(UIButton *)button{
    
    for (UIButton *but in [self.regimenDetailScrollview subviews]) {
        if ([but isKindOfClass:[UIButton class]] && but.tag==AddPreMedicationCheckBoxTag) {
            UITextView *tempTextView = (UITextView *)[regimenDetailScrollview viewWithTag:AddPreMedicationTextViewTag];
            but.selected = !but.selected;
            if(but.selected==YES)
            {
                tempTextView.editable = TRUE;
                NSDictionary *params = @{@"cancer_name":cancerName, @"regimen_name" : regimenName,@"add_premedication":@"yes"};
                [INEventLogger logEvent:@"Chemo_Orders_Add_Premedication" withParams:params];
            } else {
                tempTextView.editable = FALSE;
                NSDictionary *params = @{@"cancer_name":cancerName, @"regimen_name" : regimenName,@"add_premedication":@"no"};
                [INEventLogger logEvent:@"Chemo_Orders_Add_Premedication" withParams:params];
            }
        }
    }
    if(button.frame.origin.y > regimenDetailScrollview.contentOffset.y)
        [regimenDetailScrollview setContentOffset:CGPointMake(0,button.frame.origin.y-20) animated:YES];
    [self.regimenDetailScrollview endEditing:YES];
}

-(void)detailBtnPressed:(id)sender
{    
    UIButton *btn=(UIButton *)sender;
    detailHeader.text=btn.titleLabel.text;
    DebugLog(@"details pressed %@ and btn ref %@",detailsReferences,detailsReferencesLink);
    if ([btn.titleLabel.text isEqualToString:@"Clinical Pearls"] && detailClinicalPearls != nil && ![detailClinicalPearls isEqualToString:@""])
    {
        NSDictionary *params = @{@"cancer_name":cancerName, @"regimen_name" : regimenName};
        [INEventLogger logEvent:@"Chemo_Orders_ClinicalPearls" withParams:params];

        [backView setHidden:NO];
        [detailView setHidden:NO];
        [referencebtn setHidden:YES];
        detailTextView.frame = CGRectMake(detailTextView.frame.origin.x, detailTextView.frame.origin.y, detailTextView.frame.size.width, detailOK.frame.origin.y - (detailTextView.frame.origin.y+8));
        detailTextView.text =   detailClinicalPearls;
    }
    else if([btn.titleLabel.text isEqualToString:@"References"] && detailsReferences != nil && ![detailsReferences isEqualToString:@""])
    {
        NSDictionary *params = @{@"cancer_name":cancerName, @"regimen_name" : regimenName};
        [INEventLogger logEvent:@"Chemo_Orders_References" withParams:params];

        [backView setHidden:NO];
        [detailView setHidden:NO];
        detailTextView.text = detailsReferences;
        if (detailsReferencesLink != nil)
        {
            [referencebtn setHidden:NO];
            [referencebtn setTitle:@"PubMed" forState:UIControlStateNormal];
            detailTextView.frame = CGRectMake(detailTextView.frame.origin.x, detailTextView.frame.origin.y, detailTextView.frame.size.width, referencebtn.frame.origin.y -  (detailTextView.frame.origin.y+8));
        }
        else
        {
            [referencebtn setHidden:YES];
            detailTextView.frame = CGRectMake(detailTextView.frame.origin.x, detailTextView.frame.origin.y, detailTextView.frame.size.width, detailOK.frame.origin.y - (detailTextView.frame.origin.y+8));
        }
    }
    else if([btn.titleLabel.text isEqualToString:@"Carboplatin"])
    {
        NSDictionary *params = @{@"cancer_name":cancerName, @"regimen_name" : regimenName};
        [INEventLogger logEvent:@"Chemo_Orders_Carboplatin" withParams:params];
        
        MedicalCarboplatinViewController *carboplatinviewController = [[MedicalCarboplatinViewController alloc] initWithNibName:@"MedicalCarboplatinViewController" bundle:nil] ;
        carboplatinviewController.title = @"Carboplatin";
        carboplatinviewController.comeFromOtherThanCalculator = 1;
        DebugLog(@"self.interfaceOrientation %d",self.interfaceOrientation);
        if (self.interfaceOrientation ==  UIInterfaceOrientationLandscapeLeft || self.interfaceOrientation ==  UIDeviceOrientationLandscapeRight) {
            [self.navigationController pushViewController:carboplatinviewController animated:NO];
        }
        else
        {
            [self.navigationController pushViewController:carboplatinviewController animated:YES];
        }
    }
}

- (IBAction)referencebtnPressed:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    if(!btn.hidden)
    {
        NSDictionary *params = @{@"cancer_name":cancerName, @"regimen_name" : regimenName};
        [INEventLogger logEvent:@"Chemo_Orders_Pubmed" withParams:params];

        MedicalWebViewController *webviewController = [[MedicalWebViewController alloc] initWithNibName:@"MedicalWebViewController" bundle:nil] ;
        webviewController.medicalLink = detailsReferencesLink;
        webviewController.title = regimenName;
        [self.navigationController pushViewController:webviewController animated:YES];
    }
}

- (IBAction)detailOKBtnPressed:(id)sender
{
    [detailView setHidden:YES];
    [backView setHidden:YES];
}

#pragma Set properties callbacks
-(void)setButtonProperties:(UIButton *)btn
{
    btn.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    btn.backgroundColor     = DEFAULT_COLOR;
    btn.titleLabel.font     = DEFAULT_BOLD_FONT(18);
    btn.titleLabel.minimumScaleFactor = 12.0;
    btn.layer.cornerRadius  = 5.0;
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor colorWithWhite:1.0 alpha:0.5] forState:UIControlStateHighlighted];
}

-(CGRect)setButtonProperties:(UIButton *)but text:(NSString *)bText type:(NSString *)bType
{
    if ([bType isEqualToString:@"Radio"])
    {
        [but setImage:[UIImage imageNamed:@"radio-off.png"] forState:UIControlStateNormal];
        [but setImage:[UIImage imageNamed:@"radio-on.png"] forState:UIControlStateSelected];
        but.backgroundColor = DEFAULT_COLOR;
        [but setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        but.titleLabel.font =   DEFAULT_FONT(14);
        [but addTarget:self action:@selector(radioboxButton:) forControlEvents:UIControlEventTouchUpInside];
    }
    else  if ([bType isEqualToString:@"CheckBox"])
    {
        [but setImage:[UIImage imageNamed:@"checkBox-grey.png"] forState:UIControlStateNormal];
        [but setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [but setImage:[UIImage imageNamed:@"checkBox-blue.png"] forState:UIControlStateSelected];
        [but setTitleColor:DEFAULT_COLOR forState:UIControlStateSelected];
        
        but.backgroundColor =   [UIColor clearColor];
        but.titleLabel.font =   DEFAULT_BOLD_FONT(14);
        CGFloat spacing = 10; // the amount of spacing to appear between image and title
        but.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, spacing); //top,left,bottom,right
        but.titleEdgeInsets = UIEdgeInsetsMake(0, spacing, 0, 0);
        [but addTarget:self action:@selector(checkboxButton:) forControlEvents:UIControlEventTouchUpInside];
    }
    but.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [but setFrame:CGRectMake(10, yaxis, 300, 40)];
    but.titleLabel.lineBreakMode = NSLineBreakByWordWrapping | NSLineBreakByTruncatingTail;
    but.titleLabel.numberOfLines = 0;
    [but setTitle:[bText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forState:UIControlStateNormal];
    
    but.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [but.titleLabel sizeToFit];
    //yasix+label height
    
    // Set the height
    CGSize maximumLabelSize = CGSizeMake(300,9999);
    CGSize titleSize = [but.titleLabel.text sizeWithFont:but.titleLabel.font constrainedToSize:maximumLabelSize lineBreakMode:but.titleLabel.lineBreakMode];
    //DebugLog(@"Height: %.f  Width: %.f", titleSize.height, titleSize.width);
    
    //Adjust the label the the new height
    CGRect newFrame = but.frame;
    newFrame.size.height = titleSize.height + 20;
    yaxis = yaxis + titleSize.height + 20;
    
    DebugLog(@"title : %@ titleSize.height %f yaxis %f",but.titleLabel.text,titleSize.height,yaxis);
    return newFrame;
}

-(CGRect)setLabelProperties:(UILabel *)lLabel text:(NSString *)lText type:(int)lType
{
    lLabel.lineBreakMode = NSLineBreakByWordWrapping;
    lLabel.numberOfLines = 0;//Dynamic
    lLabel.backgroundColor = [UIColor clearColor];
    DebugLog(@"LabelText %@",lText);
    //NSString *str=[lText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if ([[lText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@"mg/m2"])
    {
        lLabel.text = MGM2;
    }
    else if ([[lText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@"mg/m2/day"] || [[lText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@"mg/m2/Day"])
    {
        lLabel.text = MGM2DAY;
    }
    else
    {
        lLabel.text = lText;
    }
    DebugLog(@"LabelText %@",lLabel.text);
    
    //    lLabel.adjustsFontSizeToFitWidth = YES;
    //    lLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    //    [lLabel sizeToFit];
    //yasix+label height
    switch (lType) {
        case 0: // heading
        {
            lLabel.font               = DEFAULT_BOLD_FONT(20);
            lLabel.backgroundColor    = DEFAULT_COLOR;
            lLabel.textColor          = [UIColor whiteColor];
            lLabel.text               = [NSString stringWithFormat:@" %@",lText];
            lLabel.autoresizingMask   = UIViewAutoresizingFlexibleWidth;
        }
            break;
        case 1:
        case 2:
        {
            lLabel.font = DEFAULT_FONT(14);
        }
            break;
        case 3: // for content heading
        {
            lLabel.font = DEFAULT_BOLD_FONT(16);
        }
            break;
        case 5: // for chemo chemo name
        {
            lLabel.font = DEFAULT_SEMIBOLD_FONT(14);
        }
            break;
        default:
            break;
    }
    // Set the height
    CGSize maximumLabelSize = CGSizeMake(300,9999);
    CGSize titleSize = [lLabel.text sizeWithFont:lLabel.font constrainedToSize:maximumLabelSize lineBreakMode:lLabel.lineBreakMode];
    // DebugLog(@"REgimenDetails: Height: %.f  Width: %.f", titleSize.height, titleSize.width);
    switch (lType) {
        case 0:
        case 3:
        {
            yaxis = yaxis + titleSize.height+ 5;
        }
            break;
        case 1: // content
        {
            yaxis = yaxis + titleSize.height+10;
        }
            break;
        case 5:
        {
            yaxis = yaxis + titleSize.height+10;
        }
            break;
        default:
            break;
    }
    //Adjust the label the the new height
    CGRect newFrame = lLabel.frame;
    if(lType == 0 || lType == 3)
    {
        newFrame.size.width = 300;
    }
    else
    {
        newFrame.size.width = titleSize.width;
    }
    newFrame.size.height = titleSize.height;
    return newFrame;
}

-(CGRect)setTextViewProperties:(UITextView *)tTextView text:(NSString *)tText
{
    tTextView.autoresizingMask      = UIViewAutoresizingFlexibleWidth;
    tTextView.clipsToBounds         =   YES;
    //  tTextView.layer.cornerRadius = 5;
    tTextView.layer.borderColor     =   [UIColor lightGrayColor].CGColor;
    tTextView.layer.borderWidth     =   1.0;
    tTextView.keyboardType          =   UIKeyboardTypeDefault;
    tTextView.delegate              =   self;
    tTextView.font                  =   DEFAULT_FONT(14);
    NSString *str                   =   [tText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    // DebugLog(@"textview %d text %@",tTextView.tag,tText);
    if ([str hasPrefix:@"Enter your"])
    {
        tTextView.textColor = [UIColor lightGrayColor];
    }
    else
    {
        tTextView.textColor = [UIColor blackColor];
    }
    tTextView.text  =   str;
    return tTextView.frame;
}


#pragma BSA callbacks
- (IBAction)BSABtnPressed:(id)sender
{
    
    NSDictionary *params = @{@"cancer_name":cancerName, @"regimen_name" : regimenName};
    [INEventLogger logEvent:@"Chemo_Orders_BSA" withParams:params];
    
    MedicalBSAViewController *bsaviewController = [[MedicalBSAViewController alloc] initWithNibName:@"MedicalBSAViewController" bundle:nil] ;
    bsaviewController.title = @"BSA";
    bsaviewController.bsadelegate = self;
    [bsaviewController setBSAValue:txtPatientBSA.text weight:txtPatientWeight.text height:txtPatientHeight.text];
    [self.navigationController pushViewController:bsaviewController animated:YES];
}

-(void) setBSResultValue:(NSString*)result weight:(NSString*)weight height:(NSString*)height;
{
    if(![result isEqualToString:@"...."])
    {
        txtPatientBSA.text = result;
        txtPatientWeight.text = weight;
        txtPatientHeight.text = height;
        // [self setChemoBSA];
        //DebugLog(@"%@",result);
    }
}

-(void)setChemoBSA
{
    for(NSString *tagid in BSAArray)
    {
        UITextField *tempText = (UITextField *)[regimenDetailScrollview viewWithTag:[tagid intValue]];
        tempText.text =[self show:[self.txtPatientBSA.text doubleValue]*[[BSAArray objectForKey:tagid] doubleValue]];
    }
}

-(NSString *)show:(double)result
{
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [formatter setMaximumFractionDigits:2];
    [formatter setRoundingMode: NSNumberFormatterRoundCeiling];
    return [NSString stringWithFormat:@"%@",[formatter stringFromNumber:[NSNumber numberWithFloat:result]]];
    
}


//validation for weigh and height fields
- (IBAction)editingChanged:(UITextField *)textField {
    [self validate:textField];
}

-(BOOL)validate:(UITextField *)textField{
    BOOL success = YES;
    double doubleValue = [textField.text doubleValue];
    if([textField isEqual:txtPatientWeight])
    {
        if ((doubleValue >= 10 && doubleValue <= 200)) {
            [self removeErrorMessageFromLabel:lblErrWeight];
        }else{
            [self setErrorMessageOnLabel:@"Enter weight value in between 10 - 200." label:lblErrWeight];
            success = NO;
        }
    }else if([textField isEqual:txtPatientHeight])
    {
        if ((doubleValue >= 30 && doubleValue <= 241.3)) {
            [self removeErrorMessageFromLabel:lblErrHeight];
        }else{
            [self setErrorMessageOnLabel:@"Enter height value in between 30 - 241." label:lblErrHeight];
            success = NO;
        }
    }
    return success;
}

-(void)setErrorMessageOnLabel:(NSString *)errorMessage label:(UILabel *)lbl{
    lbl.hidden = NO;
    lbl.text = errorMessage;
    if ([lbl isEqual:lblErrWeight]) {
        [CommonCallback changeTextFieldColor:TEXT_ERROR view:viewPatientWeight];
    }else if ([lbl isEqual:lblErrHeight]) {
        [CommonCallback changeTextFieldColor:TEXT_ERROR view:viewPatientHeight];
    }
}

-(void)removeErrorMessageFromLabel:(UILabel *)lbl{
    lbl.hidden = YES;
    if ([lbl isEqual:lblErrWeight]) {
        [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:viewPatientWeight];
    }else if ([lbl isEqual:lblErrHeight]) {
        [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:viewPatientHeight];
    }
}

@end
