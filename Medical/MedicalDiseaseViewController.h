//
//  MedicalDiseaseViewController.h
//  Medical
//
//  Created by Kirti Nikam on 03/11/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>

@class DiseaseObject;
@interface MedicalDiseaseViewController : UIViewController{
    DiseaseObject *tempObj;
}
@property (nonatomic,copy) NSString *classification;
@property (retain,nonatomic) NSMutableArray *classificationDataArray;

@property (strong, nonatomic) IBOutlet UITableView *diseaseTableView;
@end
