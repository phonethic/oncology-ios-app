//
//  NewsCategoryObject.h
//  Medical
//
//  Created by Kirti Nikam on 09/04/14.
//  Copyright (c) 2014 Kirti Nikam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NewsCategoryObject : NSObject
@property(nonatomic,copy) NSString *categoryId;
@property(nonatomic,copy) NSString *categoryName;
@property(nonatomic,copy) NSString *categorySlug;
@end
