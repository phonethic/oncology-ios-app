//
//  MedicalCancerCentersListViewController.h
//  Medical
//
//  Created by Kirti Nikam on 23/11/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MedicalCancerCentersListViewController : UIViewController<UISearchBarDelegate>
{
    BOOL isSearchOn;
    BOOL canSelectRow;
    double keyboardHeight;
}
@property (nonatomic,retain) NSMutableDictionary *sections;
@property (retain,nonatomic) NSMutableArray *searchArray;

@property (strong, nonatomic) IBOutlet UITableView *centerlistTableView;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;

@end
