//
//  MedicalANCViewController.h
//  Medical
//
//  Created by Kirti Nikam on 08/10/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import <UIKit/UIKit.h>
#define CMM3 [NSString stringWithFormat:@"c/mm\u00B3"]

@interface MedicalANCViewController : UIViewController<UITextFieldDelegate,UIActionSheetDelegate>{
    int TEXTTAG;
    BOOL keyboardIsShown;

}
@property (strong, nonatomic) IBOutlet UIScrollView *ancScrollView;
@property (strong, nonatomic) IBOutlet UILabel *cmmLabel;
@property (strong, nonatomic) IBOutlet UILabel *labelResult;
@property (strong, nonatomic) IBOutlet UILabel *lblNCount;
@property (strong, nonatomic) IBOutlet UITextField *wbcTextField;
@property (strong, nonatomic) IBOutlet UITextField *polysTextField;
@property (strong, nonatomic) IBOutlet UITextField *bandsTextField;
@property (strong, nonatomic) IBOutlet UIView *keyboardView;

@property (strong, nonatomic) IBOutlet UIView *viewWBC;
@property (strong, nonatomic) IBOutlet UIView *viewPolys;
@property (strong, nonatomic) IBOutlet UIView *viewBands;
@property (strong, nonatomic) IBOutlet UILabel *lblErrWBC;
@property (strong, nonatomic) IBOutlet UILabel *lblErrPolys;
@property (strong, nonatomic) IBOutlet UILabel *lblErrBands;

@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *viewCollection;
@property (strong, nonatomic) IBOutletCollection(UITextField) NSArray *txtCollection;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *lblCollection;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *lblErrorCollection;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *keyBoardBtnCollection;


@end
