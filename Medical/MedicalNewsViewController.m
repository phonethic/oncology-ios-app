//
//  MedicalNewsViewController.m
//  Medical
//
//  Created by Kirti Nikam on 08/04/14.
//  Copyright (c) 2014 Kirti Nikam. All rights reserved.
//

#import "MedicalNewsViewController.h"
#import "MedicalNewsDetailsViewController.h"
#import "MedicalAppDelegate.h"
#import "NewsCategoryObject.h"

#define CATEGORY_LINK @"http://cancerebook.org/?json=get_category_index"


@interface MedicalNewsViewController ()

@end

@implementation MedicalNewsViewController
@synthesize newsAlertCategoryArray;
@synthesize newsTableView;
@synthesize webData;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if (HUD) {
        [HUD removeFromSuperview];
        HUD = nil;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [INEventLogger logEvent:@"Alerts_Category_List"];

    UIBarButtonItem *refreshBarBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(refreshBarBtnClicked)];
    self.navigationItem.rightBarButtonItem = refreshBarBtn;
    
    [self addHUD];
    [newsTableView setHidden:YES];
    [self sendCategoryRequest];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewDidUnload
{
    [self setNewsTableView:nil];
    [super viewDidUnload];
}

#pragma mark - Rotation
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)addHUD{
	HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
	[self.navigationController.view addSubview:HUD];
	HUD.delegate    = self;
	HUD.labelText   = @"Loading";
    HUD.labelFont   = DEFAULT_FONT(16.0);
    HUD.color       = DEFAULT_COLOR_WITH_ALPHA(0.9);
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureTapDetacted:)];
    [HUD addGestureRecognizer:gesture];
}

//#pragma mark -
//#pragma mark MBProgressHUDDelegate methods
//- (void)hudWasHidden:(MBProgressHUD *)hud {
//	// Remove HUD from screen when the HUD was hidded
//	[HUD removeFromSuperview];
//	HUD = nil;
//}

#pragma UIGestureRecognizer selector methods
-(void)gestureTapDetacted:(UIGestureRecognizer *)sender{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:@"Do you want to cancel loading?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
    [alert show];
}

#pragma UIAlertView delegate methods
- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if ([title isEqualToString:@"YES"]) {
        if (conn) {
            [conn cancel];
            conn = nil;
        }
        if (![HUD isHidden]) {
            [HUD hide:YES];
        }
    }
}

-(void)refreshBarBtnClicked
{
    [INEventLogger logEvent:@"Alerts_Refresh"];
    [self sendCategoryRequest];
}

-(void)sendCategoryRequest{
    if (![MEDICAL_APP_DELEGATE networkavailable]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:ALERT_MESSAGE_NO_NETWORK delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return;
    }
    if (HUD == nil) {
        [self addHUD];
    }
    [HUD show:YES];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:CATEGORY_LINK] cachePolicy:NO timeoutInterval:30.0];

    conn = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
    if (conn)
    {
        webData = [NSMutableData data];
    }
}

#pragma NSURLConnection Delegate
-(void) connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *) response
{
    [webData setLength: 0];
}

-(void) connection:(NSURLConnection *)connection didReceiveData:(NSData *) data
{
    [webData appendData:data];
}

-(void) connection:(NSURLConnection *)connection didFailWithError:(NSError *) error
{
    DebugLog(@"didFailWithError error %@",error);
    [conn cancel];
    conn = nil;
    [HUD hide:YES];
}

-(void) connectionDidFinishLoading:(NSURLConnection *) connection
{
    if (webData)
    {
        NSError *error;
        NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:webData options:NSJSONReadingAllowFragments error:&error];
        DebugLog(@"jsonDict %@",jsonDict);
        if (jsonDict != nil) {
            if (newsAlertCategoryArray == nil) {
                newsAlertCategoryArray = [[NSMutableArray alloc] init];
            }
            else{
                [newsAlertCategoryArray removeAllObjects];
            }
            NSArray *lcategoriesArray = [jsonDict objectForKey:@"categories"];
            for (NSDictionary *postDict in lcategoriesArray) {
                NSString *catgId    = [postDict objectForKey:@"id"];
                NSString *catgName  = [postDict objectForKey:@"title"];
                NSString *catgSlug  = [postDict objectForKey:@"slug"];
                DebugLog(@"catgId %@ catgName %@ catgSlug %@",catgId,catgName,catgSlug);
                
                newsCatgObj = [[NewsCategoryObject alloc] init];
                newsCatgObj.categoryId      = catgId;
                newsCatgObj.categoryName    = catgName;
                newsCatgObj.categorySlug    = catgSlug;
                [newsAlertCategoryArray addObject:newsCatgObj];
                newsCatgObj = nil;
            }
            
            if ([newsAlertCategoryArray count] > 0) {
                [newsTableView setHidden:NO];
            }else{
                [newsTableView setHidden:YES];
                [HUD hide:YES];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:@"Sorry! No alerts found." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
            }
            [newsTableView reloadData];
        }else{
            DebugLog(@"Got json nil");
            [newsTableView setHidden:YES];
            [HUD hide:YES];
        }
        webData = nil;
        conn = nil;
    }
    if (![HUD isHidden]) {
        [HUD hide:YES afterDelay:0.2];
    }
}

#pragma mark Table view methods
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return STATIC_ROW_HEIGHT;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [newsAlertCategoryArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"NewsAlertCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.accessoryType   =   UITableViewCellAccessoryDisclosureIndicator;
        
        cell.textLabel.font             =   DEFAULT_FONT(FONT_STATIC_ROW_HEIGHT);
        cell.textLabel.numberOfLines    =   3;
        cell.textLabel.textColor        =   [UIColor blackColor];
        
        UIView *bgColorView         = [[UIView alloc] init];
        bgColorView.backgroundColor = DEFAULT_COLOR_WITH_ALPHA(0.5);
        cell.selectedBackgroundView = bgColorView;
    }
    NewsCategoryObject *tempCatgObj = (NewsCategoryObject *)[newsAlertCategoryArray objectAtIndex:indexPath.row];
    cell.textLabel.text= tempCatgObj.categoryName;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NewsCategoryObject *tempCatgObj = (NewsCategoryObject *)[newsAlertCategoryArray objectAtIndex:indexPath.row];

    MedicalNewsDetailsViewController *NewsdetailsViewController = [[MedicalNewsDetailsViewController alloc] initWithNibName:@"MedicalNewsDetailsViewController" bundle:nil] ;
    NewsdetailsViewController.title                 =   tempCatgObj.categoryName;
    NewsdetailsViewController.selectedAlertName     =   tempCatgObj.categoryName;
    NewsdetailsViewController.selectedSlugName      =   tempCatgObj.categorySlug;
    [self.navigationController pushViewController:NewsdetailsViewController animated:YES];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
@end
