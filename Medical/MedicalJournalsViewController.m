//
//  MedicalJournalsViewController.m
//  Medical
//
//  Created by Kirti Nikam on 11/10/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//
#import <sqlite3.h>
#import "MedicalAppDelegate.h"
#import "MedicalJournalsViewController.h"
#import "MedicalJournalCategorylistViewController.h"

@interface MedicalJournalsViewController ()

@end

@implementation MedicalJournalsViewController
@synthesize journalsTableView;
@synthesize journalCatgArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];    
    // Do any additional setup after loading the view from its nib.
    [INEventLogger logEvent:@"Journals_Category_List"];
    [self loadJournalCatgArray];
}
- (void)viewDidUnload
{
    [self setJournalsTableView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark - Rotation
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

-(void)loadJournalCatgArray{
    if (journalCatgArray == nil) {
        journalCatgArray = [[NSMutableArray alloc] init];
    }else{
        [journalCatgArray removeAllObjects];
    }
    sqlite3 *databaseP;
    if(sqlite3_open([[MEDICAL_APP_DELEGATE databasePath] UTF8String], &databaseP) == SQLITE_OK)
    {
        NSString *queryString = @"SELECT journal_category FROM journals group by journal_category order by journal_category";
        const char *queryCharP = [queryString UTF8String];
        sqlite3_stmt *compiledStatementP = nil;
        if (sqlite3_prepare_v2(databaseP, queryCharP, -1, &compiledStatementP, NULL) == SQLITE_OK)
        {
            while(sqlite3_step(compiledStatementP) == SQLITE_ROW)
            {
                NSString *journalCatg = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementP, 0)];
                [journalCatgArray addObject:journalCatg];
            }
        }
        sqlite3_finalize(compiledStatementP);
    }
    sqlite3_close(databaseP);
    [journalsTableView reloadData];
}


#pragma mark Table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return STATIC_ROW_HEIGHT;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [journalCatgArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        
        cell.accessoryType  =   UITableViewCellAccessoryDisclosureIndicator;
        cell.textLabel.font =   DEFAULT_FONT(FONT_STATIC_ROW_HEIGHT);
        cell.textLabel.numberOfLines = 3;
        
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = DEFAULT_COLOR_WITH_ALPHA(0.5);
        cell.selectedBackgroundView = bgColorView;
    }
    cell.textLabel.text =   [[journalCatgArray objectAtIndex:indexPath.row] capitalizedString];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   MedicalJournalCategorylistViewController *journalsOncologyviewController = [[MedicalJournalCategorylistViewController alloc] initWithNibName:@"MedicalJournalCategorylistViewController" bundle:nil];
    journalsOncologyviewController.journal_category = [journalCatgArray objectAtIndex:indexPath.row];
    journalsOncologyviewController.title            = [journalCatgArray objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:journalsOncologyviewController animated:YES];
}
@end
