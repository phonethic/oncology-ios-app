//
//  MedicalBillingViewController.m
//  Medical
//
//  Created by Kirti Nikam on 03/11/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import "MedicalBillingViewController.h"
#import "MedicalAppDelegate.h"
#import "MedicalDiseaseViewController.h"

@interface MedicalBillingViewController ()

@end

@implementation MedicalBillingViewController
@synthesize billingTableView;
@synthesize sections;
@synthesize searchBar;
@synthesize searchArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (isSearchOn) {
        [searchBar becomeFirstResponder];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [INEventLogger logEvent:@"IDC9_Codes_List"];

    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        searchBar.barTintColor = DEFAULT_COLOR;
        [searchBar setTranslucent:NO];
    }else{
        // searchBar.tintColor = DEFAULT_COLOR;
        for (UIView *searchBarSubView in searchBar.subviews) {
            if ([searchBarSubView conformsToProtocol:@protocol(UITextInputTraits)]) {
                @try {
                    [(UITextField *)searchBarSubView setBorderStyle:UITextBorderStyleRoundedRect];
                }
                @catch (NSException *exception) {
                    //  exception
                    DebugLog(@"Got exception : %@",exception);
                }
            }
        }
    }
    self.searchBar.autocorrectionType = UITextAutocorrectionTypeYes;
    isSearchOn = NO;
    canSelectRow = YES;
    keyboardHeight = 216;
    searchArray = [[NSMutableArray alloc] init];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:)
                                                 name:@"UIKeyboardWillShowNotification"
                                               object:nil];
    [self readBillingNameFromDatabase];
}

- (void)viewDidUnload
{
    [self setBillingTableView:nil];
    [self setSearchBar:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
#pragma mark - Rotation
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

-(void) readBillingNameFromDatabase {
    
    if (sections == nil) {
        sections = [[NSMutableDictionary alloc] init];
    }else{
        [sections removeAllObjects];
    }
    
	// Setup the database object
	sqlite3 *database;
    BOOL found;
	// Open the database from the users filessytem
	if(sqlite3_open([[MEDICAL_APP_DELEGATE databasePath] UTF8String], &database) == SQLITE_OK) {
		// Setup the SQL Statement and compile it for faster access
		const char *sqlStatement = "select classification from billing group by classification";
		sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
			// Loop through the results and add them to the feeds array
			while(sqlite3_step(compiledStatement) == SQLITE_ROW)
            {
				// Read the data from the result row
                NSString *cancerName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 0)];
                NSString *prefixChar = [[cancerName substringToIndex:1] uppercaseString];
                found = NO;
                
                for (NSString *str in [self.sections allKeys])
                {
                    if ([str isEqualToString:prefixChar])
                        found = YES;
                }
                if (!found)
                {
                    [self.sections setValue:[[NSMutableArray alloc] init] forKey:prefixChar];
                }
                [[self.sections objectForKey:prefixChar] addObject:cancerName];
                cancerName =  nil;
                
            }
        }
		// Release the compiled statement from memory
		sqlite3_finalize(compiledStatement);
	}
	sqlite3_close(database);
}

-(NSInteger) getrowsCount
{
    // Setup the database object
	sqlite3 *database;
    NSInteger value = 0;
    DebugLog(@"search TExt %@",searchBar.text);
	// Open the database from the users filessytem
	if(sqlite3_open([[MEDICAL_APP_DELEGATE databasePath] UTF8String], &database) == SQLITE_OK) {
		// Setup the SQL Statement and compile it for faster access
        NSString *regimenquerySQL = [NSString stringWithFormat:@"select count(DISTINCT classification) from billing where classification like\"%@%%\" COLLATE NOCASE", self.searchBar.text];
        const char *sqlStatement = [regimenquerySQL UTF8String];
		sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
			// Loop through the results and add them to the feeds array
			while(sqlite3_step(compiledStatement) == SQLITE_ROW)
            {
                value = sqlite3_column_int(compiledStatement, 0);
                DebugLog(@"classification value %d",value);
                
            }
        }
		// Release the compiled statement from memory
		sqlite3_finalize(compiledStatement);
	}
	sqlite3_close(database);
    //DebugLog(@"count = %d value = %d",[regimenNames count], value);
    return value;
}

-(void)reloadDataUsingSearch
{
    if (searchArray == nil) {
        searchArray = [[NSMutableArray alloc] init];
    }else{
        [searchArray removeAllObjects];
    }
    sqlite3 *database;
    // Open the database from the users filessytem
    if(sqlite3_open([[MEDICAL_APP_DELEGATE databasePath] UTF8String], &database) == SQLITE_OK) {
        // Setup the SQL Statement and compile it for faster access
        NSString *regimenquerySQL = [NSString stringWithFormat:@"select classification from billing where classification like \"%%%@%%\" group by classification COLLATE NOCASE", searchBar.text];
        const char *sqlStatement = [regimenquerySQL UTF8String];
        sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
            // Loop through the results and add them to the feeds array
            while(sqlite3_step(compiledStatement) == SQLITE_ROW)
            {
                NSString *cName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 0)];
                DebugLog(@"classification  %@",cName);
                [searchArray addObject:cName];
            }
        }
        // Release the compiled statement from memory
        sqlite3_finalize(compiledStatement);
    }
    sqlite3_close(database);
    DebugLog(@"search Array %@",searchArray);
    [billingTableView reloadData];
}

#pragma mark Table view methods
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    if (isSearchOn)
    {
        return nil;
    }
    else
    {
    return [[self.sections allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if (isSearchOn)
        return 1;
    else
        return [[self.sections allKeys] count];
    
}

-(CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section
{
    return 35;
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView* headerView          = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 35)];
    headerView.backgroundColor  = DEFAULT_COLOR;
    
    UILabel *headerLabel        = [[UILabel alloc] initWithFrame:CGRectZero];
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.frame           = CGRectMake(10,0,290,35);
    if (isSearchOn) {
        headerLabel.text    = @"";
    }else{
        headerLabel.text        = [[[self.sections allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:section];
        headerLabel.textAlignment   = NSTextAlignmentLeft;
        headerLabel.font            = DEFAULT_BOLD_FONT(17);
        headerLabel.textColor       = [UIColor whiteColor];
    }
    [headerView addSubview:headerLabel];
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *cName;
    if (isSearchOn && [searchArray count] > 0)
    {
        cName   =   [searchArray objectAtIndex:indexPath.row];
    }
    else
    {
        NSString *key = [[[self.sections allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section];
        NSArray *sortedArray = [[self.sections valueForKey:key] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
        cName = [sortedArray objectAtIndex:indexPath.row];
    }
    CGSize size = [cName sizeWithFont:DEFAULT_FONT(FONT_STATIC_ROW_HEIGHT) constrainedToSize:CGSizeMake(280, 999) lineBreakMode:NSLineBreakByWordWrapping];
    return size.height + 40;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (isSearchOn)
    {
        return [searchArray count];
    }
    else
    {
    return [[self.sections valueForKey:[[[self.sections allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:section]] count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = DEFAULT_COLOR_WITH_ALPHA(0.5);
        cell.selectedBackgroundView = bgColorView;
    }
    NSString *cName;

    if (isSearchOn && [searchArray count] > 0)
    {
        cName   =   [searchArray objectAtIndex:indexPath.row];
    }
    else
    {
        NSString *key = [[[self.sections allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section];
        NSArray *sortedArray = [[self.sections valueForKey:key] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
        cName = [sortedArray objectAtIndex:indexPath.row];
    }
    cell.textLabel.font=DEFAULT_FONT(FONT_STATIC_ROW_HEIGHT);
    cell.textLabel.text=[cName stringByReplacingCharactersInRange:NSMakeRange(0,1)
                                                       withString:[[cName  substringToIndex:1] capitalizedString]];
    cell.textLabel.numberOfLines = 0;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cName;
    if (isSearchOn && [searchArray count] > 0)
    {
        cName   =   [searchArray objectAtIndex:indexPath.row];
    }
    else
    {
        NSString *key = [[[self.sections allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section];
        NSArray *sortedArray = [[self.sections valueForKey:key] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
        cName = [sortedArray objectAtIndex:indexPath.row];
    }
    DebugLog(@"CName %@",cName);
    MedicalDiseaseViewController *diseaseviewController = [[MedicalDiseaseViewController alloc] initWithNibName:@"MedicalDiseaseViewController" bundle:nil] ;
    diseaseviewController.title =[cName stringByReplacingCharactersInRange:NSMakeRange(0,1)
                                                                withString:[[cName  substringToIndex:1] capitalizedString]];

    diseaseviewController.classification= cName;
    [self.navigationController pushViewController:diseaseviewController animated:YES];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (canSelectRow)
        return indexPath;
    else
        return nil;
}


- (void) keyboardWillShow:(NSNotification *)note {
    if ([self.view window]) //means is visible
    {    //do something
        NSDictionary *userInfo = [note userInfo];
        CGSize kbSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        
        DebugLog(@"userInfo %@", userInfo);
        
        DebugLog(@"Keyboard Height: %f Width: %f", kbSize.height, kbSize.width);
        
        keyboardHeight = kbSize.height;
        if (isSearchOn) {
            [UIView animateWithDuration:0.3 animations:^{
                [billingTableView setFrame:CGRectMake(billingTableView.frame.origin.x, billingTableView.frame.origin.y, billingTableView.frame.size.width, self.view.frame.size.height-(searchBar.frame.size.height+keyboardHeight))];
            }];
        }
    }else{
        //return
    }
}
#pragma mark SearchBar methods
//---fired when the user taps on the searchbar---
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    DebugLog(@"searchBarTextDidBeginEditing");
    
    isSearchOn=YES;
    if (self.searchBar.text.length >0)
    {
        canSelectRow=YES;
        self.billingTableView.scrollEnabled=YES;
    }
    else
    {
        canSelectRow=NO;
        self.billingTableView.scrollEnabled=NO;
    }
    
    self.navigationItem.rightBarButtonItem=[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneSearching:)];
    
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    DebugLog(@"textDidChange");
    
    if ([searchText length] > 0)
    {
        canSelectRow=YES;
        self.billingTableView.scrollEnabled=YES;
    }
    else
    {
        canSelectRow=NO;
        self.billingTableView.scrollEnabled=NO;
    }
    [self reloadDataUsingSearch];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self reloadDataUsingSearch];
}

-(void)doneSearching:(id)sender{
    DebugLog(@"doneSearching");
    
    isSearchOn=NO;
    canSelectRow=YES;
    self.billingTableView.scrollEnabled = YES;
    
    self.navigationItem.rightBarButtonItem = nil;
    [searchBar resignFirstResponder];
    searchBar.text=@"";
    [searchArray removeAllObjects];
    [self.billingTableView reloadData];

    [UIView animateWithDuration:0.3 animations:^{
        [billingTableView setFrame:CGRectMake(billingTableView.frame.origin.x, billingTableView.frame.origin.y, billingTableView.frame.size.width, self.view.frame.size.height-searchBar.frame.size.height)];
    }];
}
@end
