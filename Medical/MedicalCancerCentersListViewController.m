//
//  MedicalCancerCentersListViewController.m
//  Medical
//
//  Created by Kirti Nikam on 23/11/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//
#import <sqlite3.h>
#import "MedicalCancerCentersListViewController.h"
#import "MedicalCancerCenterDetailViewController.h"
#import "MedicalAppDelegate.h"

@interface MedicalCancerCentersListViewController ()

@end

@implementation MedicalCancerCentersListViewController
@synthesize centerlistTableView;
@synthesize searchBar;
@synthesize sections;
@synthesize searchArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (isSearchOn) {
        [searchBar becomeFirstResponder];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSDictionary *params = @{@"view_from":@"list"};
    [INEventLogger logEvent:@"Cancer_Centers_List" withParams:params];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        searchBar.barTintColor = DEFAULT_COLOR;
        [searchBar setTranslucent:NO];
    }else{
        // searchBar.tintColor = DEFAULT_COLOR;
        for (UIView *searchBarSubView in searchBar.subviews) {
            if ([searchBarSubView conformsToProtocol:@protocol(UITextInputTraits)]) {
                @try {
                    [(UITextField *)searchBarSubView setBorderStyle:UITextBorderStyleRoundedRect];
                }
                @catch (NSException *exception) {
                    //  exception
                    DebugLog(@"Got exception : %@",exception);
                }
            }
        }
    }
    isSearchOn = NO;
    canSelectRow = YES;
    keyboardHeight = 216;
    searchArray = [[NSMutableArray alloc] init];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:)
                                                 name:@"UIKeyboardWillShowNotification"
                                               object:nil];
    [self readCancerCentersFromDatabase];
}

- (void)viewDidUnload
{
    [self setCenterlistTableView:nil];
    [self setSearchBar:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
#pragma mark - Rotation
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

-(void) readCancerCentersFromDatabase {
	// Setup the database object
	sqlite3 *database;
    self.sections = [[NSMutableDictionary alloc] init];
    BOOL found;
	// Open the database from the users filessytem
	if(sqlite3_open([[MEDICAL_APP_DELEGATE databasePath] UTF8String], &database) == SQLITE_OK) {
		// Setup the SQL Statement and compile it for faster access
		const char *sqlStatement = "select center_name from cancer_centers group by center_name order by center_name";
		sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
			// Loop through the results and add them to the feeds array
			while(sqlite3_step(compiledStatement) == SQLITE_ROW)
            {
				// Read the data from the result row
                NSString *centerName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 0)];
                DebugLog(@"%@",centerName);
                if(centerName!=nil) {
                    NSString *prefixChar = [[centerName substringToIndex:1] uppercaseString];
                    found = NO;
                    
                    for (NSString *str in [self.sections allKeys])
                    {
                        if ([str isEqualToString:prefixChar])
                            found = YES;
                    }
                    if (!found)
                    {
                        [self.sections setValue:[[NSMutableArray alloc] init] forKey:prefixChar];
                    }
                    [[self.sections objectForKey:prefixChar] addObject:centerName];
                    centerName =  nil;
                }
                
            }
        }
		// Release the compiled statement from memory
		sqlite3_finalize(compiledStatement);
	}
	sqlite3_close(database);
    [centerlistTableView reloadData];
}

-(void)updateSearchArray{
    if (searchArray == nil) {
        searchArray = [[NSMutableArray alloc] init];
    }else{
        [searchArray removeAllObjects];
    }
    sqlite3 *database;
    // Open the database from the users filessytem
    if(sqlite3_open([[MEDICAL_APP_DELEGATE databasePath] UTF8String], &database) == SQLITE_OK) {
        // Setup the SQL Statement and compile it for faster access
        NSString *regimenquerySQL = [NSString stringWithFormat:@"select center_name from cancer_centers where center_name like \"%%%@%%\" group by center_name COLLATE NOCASE", searchBar.text];
        const char *sqlStatement = [regimenquerySQL UTF8String];
        sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
            // Loop through the results and add them to the feeds array
            while(sqlite3_step(compiledStatement) == SQLITE_ROW)
            {
                NSString *centerName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 0)];
                DebugLog(@"center_name  %@",centerName);
                [searchArray addObject:centerName];
            }
        }
        // Release the compiled statement from memory
        sqlite3_finalize(compiledStatement);
    }
    sqlite3_close(database);
    DebugLog(@"%d",searchArray.count);
    [centerlistTableView reloadData];
}

#pragma mark Table view methods

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    if (isSearchOn)
    {
        return nil;
    }
    else
    {
        return [[self.sections allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if (isSearchOn)
        return 1;
    else
        return [[self.sections allKeys] count];
    
}

-(CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section
{
    return 35;
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView* headerView          = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 35)];
    headerView.backgroundColor  = DEFAULT_COLOR;
    
    UILabel *headerLabel        = [[UILabel alloc] initWithFrame:CGRectZero];
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.frame           = CGRectMake(10,0,290,35);
    if (isSearchOn) {
        headerLabel.text    = @"";
    }else{
        headerLabel.text        = [[[self.sections allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:section];
        headerLabel.textAlignment   = NSTextAlignmentLeft;
        headerLabel.font            = DEFAULT_BOLD_FONT(17);
        headerLabel.textColor       = [UIColor whiteColor];
    }
    [headerView addSubview:headerLabel];
    return headerView;
}

//-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//{
//    if (isSearchOn)
//        return nil;
//    else
//    {
//        return [[[self.sections allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:section];
//    }
//}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *str;
    if (isSearchOn && [searchArray count] > 0)
    {
        str =  [searchArray objectAtIndex:indexPath.row];
    }
    else
    {
        NSString *key = [[[self.sections allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section];
        NSArray *sortedArray = [[self.sections valueForKey:key] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
        str =  [sortedArray objectAtIndex:indexPath.row];
    }
    
    CGSize size = [str sizeWithFont:DEFAULT_FONT(FONT_STATIC_ROW_HEIGHT) constrainedToSize:CGSizeMake(280, 999) lineBreakMode:NSLineBreakByWordWrapping];
    DebugLog(@"%f",size.height);
    return size.height + 40;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (isSearchOn)
    {
        return [searchArray count];
    }
    else
    {
        return [[self.sections valueForKey:[[[self.sections allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:section]] count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.textLabel.font=DEFAULT_FONT(FONT_STATIC_ROW_HEIGHT);
        cell.textLabel.numberOfLines = 3;
        
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = DEFAULT_COLOR_WITH_ALPHA(0.5);
        cell.selectedBackgroundView = bgColorView;
    }
    NSString *cName;
    
    if (isSearchOn && [searchArray count] > 0)
    {
        cName = [searchArray objectAtIndex:indexPath.row];
    }
    else
    {
        NSString *key = [[[self.sections allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section];
        NSArray *sortedArray = [[self.sections valueForKey:key] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
        cName = [sortedArray objectAtIndex:indexPath.row];
    }
    cell.textLabel.text= cName;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cName;
    if (isSearchOn && [searchArray count] > 0)
    {
        cName=[searchArray objectAtIndex:indexPath.row];
    }
    else
    {
        NSString *key = [[[self.sections allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section];
        NSArray *sortedArray = [[self.sections valueForKey:key] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
        cName = [sortedArray objectAtIndex:indexPath.row];
    }
    NSDictionary *params = @{@"center_name": cName,@"clicked_from":@"list"};
    [INEventLogger logEvent:@"Cancer_Centers_Details_Click" withParams:params];
    
    //DebugLog(@"CName %@",cName);
    MedicalCancerCenterDetailViewController *centerdetailviewController = [[MedicalCancerCenterDetailViewController alloc] initWithNibName:@"MedicalCancerCenterDetailViewController" bundle:nil] ;
    centerdetailviewController.centerName = cName;
    [self.navigationController pushViewController:centerdetailviewController animated:YES];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (canSelectRow)
        return indexPath;
    else
        return nil;
}

- (void) keyboardWillShow:(NSNotification *)note {
    if ([self.view window]) //means is visible
    {    //do something
        NSDictionary *userInfo = [note userInfo];
        CGSize kbSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        
        DebugLog(@"userInfo %@", userInfo);
        
        DebugLog(@"Keyboard Height: %f Width: %f", kbSize.height, kbSize.width);
        
        keyboardHeight = kbSize.height;
        if (isSearchOn) {
            [UIView animateWithDuration:0.3 animations:^{
                [centerlistTableView setFrame:CGRectMake(centerlistTableView.frame.origin.x, centerlistTableView.frame.origin.y, centerlistTableView.frame.size.width, self.view.frame.size.height-(searchBar.frame.size.height+keyboardHeight))];
            }];
        }
    }else{
        //return
    }
}
#pragma mark SearchBar methods
//---fired when the user taps on the searchbar---
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    DebugLog(@"searchBarTextDidBeginEditing");
    
    isSearchOn=YES;
    if (self.searchBar.text.length >0)
    {
        canSelectRow=YES;
        self.centerlistTableView.scrollEnabled=YES;
    }
    else
    {
        canSelectRow=NO;
        self.centerlistTableView.scrollEnabled=NO;
    }
    
    self.navigationItem.rightBarButtonItem=[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneSearching:)];
    
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    DebugLog(@"textDidChange");
    
    if ([searchText length] > 0)
    {
        canSelectRow=YES;
        self.centerlistTableView.scrollEnabled=YES;
    }
    else
    {
        canSelectRow=NO;
        self.centerlistTableView.scrollEnabled=NO;
    }
    [self updateSearchArray];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self updateSearchArray];
}

-(void)doneSearching:(id)sender{
    DebugLog(@"doneSearching");
    
    isSearchOn   = NO;
    canSelectRow = YES;
    self.centerlistTableView.scrollEnabled = YES;
    self.navigationItem.rightBarButtonItem = nil;
    [searchBar resignFirstResponder];
    searchBar.text=@"";
    [searchArray removeAllObjects];
    [self.centerlistTableView reloadData];

    [UIView animateWithDuration:0.3 animations:^{
        [centerlistTableView setFrame:CGRectMake(centerlistTableView.frame.origin.x, centerlistTableView.frame.origin.y, centerlistTableView.frame.size.width, self.view.frame.size.height-searchBar.frame.size.height)];
    }];
}
@end
