//
//  MedicalCTCAEViewController.m
//  Medical
//
//  Created by Kirti Nikam on 30/10/13.
//  Copyright (c) 2013 Kirti Nikam. All rights reserved.
//
#import <sqlite3.h>
#import "MedicalAppDelegate.h"
#import "MedicalCTCAEViewController.h"
#import "MedicalCTCAEObject.h"
#import "MedicalCTCAEDetailsViewController.h"
#import "MedicalWebViewController.h"

@interface MedicalCTCAEViewController ()

@end

@implementation MedicalCTCAEViewController
@synthesize searchArray;
@synthesize sections;
@synthesize ctcaeTableView;
@synthesize searchBar;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (isSearchOn) {
        [searchBar becomeFirstResponder];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [INEventLogger logEvent:@"CTCAE_List"];

    [self addInfoBarButton];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        searchBar.barTintColor = DEFAULT_COLOR;
        [searchBar setTranslucent:NO];
    }else{
        // searchBar.tintColor = DEFAULT_COLOR;
        for (UIView *searchBarSubView in searchBar.subviews) {
            if ([searchBarSubView conformsToProtocol:@protocol(UITextInputTraits)]) {
                @try {
                    [(UITextField *)searchBarSubView setBorderStyle:UITextBorderStyleRoundedRect];
                }
                @catch (NSException *exception) {
                    //  exception
                    DebugLog(@"Got exception : %@",exception);
                }
            }
        }
    }
    isSearchOn = NO;
    canSelectRow = YES;
    searchArray = [[NSMutableArray alloc] init];
    keyboardHeight = 216;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:)
                                                 name:@"UIKeyboardWillShowNotification"
                                               object:nil];
    [self loadCTCAEDataFromDatabase];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setCtcaeTableView:nil];
    [self setSearchBar:nil];
    [super viewDidUnload];
}
#pragma mark - Rotation
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma internal methods
-(void)addInfoBarButton{
    UIButton *infoBtn = [UIButton buttonWithType:UIButtonTypeInfoLight];
    infoBtn.frame = CGRectMake(200, 2, 30, 30);
    [infoBtn addTarget:self action:@selector(infoBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:infoBtn];
}

-(void)infoBtnPressed
{
    DebugLog(@"infoBtnPressed");
    [INEventLogger logEvent:@"CTCAE_i_Button"];
    [self openWebPage:DISCLAIMER_TITLE url:CTCAE_DISCLAIMER_INFO];
}

-(void)closeWebView
{
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        DebugLog(@"closeWebView");
    }];
}

-(void)openWebPage:(NSString *)title url:(NSString *)urlLink{
    if ([urlLink length] > 0) {
        MedicalWebViewController *webViewController = [[MedicalWebViewController alloc] initWithNibName:@"MedicalWebViewController" bundle:nil];
        webViewController.title = title;
        webViewController.medicalLink = urlLink;
        
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:webViewController];
        UIButton *closeBtn =  [UIButton buttonWithType:UIButtonTypeCustom];
        [closeBtn setFrame:CGRectMake(276, 0, 44, 44)];
        [closeBtn setTitle:@"" forState:UIControlStateNormal];
        [closeBtn setImage:[UIImage imageNamed:@"cancel_white.png"] forState:UIControlStateNormal];
        [closeBtn addTarget:self action:@selector(closeWebView) forControlEvents:UIControlEventTouchUpInside];
        [navController.navigationBar addSubview:closeBtn];
        [navController.navigationBar setTranslucent:NO];
        [self.navigationController presentViewController:navController animated:YES completion:nil];
    }
}

#pragma mark Table view methods
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    if (isSearchOn)
    {
        return nil;
    }
    return [[self.sections allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (isSearchOn) {
        return 1;
    }
    return [[self.sections allKeys] count];
}

-(CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section
{
    return 35;
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView* headerView          = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 35)];
    headerView.backgroundColor  = DEFAULT_COLOR;
    
    UILabel *headerLabel        = [[UILabel alloc] initWithFrame:CGRectZero];
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.frame           = CGRectMake(10,0,290,35);
    if (isSearchOn) {
        headerLabel.text    = @"";
    }else{
        headerLabel.text        = [[[self.sections allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:section];
        headerLabel.textAlignment   = NSTextAlignmentLeft;
        headerLabel.font            = DEFAULT_BOLD_FONT(17);
        headerLabel.textColor       = [UIColor whiteColor];
    }
    [headerView addSubview:headerLabel];
    return headerView;
}

//-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//{
//    if (isSearchOn) {
//        return nil;
//    }
//    return [[[self.sections allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:section];
//}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return STATIC_ROW_HEIGHT;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (isSearchOn) {
        return [searchArray count];
    }
    return [[self.sections valueForKey:[[[self.sections allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:section]] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CTCAECell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = DEFAULT_COLOR_WITH_ALPHA(0.5);
        cell.selectedBackgroundView = bgColorView;
    }
    cell.accessoryType  =   UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.font =   DEFAULT_FONT(FONT_STATIC_ROW_HEIGHT);
    cell.textLabel.numberOfLines = 3;
    if (isSearchOn && [searchArray count] > 0) {
        MedicalCTCAEObject *tempObj = (MedicalCTCAEObject *)[searchArray objectAtIndex:indexPath.row];
        cell.textLabel.text= [tempObj.term capitalizedString];
    }else{
        NSString *key = [[[self.sections allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section];
        NSArray *sortedArray = [[self.sections valueForKey:key] sortedArrayUsingDescriptors:[NSArray arrayWithObject:[[NSSortDescriptor alloc] initWithKey:@"term" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)]]];
        MedicalCTCAEObject *tempObj = (MedicalCTCAEObject *)[sortedArray objectAtIndex:indexPath.row];
        DebugLog(@"%@",tempObj.term);
        cell.textLabel.text= tempObj.term;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MedicalCTCAEObject *tempObj = nil;
    if (isSearchOn && [searchArray count] > 0) {
        tempObj = (MedicalCTCAEObject *)[searchArray objectAtIndex:indexPath.row];
    }else{
        NSString *key = [[[self.sections allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section];
        NSArray *sortedArray = [[self.sections valueForKey:key] sortedArrayUsingDescriptors:[NSArray arrayWithObject:[[NSSortDescriptor alloc] initWithKey:@"term" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)]]];
        tempObj = (MedicalCTCAEObject *)[sortedArray objectAtIndex:indexPath.row];
    }
    MedicalCTCAEDetailsViewController *CTCAEdetailsViewController = [[MedicalCTCAEDetailsViewController alloc] initWithNibName:@"MedicalCTCAEDetailsViewController" bundle:nil] ;
    CTCAEdetailsViewController.title            = tempObj.term;
    CTCAEdetailsViewController.selectedTermID   = tempObj.rowId;
    CTCAEdetailsViewController.selectedTermName = tempObj.term;
    [self.navigationController pushViewController:CTCAEdetailsViewController animated:YES];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (canSelectRow)
        return indexPath;
    else
        return nil;
}

- (void) keyboardWillShow:(NSNotification *)note {
    if ([self.view window]) //means is visible
    {    //do something
        NSDictionary *userInfo = [note userInfo];
        CGSize kbSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        
        DebugLog(@"userInfo %@", userInfo);
        
        DebugLog(@"Keyboard Height: %f Width: %f", kbSize.height, kbSize.width);
        
        keyboardHeight = kbSize.height;
        if (isSearchOn) {
            [UIView animateWithDuration:0.3 animations:^{
                [ctcaeTableView setFrame:CGRectMake(ctcaeTableView.frame.origin.x, ctcaeTableView.frame.origin.y, ctcaeTableView.frame.size.width, self.view.frame.size.height-(searchBar.frame.size.height+keyboardHeight))];
            }];
        }
    }else{
        //return
    }
}

#pragma UISearchBar Delegate methods
-(void)searchBarTextDidBeginEditing:(UISearchBar *)lsearchBar{
    
    isSearchOn = YES;
    if ([searchBar.text length] > 0) {
        canSelectRow = YES;
        self.ctcaeTableView.scrollEnabled = YES;
    }else{
        canSelectRow = NO;
        self.ctcaeTableView.scrollEnabled = NO;
    }
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneSearching:)];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if ([searchText length] > 0) {
        canSelectRow = YES;
        self.ctcaeTableView.scrollEnabled = YES;
    }else{
        canSelectRow = NO;
        self.ctcaeTableView.scrollEnabled = NO;
    }
    [self updateSearchArray];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [self updateSearchArray];
}

-(void)doneSearching:(id)sender{
    DebugLog(@"doneSearching");
    
    isSearchOn=NO;
    canSelectRow=YES;
    self.ctcaeTableView.scrollEnabled = YES;
    
    self.navigationItem.rightBarButtonItem = nil;
    [searchBar resignFirstResponder];
    searchBar.text=@"";
    [searchArray removeAllObjects];
    [self.ctcaeTableView reloadData];

    [UIView animateWithDuration:0.3 animations:^{
        [ctcaeTableView setFrame:CGRectMake(ctcaeTableView.frame.origin.x, ctcaeTableView.frame.origin.y, ctcaeTableView.frame.size.width, self.view.frame.size.height-searchBar.frame.size.height)];
    }];
    
    [self addInfoBarButton];
}

-(void)updateSearchArray{
    if (searchArray == nil) {
        searchArray = [[NSMutableArray alloc] init];
    }else{
        [searchArray removeAllObjects];
    }
    sqlite3 *databaseP = nil;
    if(sqlite3_open([[MEDICAL_APP_DELEGATE databasePath] UTF8String], &databaseP) == SQLITE_OK) {
		// Setup the SQL Statement and compile it for faster access
        NSString *regimenquerySQL = [NSString stringWithFormat:@"select rowid,term from CTCAE where term like\"%%%@%%\" COLLATE NOCASE", self.searchBar.text];
        const char *sqlStatement = [regimenquerySQL UTF8String];
		sqlite3_stmt *statement;
        if(sqlite3_prepare_v2(databaseP, sqlStatement, -1, &statement, NULL) == SQLITE_OK) {
			// Loop through the results and add them to the feeds array
			while(sqlite3_step(statement) == SQLITE_ROW)
            {
                int rowId           = sqlite3_column_int(statement, 0);
                NSString *termName  = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)];
                DebugLog(@"-%d-%@-",rowId,termName);
                
                if (termName != nil && ![termName isEqualToString:@""]) {
                    tempCTCAEObj = [[MedicalCTCAEObject alloc] init];
                    tempCTCAEObj.rowId   = rowId;
                    tempCTCAEObj.term    = termName;
                    [searchArray addObject:tempCTCAEObj];
                    tempCTCAEObj = nil;
                }
            }
        }
		sqlite3_finalize(statement);
	}
	sqlite3_close(databaseP);
    //    DebugLog(@"%d",searchArray.count);
    [ctcaeTableView reloadData];
}

-(void)loadCTCAEDataFromDatabase{
    if (sections == nil) {
        sections = [[NSMutableDictionary alloc] init];
    }else{
        [sections removeAllObjects];
    }
    BOOL found;
    sqlite3 *databaseP = nil;
    if (sqlite3_open([[MEDICAL_APP_DELEGATE databasePath] UTF8String], &databaseP) == SQLITE_OK) {
        NSString *sqlQueryString = @"select rowid,term from CTCAE order by term";
        
        const char *sqlQueryChar = [sqlQueryString UTF8String];
        sqlite3_stmt *statement = nil;
        if (sqlite3_prepare(databaseP, sqlQueryChar, -1, &statement, NULL)  == SQLITE_OK) {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                int rowId           = sqlite3_column_int(statement, 0);
                NSString *termName  = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)];
                DebugLog(@"-%d-%@-",rowId,termName);
                
                if (termName != nil && ![termName isEqualToString:@""]) {
                    tempCTCAEObj = [[MedicalCTCAEObject alloc] init];
                    tempCTCAEObj.rowId   = rowId;
                    tempCTCAEObj.term    = termName;
                    
                    NSString *prefixChar = [[termName substringToIndex:1] uppercaseString];
                    found = NO;
                    
                    for(NSString *str in sections.allKeys){
                        if ([str isEqualToString:prefixChar])
                        {
                            found = YES;
                            break;
                        }
                    }
                    if (!found) {
                        [sections setValue:[[NSMutableArray alloc] init] forKey:prefixChar];
                    }
                    [[self.sections objectForKey:prefixChar] addObject:tempCTCAEObj];
                    tempCTCAEObj = nil;
                }
            }
            sqlite3_finalize(statement);
        }
        sqlite3_close(databaseP);
    }
    //    DebugLog(@"%@",sections);
    [ctcaeTableView reloadData];
}
@end
