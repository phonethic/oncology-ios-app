//
//  MedicalDiseaseViewController.m
//  Medical
//
//  Created by Kirti Nikam on 03/11/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import "MedicalDiseaseViewController.h"
#import "MedicalAppDelegate.h"
#import "MedicalDiseaseDetailsViewController.h"
#import "DiseaseObject.h"

@interface MedicalDiseaseViewController ()

@end

@implementation MedicalDiseaseViewController
@synthesize diseaseTableView,classification;
@synthesize classificationDataArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSDictionary *params = @{@"code_name":classification};
    [INEventLogger logEvent:@"IDC9_Codes_Details_List" withParams:params];

    DebugLog(@"classification %@",classification);
    
    [self loadClassificationDataArray];
}

- (void)viewDidUnload
{
    [self setDiseaseTableView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
#pragma mark - Rotation
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

-(void)loadClassificationDataArray{
    if (classificationDataArray == nil) {
        classificationDataArray = [[NSMutableArray alloc] init];
    }else{
        [classificationDataArray removeAllObjects];
    }
    
    sqlite3 *database;
    NSString *dcode;
    NSString *dName;
    NSString *dDetails;
	// Open the database from the users filessytem
	if(sqlite3_open([[MEDICAL_APP_DELEGATE databasePath] UTF8String], &database) == SQLITE_OK) {
		// Setup the SQL Statement and compile it for faster access
        NSString *regimenquerySQL = [NSString stringWithFormat:@"select disease_code,disease_name,disease_details from billing where classification=\"%@\" COLLATE NOCASE order by disease_name", self.classification];
        const char *sqlStatement = [regimenquerySQL UTF8String];
		sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
			// Loop through the results and add them to the feeds array
			while(sqlite3_step(compiledStatement) == SQLITE_ROW)
            {
                dcode = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 0)];
                dName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 1)];
                dDetails = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 2)];
                DebugLog(@"classification disease %@",dName);
                
                tempObj = [[DiseaseObject alloc] init];
                tempObj.dcode = dcode;
                tempObj.dName = dName;
                tempObj.dDetails = dDetails;
                [classificationDataArray addObject:tempObj];
                tempObj = nil;
            }
        }
		// Release the compiled statement from memory
		sqlite3_finalize(compiledStatement);
	}
	sqlite3_close(database);
    [diseaseTableView reloadData];
}

#pragma mark Table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [classificationDataArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return STATIC_ROW_HEIGHT;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UILabel *lblValue,*lblLevel;
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        
        lblValue = [[UILabel alloc] initWithFrame:CGRectMake(cell.frame.origin.x+6, cell.frame.origin.y , 76, STATIC_ROW_HEIGHT)];
        lblValue.tag                = 200;
        lblValue.font               = DEFAULT_FONT(16);
        lblValue.textAlignment      = NSTextAlignmentCenter;
        lblValue.textColor          = [UIColor whiteColor];
        lblValue.backgroundColor    = DEFAULT_COLOR;//NAVIGATION_THEMELIGHTCOLOR;
        [cell.contentView addSubview:lblValue];
        
        lblLevel = [[UILabel alloc] initWithFrame:CGRectMake(lblValue.frame.size.width + 10, cell.frame.origin.y , 220, STATIC_ROW_HEIGHT)];
        lblLevel.tag                = 300;
        lblLevel.font               = DEFAULT_FONT(16);
        lblLevel.textAlignment      = NSTextAlignmentLeft;
        lblLevel.textColor          = [UIColor blackColor];
        lblLevel.backgroundColor    = [UIColor clearColor];
        lblLevel.numberOfLines      = 7;
        [cell.contentView addSubview:lblLevel];
        
        UIView *bgColorView         = [[UIView alloc] init];
        bgColorView.backgroundColor = DEFAULT_COLOR_WITH_ALPHA(0.5);
        cell.selectedBackgroundView = bgColorView;
    }
    DiseaseObject *dObj = (DiseaseObject *)[classificationDataArray objectAtIndex:indexPath.row];
    if(dObj.dDetails != nil && ![dObj.dDetails isEqualToString:@""])
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    else
        cell.accessoryType = UITableViewCellAccessoryNone;
    
    lblValue = (UILabel *)[cell viewWithTag:200];
    lblLevel = (UILabel *)[cell viewWithTag:300];
    lblValue.text = dObj.dcode;
    lblLevel.text = dObj.dName;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DiseaseObject *dObj = (DiseaseObject *)[classificationDataArray objectAtIndex:indexPath.row];
    DebugLog(@"class %@ dObj.dName  %@",classification ,dObj.dName);
    if(dObj.dDetails != nil && ![dObj.dDetails isEqualToString:@""])
    {
        NSDictionary *params = @{@"code_name":classification,@"level_name":dObj.dName};
        [INEventLogger logEvent:@"IDC9_Codes_Details" withParams:params];

        MedicalDiseaseDetailsViewController *diseaseDetailsviewController = [[MedicalDiseaseDetailsViewController alloc] initWithNibName:@"MedicalDiseaseDetailsViewController" bundle:nil] ;
        diseaseDetailsviewController.diseaseDetailText  =   dObj.dDetails;
        diseaseDetailsviewController.diseaseNameText    =   [NSString stringWithFormat:@"%@ : %@",dObj.dcode,dObj.dName];
        [self.navigationController pushViewController:diseaseDetailsviewController animated:YES];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
@end
