//
//  MedicalCTCAEDetailsViewController.h
//  Medical
//
//  Created by Kirti Nikam on 30/10/13.
//  Copyright (c) 2013 Kirti Nikam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MedicalCTCAEDetailsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (readwrite,nonatomic) int selectedTermID;
@property (copy,nonatomic) NSString *selectedTermName;
@property (strong,nonatomic) NSMutableDictionary *termObjectDict;


@property (strong, nonatomic) IBOutlet UITableView *ctcaedetailTableView;
@end
