//
//  MedicalViewController.m
//  Medical
//
//  Created by Kirti Nikam on 08/10/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//
#import <Social/Social.h>

#import "MedicalViewController.h"
#import "MedicalFormulaViewController.h"
#import "MedicalJournalsViewController.h"
#import "MedicalChemoViewController.h"
#import "MedicalBillingViewController.h"
#import "MedicalResourceViewController.h"
#import "MedicalChemoDrugsViewController.h"
#import "MedicalUsefulSitesViewController.h"
#import "MedicalMeetingViewController.h"
#import "MedicalCTCAEViewController.h"

#import "MedicalRSSFeedsViewController.h"
#import "MedicalDrugInteractionsViewController.h"
#import "MedicalNewsViewController.h"
#import "MedicalWebViewController.h"

#import "MedicalSignUpViewController.h"
#import "MedicalAppDelegate.h"
#import "MedicalUserDefaults.h"

#import "CommonCallback.h"

#import "iRate.h"

#define ALERT_POST_LINK @"http://cancerebook.org/?json=get_posts&orderby=date&order=desc"

#define SHARE_APP_MESSAGE_BODY_TEXT_EMAIL @"<!DOCTYPE html><body><div><h2 style=\"font-size:16px;font-weight: bold;font-family:OpenSans-Bold;color:#3498db;\">Cancer eBook: an App for busy oncology professionals</h2><p style=\"font-size:12px;font-weight: normal;font-family:OpenSans\">Designed by oncology professionals for all those who are involved in caring for cancer patients and cancer research.</p><p style=\"font-size:14px;font-weight: bold;font-family:OpenSans-Bold;color:#3498db;\">What's in the app?</p><ul style=\"font-size:12px;font-weight: normal;font-family:OpenSans;\"><li> Chemotherapy Regimens and Oncology Drugs</li><li> Drug-Drug interactions</li><li> Interesting and intriguing Oncology Alerts!</li><li> CTCAE version 4.0</li><li> and much more .. </li></ul><p style=\"font-size:14px;font-weight: bold;font-family:OpenSans-Bold;color:#3498db;\">Download the Cancer eBook App Now </p><p><a href=\"http://bit.ly/CancereBook\" style=\"text-decoration:none;vertical-align: top;padding: 10px 19px;letter-spacing: 1;font-weight: normal;font-size:16px;font-family:OpenSans;color: white;background-color: #3498db;text-align: center;text-shadow: 0 1px 2px rgba(0, 0, 0, 0.25);border-radius: 4px;white-space: normal;border: none;border-bottom: 2px solid #2a8bcc;-webkit-box-shadow: inset 0 -2px #2a8bcc;box-shadow: inset 0 -2px #2a8bcc;\">iPhone®</a>&nbsp;&nbsp;<a href=\"http://bit.ly/CancereBook\" style=\"text-decoration:none;vertical-align: top;padding: 10px 19px;letter-spacing: 1;font-weight: normal;font-size:16px;font-family:OpenSans;color: white;background-color: #3498db;text-align: center;text-shadow: 0 1px 2px rgba(0, 0, 0, 0.25);border-radius: 4px;white-space: normal;border: none;border-bottom: 2px solid #2a8bcc;-webkit-box-shadow: inset 0 -2px #2a8bcc;box-shadow: inset 0 -2px #2a8bcc;\">iPad®</a></p></div></body></html>"

#define SHARE_APP_MESSAGE_BODY_TEXT_OTHER @"Cancer eBook: An app designed by oncology professionals for all those involved in caring for cancer patients and cancer research.\nDownload the Cancer eBook app Now - http://bit.ly/CancereBook"

#define SHARE_APP_MESSAGE_BODY_TEXT_TWITTER @"Hey, I found a Cancer eBook app: http://bit.ly/CancereBook designed for all oncology professionals caring for cancer patients."

#define XAXIS 5
#define YAXIS 50
#define HORIZONTAL_PADDING 5
#define VERTICAL_PADDING 25
#define WIDTH 100
#define HEIGHT 100

//Info alert
#define ABOUT @"About"
#define CONTRIBUTERS @"Contributors"
#define TERMS_CONDITIONS @"Terms and conditions"
#define PRIVATE_POLICY @"Privacy Policy"
#define TERMS_AND_PRIVACY @"Terms and Privacy"
#define CONTACTUS @"Contact us"
#define SIGNUP @"SIGN UP"
#define RATE_APP @"Rate this app"
#define SHARE_THIS_APP @"Share this app"

@interface MedicalViewController () <iRateDelegate>
@property (strong, nonatomic) IBOutlet UIView *postAlertView;
@property (strong, nonatomic) IBOutlet UIImageView *postAlertImageView;

@property (strong, nonatomic) IBOutlet UILabel *postAlertLabel;

@end

@implementation MedicalViewController
@synthesize gridScrollView;
@synthesize gridpageControl;
@synthesize gridbuttonNamesArray;
@synthesize btnSignUp,btnCollection;
@synthesize btnChemoOrders,btnChemotherapy,btnDrugInteraction,btnCalc,btnCTCAE,btnAlerts,btnWebLinks,btnResources,btnRSS;
@synthesize webData,newsAlerts;
@synthesize postAlertView,postAlertImageView,postAlertLabel;
@synthesize btnMeeting;

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self performSelector:@selector(sendGetAlertPostsRequest) withObject:nil afterDelay:1.0];
    [self performSelector:@selector(checkUpdatedAppVersion) withObject:nil afterDelay:1.0];
}


-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if (conn) {
        [conn cancel];
        conn = nil;
    }
}

-(void)checkUpdatedAppVersion
{
    [MEDICAL_APP_DELEGATE checkAppVersion];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.navigationController.navigationBar.translucent = NO;

    self.navigationItem.title = ALERT_TITLE;
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.backBarButtonItem = backButton;
    
    UIButton *infoBtn = [UIButton buttonWithType:UIButtonTypeInfoLight];
    infoBtn.frame = CGRectMake(200, 2, 30, 30);
    [infoBtn addTarget:self action:@selector(infoBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:infoBtn];
    
//    gridbuttonNamesArray=[[NSArray alloc]initWithObjects:ChemoOrders,ChemoDrugs,Drug_Interactions,MedCalc,CTCAE,News,UsefulSites,Resources,RSS, nil];
//
//    [gridScrollView setCanCancelContentTouches:NO];
//	gridScrollView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
//	gridScrollView.clipsToBounds = YES;		// default is NO, we want to restrict drawing within our gridScrollView
//	gridScrollView.scrollEnabled = YES;
//    gridScrollView.bounces = NO;
//    gridScrollView.showsVerticalScrollIndicator = NO;
//    gridScrollView.directionalLockEnabled = YES;
//    gridScrollView.delegate = self;
//    gridScrollView.pagingEnabled = YES;
//    
//    [self createGridWithButtons];
    
    [self setUI];
    [self hidePostAlertWithanimation:NO];

    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected:)];
    tapGesture.numberOfTapsRequired = 1;
    tapGesture.cancelsTouchesInView = NO;
    [postAlertView addGestureRecognizer:tapGesture];
    
	//set myself as iRate delegate
    //you don't actually need to set this if you
    //are using the AppDelegate as your iRate delegate
    //as that is the default iRate delegate anyway
	[iRate sharedInstance].delegate = self;
}

- (void)viewDidUnload
{
    [self setGridScrollView:nil];
    [self setGridpageControl:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

#pragma mark - Rotation
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma internal methods
-(void) setUI{
//    postAlertView.layer.borderWidth = 2.0;
//    postAlertView.layer.borderColor = DEFAULT_COLOR.CGColor;
    
    postAlertLabel.font = DEFAULT_BOLD_FONT(15);
    postAlertLabel.text = @"";
    postAlertLabel.numberOfLines = 4;
    postAlertLabel.textColor = DEFAULT_COLOR;
    
    [btnChemoOrders setTitle:ChemoOrders forState:UIControlStateNormal];
    [btnChemotherapy setTitle:ChemoDrugs forState:UIControlStateNormal];
    [btnDrugInteraction setTitle:Drug_Interactions forState:UIControlStateNormal];
    [btnCalc setTitle:MedCalc forState:UIControlStateNormal];
    [btnMeeting setTitle:Meetings forState:UIControlStateNormal];
    [btnCTCAE setTitle:CTCAE forState:UIControlStateNormal];
    [btnAlerts setTitle:News forState:UIControlStateNormal];
    [btnWebLinks setTitle:UsefulSites forState:UIControlStateNormal];
    [btnResources setTitle:Resources forState:UIControlStateNormal];
    [btnRSS setTitle:RSS forState:UIControlStateNormal];
    
    for (UIButton *btn in btnCollection) {
        btn.backgroundColor     = [UIColor clearColor];
    }
    btnSignUp.backgroundColor     = [UIColor whiteColor];
    [btnSignUp setTitleColor:DEFAULT_COLOR forState:UIControlStateNormal];
    [btnSignUp setTitleColor:DEFAULT_COLOR_WITH_ALPHA(0.5) forState:UIControlStateHighlighted];
    btnSignUp.titleLabel.font     = DEFAULT_BOLD_FONT(25);
    [btnSignUp setTitle:SIGNUP forState:UIControlStateNormal];
    btnSignUp.layer.cornerRadius  = 5.0;
}

-(void)tapDetected:(UIGestureRecognizer *)sender{
    [self hidePostAlertWithanimation:YES];
}

-(void)infoBtnPressed
{
    DebugLog(@"infoBtnPressed");
    [INEventLogger logEvent:@"Home_i_button"];
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Select option" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:ABOUT,CONTRIBUTERS,TERMS_AND_PRIVACY,CONTACTUS,RATE_APP,SHARE_THIS_APP, nil];
    [actionSheet setActionSheetStyle:UIActionSheetStyleDefault];
    [actionSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSString *title = [actionSheet buttonTitleAtIndex:buttonIndex];
    if ([title isEqualToString:ABOUT])
    {
        DebugLog(@"ABOUT");
        [INEventLogger logEvent:@"Tab_About"];
        [self openWebPage:ABOUT url:ABOUT_INFO];
    }
    else if ([title isEqualToString:CONTRIBUTERS])
    {
          DebugLog(@"CONTRIBUTERS");
        [INEventLogger logEvent:@"Tab_Contributors"];
        [self openWebPage:CONTRIBUTERS url:CONTRIBUTERS_INFO];
    }
    else if ([title isEqualToString:TERMS_CONDITIONS])
    {
          DebugLog(@"TERMS_CONDITIONS");
        [self openWebPage:TERMS_CONDITIONS url:TERMS_CONDITIONS_INFO];
    }
    else if ([title isEqualToString:PRIVATE_POLICY])
    {
          DebugLog(@"PRIVATE_POLICY");
        [self openWebPage:PRIVATE_POLICY url:PRIVATE_POLICY_INFO];
    }
    else if ([title isEqualToString:TERMS_AND_PRIVACY])
    {
        DebugLog(@"TERMS_AND_PRIVACY");
        [INEventLogger logEvent:@"Tab_Terms_And_Privacy"];
        [self openWebPage:TERMS_AND_PRIVACY url:TERMS_AND_PRIVATE_POLICY_INFO];
    }
    else if ([title isEqualToString:CONTACTUS])
    {
          DebugLog(@"CONTACTUS");
        [INEventLogger logEvent:@"Tab_ContactUs"];
         [self sendEmail];
         return;
    }
    else if ([title isEqualToString:RATE_APP])
    {
        DebugLog(@"RATE_APP");
        [INEventLogger logEvent:@"Tab_Rate_This_App"];
        [self showRateAppAlert];
        return;
    }
    else if ([title isEqualToString:SHARE_THIS_APP])
    {
        DebugLog(@"SHARE_THIS_APP");
        [self showShareAppAlert];
        return;
    }
    else if([title isEqualToString:@"SMS/Text"])
    {
        [self smsShareClicked];
    }
    else if([title isEqualToString:@"Email"])
    {
        [self emailShareClicked];
    }
    else if([title isEqualToString:@"WhatsApp"])
    {
        [self whatsAppShareClicked];
    }
    else if([title isEqualToString:@"Facebook"])
    {
        [self facebookShareClicked];
    }
    else if([title isEqualToString:@"Twitter"])
    {
        [self twitterShareClicked];
    }
}

-(void)showShareAppAlert{
    UIActionSheet *shareActionSheet = [[UIActionSheet alloc] initWithTitle:@"Share via" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"SMS/Text",@"Email",@"WhatsApp",@"Facebook",@"Twitter", nil];
    [shareActionSheet showInView:self.view];
}


- (void)smsShareClicked{
    if ([MFMessageComposeViewController canSendText]) {
        MFMessageComposeViewController *messageComposer = [[MFMessageComposeViewController alloc] init];
        messageComposer.messageComposeDelegate = self;
        [messageComposer setRecipients:nil];
        [messageComposer setBody:SHARE_APP_MESSAGE_BODY_TEXT_OTHER];
        [self presentViewController:messageComposer animated:YES completion:nil];
    }else{
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:ALERT_TITLE
                                  message:@"You may not have set up sms service on your device.\nPlease check and try again."
                                  delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
}

#pragma mark MFMessageComposeViewController Delegate Methods
-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
		case MessageComposeResultCancelled:
            DebugLog(@"SMS Cancelled");
			break;
		case MessageComposeResultFailed:
        {
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:@"Unknown Error\nSMS failed"
														   delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
			[alert show];
        }
			break;
		case MessageComposeResultSent:
        {
            DebugLog(@"SMS Sent");
            NSDictionary *params = @{@"share_via":@"sms"};
            [INEventLogger logEvent:@"Tab_Share_This_App" withParams:params];
        }
			break;
		default:
			break;
	}
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)emailShareClicked{
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailComposer = [[MFMailComposeViewController alloc] init];
        mailComposer.mailComposeDelegate = self;
        [mailComposer setToRecipients:nil];
        [mailComposer setSubject:@"Download the Cancer eBook App Now"];
        [mailComposer setMessageBody:SHARE_APP_MESSAGE_BODY_TEXT_EMAIL isHTML:YES];
        [mailComposer addAttachmentData:UIImageJPEGRepresentation([UIImage imageNamed:@"Icon-120.png"], 1) mimeType:@"image/jpeg" fileName:@"Cancer eBook.png"];
        mailComposer.toolbar.tag = 2;
        [self presentViewController:mailComposer animated:YES completion:nil];
    }else
    {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:ALERT_TITLE
                                  message:@"You may not have set up mail service on your device or You may not connected to internet.\nPlease check and try again."
                                  delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
}

- (void)whatsAppShareClicked{
    NSString *urlString = [NSString stringWithFormat:@"whatsapp://send?text=%@",[SHARE_APP_MESSAGE_BODY_TEXT_OTHER stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSURL *whatsappURL = [NSURL URLWithString:urlString];
    if ([[UIApplication sharedApplication] canOpenURL: whatsappURL])
    {
        NSDictionary *params = @{@"share_via":@"whatsapp"};
        [INEventLogger logEvent:@"Tab_Share_This_App" withParams:params];
        [[UIApplication sharedApplication] openURL: whatsappURL];
    }else{
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:ALERT_TITLE
                                  message:@"You may not have set up whatsapp service on your device.\nPlease check and try again."
                                  delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
}

- (void)facebookShareClicked {
    if ([MEDICAL_APP_DELEGATE networkavailable]) {
        if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
            
            SLComposeViewController *socialComposer = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
            [socialComposer setInitialText:SHARE_APP_MESSAGE_BODY_TEXT_OTHER];
            [socialComposer addImage:[UIImage imageNamed:@"Icon-120.png"]];
            [socialComposer setCompletionHandler:^(SLComposeViewControllerResult result){
                switch (result) {
                    case SLComposeViewControllerResultCancelled:
                        DebugLog(@"SLComposeViewControllerResultCancelled");
                        break;
                    case SLComposeViewControllerResultDone:
                    {
                        DebugLog(@"SLComposeViewControllerResultDone");
                        NSDictionary *params = @{@"share_via":@"facebook"};
                        [INEventLogger logEvent:@"Tab_Share_This_App" withParams:params];
                    }
                        break;
                    default:
                        DebugLog(@"SLComposeViewControllerResultFailed");
                        break;
                }
                [self dismissViewControllerAnimated:YES completion:NULL];
            }];
            [self presentViewController:socialComposer animated:YES completion:Nil];
        }
        else{
            UIAlertView *alertView = [[UIAlertView alloc]
                                      initWithTitle:ALERT_TITLE
                                      message:@"You may not have set up facebook service on your device.Please check and try again."
                                      delegate:nil
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil];
            [alertView show];
        }
    }else{
        [CommonCallback showOfflineAlert];
    }
}

- (void)twitterShareClicked{
    if ([MEDICAL_APP_DELEGATE networkavailable]) {
        if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
            SLComposeViewController *socialComposer = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
            [socialComposer setInitialText:SHARE_APP_MESSAGE_BODY_TEXT_TWITTER];
            [socialComposer addImage:[UIImage imageNamed:@"Icon-120.png"]];
            [socialComposer setCompletionHandler:^(SLComposeViewControllerResult result){
                switch (result) {
                    case SLComposeViewControllerResultCancelled:
                        DebugLog(@"SLComposeViewControllerResultCancelled");
                        break;
                    case SLComposeViewControllerResultDone:
                    {
                        DebugLog(@"SLComposeViewControllerResultDone");
                        NSDictionary *params = @{@"share_via":@"twitter"};
                        [INEventLogger logEvent:@"Tab_Share_This_App" withParams:params];
                    }
                        break;
                    default:
                        DebugLog(@"SLComposeViewControllerResultFailed");
                        break;
                }
                [self dismissViewControllerAnimated:YES completion:NULL];
            }];
            [self presentViewController:socialComposer animated:YES completion:Nil];
        }
        else{
            UIAlertView *alertView = [[UIAlertView alloc]
                                      initWithTitle:ALERT_TITLE
                                      message:@"You may not have set up twitter service on your device.Please check and try again."
                                      delegate:nil
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil];
            [alertView show];
        }
    }else{
        [CommonCallback showOfflineAlert];
    }
}

-(void)closeSignUpView
{
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        DebugLog(@"closeSignUpView");
    }];
}

-(void)ShowSignUpView{
    MedicalSignUpViewController *medicalsignupviewController = [[MedicalSignUpViewController alloc] initWithNibName:@"MedicalSignUpViewController" bundle:nil];
    medicalsignupviewController.title = SIGNUP;

    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:medicalsignupviewController];
    UIButton *closeBtn =  [UIButton buttonWithType:UIButtonTypeCustom];
    [closeBtn setFrame:CGRectMake(276, 0, 44, 44)];
    [closeBtn setTitle:@"" forState:UIControlStateNormal];
    [closeBtn setImage:[UIImage imageNamed:@"cancel_white.png"] forState:UIControlStateNormal];
    [closeBtn addTarget:self action:@selector(closeSignUpView) forControlEvents:UIControlEventTouchUpInside];
    [navController.navigationBar addSubview:closeBtn];
    [navController.navigationBar setTranslucent:NO];
    [self.navigationController presentViewController:navController animated:YES completion:nil];
}

-(void)closeWebView
{
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        DebugLog(@"closeWebView");
    }];
}

-(void)openWebPage:(NSString *)title url:(NSString *)urlLink{
    if ([urlLink length] > 0) {
        MedicalWebViewController *webViewController = [[MedicalWebViewController alloc] initWithNibName:@"MedicalWebViewController" bundle:nil];
        webViewController.title = title;
        webViewController.medicalLink = urlLink;
        
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:webViewController];
        UIButton *closeBtn =  [UIButton buttonWithType:UIButtonTypeCustom];
        [closeBtn setFrame:CGRectMake(276, 0, 44, 44)];
        [closeBtn setTitle:@"" forState:UIControlStateNormal];
        [closeBtn setImage:[UIImage imageNamed:@"cancel_white.png"] forState:UIControlStateNormal];
        [closeBtn addTarget:self action:@selector(closeWebView) forControlEvents:UIControlEventTouchUpInside];
        [navController.navigationBar addSubview:closeBtn];
        [navController.navigationBar setTranslucent:NO];
        [self.navigationController presentViewController:navController animated:YES completion:nil];
    }
}

-(void)sendEmail{
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *emailComposer = [[MFMailComposeViewController alloc] init];
        emailComposer.mailComposeDelegate = self;
        [emailComposer setToRecipients:[NSArray arrayWithObjects:CONTACTUS_INFO, nil]];
        [emailComposer setMessageBody:@"" isHTML:NO];
        [emailComposer setSubject:@""];
        emailComposer.toolbar.tag = 1;
        [self presentViewController:emailComposer animated:YES completion:nil];
    }else
    {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:ALERT_TITLE
                                  message:@"You may not have set up mail service on your device or You may not connected to internet.\nPlease check and try again."
                                  delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
}
#pragma mark Mail Compose Delegate
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    if (result == MFMailComposeResultFailed) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                        message:NSLocalizedString(@"Email failed to send. Please try again.", nil)
                                                       delegate:nil cancelButtonTitle:NSLocalizedString(@"Dismiss", nil) otherButtonTitles:nil];
		[alert show];
    }else if (result == MFMailComposeResultSent)
    {
        DebugLog(@"mail sent");
        if (controller.toolbar.tag == 2) {
            NSDictionary *params = @{@"share_via":@"email"};
            [INEventLogger logEvent:@"Tab_Share_This_App" withParams:params];
        }
    }
	[self dismissViewControllerAnimated:YES completion:nil];
}

////////////////////////////////NOT USED RIGHT NOW///////////////////////////////////////
-(void)createGridWithButtons{
	// load all the images from our bundle and add them to the scroll view
    int pageCount = (int)ceil([gridbuttonNamesArray count]/NUMBEROFBUTTONSPERPAGE);
    //DebugLog(@"page count = %d %f %f %f", pageCount,ceil(1/5),ceil(0.2),ceil(1/5.0));
    
    gridpageControl.numberOfPages = pageCount;
    gridpageControl.currentPage = 0;
    NSString *btnTitle = @"";

	for (int pageIndex= 1; pageIndex <= pageCount; pageIndex++)
	{
		int tag = pageIndex * 100;
        
        UIView *view=[[UIView alloc] init];
		CGRect rect;
        view.backgroundColor=[UIColor clearColor];
        
		rect.size.height = gridScrollView.frame.size.height;
		rect.size.width = gridScrollView.frame.size.width;
		view.frame = rect;
		view.tag = pageIndex;	// tag our images for later use when we place them in serial fashion
		
        /* UIImageView *img = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"gray_background.png"]];
         img.frame = view.frame;
         img.alpha = 0.9;
         [view addSubview:img];*/
        //DebugLog(@"x=%f y=%f w=%f h=%f ",gridScrollView.frame.origin.x,gridScrollView.frame.origin.y,gridScrollView.frame.size.width,gridScrollView.frame.size.height );
        
        int X= XAXIS,Y=YAXIS;
        
        int labelHeight=0;//20;
        //  int labelWidth=110;
        UIButton *button;
        //  UILabel *label;
        
        CGRect frame;
        for (int buttonIndexOnPage=0;buttonIndexOnPage<NUMBEROFBUTTONSPERPAGE; buttonIndexOnPage++) {
            
            if ((buttonIndexOnPage+(NUMBEROFBUTTONSPERPAGE*(pageIndex-1))) == [gridbuttonNamesArray count])
            {
                break;
            }
            btnTitle = [gridbuttonNamesArray objectAtIndex:(buttonIndexOnPage+(NUMBEROFBUTTONSPERPAGE*(pageIndex-1)))];
            
            frame = CGRectMake(X, Y, WIDTH, HEIGHT);
            button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.frame = frame;
            button.backgroundColor  = [UIColor clearColor];
//            button.titleLabel.font  = DEFAULT_BOLD_FONT(12.8);
//            button.titleLabel.numberOfLines = 3;
//            button.titleLabel.textAlignment = NSTextAlignmentCenter;
            [button setTitle:btnTitle forState:UIControlStateNormal];
           // [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            button.tag = tag+buttonIndexOnPage;
            [button addTarget:self action:@selector(btnActionMethod:)forControlEvents:UIControlEventTouchUpInside];
            
            //button.layer.cornerRadius = 5;
            
            
            if ([btnTitle isEqualToString:ChemoOrders]) {
                [button setImage:[UIImage imageNamed:@"chemotherapy-orders.png"] forState:UIControlStateNormal];
            }else if ([btnTitle isEqualToString:ChemoDrugs]) {
                [button setImage:[UIImage imageNamed:@"Oncology-Drugs"] forState:UIControlStateNormal];
            }else if ([btnTitle isEqualToString:Drug_Interactions]) {
                [button setImage:[UIImage imageNamed:@"Drug-Drug-Interactions.png"] forState:UIControlStateNormal];
            }else if ([btnTitle isEqualToString:MedCalc]) {
                [button setImage:[UIImage imageNamed:@"medical-calculator"] forState:UIControlStateNormal];
            }else if ([btnTitle isEqualToString:CTCAE]) {
                [button setImage:[UIImage imageNamed:@"CTCAE-v.4.0.png"] forState:UIControlStateNormal];
            }else if ([btnTitle isEqualToString:News]) {
                [button setImage:[UIImage imageNamed:@"Alerts.png"] forState:UIControlStateNormal];
            }else if ([btnTitle isEqualToString:UsefulSites]) {
                [button setImage:[UIImage imageNamed:@"Web-Links"] forState:UIControlStateNormal];
            }else if ([btnTitle isEqualToString:Resources]) {
                [button setImage:[UIImage imageNamed:@"Resources.png"] forState:UIControlStateNormal];
            }else if ([btnTitle isEqualToString:RSS]) {
                [button setImage:[UIImage imageNamed:@"Must-reads.png"] forState:UIControlStateNormal];
            }
            
            /* frame = CGRectMake(X, Y+height, labelWidth, labelHeight);
             label = [[UILabel alloc]initWithFrame:frame];
             label.backgroundColor=[UIColor clearColor];
             label.text=[buttonImages objectAtIndex:indexofButtonImage+b];
             label.textAlignment=NSTextAlignmentCenter;
             label.textColor = [UIColor whiteColor];
             label.font=[UIFont boldSystemFontOfSize:15];*/
            
            [view addSubview:button];
            //[view addSubview:label];
            
            button=nil;
            // label=nil;
            int NextX = X + WIDTH + HORIZONTAL_PADDING;
            //DebugLog(@"NextX %d %f",NextX,view.frame.size.width-100);
            
            if (NextX < view.frame.size.width-60)
            {
                X   =   NextX;
            }
            else
            {
                X   =   XAXIS;
                Y   =   Y + HEIGHT + labelHeight + VERTICAL_PADDING;
            }
        }
        [gridScrollView addSubview:view];
    }
    [self layoutScrollImages:pageCount];
}

////////////////////////////////NOT USED RIGHT NOW///////////////////////////////////////

- (void)layoutScrollImages:(int)count
{
	UIView *view = nil;
	NSArray *subviews = [gridScrollView subviews];
    
	// reposition all subviews in a horizontal serial fashion
	CGFloat curXLoc = 0;
	for (view in subviews)
	{
		if ([view isKindOfClass:[UIView class]] && view.tag > 0)
		{
			CGRect frame = view.frame;
			frame.origin = CGPointMake(curXLoc, 0);
			view.frame = frame;
			
			curXLoc += (gridScrollView.frame.size.width);
		}
	}
	
	// set the content size so it can be scrollable
	[gridScrollView setContentSize:CGSizeMake((count * gridScrollView.frame.size.width), gridScrollView.frame.size.height)];
}

#pragma UIScrollView Delegate Callbacks
- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    
    if (pageControlUsed)
    {
        // do nothing - the scroll was initiated from the page control, not the user dragging
        return;
    }
    CGFloat pageWidth = self.gridScrollView.frame.size.width;
    int page = floor((self.gridScrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    if(gridpageControl.currentPage != page) {
        gridpageControl.currentPage = page;
    }
}

// At the begin of scroll dragging, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    pageControlUsed = NO;
}

// At the end of scroll animation, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    pageControlUsed = NO;
}
- (IBAction)pageChange:(id)sender {
    int page = gridpageControl.currentPage;
	
	
	// update the scroll view to the appropriate page
    CGRect frame = self.gridScrollView.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    
    
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{ [gridScrollView scrollRectToVisible:frame animated:NO]; }
                     completion:NULL];
    
    //[gridScrollView scrollRectToVisible:frame animated:YES];
    
	// Set the boolean used when scrolls originate from the UIPageControl. See scrollViewDidScroll: above.
    pageControlUsed = YES;
}

- (IBAction)btnActionMethod:(id)sender {
    UIButton *button=(UIButton *)sender;
    DebugLog(@"Button %@ is clicked",[button titleForState:UIControlStateNormal]);
    NSString *btnTitle=[button titleForState:UIControlStateNormal];
    DebugLog(@"Button title %@ ",btnTitle);

    if ([btnTitle isEqualToString:ChemoOrders])
    {
        [INEventLogger logEvent:@"Tab_Chemo_Orders"];
        MedicalChemoViewController *chemoviewController = [[MedicalChemoViewController alloc] initWithNibName:@"MedicalChemoViewController" bundle:nil] ;
        chemoviewController.title = btnTitle;
        [self.navigationController pushViewController:chemoviewController animated:YES];
    }
    else if ([btnTitle isEqualToString:ChemoDrugs])
    {
        [INEventLogger logEvent:@"Tab_Oncology_Drugs"];
        MedicalChemoDrugsViewController *chemodrugsViewController = [[MedicalChemoDrugsViewController alloc] initWithNibName:@"MedicalChemoDrugsViewController" bundle:nil] ;
        chemodrugsViewController.title = btnTitle;
        [self.navigationController pushViewController:chemodrugsViewController animated:YES];
    }
    else if ([btnTitle isEqualToString:Drug_Interactions])
    {
        [INEventLogger logEvent:@"Tab_Drug_Drug_Interaction"];
        MedicalDrugInteractionsViewController *medicaldrugsInteractionsviewController = [[MedicalDrugInteractionsViewController alloc] initWithNibName:@"MedicalDrugInteractionsViewController" bundle:nil];
        medicaldrugsInteractionsviewController.title = btnTitle;
        [self.navigationController pushViewController:medicaldrugsInteractionsviewController animated:YES];
    }
    else if ([btnTitle isEqualToString:MedCalc])
    {
        [INEventLogger logEvent:@"Tab_Medical_Calculator"];
        MedicalFormulaViewController *medCalcViewController = [[MedicalFormulaViewController alloc] initWithNibName:@"MedicalFormulaViewController" bundle:nil] ;
        [self.navigationController pushViewController:medCalcViewController animated:YES];
    }
    else if ([btnTitle isEqualToString:Meetings])
    {
        [INEventLogger logEvent:@"Tab_Meetings"];
        MedicalMeetingViewController *medicalmeetingviewController = [[MedicalMeetingViewController alloc] initWithNibName:@"MedicalMeetingViewController" bundle:nil];
        medicalmeetingviewController.title = btnTitle;
        [self.navigationController pushViewController:medicalmeetingviewController animated:YES];
    }
    else if ([btnTitle isEqualToString:CTCAE])
    {
        [INEventLogger logEvent:@"Tab_CTCAE_4.0"];
        MedicalCTCAEViewController *medicalctcaeviewController = [[MedicalCTCAEViewController alloc] initWithNibName:@"MedicalCTCAEViewController" bundle:nil];
        medicalctcaeviewController.title = btnTitle;
        [self.navigationController pushViewController:medicalctcaeviewController animated:YES];
    }
    else if([btnTitle isEqualToString:News])
    {
        [INEventLogger logEvent:@"Tab_Alerts"];
        MedicalNewsViewController *medicalnewsviewController = [[MedicalNewsViewController alloc] initWithNibName:@"MedicalNewsViewController" bundle:nil];
        medicalnewsviewController.title = btnTitle;
        [self.navigationController pushViewController:medicalnewsviewController animated:YES];
    }
    else if ([btnTitle isEqualToString:UsefulSites])
    {
        [INEventLogger logEvent:@"Tab_WebLinks"];
        MedicalUsefulSitesViewController *sitesviewController = [[MedicalUsefulSitesViewController alloc] initWithNibName:@"MedicalUsefulSitesViewController" bundle:nil];
        sitesviewController.title = btnTitle;
        [self.navigationController pushViewController:sitesviewController animated:YES];
    }
    else if ([btnTitle isEqualToString:Resources])
    {
        [INEventLogger logEvent:@"Tab_Resources"];
        MedicalResourceViewController *resourcesviewController = [[MedicalResourceViewController alloc] initWithNibName:@"MedicalResourceViewController" bundle:nil] ;
        resourcesviewController.title = btnTitle;
        [self.navigationController pushViewController:resourcesviewController animated:YES];
    }
    else if ([btnTitle isEqualToString:RSS])
    {
        [INEventLogger logEvent:@"Tab_Must Reads"];
        MedicalRSSFeedsViewController *rssfeedviewController = [[MedicalRSSFeedsViewController alloc] initWithNibName:@"MedicalRSSFeedsViewController" bundle:nil];
        rssfeedviewController.title = RSS;
        [self.navigationController pushViewController:rssfeedviewController animated:YES];
    }
//    else if ([btnTitle isEqualToString:Meetings])
//    {
//        [INEventLogger logEvent:@"Tab_SignUp"];
//        MedicalMeetingViewController *medicalmeetingviewController = [[MedicalMeetingViewController alloc] initWithNibName:@"MedicalMeetingViewController" bundle:nil];
//        medicalmeetingviewController.title = btnTitle;
//        [self.navigationController pushViewController:medicalmeetingviewController animated:YES];
//    }
//    else if ([btnTitle isEqualToString:Journals])
//    {
//        MedicalJournalsViewController *journalsViewController = [[MedicalJournalsViewController alloc] initWithNibName:@"MedicalJournalsViewController" bundle:nil] ;
//        journalsViewController.title = btnTitle;
//        [self.navigationController pushViewController:journalsViewController animated:YES];
//    }
//    else if ([btnTitle isEqualToString:IDC9Codes])
//    {
//        [INEventLogger logEvent:@"Tab_SignUp"];
//        MedicalBillingViewController *billingviewController = [[MedicalBillingViewController alloc] initWithNibName:@"MedicalBillingViewController" bundle:nil] ;
//        billingviewController.title = btnTitle;
//        [self.navigationController pushViewController:billingviewController animated:YES];
//    }
    [self hidePostAlertWithanimation:YES];
}

- (IBAction)btnSignUpPressed:(id)sender {
    [INEventLogger logEvent:@"Tab_SignUp"];
    [self ShowSignUpView];
    [self hidePostAlertWithanimation:YES];
}


#pragma Alerts
-(void)sendGetAlertPostsRequest{
    if ([MEDICAL_APP_DELEGATE networkavailable] && self.view.window && self.isViewLoaded) {
        NSDate *lastPostSyncDate = [MedicalUserDefaults getPostSyncDate];
        DebugLog(@"lastPostSyncDate %@",lastPostSyncDate);
        if (lastPostSyncDate != nil) {
            NSDate *currentDate = [NSDate date];
            
            NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
            NSDateComponents *components = [gregorianCalendar components:NSHourCalendarUnit
                                                                fromDate:lastPostSyncDate
                                                                  toDate:currentDate
                                                                 options:0];
            DebugLog(@"days = %d",[components hour]);
            DebugLog(@"-----lastvalidateloginDate %@ --currentDate %@--",lastPostSyncDate,currentDate);
            
            if([components hour] == 0) // Compare expiration dates. if greater than one then users session timeout occurs.
            {
                return; // lastvalidateloginDate != current date
            }
        }
        
        NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:ALERT_POST_LINK] cachePolicy:NO timeoutInterval:30.0];
        conn = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
        if (conn)
        {
            if (webData == nil) {
                webData = [NSMutableData data];
            }
        }
    }
}

#pragma NSURLConnection Delegate
-(void) connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *) response
{
    [webData setLength: 0];
}

-(void) connection:(NSURLConnection *)connection didReceiveData:(NSData *) data
{
    [webData appendData:data];
}

-(void) connection:(NSURLConnection *)connection didFailWithError:(NSError *) error
{
    DebugLog(@"didFailWithError error %@",error);
    [conn cancel];
    conn = nil;
}

-(void) connectionDidFinishLoading:(NSURLConnection *) connection
{
    if (self.view.window && self.isViewLoaded) {
        DebugLog(@"self.isViewLoaded");
        if (webData)
        {
            NSError *error;
            NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:webData options:NSJSONReadingAllowFragments error:&error];
            DebugLog(@"jsonDict %@",jsonDict);
            if (jsonDict != nil) {
                NSArray *lpostArray = [jsonDict objectForKey:@"posts"];
                //            for (NSDictionary *postDict in lpostArray) {
                //                NSString *postId = [postDict objectForKey:@"id"];
                //                NSString *title = [postDict objectForKey:@"title"];
                //                NSString *content = [postDict objectForKey:@"content"];
                //                DebugLog(@"postId %@ title %@ content %@",postId,title,content);
                //            }
                DebugLog(@"lpostArray %@",lpostArray);
                if ([lpostArray count] > 0) {
                    NSDictionary *postDict  = [lpostArray objectAtIndex:0];
                    int postId              = [[postDict objectForKey:@"id"] integerValue];
                    NSString *title         = [postDict objectForKey:@"title"];
                    NSString *content       = [postDict objectForKey:@"content"];
                    NSString *date       = [postDict objectForKey:@"modified"];
                    DebugLog(@"postId %d title %@ content %@ date %@",postId,title,content,date);
                    
                    DebugLog(@"[MedicalUserDefaults getPostDate] %@",[MedicalUserDefaults getPostDate]);
                    if (postId == [MedicalUserDefaults getPostId] && [date isEqualToString:[MedicalUserDefaults getPostDate]]) {
                        DebugLog(@"Got same post id");
                        [MedicalUserDefaults setPostSyncDate];
                    }else{
                        DebugLog(@"Not same post id");
                        [MedicalUserDefaults addAlertWithPostId:postId title:title content:content date:date];
                        [self showPostAlert];
                    }
                }
            }
        }
    }else{
        DebugLog(@"not self.isViewLoaded");
    }
    webData = nil;
    conn = nil;
}

-(void)showPostAlert
{
    NSString *postTitle     = [MedicalUserDefaults getPostTitle];
   // NSString *content   = [MedicalUserDefaults getPostContent];
    if ([postTitle length] > 0) {
        postAlertLabel.text = postTitle;
        [postAlertView setHidden:FALSE];
        [UIView animateWithDuration:1.0
                         animations:^{
                             postAlertView.alpha = 1.0;
                         }];
        [self performSelector:@selector(hidePostAlertWithanimation:) withObject:[NSNumber numberWithBool:1] afterDelay:15.0];
    }else{
        [self hidePostAlertWithanimation:NO];
    }
}

-(void)hidePostAlertWithanimation:(int)withAnimation
{
    if (postAlertView.hidden) {
        return;
    }
    if (withAnimation) {
        [UIView animateWithDuration:1.0
                         animations:^{
                             postAlertView.alpha = 0.0;
                         }
                         completion:^(BOOL finished){
                             [postAlertView setHidden:TRUE];
                         }];
    }else{
        [postAlertView setHidden:TRUE];
        postAlertView.alpha = 0.0;
    }
}

-(void)showRateAppAlert{
    if ([MEDICAL_APP_DELEGATE networkavailable]) {
        showRateAppAlert = YES;
        //perform manual check
        [[iRate sharedInstance] promptIfNetworkAvailable];
    }else{
        [CommonCallback showOfflineAlert];
    }
}

#pragma mark -
#pragma mark iVersionDelegate methods
- (void)iRateCouldNotConnectToAppStore:(NSError *)error
{
    if (showRateAppAlert) {
        showRateAppAlert = NO;
    }
	UIAlertView *errorView = [[UIAlertView alloc] initWithTitle:RATE_APP
                                                        message:error.description
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
    [errorView show];
}

- (BOOL)iRateShouldPromptForRating
{
    if (showRateAppAlert) {
        showRateAppAlert = NO;
        //don't show prompt, just open app store
        [[iRate sharedInstance] openRatingsPageInAppStore];
        return NO;
    }
	return YES;
}
@end