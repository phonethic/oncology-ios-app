//
//  MedicalSurvivalRatesViewController.h
//  Medical
//
//  Created by Rishi on 26/05/14.
//  Copyright (c) 2014 Kirti Nikam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MedicalSurvivalRatesViewController : UIViewController
@property (strong, nonatomic) NSMutableArray *ratesArray;
@property (strong, nonatomic) NSMutableDictionary *ratesDict;
@property (strong, nonatomic) IBOutlet UITableView *ratesTableView;
@end
