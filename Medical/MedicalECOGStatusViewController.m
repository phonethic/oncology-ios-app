//
//  MedicalECOGStatusViewController.m
//  Medical
//
//  Created by Kirti Nikam on 17/10/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import "MedicalECOGStatusViewController.h"
#import "MedicalAppDelegate.h"
#import "MedicalInfoImageViewController.h"

@interface MedicalECOGStatusViewController ()

@end

@implementation MedicalECOGStatusViewController
@synthesize ECOGStatusarray;
@synthesize ecogTableView;
@synthesize infoBtn;
@synthesize lblUnderLine,lblDefination,lblGrade;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)showActionsheet
{
	UIActionSheet *uiActionSheetP= [[UIActionSheet alloc]
                                    initWithTitle: @""
                                    delegate:self
                                    cancelButtonTitle:@"CANCEL"
                                    destructiveButtonTitle:nil
                                    otherButtonTitles:@"Print",@"Email", nil];
	
	uiActionSheetP.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
	[uiActionSheetP showInView:self.navigationController.view];
    
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    DebugLog(@"buttonIndex = %d",buttonIndex);
	
	if (buttonIndex == 0) {
        [self takescreenshot:nil];
    } else if (buttonIndex == 1) {
        [self sendMail];
    }
	
}
- (IBAction)takescreenshot:(id)sender
{
   
    CGRect oldFrame = ecogTableView.frame;
    ecogTableView.frame=CGRectMake(ecogTableView.frame.origin.x, ecogTableView.frame.origin.y, ecogTableView.contentSize.width, ecogTableView.contentSize.height);
    [MEDICAL_APP_DELEGATE printImage:ecogTableView];
    //save into photolibrary
	//UIImageWriteToSavedPhotosAlbum(viewImage, nil, nil, nil);
	ecogTableView.frame=oldFrame;
	
}
- (void)sendMail
{
    [MEDICAL_APP_DELEGATE mailImage:self.view];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSDictionary *params = @{@"calculator_name":self.title};
    [INEventLogger logEvent:@"MedCalc_Calculator_Details" withParams:params];
  //  self.view.backgroundColor = BACKGROUND_COLOR;

    ECOGStatusarray=[[NSMutableArray alloc] init];
    [ECOGStatusarray addObject:@"Fully active, able to carry on all pre-disease performance without restriction."];
    [ECOGStatusarray addObject:@"Restricted in physically strenuous activity but ambulatory and able to carry out work of a light or sedentary nature, e.g., light house work, office work."];
    [ECOGStatusarray addObject:@"Ambulatory and capable of all selfcare but unable to carry out any work activities. Up and about more than 50% of waking hours."];
    [ECOGStatusarray addObject:@"Capable of only limited selfcare, confined to bed or chair more than 50% of waking hours."];
    [ECOGStatusarray addObject:@"Completely disabled. Cannot carry on any selfcare. Totally confined to bed or chair."];
    [ECOGStatusarray addObject:@"Dead."];

    lblGrade.backgroundColor           = [UIColor clearColor];
    lblGrade.textColor                 = DEFAULT_COLOR;
    lblGrade.font                      = DEFAULT_BOLD_FONT(22);
    
    lblDefination.backgroundColor     = [UIColor clearColor];
    lblDefination.textColor           = DEFAULT_COLOR;
    lblDefination.font                = DEFAULT_BOLD_FONT(22);
    
    lblUnderLine.backgroundColor= DEFAULT_COLOR;
    lblUnderLine.text = @"";
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]
//                                              initWithBarButtonSystemItem:UIBarButtonSystemItemAction
//                                              target:self action:@selector(save_Clicked:)];
}
- (void) save_Clicked:(id)sender {
    [self showActionsheet];
}

- (void)viewDidUnload
{
    [self setEcogTableView:nil];
    [self setInfoBtn:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
#pragma mark - Rotation
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark Table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [ECOGStatusarray count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UILabel *lblValue,*lblLevel,*lblUnderline;

    static NSString *CellIdentifier = @"EcogCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        
        lblValue = [[UILabel alloc] initWithFrame:CGRectMake(5,0 , 85,98)];
        lblValue.tag            = 1200;
        lblValue.font           = DEFAULT_BOLD_FONT(35);
        lblValue.textAlignment  = NSTextAlignmentCenter;
        lblValue.textColor      = DEFAULT_COLOR;
        lblValue.backgroundColor= [UIColor clearColor];
        [cell.contentView addSubview:lblValue];
        
        lblLevel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(lblValue.frame),0, 225, 98)];
        lblLevel.tag            = 1300;
        lblLevel.font           = DEFAULT_FONT(14);
        lblLevel.textAlignment  = NSTextAlignmentLeft;
        lblLevel.textColor      = DEFAULT_COLOR;
        lblLevel.backgroundColor= [UIColor clearColor];
        lblLevel.numberOfLines  = 7;
        [cell.contentView addSubview:lblLevel];
        
        lblUnderline= [[UILabel alloc] initWithFrame:CGRectMake(20,CGRectGetMaxY(lblValue.frame), 280, 2)];
        lblUnderline.tag = 1400;
        lblUnderline.backgroundColor= DEFAULT_COLOR;
        lblUnderline.text = @"";
        [cell.contentView addSubview:lblUnderline];
    }    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    lblValue = (UILabel *)[cell viewWithTag:1200];
    lblValue.text = [NSString stringWithFormat:@"%d",indexPath.row];  
    lblLevel = (UILabel *)[cell viewWithTag:1300];
    lblLevel.text = [self.ECOGStatusarray objectAtIndex:indexPath.row];
    
    lblUnderline = (UILabel *)[cell viewWithTag:1400];
    if (indexPath.row == ECOGStatusarray.count - 1) {
        lblUnderline.hidden = YES;
    }else{
        lblUnderline.hidden = NO;
    }
    return cell;
}

- (IBAction)infoButtonAction:(id)sender {
    NSDictionary *params = @{@"calculator_name":self.title};
    [INEventLogger logEvent:@"MedCalc_Calculator_Info_Image" withParams:params];
    
    MedicalInfoImageViewController *typedetailController = [[MedicalInfoImageViewController alloc] initWithimageName:@"ecog_info.png"] ;
    [self.navigationController pushViewController:typedetailController animated:YES];
}
@end
