//
//  MedicalMeetingViewController.m
//  Medical
//
//  Created by Sagar Mody on 05/11/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//
#import "MedicalMeetingViewController.h"
#import "MedicalWebViewController.h"
#import "constants.h"

@interface MedicalMeetingViewController ()

@end

@implementation MedicalMeetingViewController
@synthesize medicalMeetingTableView;
@synthesize medicalMeetingObjdict;
@synthesize monthsArray;
@synthesize eventStore;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //MeetingObject *obj = [[MeetingObject alloc] initWithName:@"" eventDate:@"" eventPlace:@"" eventLink:@""];
    // Do any additional setup after loading the view from its nib.
    [INEventLogger logEvent:@"Meetings_List"];

    eventStore  = [[EKEventStore alloc] init];

    medicalMeetingObjdict = [[NSMutableDictionary alloc] init];
	monthsArray = [[NSMutableArray alloc] initWithObjects:@"January",@"February",@"March",@"April",@"May",@"July",@"September",@"October",@"November",@"December", nil];
    
    MeetingObject *janObj = [[MeetingObject alloc] initWithName:@"ASCO GI Cancers Symposium" eventStartDate:@"1-15-2015" eventEndDate:@"1-17-2015" eventPlace:@"San Francisco, California" eventLink:@"http://www.gicasym.org" eventId:@""];
    
   [medicalMeetingObjdict setValue:[[NSMutableArray alloc] initWithObjects:janObj, nil] forKey:@"January"];
    
    MeetingObject *febObj1 = [[MeetingObject alloc] initWithName:@"ASCO GU Cancers Symposium" eventStartDate:@"2-26-2015" eventEndDate:@"2-28-2015" eventPlace:@"Orlando, Florida" eventLink:@"http://gucasym.org" eventId:@""];
    
    MeetingObject *febObj2 = [[MeetingObject alloc] initWithName:@"BMT Tandem Meetings" eventStartDate:@"2-11-2015" eventEndDate:@"2-15-2015" eventPlace:@"San Diego, CA" eventLink:@"https://bmt.confex.com/tandem/2015/webprogram/programs.html" eventId:@""];
    
    [medicalMeetingObjdict setValue:[[NSMutableArray alloc] initWithObjects:febObj1,febObj2, nil] forKey:@"February"];
    
    
    MeetingObject *marchObj1 = [[MeetingObject alloc] initWithName:@"Society of Surgical Oncology (SSO)" eventStartDate:@"3-25-2015" eventEndDate:@"3-28-2015" eventPlace:@"Houston,TX" eventLink:@"http://events.jspargo.com/sso15/public/enter.aspx" eventId:@""];
    
    MeetingObject *marchObj2 = [[MeetingObject alloc] initWithName:@"NCCN Annual Conference" eventStartDate:@"3-12-2015" eventEndDate:@"3-14-2015" eventPlace:@"Hollywood, Florida" eventLink:@"http://www.nccn.org/professionals/meetings/annual_conference.asp" eventId:@""];
    
    MeetingObject *marchObj3 = [[MeetingObject alloc] initWithName:@"Society of Gynecologic Oncologists (SGO)" eventStartDate:@"3-28-2015" eventEndDate:@"3-31-2015" eventPlace:@"Chicago,IL" eventLink:@"https://www.sgo.org/education/annual-meeting-on-womens-cancer/" eventId:@""];
    
    MeetingObject *marchObj4 = [[MeetingObject alloc] initWithName:@"Annual Hematology Oncology Pharmacists Association (HOPA) Meeting" eventStartDate:@"3-25-2015" eventEndDate:@"3-28-2015" eventPlace:@"Austin, TX" eventLink:@"http://www.hoparx.org/education/default/index.html" eventId:@""];
    
    [medicalMeetingObjdict setValue:[[NSMutableArray alloc] initWithObjects:marchObj1,marchObj2,marchObj3,marchObj4, nil] forKey:@"March"];
    
    MeetingObject *aprilObj1 = [[MeetingObject alloc] initWithName:@"AACR Annual Meeting" eventStartDate:@"4-18-2015" eventEndDate:@"4-22-2015" eventPlace:@"Philadelphia,PA" eventLink:@"http://www.aacr.org/Meetings/Pages/MeetingDetail.aspx?EventItemID=25" eventId:@""];
    
    MeetingObject *aprilObj2 = [[MeetingObject alloc] initWithName:@"ONS Annual Congress" eventStartDate:@"4-23-2015" eventEndDate:@"4-26-2015" eventPlace:@"Orlando,FL" eventLink:@"https://www.ons.org/education/conferences" eventId:@""];
    
    [medicalMeetingObjdict setValue:[[NSMutableArray alloc] initWithObjects:aprilObj1,aprilObj2, nil] forKey:@"April"];
    
    
    

    MeetingObject *mayObj1 = [[MeetingObject alloc] initWithName:@"American Urological Association"  eventStartDate:@"5-15-2015" eventEndDate:@"5-19-2015" eventPlace:@"New Orleans,LA" eventLink:@"http://www.aua2015.org/" eventId:@""];
    
//    MeetingObject *mayObj2 = [[MeetingObject alloc] initWithName:@"European Multidisciplinary Conference in Thoracic Oncology - EMCTO " eventStartDate:@"9-5-2014" eventEndDate:@"11-5-2014" eventPlace:@"Lugano, Switzerland " eventLink:@"http://www.esmo.org/events/lung-2014-EMCTO.html" eventId:@""];
    
    MeetingObject *mayObj2 = [[MeetingObject alloc] initWithName:@"ASCO Annual Meeting" eventStartDate:@"5-29-2015" eventEndDate:@"6-2-2015" eventPlace:@"Chicago, USA" eventLink:@"http://am.asco.org/" eventId:@""];
    [medicalMeetingObjdict setValue:[[NSMutableArray alloc] initWithObjects:mayObj1,mayObj2, nil] forKey:@"May"];
    

//    MeetingObject *juneObj1 = [[MeetingObject alloc] initWithName:@"ASCO Annual Meeting" eventStartDate:@"31-5-2014" eventEndDate:@"4-6-2014" eventPlace:@"Chicago, USA" eventLink:@"http://chicago2013.asco.org" eventId:@""];
    
//    MeetingObject *juneObj1 = [[MeetingObject alloc] initWithName:@"ESMO World Congress on Gastrointestinal Cancer"  eventStartDate:@"6-25-2014" eventEndDate:@"6-28-2014" eventPlace:@"Barcelona, Spain" eventLink:@"http://worldgicancer.com/WCGI/WGIC2014/index.asp" eventId:@""];
//    
//    [medicalMeetingObjdict setValue:[[NSMutableArray alloc] initWithObjects:juneObj1, nil] forKey:@"June"];
    
    MeetingObject *julyObj1 = [[MeetingObject alloc] initWithName:@"ASCO/AACR Methods in Clinical Cancer Research Workshop" eventStartDate:@"7-25-2015" eventEndDate:@"7-31-2015" eventPlace:@"Vail,CO" eventLink:@"http://www.aacr.org/Meetings/Pages/MeetingDetail.aspx?EventItemID=51" eventId:@""];
    
    MeetingObject *julyObj2 = [[MeetingObject alloc] initWithName:@"ESMO World Congress on Gastrointestinal Cancer" eventStartDate:@"7-25-2015" eventEndDate:@"7-31-2015" eventPlace:@"Barcelona, Spain" eventLink:@"http://www.esmo.org/Conferences/World-GI-2015-Gastrointestinal-Cancer" eventId:@""];
    
    [medicalMeetingObjdict setValue:[[NSMutableArray alloc] initWithObjects:julyObj1,julyObj2, nil] forKey:@"July"];
    

    MeetingObject *sepObj1 = [[MeetingObject alloc] initWithName:@"ASCO Breast Cancer Symposium" eventStartDate:@"9-25-2015" eventEndDate:@"9-27-2015" eventPlace:@"San Francisco, CA" eventLink:@"http://breastcasym.org" eventId:@""];
    
    MeetingObject *sepObj2 = [[MeetingObject alloc] initWithName:@"European Cancer Congress" eventStartDate:@"9-25-2015" eventEndDate:@"9-29-2015" eventPlace:@"Vienna,Austria" eventLink:@"http://www.esmo.org/Conferences/European-Cancer-Congress-2015" eventId:@""];
    [medicalMeetingObjdict setValue:[[NSMutableArray alloc] initWithObjects:sepObj1,sepObj2, nil] forKey:@"September"];
  
    
    MeetingObject *octObj1 = [[MeetingObject alloc] initWithName:@"American Society For Radiation Oncology (ASTRO)" eventStartDate:@"10-18-2015" eventEndDate:@"10-21-2015" eventPlace:@"San Antonio, TX" eventLink:@"https://www.astro.org/Meetings-and-Events/Index.aspx" eventId:@""];
     [medicalMeetingObjdict setValue:[[NSMutableArray alloc] initWithObjects:octObj1, nil] forKey:@"October"];
    
    MeetingObject *novObj1 = [[MeetingObject alloc] initWithName:@"AACR-NCI-EORTC International Conference on Molecular Targets and Cancer Therapeutics" eventStartDate:@"11-5-2015" eventEndDate:@"11-9-2015" eventPlace:@"Boston,MA" eventLink:@"http://www.aacr.org/Meetings/Pages/MeetingDetail.aspx?EventItemID=52" eventId:@""];
    [medicalMeetingObjdict setValue:[[NSMutableArray alloc] initWithObjects:novObj1, nil] forKey:@"November"];

    
    MeetingObject *decObj1 = [[MeetingObject alloc] initWithName:@"San Antonio Breast Cancer Symposium" eventStartDate:@"12-8-2015" eventEndDate:@"12-12-2015" eventPlace:@"San Antonio, TX" eventLink:@"http://www.sabcs.org/AboutSABCS/index.asp" eventId:@""];
    
    MeetingObject *decObj2 = [[MeetingObject alloc] initWithName:@"American Society of Hematology (ASH)" eventStartDate:@"12-6-2014" eventEndDate:@"12-9-2014" eventPlace:@"San Francisco, CA" eventLink:@"http://www.hematology.org/Meetings/Annual-Meeting/" eventId:@""];
    [medicalMeetingObjdict setValue:[[NSMutableArray alloc] initWithObjects:decObj1,decObj2, nil] forKey:@"December"];
       
}

- (void)viewDidUnload
{
    [self setMedicalMeetingTableView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
#pragma mark - Rotation
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark Table view methods
-(CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section
{
    return 35;
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView* headerView          = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 35)];
    headerView.backgroundColor  = DEFAULT_COLOR;
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10,1,30,30)];
    imageView.image = [UIImage imageNamed:@"Calender.png"];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [headerView addSubview:imageView];

    UILabel *headerLabel        = [[UILabel alloc] initWithFrame:CGRectZero];
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.frame           = CGRectMake(CGRectGetMaxX(imageView.frame)+5,0,CGRectGetWidth(headerView.frame)-(CGRectGetMaxX(imageView.frame)+10),35);
    headerLabel.textAlignment   = NSTextAlignmentLeft;
    headerLabel.font            = DEFAULT_BOLD_FONT(17);
    headerLabel.text            = [monthsArray objectAtIndex:section];
    headerLabel.textColor       = [UIColor whiteColor];
    [headerView addSubview:headerLabel];
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 120;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [monthsArray count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
        return [[medicalMeetingObjdict valueForKey:[monthsArray objectAtIndex:section]] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"MeeingCellIdentifier";    
    UILabel *lblName,*lblDate,*lblLocation;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        
        lblName = [[UILabel alloc] initWithFrame:CGRectMake(10,3,300, 50)];
        lblName.tag = 101;
        lblName.font = DEFAULT_BOLD_FONT(14);
        lblName.minimumScaleFactor = 13.0;
        lblName.textAlignment = NSTextAlignmentLeft;
        lblName.textColor = [UIColor blackColor];
        lblName.backgroundColor =  [UIColor clearColor];
        lblName.numberOfLines = 3;
        lblName.adjustsFontSizeToFitWidth = YES;
        [cell.contentView addSubview:lblName];
        
        lblDate = [[UILabel alloc] initWithFrame:CGRectMake(10, CGRectGetMaxY(lblName.frame), cell.frame.size.width-10, 30)];
        lblDate.tag = 102;
        lblDate.font = DEFAULT_FONT(14);
        lblDate.textAlignment = NSTextAlignmentLeft;
        lblDate.textColor = [UIColor blackColor];
        lblDate.backgroundColor =  [UIColor clearColor];
        lblDate.numberOfLines = 0;
        [cell.contentView addSubview:lblDate];
        
        lblLocation = [[UILabel alloc] initWithFrame:CGRectMake(10, CGRectGetMaxY(lblDate.frame), cell.frame.size.width-10, 30)];
        lblLocation.tag = 103;
        lblLocation.font = DEFAULT_FONT(14);
        lblLocation.textAlignment = NSTextAlignmentLeft;
        lblLocation.textColor = [UIColor blackColor];
        lblLocation.backgroundColor =  [UIColor clearColor];
        lblLocation.numberOfLines = 0;
        [cell.contentView addSubview:lblLocation];

        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = DEFAULT_COLOR_WITH_ALPHA(0.5);
        cell.selectedBackgroundView = bgColorView;
    }
    MeetingObject *tempObj = [[medicalMeetingObjdict valueForKey:[monthsArray objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
    //DebugLog(@"cellforindex Key %@ rowname %@",[monthsArray objectAtIndex:indexPath.section],tempObj.name);

    if(![tempObj.weblink isEqualToString:@""])
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    else
        cell.accessoryType = UITableViewCellAccessoryNone;
    
    lblName = (UILabel *)[cell viewWithTag:101];
    lblDate = (UILabel *)[cell viewWithTag:102];
    lblLocation = (UILabel *)[cell viewWithTag:103];

    lblName.text = tempObj.name;
    if ([tempObj.startDate length] == 0 || [tempObj.startDate isEqualToString:@"-"]) {
        lblDate.text = [NSString stringWithFormat:@"Date: %@",tempObj.startDate];
    }else{
        lblDate.text = [NSString stringWithFormat:@"Date: %@ to %@",tempObj.startDate,tempObj.endDate];
    }
    lblLocation.text = [NSString stringWithFormat:@"Location: %@",tempObj.place];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    currentObj = [[medicalMeetingObjdict valueForKey:[monthsArray objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];    
    if ([currentObj.eventIdentifier isEqualToString:@""])
    {
        UIAlertView *calendersyncalert = [[UIAlertView alloc] initWithTitle:@"Calendar Sync" message:@"Do you want to sync this meeting schedule in calendar?" delegate:self cancelButtonTitle:@"YES" otherButtonTitles:@"NO", nil];
        [calendersyncalert show];
    }else{
        [self pushToOpenWebsite];
    }
}

-(double)getTextHeight:(NSString *)str{
    CGSize size = [str sizeWithFont:DEFAULT_FONT(14) constrainedToSize:CGSizeMake(280, 999) lineBreakMode:NSLineBreakByWordWrapping];
    DebugLog(@"%f",size.height);
    return size.height;
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if ([title isEqualToString:@"OK"]) {
        if(![currentObj.weblink isEqualToString:@""])
        {
            NSDictionary *params = @{@"meeting_name":currentObj.name,@"link":currentObj.weblink};
            [INEventLogger logEvent:@"Meetings_Website" withParams:params];
            
            MedicalWebViewController *webviewController = [[MedicalWebViewController alloc] initWithNibName:@"MedicalWebViewController" bundle:nil];
            webviewController.medicalLink   = currentObj.weblink;
            webviewController.title         = currentObj.name;
            [self.navigationController pushViewController:webviewController animated:YES];
        }
        NSIndexPath *indexPath = [medicalMeetingTableView indexPathForSelectedRow];
        if (indexPath) [medicalMeetingTableView deselectRowAtIndexPath:indexPath animated:YES];
        
    }else if ([title isEqualToString:@"YES"] || [title isEqualToString:@"NO"]) {
        if ([title isEqualToString:@"YES"])
        {
                DebugLog(@"calendar sync");
                //Request the access to the Calendar
                [eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (error)
                        {
                            // display error message here
                            DebugLog(@"error %@",error.description);
                        }
                        else if (granted)
                        {
                            // access granted
                            // do the important stuff here
                            DebugLog(@"granted");
                            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                            [dateFormatter setDateFormat:@"MM-dd-yyyy"];
                            
                            NSError *error;
                            EKEvent *meetingEvent  = [EKEvent eventWithEventStore:eventStore];
                            
                            meetingEvent.title     = currentObj.name;
                            meetingEvent.location  = currentObj.place;
                            meetingEvent.startDate = [dateFormatter dateFromString:currentObj.startDate];
                            meetingEvent.endDate   = [dateFormatter dateFromString:currentObj.endDate];
                            meetingEvent.allDay = YES;
                            
                            // To set alarm
                            NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
                            NSDateComponents *dateComponents = [gregorian components:(NSYearCalendarUnit  | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:meetingEvent.startDate];
                            [dateComponents setHour:9];
                            [dateComponents setMinute:0];
                            [dateComponents setSecond:0];
                            NSDate *newDateWithTime = [gregorian dateFromComponents:dateComponents];
                            [meetingEvent addAlarm:[EKAlarm alarmWithAbsoluteDate:newDateWithTime]];
                            
                            [meetingEvent setCalendar:[eventStore defaultCalendarForNewEvents]];
                            BOOL success = [eventStore saveEvent:meetingEvent span:EKSpanThisEvent error:&error];
                            DebugLog(@"meetingEvent %@",meetingEvent);
                            
                            DebugLog(@"calendar sync %d",success);
                            currentObj.eventIdentifier = [[NSString alloc] initWithFormat:@"%@", meetingEvent.eventIdentifier];
                            if (success) {
                                [self showAlert:@"success"];
                            }else{
                                [self showAlert:@"fail"];
                            }
                        }
                        else
                        {
                            // display access denied error message here
                            DebugLog(@"not granted");
                            [self showAlert:@"not granted"];
                        }
                    });
                }];
        }else{
            NSDictionary *params = @{@"meeting_name":currentObj.name,@"calendar_sync":@"no"};
            [INEventLogger logEvent:@"Meetings_Calendar_Sync" withParams:params];
            [self pushToOpenWebsite];
        }
    }else if ([title isEqualToString:@"Ok"]){
        NSDictionary *params = @{@"meeting_name":currentObj.name,@"calendar_sync":@"yes"};
        [INEventLogger logEvent:@"Meetings_Calendar_Sync" withParams:params];
        [self pushToOpenWebsite];
    }
}

-(void)showAlert:(NSString *)typeAlert{
    if ([typeAlert isEqualToString:@"success"]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:@"This meeting is added into your calendar." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }else if ([typeAlert isEqualToString:@"fail"]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:@"Not able to add this meeting into your calendar. Please try again later." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:@"This app does not have access to your calendar.You can enable access this feature from Privacy Settings." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
}

-(void)pushToOpenWebsite
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:WARNING_TITLE
                                                        message:WARNING_MSG_FOR_WEBLINK
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
    [alertView show];
}

//-(void)checkDates
//{
//    NSPredicate *predicateForEvents = [eventStore predicateForEventsWithStartDate:nil endDate:nil calendars:[NSArray arrayWithObject:[eventStore defaultCalendarForNewEvents]]];
//    //set predicate to search for an event of the calendar(you can set the startdate, enddate and check in the calendars other than the default Calendar)
//    
//    NSArray *events_Array = [eventStore eventsMatchingPredicate: predicateForEvents];
//    //get array of events from the eventStore
//    
//    for (EKEvent *eventToCheck in events_Array)
//    {
//        if ([eventToCheck.title isEqualToString: @"yourEventTitle")
//             {
//                 NSError *err;
//                 //BOOL success = [eventStore removeEvent:eventToCheck span:EKSpanThisEvent error:&err];
//                 DebugLog(@"event deleted success if value = 1 : %d", success);
//                 break;
//                 
//             }
//    }
//}
@end
