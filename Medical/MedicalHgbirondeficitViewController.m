//
//  MedicalHgbirondeficitViewController.m
//  Medical
//
//  Created by Kirti Nikam on 16/10/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import "MedicalHgbirondeficitViewController.h"
#import "MedicalInfoImageViewController.h"
#import "MedicalAppDelegate.h"
#import "CommonCallback.h"

@interface MedicalHgbirondeficitViewController ()

@end

@implementation MedicalHgbirondeficitViewController
@synthesize scrollview;
@synthesize lbGender,genderSegment;
@synthesize viewWeight,viewHeightInCM,viewHeightInFT,viewTargetHemoglobin,viewCurrentHemoglobin;
@synthesize weightTextField,heightTextField,ftTextField,inchTextField,targerHGBTextfield,currentHGBTextfield;
@synthesize lblWeight,lblCM,lblFT,lblInch,lblTargetHGB,lblCurrentHGB;
@synthesize lblDosingWeight,lblTotalDoseIronDextran;
//Make Bold
@synthesize lbwLabel,totalDoseLabel,lblIronDeficit,HGBIrondeficitLabel;
//KeyboardViews
@synthesize keyBoardViewWeight,keyBoardViewHeight;
@synthesize infoButton;
@synthesize viewCollection,keyboardViewCollection,txtCollection,lblCollection,lblErrCollection,keyBoardBtnCollection;
@synthesize lblErrWeight,lblErrHeight,lblErrTargetHGB,lblErrCurrentHGB;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSDictionary *params = @{@"calculator_name":self.title};
    [INEventLogger logEvent:@"MedCalc_Calculator_Details" withParams:params];
  //  [self roundViewWithBorder];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]
                                              initWithBarButtonSystemItem:UIBarButtonSystemItemAction
                                              target:self action:@selector(shareBarBtnClicked:)];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.backBarButtonItem = backButton;
    
    [self setUI];
    
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected:)];
    tapGesture.numberOfTapsRequired = 1;
    tapGesture.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapGesture];
    
    scrollview.contentSize = CGSizeMake(scrollview.frame.size.width, CGRectGetMaxY(lblIronDeficit.frame)+30);
}

- (void)viewDidUnload
{
    [self setWeightTextField:nil];
    [self setLblWeight:nil];
    [self setGenderSegment:nil];
    [self setTargerHGBTextfield:nil];
    [self setCurrentHGBTextfield:nil];
    [self setLbwLabel:nil];
    [self setTotalDoseLabel:nil];
    [self setScrollview:nil];
    [self setHGBIrondeficitLabel:nil];
    [self setInfoButton:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
#pragma mark - Rotation
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma internal methods
-(void)setUI{
    
    lbGender.backgroundColor     = [UIColor clearColor];
    lbGender.textColor           = DEFAULT_COLOR;
    lbGender.font                = DEFAULT_FONT(20);
    
    lblDosingWeight.backgroundColor     = [UIColor clearColor];
    lblDosingWeight.textColor           = DEFAULT_COLOR;
    lblDosingWeight.font                = DEFAULT_FONT(16);
    
    lbwLabel.backgroundColor            = [UIColor clearColor];
    lbwLabel.textColor                  = DEFAULT_COLOR;
    lbwLabel.font                       = DEFAULT_FONT(17);
    
    lblTotalDoseIronDextran.backgroundColor     = [UIColor clearColor];
    lblTotalDoseIronDextran.textColor           = DEFAULT_COLOR;
    lblTotalDoseIronDextran.font                = DEFAULT_BOLD_FONT(17);
    lblTotalDoseIronDextran.numberOfLines       = 2;

    totalDoseLabel.backgroundColor     = [UIColor clearColor];
    totalDoseLabel.textColor           = DEFAULT_COLOR;
    totalDoseLabel.font                = DEFAULT_BOLD_FONT(20);
    
    lblIronDeficit.backgroundColor     = [UIColor clearColor];
    lblIronDeficit.textColor           = DEFAULT_COLOR;
    lblIronDeficit.font                = DEFAULT_BOLD_FONT(17);
    lblIronDeficit.numberOfLines       = 2;
    
    HGBIrondeficitLabel.backgroundColor     = [UIColor clearColor];
    HGBIrondeficitLabel.textColor           = DEFAULT_COLOR;
    HGBIrondeficitLabel.font                = DEFAULT_BOLD_FONT(20);
    
    for (UIView *lview in viewCollection) {
        lview.backgroundColor     = [UIColor whiteColor];
        lview.layer.borderColor   = [UIColor lightGrayColor].CGColor;
        lview.layer.borderWidth   = 1.0;
    }
    
    for (UITextField *txt in txtCollection) {
        txt.delegate            = self;
        txt.backgroundColor     = [UIColor clearColor];
        txt.textColor           = [UIColor blackColor];
        txt.font                = DEFAULT_FONT(22);
        txt.keyboardType        = UIKeyboardTypeDecimalPad;
        txt.textAlignment       = NSTextAlignmentCenter;
        txt.layer.borderColor   = [UIColor clearColor].CGColor;
        txt.text                = @"";
    }
    
    weightTextField.inputAccessoryView  = keyBoardViewWeight;
    heightTextField.inputAccessoryView  = keyBoardViewHeight;
    ftTextField.inputAccessoryView      = keyBoardViewHeight;
    inchTextField.inputAccessoryView    = keyBoardViewHeight;
    
    for (UILabel *lbl in lblCollection) {
        lbl.backgroundColor     = [UIColor clearColor];
        lbl.textColor           = DEFAULT_COLOR;
        lbl.font                = DEFAULT_SEMIBOLD_FONT(20);
    }
    
    for (UILabel *lbl in lblErrCollection) {
        lbl.backgroundColor     = [UIColor clearColor];
        lbl.textColor           = [UIColor redColor];
        lbl.font                = DEFAULT_SEMIBOLD_FONT(12);
        lbl.hidden              = YES;
    }
    
    for (UIView *keyboardView in keyboardViewCollection) {
        keyboardView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.4];
    }

    for (UIButton *btn in keyBoardBtnCollection) {
        btn.backgroundColor     = DEFAULT_COLOR;
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor colorWithWhite:1.0 alpha:0.5] forState:UIControlStateHighlighted];
        btn.titleLabel.font     = DEFAULT_FONT(20);
        
    }
}

-(void)tapDetected:(UIGestureRecognizer *)sender{
    [self.view endEditing:YES];
    CGPoint tapLocation = [sender locationInView:scrollview];
    UIView *view = [scrollview hitTest:tapLocation withEvent:nil];
    if (![view isKindOfClass:[UIButton class]]) {
        [self scrollTobottom];
    }
}

- (void) shareBarBtnClicked:(id)sender {
    UIActionSheet *uiActionSheetP= [[UIActionSheet alloc]
                                    initWithTitle: @""
                                    delegate:self
                                    cancelButtonTitle:@"CANCEL"
                                    destructiveButtonTitle:nil
                                    otherButtonTitles:@"Print",@"Email", nil];
	
	uiActionSheetP.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
	[uiActionSheetP showInView:self.navigationController.view];
    
}

#pragma UIActionSheet Delegate methods
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    DebugLog(@"buttonIndex = %d",buttonIndex);
	[self.view endEditing:YES];
    CGRect oldFrame = scrollview.frame;
    scrollview.frame=CGRectMake(scrollview.frame.origin.x, scrollview.frame.origin.y, scrollview.contentSize.width, scrollview.contentSize.height);
	if (buttonIndex == 0)
    {
        NSDictionary *params = @{@"calculator_name":self.title, @"share_via" : @"print"};
        [INEventLogger logEvent:@"MedCalc_Calculator_Share" withParams:params];
        [MEDICAL_APP_DELEGATE printImage:scrollview];
    }
    else if (buttonIndex == 1)
    {
        NSDictionary *params = @{@"calculator_name":self.title, @"share_via" : @"email"};
        [INEventLogger logEvent:@"MedCalc_Calculator_Share" withParams:params];
        [MEDICAL_APP_DELEGATE mailImage:scrollview];
    }
	scrollview.frame=oldFrame;
    [self scrollTobottom];
}


-(void) roundViewWithBorder
{
    for (int i=100; i<=105; i++)
    {
        UIView *text=(UIView *)[self.view viewWithTag:i];
        text.layer.cornerRadius = TEXTFIELDVIEW_CORNERRADIUS;
        text.clipsToBounds = YES;
        text.layer.borderColor = TEXTFIELDVIEW_DEFAULT_BORDERCOLOR;
        text.layer.borderWidth = TEXTFIELDVIEW_BORDERWIDTH;
    }
}


- (IBAction)keyboardButtonAction:(id)sender {
    UIButton *btn=(UIButton *)sender;
    switch (btn.tag) {
        case 301:
        case 302:
        {
            lblWeight.text  = btn.titleLabel.text;
            [self validate:weightTextField];
        }
            break;
        case 303: // cm
        {
            HeightViewFLAG  = 0;
            lblCM.text      = @"cm";
            [viewHeightInFT setHidden:TRUE];
            [viewHeightInCM setHidden:FALSE];
            [self validate:heightTextField];
            [heightTextField becomeFirstResponder];
        }
            break;
        case 304: // feet/inch
        {
            HeightViewFLAG  = 1;
            [viewHeightInCM setHidden:TRUE];
            [viewHeightInFT setHidden:FALSE];
            [ftTextField becomeFirstResponder];
        }
            break;
        case 305: // inches
        {
            HeightViewFLAG  = 2;
            lblCM.text      = @"inches";
            [viewHeightInFT setHidden:TRUE];
            [viewHeightInCM setHidden:FALSE];
            [self validate:heightTextField];
            [heightTextField becomeFirstResponder];
        }
            break;
        default:
            break;
    }
    [self compute];
    
}

- (IBAction)editingChanged:(id)sender {
    UITextField *textField=(UITextField *)sender;
    if (![textField isEqual:targerHGBTextfield] && ![textField isEqual:currentHGBTextfield]) {
        [self validate:textField];
    }
    [self compute];
}


-(BOOL)validate:(UITextField *)textField{
    BOOL success = YES;
    double doubleValue = [textField.text doubleValue];
    if([textField isEqual:weightTextField])
    {
        if ([lblWeight.text isEqualToString:@"kg"])
        {
            if ((doubleValue >= 10 && doubleValue <= 200)) {
                [self removeErrorMessageFromLabel:lblErrWeight];
            }else{
                [self setErrorMessageOnLabel:@"Enter weight value in between 10 - 200." label:lblErrWeight];
                success = NO;
            }
        }else if ([lblWeight.text isEqualToString:@"lb"])
        {
            if ((doubleValue >= 22 && doubleValue <= 441)) {
                [self removeErrorMessageFromLabel:lblErrWeight];
            }else{
                [self setErrorMessageOnLabel:@"Enter weight value in between 22 - 441." label:lblErrWeight];
                success = NO;
            }
        }
    }else if([textField isEqual:heightTextField])
    {
        if ([lblCM.text isEqualToString:@"cm"])
        {
            if ((doubleValue >= 30 && doubleValue <= 241.3)) {
                [self removeErrorMessageFromLabel:lblErrHeight];
            }else{
                [self setErrorMessageOnLabel:@"Enter height value in between 30 - 241." label:lblErrHeight];
                success = NO;
            }
        }else if ([lblCM.text isEqualToString:@"inches"])
        {
            if ((doubleValue >= 11.8 && doubleValue <= 95)) {
                [self removeErrorMessageFromLabel:lblErrHeight];
            }else{
                [self setErrorMessageOnLabel:@"Enter height value in between 11.8 - 95." label:lblErrHeight];
                success = NO;
            }
        }
    }else if([textField isEqual:ftTextField])
    {
        if ((doubleValue >= 1 && doubleValue <= 7)) {
            [self removeErrorMessageFromLabel:lblErrHeight];
        }else{
            [self setErrorMessageOnLabel:@"Enter feet value in between 1 - 7." label:lblErrHeight];
            success = NO;
        }
    }
    else if([textField isEqual:inchTextField])
    {
        if ((doubleValue >= 0 && doubleValue <= 11)) {
            [self removeErrorMessageFromLabel:lblErrHeight];
        }else{
            [self setErrorMessageOnLabel:@"Enter inch value in between 0 - 11." label:lblErrHeight];
            success = NO;
        }
    }
    return success;
}


-(void)compute{
    if (![weightTextField.text isEqualToString:@""] && (![heightTextField.text isEqualToString:@""] || (![ftTextField.text isEqualToString:@""] && ![inchTextField.text isEqualToString:@""])) && ![targerHGBTextfield.text isEqualToString:@""] && ![currentHGBTextfield.text isEqualToString:@""])
    {
        //formula ------ sqrt((height*weight)/3600)
        double heightValue = [heightTextField.text doubleValue];
        double weightValue = [weightTextField.text doubleValue];
        
        int count=0;
        if ([self validate:weightTextField])
        {
            if ([lblWeight.text isEqualToString:@"lb"])
                weightValue = weightValue * ONE_POUND_IN_KG;
            
            count ++;
        }
        switch (HeightViewFLAG) {
            case 0: //cm
            {
                if ([self validate:heightTextField])
                {
                    count ++;
                }
            }
                break;
            case 1:
            {
                if ([self validate:ftTextField] && [self validate:inchTextField])
                {
                    heightValue =    [self convertintocm:[ftTextField.text doubleValue] inch:[inchTextField.text doubleValue]];
                    count ++;
                }
            }
                break;
            case 2:
            {
                if ([self validate:heightTextField])
                {
                    heightValue =    heightValue * ONE_INCHES_IN_CM;
                    count ++;
                }
            }
                break;
            default:
                break;
        }

        heightValue = heightValue * ONE_CM_IN_INCHES;
        DebugLog(@"height value %f",heightValue);
        if(count == 2)
        {
            double LBW = 0;
            switch (self.genderSegment.selectedSegmentIndex)
            {
                case 0: //Male
                {
                    LBW = 50 + (2.3 * (heightValue - 60));
                }
                    break;
                case 1: // Female
                {
                     LBW = 45.5 + (2.3 * (heightValue - 60));
                }
                    break;
                default:
                    break;
            }
            
            if (weightValue < LBW) {
                LBW = weightValue;
            }
            
            DebugLog(@"Taking LBW as %f",LBW);
            lbwLabel.text= [NSString stringWithFormat:@"%.1f",LBW];
            
            // Dose (mL) = 0.0442 (Desired Hb - Observed Hb) x LBW + (0.26 x LBW)
            double dose = 0.0442 *([targerHGBTextfield.text doubleValue] - [currentHGBTextfield.text doubleValue]) * [lbwLabel.text doubleValue] + (0.26 * [lbwLabel.text doubleValue]);
            DebugLog(@"dose %f",dose);
            totalDoseLabel.text = [NSString stringWithFormat:@"%.1f",dose];
            HGBIrondeficitLabel.text = [NSString stringWithFormat:@"%.1f",[totalDoseLabel.text doubleValue]*50];
            
            // Total iron deficit [mg] = body weight [kg] x (target Hb-actual Hb) [g/dl] x 2.4 + depot iron [mg]
            //            double depot_iron = 0;
            //            double targetHGB = 0;
            //            if (weightValue < 35) {
            //                targetHGB = 13;
            //                depot_iron = 15;
            //            }
            //            else{
            //                targetHGB = 15;
            //                depot_iron = 500;
            //            }
            //            DebugLog(@"Taking targetHGB as %f and depot_iron as %f",targetHGB,depot_iron);
            //            targetHGB = [targerHGBTextfield.text doubleValue];
            //            double ironDeficit = LBW * (targetHGB - [currentHGBTextfield.text doubleValue]) * 2.4 + depot_iron;
            //            HGBIrondeficitLabel.text = [NSString stringWithFormat:@"%.2f",ironDeficit];
        }
        else
        {
            lbwLabel.text = @"...."; totalDoseLabel.text  = @"...."; HGBIrondeficitLabel.text = @"....";
        }
    }
    else
    {
        lbwLabel.text = @"...."; totalDoseLabel.text  = @"...."; HGBIrondeficitLabel.text = @"....";
    }
}

//-(BOOL)validate:(double)doublevalue checktype:(int)type viewTag:(int)tag
//{
//    switch (type) {
//        case 0:
//            // kg or lb
//            if ((doublevalue >=10 && doublevalue<=200) && [lblWeight.text isEqualToString:@"kg"])
//            {
//                [self changeTextFieldColor:TRUE viewTag:tag];
//                return TRUE;
//            }
//            else if ((doublevalue >=22 && doublevalue<=441) && [lblWeight.text isEqualToString:@"lb"])
//            {
//                [self changeTextFieldColor:TRUE viewTag:tag];
//                return TRUE;
//            }
//            else
//            {
//                [self changeTextFieldColor:FALSE viewTag:tag];
//                return FALSE;
//            }
//            break;
//        case 1://hgb 
//            if (doublevalue >=2 && doublevalue<=20)
//            {
//                [self changeTextFieldColor:TRUE viewTag:tag];
//                return TRUE;
//                
//            }
//            else
//            {
//                [self changeTextFieldColor:FALSE viewTag:tag];
//                return FALSE;
//            }
//            break;
//        case 2: //cm
//            if (doublevalue >=30 && doublevalue<=241.3)
//            {
//                [self changeTextFieldColor:TRUE viewTag:tag];
//                return TRUE;
//                
//            }
//            else
//            {
//                [self changeTextFieldColor:FALSE viewTag:tag];
//                return FALSE;
//            }
//            break;
//        case 3: //ft
//            if (doublevalue >= 1 && doublevalue<=7)
//            {
//                [self changeTextFieldColor:TRUE viewTag:tag];
//                return TRUE;
//                
//            }
//            else
//            {
//                [self changeTextFieldColor:FALSE viewTag:tag];
//                return FALSE;
//            }
//            break;
//        case 4: // inch
//            if (doublevalue >= 0 && doublevalue<=11)
//            {
//                [self changeTextFieldColor:TRUE viewTag:tag];
//                return TRUE;
//                
//            }
//            else
//            {
//                [self changeTextFieldColor:FALSE viewTag:tag];
//                return FALSE;
//            }
//            break;
//        case 5: // only inches
//            if (doublevalue >= 11.8 && doublevalue<=95)
//            {
//                [self changeTextFieldColor:TRUE viewTag:tag];
//                return TRUE;
//                
//            }
//            else
//            {
//                [self changeTextFieldColor:FALSE viewTag:tag];
//                return FALSE;
//            }
//            break;
//        default:
//            break;
//    }
//    return FALSE;
//}
//
//
//-(void)changeTextFieldColor:(BOOL)type viewTag:(int)tag
//{
//    UIView *view=(UIView *)[self.view viewWithTag:tag];
//    if (type)
//    {
//        view.backgroundColor=TEXTFIELDVIEW_DEFAULT_BACKGROUNDCOLOR;
//        view.layer.borderColor=TEXTFIELDVIEW_DEFAULT_BORDERCOLOR;
//        
//    }
//    else
//    {
//        view.backgroundColor=TEXTFIELDVIEW_ERROR_BACKGROUNDCOLOR;
//        view.layer.borderColor=TEXTFIELDVIEW_ERROR_BORDERCOLOR;
//    }
//}
//
//-(void)compute{
//    
//    
//    if (![weightTextField.text isEqualToString:@""] && (![heightTextField.text isEqualToString:@""] || (![ftTextField.text isEqualToString:@""] && ![inchTextField.text isEqualToString:@""])) && ![targerHGBTextfield.text isEqualToString:@""] && ![currentHGBTextfield.text isEqualToString:@""])
//    {
//        //formula ------ sqrt((height*weight)/3600)
//        double heightValue = [heightTextField.text doubleValue];
//        double weightValue = [weightTextField.text doubleValue];
//        
//        int count=0;
//        if ([self validate:weightValue checktype:0 viewTag:100])
//        {
//            if ([lblWeight.text isEqualToString:@"lb"])
//                weightValue=[weightTextField.text doubleValue] * ONE_POUND_IN_KG;
//            
//            count ++;
//        }
//        switch (HeightViewFLAG) {
//            case 0: //cm
//            {
//                if ([self validate:heightValue checktype:2 viewTag:102])
//                {
//                    count ++;
//                }
//            }
//                break;
//            case 1:
//            {
//                if ([self validate:[ftTextField.text doubleValue] checktype:3 viewTag:103] && [self validate:[inchTextField.text doubleValue] checktype:4 viewTag:103])
//                {
//                    heightValue=[self convertintocm:[ftTextField.text doubleValue] inch:[inchTextField.text doubleValue]];
//                    count ++;
//                }
//            }
//                break;
//            case 2:
//            {
//                if ([self validate:heightValue checktype:5 viewTag:102])
//                {
//                    heightValue=heightValue * ONE_INCHES_IN_CM;
//                    count ++;
//                }
//            }
//                break;
//            default:
//                break;
//        }
//        //        if (HeightViewFLAG &&  [self validate:heightValue checktype:1 viewTag:101])
//        //         {
//        //             count ++;
//        //         }
//        //         else if((!HeightViewFLAG) && ([self validate:[ftTextField.text doubleValue] checktype:2 viewTag:102] && [self validate:[inchTextField.text doubleValue] checktype:3 viewTag:102])) // ft inch
//        //         {
//        //             heightValue=[self convertintocm:[ftTextField.text doubleValue] inch:[inchTextField.text doubleValue]];
//        //             count ++;
//        //         }
//        heightValue = heightValue * ONE_CM_IN_INCHES;
//        DebugLog(@"height value %f",heightValue);
//        if(count == 2)
//        {
//            double LBW = 0;
//            switch (self.genderSegment.selectedSegmentIndex)
//            {
//                case 0: //Male
//                {
////                    if (weightValue < 50) {
////                        LBW = weightValue;
////                    }
////                    else
//                    {
//                        LBW = 50 + (2.3 * (heightValue - 60));
//                    }
////                    DebugLog(@"Calculated LBW %f",(50 + (2.3 * (heightValue - 60))));
//                }
//                    break;
//                case 1: // Female
//                {
////                    if (weightValue < 45.5) {
////                        LBW = weightValue;
////                    }
////                    else
//                    {
//                        LBW = 45.5 + (2.3 * (heightValue - 60));
//                    }
////                    DebugLog(@"Calculated LBW %f",(45.5 + (2.3 * (heightValue - 60))));
//                }
//                    break;
//                default:
//                    break;
//            }
//            
//            if (weightValue < LBW) {
//                LBW = weightValue;
//            }
//            DebugLog(@"Taking LBW as %f",LBW);
//            lbwLabel.text= [NSString stringWithFormat:@"%.1f",LBW];
//            
//            // Dose (mL) = 0.0442 (Desired Hb - Observed Hb) x LBW + (0.26 x LBW)
//            double dose = 0.0442 *([targerHGBTextfield.text doubleValue] - [currentHGBTextfield.text doubleValue]) * [lbwLabel.text doubleValue] + (0.26 * [lbwLabel.text doubleValue]);
//            DebugLog(@"dose %f",dose);
//            totalDoseLabel.text = [NSString stringWithFormat:@"%.1f",dose];
//            HGBIrondeficitLabel.text = [NSString stringWithFormat:@"%.1f",[totalDoseLabel.text doubleValue]*50];
//            
//           // Total iron deficit [mg] = body weight [kg] x (target Hb-actual Hb) [g/dl] x 2.4 + depot iron [mg]
////            double depot_iron = 0;
////            double targetHGB = 0;
////            if (weightValue < 35) {
////                targetHGB = 13;
////                depot_iron = 15;
////            }
////            else{
////                targetHGB = 15;
////                depot_iron = 500;
////            }
////            DebugLog(@"Taking targetHGB as %f and depot_iron as %f",targetHGB,depot_iron);
////            targetHGB = [targerHGBTextfield.text doubleValue];
////            double ironDeficit = LBW * (targetHGB - [currentHGBTextfield.text doubleValue]) * 2.4 + depot_iron;
////            HGBIrondeficitLabel.text = [NSString stringWithFormat:@"%.2f",ironDeficit];
//        }
//        else
//        {
//            lbwLabel.text = @"...."; totalDoseLabel.text  = @"...."; HGBIrondeficitLabel.text = @"....";
//        }
//    }
//    else
//    {
//        lbwLabel.text = @"...."; totalDoseLabel.text  = @"...."; HGBIrondeficitLabel.text = @"....";
//    }
//    
//    
////    //Formula  Hemoglobin iron deficit (mg) = weight (kg)  x (14 - Hgb) x (2.145)
////    if (![weightTextField.text isEqualToString:@""] &&![hbgTextField.text isEqualToString:@""])
////    {
////        int count=0;
////        double weightValue = [weightTextField.text doubleValue];
////        double hgbValue= [hbgTextField.text doubleValue];
////         
////        if ([self validate:weightValue checktype:0 viewTag:100])
////        {
////            if ([lblWeight.text isEqualToString:@"lb"])
////                weightValue=[weightTextField.text doubleValue] * ONE_POUND_IN_KG;
////            
////            count ++;
////        }        
////        if ([self validate:hgbValue checktype:1 viewTag:101]) //hgb
////            count ++;
////        
////        if (count == 2)
////        {
////            [self show:(weightValue*(14-hgbValue)*2.145)];
////        }
////        else
////            labelResult.text=@"....";
////        
////    }
////    else
////    {
////        labelResult.text=@"....";
////    }
//
//}

-(double)convertintocm:(double) feet inch:(double)inchvalue {
    
    double temp=(feet*12)+inchvalue;
    return (temp*2.54);
}

-(void)show:(double)result{
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [formatter setMaximumFractionDigits:2];
    [formatter setRoundingMode: NSNumberFormatterRoundCeiling];
    DebugLog(@"result %f",result);
   // labelResult.text=[NSString stringWithFormat:@"%@ mg",[formatter stringFromNumber:[NSNumber numberWithFloat:result]]];
   //labelResult.text=[NSString stringWithFormat:@"%.2f mg",result];
}

#pragma UITextField Delegate methods
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if([textField isEqual:weightTextField])
    {
        [scrollview setContentOffset:CGPointMake(0,viewWeight.frame.origin.y-100) animated:YES];
        [CommonCallback changeTextFieldColor:TEXT_EDITING view:viewWeight];
    }
    else if([textField isEqual:heightTextField])
    {
        [scrollview setContentOffset:CGPointMake(0,viewHeightInCM.frame.origin.y-100) animated:YES];
        [CommonCallback changeTextFieldColor:TEXT_EDITING view:viewHeightInCM];
    }
    else if([textField isEqual:ftTextField] || [textField isEqual:inchTextField])
    {
        [scrollview setContentOffset:CGPointMake(0,viewHeightInFT.frame.origin.y-100) animated:YES];
        [CommonCallback changeTextFieldColor:TEXT_EDITING view:viewHeightInFT];
    }
    else if([textField isEqual:targerHGBTextfield])
    {
        [scrollview setContentOffset:CGPointMake(0,viewTargetHemoglobin.frame.origin.y-100) animated:YES];
        [CommonCallback changeTextFieldColor:TEXT_EDITING view:viewTargetHemoglobin];
    }
    else if([textField isEqual:currentHGBTextfield])
    {
        [scrollview setContentOffset:CGPointMake(0,viewCurrentHemoglobin.frame.origin.y-100) animated:YES];
        [CommonCallback changeTextFieldColor:TEXT_EDITING view:viewCurrentHemoglobin];
    }
    scrollview.contentSize = CGSizeMake(scrollview.frame.size.width, CGRectGetMaxY(lblIronDeficit.frame)+300);
    return YES;
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if([textField isEqual:weightTextField])
    {
        [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:viewWeight];
    }
    else if([textField isEqual:heightTextField])
    {
        [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:viewHeightInCM];
    }
    else if([textField isEqual:ftTextField] || [textField isEqual:inchTextField])
    {
        [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:viewHeightInFT];
    }
    else if([textField isEqual:targerHGBTextfield])
    {
        [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:viewTargetHemoglobin];
    }
    else if([textField isEqual:currentHGBTextfield])
    {
        [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:viewCurrentHemoglobin];
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    // allow backspace
    if (range.length > 0 && [string length] == 0) {
        return YES;
    }
    // do not allow . at the beggining
    //    if (range.location == 0 && [string isEqualToString:@"."]) {
    //        return NO;
    //    }
    
    NSRange temprange = [textField.text rangeOfString:@"."];
    if ((temprange.location != NSNotFound) && [string isEqualToString:@"."])
    {
        return NO;
    }
    else
        return YES;
}

-(void)scrollTobottom
{
    [UIView animateWithDuration:0.2 animations:^{
        scrollview.contentSize = CGSizeMake(scrollview.frame.size.width, CGRectGetMaxY(lblIronDeficit.frame)+30);
        CGPoint bottomOffset = CGPointMake(0, 0);
        [scrollview setContentOffset:bottomOffset animated:YES];
    }];
}


- (IBAction)infoButtonAction:(id)sender {
    NSDictionary *params = @{@"calculator_name":self.title};
    [INEventLogger logEvent:@"MedCalc_Calculator_Info_Image" withParams:params];
    
    MedicalInfoImageViewController *typedetailController = [[MedicalInfoImageViewController alloc] initWithimageName:@"HGBIron_info.png"] ;
    [self.navigationController pushViewController:typedetailController animated:YES];
}

- (IBAction)segmentControlChanged:(id)sender {
    [self compute];
}

-(void)setErrorMessageOnLabel:(NSString *)errorMessage label:(UILabel *)lbl{
    lbwLabel.text = @"...."; totalDoseLabel.text  = @"...."; HGBIrondeficitLabel.text = @"....";
    lbl.hidden = NO;
    lbl.text = errorMessage;
    if ([lbl isEqual:lblErrWeight]) {
        [CommonCallback changeTextFieldColor:TEXT_ERROR view:viewWeight];
    }else if ([lbl isEqual:lblErrHeight]) {
        if (viewHeightInCM.hidden) {
            [CommonCallback changeTextFieldColor:TEXT_ERROR view:viewHeightInFT];
        }else{
            [CommonCallback changeTextFieldColor:TEXT_ERROR view:viewHeightInCM];
        }
    }else if ([lbl isEqual:lblErrTargetHGB]) {
        [CommonCallback changeTextFieldColor:TEXT_ERROR view:viewTargetHemoglobin];
    }else if ([lbl isEqual:lblErrCurrentHGB]) {
        [CommonCallback changeTextFieldColor:TEXT_ERROR view:viewCurrentHemoglobin];
    }
}

-(void)removeErrorMessageFromLabel:(UILabel *)lbl{
    lbl.hidden = YES;
    if ([lbl isEqual:lblErrWeight]) {
        [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:viewWeight];
    }else if ([lbl isEqual:lblErrHeight]) {
        if (viewHeightInCM.hidden) {
            [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:viewHeightInFT];
        }else{
            [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:viewHeightInCM];
        }
    }else if ([lbl isEqual:lblErrTargetHGB]) {
        [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:viewTargetHemoglobin];
    }else if ([lbl isEqual:lblErrCurrentHGB]) {
        [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:viewCurrentHemoglobin];
    }
}
@end
