//
//  MedicalViewController.h
//  Medical
//
//  Created by Kirti Nikam on 08/10/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <MessageUI/MFMessageComposeViewController.h>

#define _GRID_VIEW_ 3X3
#define HEADER_VIEW_HEIGHT 64
#define NUMBEROFBUTTONSPERPAGE 9.0

#define MedCalc @"Medical Calculator"
#define ChemoOrders @"Chemotherapy Orders"
#define ChemoDrugs @"Oncology Drugs"
#define Meetings @"Meetings"
#define Journals @"Journals"
#define IDC9Codes @"Coding"
#define Resources @"Resources"
#define News @"Alerts"
#define Pathology @"Pathology"
#define OncologyHandBook @"Oncology Book"
#define HematologyHandBook @"Hematology Book"
#define Images @"Images"
#define HealthCare @"Healthcare Providers"

#define RSS @"Must Reads"
#define Drug_Interactions @"Drug-Drug Interactions"


#define UsefulSites  @"Web Links"
#define Meetings  @"Meetings"
#define CTCAE   @"CTCAE"


@interface MedicalViewController : UIViewController<UIScrollViewDelegate,UIActionSheetDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate,NSURLConnectionDelegate>
{
    NSURLConnection *conn;
    BOOL pageControlUsed;
    
    BOOL showRateAppAlert;
}
@property (strong,nonatomic) NSMutableData *webData;
@property (strong,nonatomic) NSString *newsAlerts;

@property (strong, nonatomic) NSArray *gridbuttonNamesArray;
@property (strong, nonatomic) IBOutlet UIScrollView *gridScrollView;
@property (strong, nonatomic) IBOutlet UIPageControl *gridpageControl;

@property (strong, nonatomic) IBOutlet UIButton *btnSignUp;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *btnCollection;

@property (strong, nonatomic) IBOutlet UIButton *btnChemoOrders;
@property (strong, nonatomic) IBOutlet UIButton *btnChemotherapy;
@property (strong, nonatomic) IBOutlet UIButton *btnDrugInteraction;
@property (strong, nonatomic) IBOutlet UIButton *btnCalc;
@property (strong, nonatomic) IBOutlet UIButton *btnCTCAE;
@property (strong, nonatomic) IBOutlet UIButton *btnAlerts;
@property (strong, nonatomic) IBOutlet UIButton *btnWebLinks;
@property (strong, nonatomic) IBOutlet UIButton *btnResources;
@property (strong, nonatomic) IBOutlet UIButton *btnRSS;
@property (strong, nonatomic) IBOutlet UIButton *btnMeeting;


- (IBAction)btnActionMethod:(id)sender;

- (IBAction)btnSignUpPressed:(id)sender;
@end
