//
//  MedicalCTCAEObject.h
//  Medical
//
//  Created by Kirti Nikam on 30/10/13.
//  Copyright (c) 2013 Kirti Nikam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MedicalCTCAEObject : NSObject
@property (readwrite,nonatomic) int rowId;
@property (copy,nonatomic) NSString *term;
@property (copy,nonatomic) NSString *termDefinition;
@property (copy,nonatomic) NSString *grade1;
@property (copy,nonatomic) NSString *grade2;
@property (copy,nonatomic) NSString *grade3;
@property (copy,nonatomic) NSString *grade4;
@property (copy,nonatomic) NSString *grade5;
@end
