//
//  RegimenObject.h
//  Medical
//
//  Created by Kirti Nikam on 18/06/14.
//  Copyright (c) 2014 Kirti Nikam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RegimenObject : NSObject
@property (readwrite,nonatomic) int regimenID;
@property (copy,nonatomic) NSString *regimenName;
@property (readwrite,nonatomic) int regimenWarn;
@end
