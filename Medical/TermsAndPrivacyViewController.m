//
//  TermsAndPrivacyViewController.m
//  Medical
//
//  Created by Kirti Nikam on 21/04/14.
//  Copyright (c) 2014 Kirti Nikam. All rights reserved.
//

#import "TermsAndPrivacyViewController.h"
#import "MedicalAppDelegate.h"
#import "MedicalUserDefaults.h"

@interface TermsAndPrivacyViewController ()

@end

@implementation TermsAndPrivacyViewController
@synthesize btnAccept;
@synthesize webView;
@synthesize termsDelegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self addHUD];

    btnAccept.backgroundColor     = DEFAULT_COLOR;
    [btnAccept setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnAccept setTitleColor:[UIColor colorWithWhite:1.0 alpha:0.5] forState:UIControlStateHighlighted];
    btnAccept.titleLabel.font     = DEFAULT_BOLD_FONT(25);
    [btnAccept setTitle:@"I Agree" forState:UIControlStateNormal];
    btnAccept.layer.cornerRadius  = 5.0;
    
    NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"terms-and-privacy" ofType:@"html"]isDirectory:NO];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    DebugLog(@"url %@",url);
    [webView loadRequest:requestObj];
    webView.scalesPageToFit = TRUE;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Rotation
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma internal methods
- (void)addHUD{
	HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
	[self.navigationController.view addSubview:HUD];
	HUD.delegate    = self;
	HUD.labelText   = @"Loading";
    HUD.labelFont   = DEFAULT_FONT(16.0);
    HUD.color       = DEFAULT_COLOR_WITH_ALPHA(0.9);
    
//    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureTapDetacted:)];
//    [HUD addGestureRecognizer:gesture];
}

//#pragma mark -
//#pragma mark MBProgressHUDDelegate methods
//- (void)hudWasHidden:(MBProgressHUD *)hud {
//	// Remove HUD from screen when the HUD was hidded
//	[HUD removeFromSuperview];
//	HUD = nil;
//}

#pragma UIGestureRecognizer selector methods
-(void)gestureTapDetacted:(UIGestureRecognizer *)sender{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:@"Do you want to cancel loading?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
    [alert show];
}

#pragma UIAlertView delegate methods
- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if ([title isEqualToString:@"YES"]) {
        if (webView.loading) {
            [webView stopLoading];
        }
        if (![HUD isHidden]) {
            [HUD hide:YES];
        }
    }
}

#pragma UIWebView delegate methods
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [HUD show:YES];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [HUD hide:YES];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [HUD hide:YES];
}

- (IBAction)btnAcceptPressed:(id)sender {
    [MedicalUserDefaults setLicenseAgreement:1];
//    [termsDelegate closeTermsAndPrivacyView];
    [MEDICAL_APP_DELEGATE removeSplashScreen];
}
@end
