//
//  MedicalSignUpViewController.h
//  Medical
//
//  Created by Kirti Nikam on 16/04/14.
//  Copyright (c) 2014 Kirti Nikam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TermsAndPrivacyViewController.h"
#import "MBProgressHUD.h"

@interface MedicalSignUpViewController : UIViewController<UITextFieldDelegate,TermsAndPrivacyDelegate,NSURLConnectionDelegate,MBProgressHUDDelegate>
{
    NSURLConnection *conn;
    
    MBProgressHUD *HUD;
    int status;
}
@property (strong,nonatomic) NSMutableArray *arryOccupation;
@property (strong,nonatomic) NSMutableArray *arryAge;
@property (strong,nonatomic) NSMutableArray *arryCountry;

@property(strong,nonatomic) NSMutableData *reponseData;

@property (strong, nonatomic) IBOutlet UIScrollView *signUpScrollView;

@property (strong, nonatomic) IBOutlet UITextField *txtEmailId;
@property (strong, nonatomic) IBOutlet UILabel *lblEmailId;
@property (strong, nonatomic) IBOutlet UITextField *txtFirstname;
@property (strong, nonatomic) IBOutlet UILabel *lblFirstname;
@property (strong, nonatomic) IBOutlet UITextField *txtLastname;
@property (strong, nonatomic) IBOutlet UILabel *lblLastname;
@property (strong, nonatomic) IBOutlet UITextField *txtAge;
@property (strong, nonatomic) IBOutlet UILabel *lblAge;
@property (strong, nonatomic) IBOutlet UITextField *txtOccupation;
@property (strong, nonatomic) IBOutlet UILabel *lblOccupation;
@property (strong, nonatomic) IBOutlet UITextField *txtCountry;
@property (strong, nonatomic) IBOutlet UILabel *lblCountry;
@property (strong, nonatomic) IBOutlet UITextField *txtZipCode;
@property (strong, nonatomic) IBOutlet UILabel *lblZipCode;

@property (strong, nonatomic) IBOutlet UIButton *btnSendEmailAlerts;
@property (strong, nonatomic) IBOutlet UIButton *btnSubmit;
@property (strong, nonatomic) IBOutlet UIButton *btnLater;

@property (strong, nonatomic) IBOutletCollection(UITextField) NSArray *txtCollection;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *lblCollection;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *btnCollection;
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *viewCollection;

@property (strong, nonatomic) IBOutlet UIImageView *imgViewAge;
@property (strong, nonatomic) IBOutlet UIImageView *imgViewOccupation;
@property (strong, nonatomic) IBOutlet UIImageView *imgViewCountry;

@property (strong, nonatomic) IBOutlet UIView *vEmailId;
@property (strong, nonatomic) IBOutlet UIView *vFirstname;
@property (strong, nonatomic) IBOutlet UIView *vLastname;
@property (strong, nonatomic) IBOutlet UIView *vAge;
@property (strong, nonatomic) IBOutlet UIView *vOccupation;
@property (strong, nonatomic) IBOutlet UIView *vCountry;
@property (strong, nonatomic) IBOutlet UIView *vZipCode;


- (IBAction)btnSendEmailAlertsPressed:(id)sender;
- (IBAction)btnSubmitPressed:(id)sender;
- (IBAction)btnLaterPressed:(id)sender;
@end
