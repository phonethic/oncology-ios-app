//
//  MedicalCTCAEViewController.h
//  Medical
//
//  Created by Kirti Nikam on 30/10/13.
//  Copyright (c) 2013 Kirti Nikam. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MedicalCTCAEObject;
@interface MedicalCTCAEViewController : UIViewController<UISearchBarDelegate>
{
    MedicalCTCAEObject *tempCTCAEObj;
    BOOL isSearchOn;
    BOOL canSelectRow;
    double keyboardHeight;
}


@property (retain,nonatomic) NSMutableDictionary *sections;
@property (retain,nonatomic) NSMutableArray *searchArray;

@property (strong, nonatomic) IBOutlet UITableView *ctcaeTableView;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@end
