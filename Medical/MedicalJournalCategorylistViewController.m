//
//  MedicalJournalOncologyViewController.m
//  Medical
//
//  Created by Kirti Nikam on 17/11/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//
#import <sqlite3.h>
#import "MedicalAppDelegate.h"
#import "MedicalJournalCategorylistViewController.h"
#import "MedicalWebViewController.h"

@interface MedicalJournalCategorylistViewController ()
@end

@implementation MedicalJournalCategorylistViewController
@synthesize journal_category;
@synthesize jtableView,searchBar;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (isSearchOn) {
        [searchBar becomeFirstResponder];
    }
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSDictionary *params = @{@"journal_category":journal_category};
    [INEventLogger logEvent:@"Journals_List" withParams:params];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        searchBar.barTintColor = DEFAULT_COLOR;
        [searchBar setTranslucent:NO];
    }else{
        // searchBar.tintColor = DEFAULT_COLOR;
        for (UIView *searchBarSubView in searchBar.subviews) {
            if ([searchBarSubView conformsToProtocol:@protocol(UITextInputTraits)]) {
                @try {
                    [(UITextField *)searchBarSubView setBorderStyle:UITextBorderStyleRoundedRect];
                }
                @catch (NSException *exception) {
                    //  exception
                    DebugLog(@"Got exception : %@",exception);
                }
            }
        }
    }
    isSearchOn = NO;
    canSelectRow = YES;
    keyboardHeight = 216;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:)
                                                 name:@"UIKeyboardWillShowNotification"
                                               object:nil];
    self.searchBar.delegate = self;
}
- (void)viewDidUnload
{
    [self setSearchBar:nil];
    [self setJtableView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
#pragma mark - Rotation
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma UITableView DataSource Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (isSearchOn)
        return 1;
    else
    {
        sqlite3 *databaseP;
        NSInteger sectionCount = 0;
       if(sqlite3_open([[MEDICAL_APP_DELEGATE databasePath] UTF8String], &databaseP) == SQLITE_OK)
        {
            NSString *queryString = [NSString stringWithFormat:@"select count (*) from (select * from journals where journal_category = \"%@\" COLLATE NOCASE group by journal_rate)",journal_category];
            const char *queryCharP = [queryString UTF8String];
            
            sqlite3_stmt *compiledStatementP = nil;
            
            if (sqlite3_prepare_v2(databaseP, queryCharP, -1, &compiledStatementP, NULL) == SQLITE_OK)
            {
                if (sqlite3_step(compiledStatementP) == SQLITE_ROW)
                {
                    sectionCount = sqlite3_column_int(compiledStatementP, 0);
                }            
            }
            sqlite3_finalize(compiledStatementP);
            sqlite3_close(databaseP);
        }
        return sectionCount;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (isSearchOn){
        UIView *customHeaderView = [[UIView alloc] initWithFrame:CGRectMake(10,0,300,35)];
        customHeaderView.backgroundColor = DEFAULT_COLOR;
        return customHeaderView;
    }else
    {
        sqlite3 *databaseP;
        NSString *sectionImage = nil;
        if (sqlite3_open([[MEDICAL_APP_DELEGATE databasePath] UTF8String], &databaseP) == SQLITE_OK)
        {
            NSString *queryString = [NSString stringWithFormat:@"select journal_rate from journals where journal_category = \"%@\" COLLATE NOCASE group by journal_rate order by journal_rate desc limit %d,1",journal_category,section];
            const char *queryCharP = [queryString UTF8String];
            
            sqlite3_stmt *compiledStatementP = nil;
            
            if (sqlite3_prepare_v2(databaseP, queryCharP, -1, &compiledStatementP, NULL) == SQLITE_OK)
            {
                if (sqlite3_step(compiledStatementP) == SQLITE_ROW)
                {
                    sectionImage = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementP, 0)];
                }
            }
            sqlite3_finalize(compiledStatementP);
            sqlite3_close(databaseP);
        }    
        UIView *customHeaderView = [[UIView alloc] initWithFrame:CGRectMake(10,0,300,35)];
        customHeaderView.clipsToBounds = YES;
        customHeaderView.backgroundColor = DEFAULT_COLOR;//NAVIGATION_THEMELIGHTCOLOR;
        UIImageView *starImageView = [[UIImageView alloc] init];    
        starImageView.frame = CGRectMake(10,0, 170, 34);
        starImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"stars_%@.png",sectionImage]];
        [customHeaderView addSubview:starImageView];
        return customHeaderView;
    }
}

-(CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section
{    
    return 35;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    sqlite3 *databaseP;
    NSInteger rowCount = 0;
    if(sqlite3_open([[MEDICAL_APP_DELEGATE databasePath] UTF8String], &databaseP) == SQLITE_OK)
    {
        NSString *queryString = @"";
        if (isSearchOn)
        {
             queryString = [NSString stringWithFormat:@"select count (*) from journals where journal_name like \"%%%@%%\" and journal_category = \"%@\" COLLATE NOCASE",searchBar.text,journal_category];
        }else{
            queryString = [NSString stringWithFormat:@"select count (*) from (select * from journals where journal_rate = (select journal_rate from journals group by journal_rate order by journal_rate desc limit %d,1) and journal_category = \"%@\" COLLATE NOCASE)",section,journal_category];
        }
//        DebugLog(@"-%@-",queryString);
        const char *queryCharP = [queryString UTF8String];
        sqlite3_stmt *compiledStatementP = nil;
        
        if(sqlite3_prepare_v2(databaseP, queryCharP, -1, &compiledStatementP, NULL) == SQLITE_OK)
        {
            if (sqlite3_step(compiledStatementP) == SQLITE_ROW)
            {
                rowCount = sqlite3_column_int(compiledStatementP, 0);
            }
        }
        sqlite3_finalize(compiledStatementP);
        sqlite3_close(databaseP);
    }
    return rowCount;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return STATIC_ROW_HEIGHT;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"oncologyCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
        cell.textLabel.font = DEFAULT_FONT(FONT_STATIC_ROW_HEIGHT);
        cell.textLabel.numberOfLines = 3;

        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = DEFAULT_COLOR_WITH_ALPHA(0.5);
        cell.selectedBackgroundView = bgColorView;
    }
    
    sqlite3 *databaseP;
    NSString *cellText = nil;
    NSString *cellLink = nil;
    
    if(sqlite3_open([[MEDICAL_APP_DELEGATE databasePath] UTF8String], &databaseP) == SQLITE_OK)
    {
        NSString *queryString = @"";
        if (isSearchOn)
        {
            queryString = [NSString stringWithFormat:@"select journal_name,journal_link from journals where journal_name like \"%%%@%%\" and journal_category = \"%@\" COLLATE NOCASE order by journal_name limit %d,1",searchBar.text,journal_category,indexPath.row];
        }else{
            queryString = [NSString stringWithFormat:@"select journal_name,journal_link from journals where journal_rate = (select journal_rate from journals group by journal_rate order by journal_rate desc limit %d,1) and journal_category = \"%@\" COLLATE NOCASE order by journal_name limit %d,1",indexPath.section,journal_category,indexPath.row];
        }
        const char *queryCharP = [queryString UTF8String];
//        DebugLog(@"-%@-",queryString);

        sqlite3_stmt *compiledStatementP = nil;
        
        if(sqlite3_prepare_v2(databaseP, queryCharP, -1, &compiledStatementP, NULL) == SQLITE_OK)
        {
            if (sqlite3_step(compiledStatementP) == SQLITE_ROW)
            {
                cellText = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementP, 0)];
                cellLink = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementP, 1)];
            }
        }
        sqlite3_finalize(compiledStatementP);
        sqlite3_close(databaseP);
    }
    if ([cellLink isEqualToString:@""])
    {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    else
    {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    cell.textLabel.text = [cellText capitalizedString];
    return cell;
}

#pragma UITableView Delegate Methods
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:WARNING_TITLE
                                                        message:WARNING_MSG_FOR_WEBLINK
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
	[alertView show];
}


- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (canSelectRow)
        return indexPath;
    else
        return nil;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	DebugLog(@"buttonIndex = %d",buttonIndex);
    NSIndexPath *indexPath = [jtableView indexPathForSelectedRow];
    if (indexPath != nil) {
        sqlite3 *databaseP;
        NSString *selectedText = nil;
        NSString *selectedLink = nil;
        if(sqlite3_open([[MEDICAL_APP_DELEGATE databasePath] UTF8String], &databaseP) == SQLITE_OK)
        {
            NSString *queryString = @"";
            if (isSearchOn)
            {
                queryString = [NSString stringWithFormat:@"select journal_name,journal_link from journals where journal_name like \"%%%@%%\" and journal_category = \"%@\" COLLATE NOCASE order by journal_name limit %d,1",searchBar.text,journal_category,indexPath.row];
            }else{
                queryString = [NSString stringWithFormat:@"select journal_name,journal_link from journals where journal_rate = (select journal_rate from journals group by journal_rate order by journal_rate desc limit %d,1) and journal_category = \"%@\" COLLATE NOCASE order by journal_name limit %d,1",indexPath.section,journal_category,indexPath.row];
            }
            //        DebugLog(@"-%@-",queryString);
            
            const char *queryCharP = [queryString UTF8String];
            
            sqlite3_stmt *compiledStatementP = nil;
            
            if(sqlite3_prepare_v2(databaseP, queryCharP, -1, &compiledStatementP, NULL) == SQLITE_OK)
            {
                if (sqlite3_step(compiledStatementP) == SQLITE_ROW)
                {
                    selectedText = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementP, 0)];
                    selectedLink = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementP, 1)];
                }
            }
            sqlite3_finalize(compiledStatementP);
            sqlite3_close(databaseP);
        }
        //DebugLog(@"%@",value);
        if(![selectedLink isEqualToString:@""])
        {
            NSDictionary *params = @{@"journal_category":journal_category,@"journal_name":selectedText,@"link":selectedLink};
            [INEventLogger logEvent:@"Journals_Website" withParams:params];
            
            MedicalWebViewController *webviewController = [[MedicalWebViewController alloc] initWithNibName:@"MedicalWebViewController" bundle:nil] ;
            webviewController.medicalLink   = selectedLink;
            webviewController.title         = selectedText;
            [self.navigationController pushViewController:webviewController animated:YES];
        }
        [jtableView deselectRowAtIndexPath:indexPath animated:YES];
    }
}

- (void) keyboardWillShow:(NSNotification *)note {
    if ([self.view window]) //means is visible
    {    //do something
        NSDictionary *userInfo = [note userInfo];
        CGSize kbSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        
        DebugLog(@"userInfo %@", userInfo);
        
        DebugLog(@"Keyboard Height: %f Width: %f", kbSize.height, kbSize.width);
        
        keyboardHeight = kbSize.height;
        if (isSearchOn) {
            [UIView animateWithDuration:0.3 animations:^{
                [jtableView setFrame:CGRectMake(jtableView.frame.origin.x, jtableView.frame.origin.y, jtableView.frame.size.width, self.view.frame.size.height-(searchBar.frame.size.height+keyboardHeight))];
            }];
        }
    }else{
        //return
    }
}

#pragma UISearchBar Delegate Methods
-(void)searchBarTextDidBeginEditing:(UISearchBar *)lsearchBar{
    isSearchOn = YES;
    if ([lsearchBar.text length] > 0) {
        canSelectRow = YES;
        self.jtableView.scrollEnabled = YES;
    }else{
        canSelectRow = NO;
        self.jtableView.scrollEnabled = NO;
    }
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneSearching:)];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if ([searchText length] > 0) {
        canSelectRow = YES;
        self.jtableView.scrollEnabled = YES;
    }else{
        canSelectRow = NO;
        self.jtableView.scrollEnabled = NO;
    }
    [self.jtableView reloadData];
}

-(void)doneSearching:(id)sender{
    DebugLog(@"doneSearching");
    
    isSearchOn=NO;
    canSelectRow=YES;
    self.jtableView.scrollEnabled = YES;
    
    self.navigationItem.rightBarButtonItem = nil;
    [searchBar resignFirstResponder];
    searchBar.text=@"";
    [UIView animateWithDuration:0.3 animations:^{
        [jtableView setFrame:CGRectMake(jtableView.frame.origin.x, jtableView.frame.origin.y, jtableView.frame.size.width, self.view.frame.size.height-searchBar.frame.size.height)];
    }];
    [self.jtableView reloadData];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [self.jtableView reloadData];
}
@end
