//
//  MedicalSurvivalRatesViewController.m
//  Medical
//
//  Created by Rishi on 26/05/14.
//  Copyright (c) 2014 Kirti Nikam. All rights reserved.
//
#import <sqlite3.h>

#import "MedicalSurvivalRatesViewController.h"
#import "constants.h"
#import "MedicalAppDelegate.h"
#import "MedicalWebViewController.h"

@interface MedicalSurvivalRatesViewController ()

@end

@implementation MedicalSurvivalRatesViewController
@synthesize ratesTableView;
@synthesize ratesDict;
@synthesize ratesArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [INEventLogger logEvent:@"5_Survival_Rates_Cancer_List"];
    [self getDetailsFromTable];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

-(void)getDetailsFromTable
{
    if (ratesDict == nil) {
        ratesDict = [[NSMutableDictionary alloc] init];
    }else{
        [ratesDict removeAllObjects];
    }
    if (ratesArray == nil) {
        ratesArray = [[NSMutableArray alloc] init];
    }else{
        [ratesArray removeAllObjects];
    }
    sqlite3 *datbaseP = nil;
    if (sqlite3_open([[MEDICAL_APP_DELEGATE databasePath] UTF8String], &datbaseP) == SQLITE_OK) {
        NSString *queryString = @"SELECT cancer,definition FROM FiveYearSurvival";
        sqlite3_stmt *statement = nil;
        if (sqlite3_prepare_v2(datbaseP, [queryString UTF8String], -1, &statement, NULL) == SQLITE_OK) {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                NSString *name = [NSString stringWithUTF8String:(const char *)sqlite3_column_text(statement, 0)];
                NSString *definition = [NSString stringWithUTF8String:(const char *)sqlite3_column_text(statement, 1)];
                [ratesArray addObject:name];
                [ratesDict setObject:definition forKey:name];
            }
        }
        sqlite3_finalize(statement);
    }
    sqlite3_close(datbaseP);
    [ratesTableView reloadData];
}

#pragma mark Table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
   return STATIC_ROW_HEIGHT;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [ratesArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        
        cell.accessoryType  =   UITableViewCellAccessoryDisclosureIndicator;
        cell.textLabel.font =   DEFAULT_FONT(FONT_STATIC_ROW_HEIGHT);
        cell.textLabel.numberOfLines = 3;
        
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = DEFAULT_COLOR_WITH_ALPHA(0.5);
        cell.selectedBackgroundView = bgColorView;
    }
    
    cell.textLabel.text= [ratesArray objectAtIndex:indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *key   = [ratesArray objectAtIndex:indexPath.row];
    DebugLog(@"key %@",key);
    NSString *value = [ratesDict objectForKey:key];

    NSDictionary *params = @{@"cancer_name":key};
    [INEventLogger logEvent:@"5_Survival_Rates_Cancer_Details" withParams:params];
    
    MedicalWebViewController *webviewController = [[MedicalWebViewController alloc] initWithNibName:@"MedicalWebViewController" bundle:nil];
    webviewController.title         = @"";
    webviewController.titleString   = key;
    webviewController.details       = value;
    [self.navigationController pushViewController:webviewController animated:YES];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
@end
