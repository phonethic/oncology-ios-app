//
//  MedicalChemoDrugTypeDetailsViewController.m
//  Medical
//
//  Created by Kirti Nikam on 21/11/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//
#import "MedicalChemoDrugTypeDetailsViewController.h"

@interface MedicalChemoDrugTypeDetailsViewController ()
@end

@implementation MedicalChemoDrugTypeDetailsViewController
@synthesize titleString,details;
@synthesize lblTitle;
@synthesize detailsWebView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    lblTitle.text = titleString;    
    NSString *css = [NSString stringWithFormat:@"<!DOCTYPE html><head><style type=\"text/css\">h1{font-family:\"Arial\"; font-size:20px;width:300px;background-color:rgb(115,137,165);color:white;text-indent:5px;}</style></head><body><font face = \"Arial\">%@</font></body></html>",details];
    [detailsWebView loadHTMLString:css baseURL:nil];
}
- (void)viewDidUnload
{
    [self setLblTitle:nil];
    [self setDetailsWebView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
#pragma mark - Rotation
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
@end
