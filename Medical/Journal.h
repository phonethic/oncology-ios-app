//
//  Journal.h
//  Medical
//
//  Created by Kirti Nikam on 11/10/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Journal : NSObject

@property(nonatomic,copy) NSString *name;
@property(nonatomic,copy) NSString *abbreviation;
@property(nonatomic,copy) NSString *pubmedid;
@property(nonatomic,copy) NSString *imapactfactor;
@property(nonatomic,copy) NSString *weblink;

//-(id)init:(NSString *)namevalue abbreviaiton:(NSString *)abbreviaitonvalue pubmedid:(NSString *)pubmedidvalue imapactfactor:(NSString *)imapactfactorvalue weblink:(NSString *)weblinkvalue;
@end
