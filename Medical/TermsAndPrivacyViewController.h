//
//  TermsAndPrivacyViewController.h
//  Medical
//
//  Created by Kirti Nikam on 21/04/14.
//  Copyright (c) 2014 Kirti Nikam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@protocol TermsAndPrivacyDelegate <NSObject>
-(void)closeTermsAndPrivacyView;
@end

@interface TermsAndPrivacyViewController : UIViewController<MBProgressHUDDelegate> {
	MBProgressHUD *HUD;
}

@property (weak, nonatomic) id<TermsAndPrivacyDelegate> termsDelegate;

@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) IBOutlet UIButton *btnAccept;

- (IBAction)btnAcceptPressed:(id)sender;
@end
