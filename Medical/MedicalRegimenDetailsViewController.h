//
//  MedicalRegimenDetailsViewController.h
//  Medical
//
//  Created by Rishi on 26/10/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>
#import "MedicalBSAViewController.h"
#define MGM2 [NSString stringWithFormat:@"mg/m\u00B2"]
#define MGM2DAY [NSString stringWithFormat:@"mg/m\u00B2/day"]
#define SECTIONSPACE 30

#define AddPreMedicationCheckBoxTag 7000
#define AddPreMedicationTextViewTag 4002
#define CARBOPLATIN_WARN @"Dosing Warning"

#define PREMEDICATIONHEADERTAG 7001
#define CHEMOTHERAPYHEADERTAG 7002
#define FREQUENCYHEADERTAG 7003
#define LABSHEADERTAG 7004
#define COMMENTSHEADERTAG 7005

@interface MedicalRegimenDetailsViewController : UIViewController<UITextFieldDelegate,UIActionSheetDelegate,UITextViewDelegate,BSAViewControllerDelegate> {
    // Database variables
    int textViewTag;
    double yaxis;
    double chemoDose;
    
}
@property (nonatomic,copy) NSString *cancerName;
@property(nonatomic,copy) NSString *regimenName;
@property(nonatomic,copy) NSString *regimenId;
@property(nonatomic,copy) NSString *detailClinicalPearls;
@property(nonatomic,copy) NSString *detailsReferences;
@property(nonatomic,copy) NSString *detailsReferencesLink;
@property (nonatomic,readwrite) int regionWarnResult;
@property(strong, nonatomic) NSMutableArray *preDayArray;
@property(strong, nonatomic) NSMutableDictionary *BSAArray;

@property (strong, nonatomic) IBOutlet UIScrollView *regimenDetailScrollview;
@property (strong, nonatomic) IBOutlet UILabel *lblRegimenNameHeader;

@property (strong, nonatomic) IBOutlet UIView *viewPatientName;
@property (strong, nonatomic) IBOutlet UIView *viewPatientAge;
@property (strong, nonatomic) IBOutlet UIView *transviewPatientBSA;

@property (strong, nonatomic) IBOutlet UIView *viewPatientBSA;
@property (strong, nonatomic) IBOutlet UIView *viewPatientWeight;
@property (strong, nonatomic) IBOutlet UIView *viewPatientHeight;


@property (strong, nonatomic) IBOutlet UITextField *txtPatientName;
@property (strong, nonatomic) IBOutlet UITextField *txtPatientAge;
@property (strong, nonatomic) IBOutlet UITextField *txtPatientBSA;
@property (strong, nonatomic) IBOutlet UITextField *txtPatientWeight;
@property (strong, nonatomic) IBOutlet UITextField *txtPatientHeight;

@property (strong, nonatomic) IBOutlet UILabel *lblKg;
@property (strong, nonatomic) IBOutlet UILabel *lblcm;

@property (strong, nonatomic) IBOutlet UIButton *BSABtn;

@property (strong, nonatomic) IBOutlet UIView *backView;
@property (strong, nonatomic) IBOutlet UIView *detailView;
@property (strong, nonatomic) IBOutlet UITextView *detailTextView;
@property (strong, nonatomic) IBOutlet UILabel *detailHeader;
@property (strong, nonatomic) IBOutlet UIButton *detailOK;
@property (strong, nonatomic) IBOutlet UIButton *referencebtn;
@property (strong, nonatomic) IBOutlet UILabel *lblErrBSA;
@property (strong, nonatomic) IBOutlet UILabel *lblErrWeight;
@property (strong, nonatomic) IBOutlet UILabel *lblErrHeight;


@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *viewCollection;
@property (strong, nonatomic) IBOutletCollection(UITextField) NSArray *txtCollection;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *lblCollection;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *lblErrCollection;


- (IBAction)BSABtnPressed:(id)sender;
-(void) setBSResultValue:(NSString*)result weight:(NSString*)weight height:(NSString*)height;


- (IBAction)editingChanged:(UITextField *)sender;

- (IBAction)referencebtnPressed:(id)sender;
- (IBAction)detailOKBtnPressed:(id)sender;

//- (void)useNotificationWithString:(NSNotification*)notification;

@end
