//
//  MedicalCancerCenterDetailViewController.h
//  Medical
//
//  Created by Rishi on 05/11/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>
#import <MapKit/MapKit.h>

@interface MedicalCancerCenterDetailViewController : UIViewController<MKMapViewDelegate>
{
    double latValue;
    double longValue;
}
@property (nonatomic,copy) NSString *centerName;
@property (strong, nonatomic) IBOutlet UITableView *detailsTableView;
@property (retain,nonatomic) NSMutableArray *detailsArray;

@property (strong, nonatomic) IBOutlet MKMapView *centerMapView;
@end
