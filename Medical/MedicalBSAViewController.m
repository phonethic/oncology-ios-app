//
//  MedicalBSAViewController.m
//  Medical
//
//  Created by Kirti Nikam on 08/10/12.
//  Copyright (c) 2012 Kirti Nikam. All rights reserved.
//

#import "MedicalBSAViewController.h"
#import "MedicalInfoImageViewController.h"
#import "MedicalAppDelegate.h"
#import "MedicalRegimenDetailsViewController.h"
#import "MedicalFormulaViewController.h"
#import "CommonCallback.h"

#define SYSTEM_VERSION_LESS_THAN(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
@interface MedicalBSAViewController ()

@end

@implementation MedicalBSAViewController
@synthesize lblBSA,labelResult;
@synthesize weightLabel;
@synthesize heightLabel;
@synthesize weightTextField;
@synthesize heightTextField;
@synthesize heightkeyboardView;
@synthesize weightkeyboardView;
@synthesize ftTextField;
@synthesize inchTextField;
@synthesize segmentControl;
@synthesize BSAText,weightText,heightText;
@synthesize bsadelegate;
@synthesize scrollView;
@synthesize viewWeight,viewHeightInCM,viewHeightInFT;
@synthesize viewCollection,txtCollection,lblCollection,lblErrCollection,keyBoardBtnCollection,keyBoardViewCollection;
@synthesize lblErrHeight,lblErrWeight;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


-(void) setBSAValue:(NSString*)lbsaValue weight:(NSString*)lweightValue height:(NSString*)lheightValue{
    BSAText     =   lbsaValue;
    weightText  =   lweightValue;
    heightText  =   lheightValue;
}

-(void) viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
}
-(void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
//    DebugLog(@"viewWillAppear %d %d",self.interfaceOrientation,UIInterfaceOrientationIsPortrait(self.interfaceOrientation));
//    
//    if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation))
//    {
//        DebugLog(@"already in Portrait");
//    }
//    else
//    {
//        UIViewController *c = [[UIViewController alloc]init];
//        [self presentModalViewController:c animated:NO];
//        [self dismissModalViewControllerAnimated:NO];
//    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSDictionary *params = @{@"calculator_name":self.title};
    [INEventLogger logEvent:@"MedCalc_Calculator_Details" withParams:params];
    
 //   [self roundViewWithBorder];
    
    HeightViewFLAG=0;
    if(bsadelegate) {
        //Regimen BSA View
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]
                                                  initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                  target:self action:@selector(doneBarBtnClicked:)];
    }else{
        //Normal BSA View
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]
                                                  initWithBarButtonSystemItem:UIBarButtonSystemItemAction
                                                  target:self action:@selector(shareBarBtnClicked:)];
    }
    
    [self setUI];

    if ([BSAText length] > 0)
    {
        labelResult.text        =   BSAText;
        weightTextField.text    =   weightText;
        heightTextField.text    =   heightText;
        weightValue             =   [weightText doubleValue];
        heightValue             =   [heightText doubleValue];
    }
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected:)];
    tapGesture.numberOfTapsRequired = 1;
    tapGesture.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapGesture];
    
    scrollView.contentSize = CGSizeMake(scrollView.frame.size.width, scrollView.frame.size.height);
}

- (void)viewDidUnload
{
    [self setWeightTextField:nil];
    [self setHeightTextField:nil];
    [self setHeightkeyboardView:nil];
    [self setWeightkeyboardView:nil];
    [self setWeightLabel:nil];
    [self setLabelResult:nil];
    [self setFtTextField:nil];
    [self setInchTextField:nil];
    [self setViewHeightInCM:nil];
    [self setViewHeightInFT:nil];
    [self setSegmentControl:nil];
    [self setHeightLabel:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
#pragma mark - Rotation
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    DebugLog(@"shouldAutorotateToInterfaceOrientation %u",toInterfaceOrientation);
     if(bsadelegate) {
         return (toInterfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
     }else{
          return (toInterfaceOrientation == UIInterfaceOrientationPortrait);
     }
}
- (BOOL)shouldAutorotate
{
    if(bsadelegate) {
        return YES;
    }else{
        return NO;
    }
}

- (NSUInteger)supportedInterfaceOrientations
{
    if(bsadelegate) {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    }else{
        return UIInterfaceOrientationMaskPortrait;
    }
}

#pragma internal methods
-(void)setUI{
    lblBSA.backgroundColor     = [UIColor clearColor];
    lblBSA.textColor           = DEFAULT_COLOR;
    lblBSA.font                = DEFAULT_BOLD_FONT(25);
    
    labelResult.backgroundColor     = [UIColor clearColor];
    labelResult.textColor           = DEFAULT_COLOR;
    labelResult.font                = DEFAULT_BOLD_FONT(25);

    for (UIView *lview in viewCollection) {
        lview.backgroundColor     = [UIColor whiteColor];
        lview.layer.borderColor   = [UIColor lightGrayColor].CGColor;
        lview.layer.borderWidth   = 1.0;
    }
    
    for (UITextField *txt in txtCollection) {
        txt.delegate            = self;
        txt.backgroundColor     = [UIColor clearColor];
        txt.textColor           = [UIColor blackColor];
        txt.font                = DEFAULT_FONT(22);
        txt.keyboardType        = UIKeyboardTypeDecimalPad;
        txt.textAlignment       = NSTextAlignmentCenter;
        txt.layer.borderColor   = [UIColor clearColor].CGColor;
        txt.text                = @"";
    }
    
    weightTextField.inputAccessoryView  = weightkeyboardView;
    heightTextField.inputAccessoryView  = heightkeyboardView;
    ftTextField.inputAccessoryView      = heightkeyboardView;
    inchTextField.inputAccessoryView    = heightkeyboardView;
    
    for (UILabel *lbl in lblCollection) {
        lbl.backgroundColor     = [UIColor clearColor];
        lbl.textColor           = DEFAULT_COLOR;
        lbl.font                = DEFAULT_SEMIBOLD_FONT(20);
        lbl.textAlignment       = NSTextAlignmentCenter;
    }
    
    for (UILabel *lbl in lblErrCollection) {
        lbl.backgroundColor     = [UIColor clearColor];
        lbl.textColor           = [UIColor redColor];
        lbl.font                = DEFAULT_SEMIBOLD_FONT(12);
        lbl.hidden              = YES;
    }
    
    for (UIView *keyboardView in keyBoardViewCollection) {
        keyboardView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.4];
    }
    
    for (UIButton *btn in keyBoardBtnCollection) {
        btn.backgroundColor     = DEFAULT_COLOR;
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor colorWithWhite:1.0 alpha:0.5] forState:UIControlStateHighlighted];
        btn.titleLabel.font     = DEFAULT_FONT(20);
    }
}

-(void)tapDetected:(UIGestureRecognizer *)sender{
    [self.view endEditing:YES];
    CGPoint tapLocation = [sender locationInView:scrollView];
    UIView *view = [scrollView hitTest:tapLocation withEvent:nil];
    if (![view isKindOfClass:[UIButton class]]) {
        [self scrollTobottom];
    }
}

- (void) shareBarBtnClicked:(id)sender {
    UIActionSheet *uiActionSheetP= [[UIActionSheet alloc]
                                    initWithTitle: @""
                                    delegate:self
                                    cancelButtonTitle:@"CANCEL"
                                    destructiveButtonTitle:nil
                                    otherButtonTitles:@"Print",@"Email", nil];
	
	uiActionSheetP.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
	[uiActionSheetP showInView:self.navigationController.view];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    DebugLog(@"buttonIndex = %d",buttonIndex);
    [heightkeyboardView setHidden:YES];
    [weightkeyboardView setHidden:YES];
    [self.view endEditing:YES];
    CGRect oldFrame = scrollView.frame;
    scrollView.frame=CGRectMake(scrollView.frame.origin.x, scrollView.frame.origin.y, scrollView.contentSize.width, scrollView.contentSize.height);
	if (buttonIndex == 0)
    {
        NSDictionary *params = @{@"calculator_name":self.title, @"share_via" : @"print"};
        [INEventLogger logEvent:@"MedCalc_Calculator_Share" withParams:params];
        [MEDICAL_APP_DELEGATE printImage:scrollView];
    }
    else if (buttonIndex == 1)
    {
        NSDictionary *params = @{@"calculator_name":self.title, @"share_via" : @"email"};
        [INEventLogger logEvent:@"MedCalc_Calculator_Share" withParams:params];
        [MEDICAL_APP_DELEGATE mailImage:scrollView];
    }
	scrollView.frame=oldFrame;
    [self scrollTobottom];
}

- (void) doneBarBtnClicked:(id)sender {
    DebugLog(@"bsadelegate = %@",bsadelegate);
    if(self.bsadelegate && [self.bsadelegate respondsToSelector:@selector(setBSResultValue:weight:height:)])
    {
        [self.bsadelegate setBSResultValue:labelResult.text weight:[NSString stringWithFormat:@"%.2f",weightValue] height:[NSString stringWithFormat:@"%.2f",heightValue]];
    }
    [self.navigationController popViewControllerAnimated:YES];
    self.bsadelegate = nil;
}

-(void) roundViewWithBorder
{
    for (int i=100; i<103; i++)
    {
        UIView *text=(UIView *)[self.view viewWithTag:i];
        text.layer.cornerRadius = TEXTFIELDVIEW_CORNERRADIUS;
        text.clipsToBounds = YES;
        text.layer.borderColor = TEXTFIELDVIEW_DEFAULT_BORDERCOLOR;
        text.layer.borderWidth = TEXTFIELDVIEW_BORDERWIDTH;
    }
}

- (IBAction)keyboardButtonAction:(id)sender {
    
    UIButton *btn=(UIButton *)sender;
    switch (btn.tag) {
        case 301:
            {
                weightLabel.text    =   @"kg";
                [self validate:weightTextField];
            }
            break;
        case 302:
            {
                weightLabel.text    =   @"lb";
                [self validate:weightTextField];
            }
            break;
        case 303: // cm
            {
                HeightViewFLAG      =   0;
                heightLabel.text    =   @"cm";
                [viewHeightInFT setHidden:TRUE];
                [viewHeightInCM setHidden:FALSE];
                [self validate:heightTextField];
                [heightTextField becomeFirstResponder];
            }
            break;
        case 304: // feet/inch
            {
                HeightViewFLAG  =   1;
                [viewHeightInCM setHidden:TRUE];
                [viewHeightInFT setHidden:FALSE];             
                [ftTextField becomeFirstResponder];
            }
            break;
        case 305: // inches
        {
            HeightViewFLAG      =   2;
            heightLabel.text    =   @"inches";
            [viewHeightInFT setHidden:TRUE];
            [viewHeightInCM setHidden:FALSE];
            [self validate:heightTextField];
            [heightTextField becomeFirstResponder];
        }
            break;
        default:
            break;
    }
    [self compute];
}

- (IBAction)segmentControlChanged:(id)sender {
    [self compute];
}

- (IBAction)editingChanged:(UITextField *)textField {
    [self validate:textField];
    [self compute];
}


-(BOOL)validate:(UITextField *)textField{
    BOOL success = YES;
    double doubleValue = [textField.text doubleValue];
    if([textField isEqual:weightTextField])
    {
        if ([weightLabel.text isEqualToString:@"kg"])
        {
            if ((doubleValue >= 10 && doubleValue <= 200)) {
                [self removeErrorMessageFromLabel:lblErrWeight];
            }else{
                [self setErrorMessageOnLabel:@"Enter weight value in between 10 - 200." label:lblErrWeight];
                success = NO;
            }
        }else if ([weightLabel.text isEqualToString:@"lb"])
        {
            if ((doubleValue >= 22 && doubleValue <= 441)) {
                [self removeErrorMessageFromLabel:lblErrWeight];
            }else{
                [self setErrorMessageOnLabel:@"Enter weight value in between 22 - 441." label:lblErrWeight];
                success = NO;
            }
        }
    }else if([textField isEqual:heightTextField])
    {
        if ([heightLabel.text isEqualToString:@"cm"])
        {
            if ((doubleValue >= 30 && doubleValue <= 241.3)) {
                [self removeErrorMessageFromLabel:lblErrHeight];
            }else{
                [self setErrorMessageOnLabel:@"Enter height value in between 30 - 241." label:lblErrHeight];
                success = NO;
            }
        }else if ([heightLabel.text isEqualToString:@"inches"])
        {
            if ((doubleValue >= 11.8 && doubleValue <= 95)) {
                [self removeErrorMessageFromLabel:lblErrHeight];
            }else{
                [self setErrorMessageOnLabel:@"Enter height value in between 11.8 - 95." label:lblErrHeight];
                success = NO;
            }
        }
    }else if([textField isEqual:ftTextField])
    {
        if ((doubleValue >= 1 && doubleValue <= 7)) {
            [self removeErrorMessageFromLabel:lblErrHeight];
        }else{
            [self setErrorMessageOnLabel:@"Enter feet value in between 1 - 7." label:lblErrHeight];
            success = NO;
        }
    }
    else if([textField isEqual:inchTextField])
    {
        if ((doubleValue >= 0 && doubleValue <= 11)) {
            [self removeErrorMessageFromLabel:lblErrHeight];
        }else{
            [self setErrorMessageOnLabel:@"Enter inch value in between 0 - 11." label:lblErrHeight];
            success = NO;
        }
    }
    return success;
}

-(void)compute{
    
    
    if (![weightTextField.text isEqualToString:@""] && (![heightTextField.text isEqualToString:@""] || (![ftTextField.text isEqualToString:@""] && ![inchTextField.text isEqualToString:@""])))
    {
        //formula ------ sqrt((height*weight)/3600)
        heightValue = [heightTextField.text doubleValue];
        weightValue = [weightTextField.text doubleValue];
        
        int count=0;
        if ([self validate:weightTextField])
        {
            if ([weightLabel.text isEqualToString:@"lb"])
                weightValue=[weightTextField.text doubleValue] * ONE_POUND_IN_KG;
            
            count ++;
        }
        switch (HeightViewFLAG) {
            case 0: //cm
            {
                if ([self validate:heightTextField])
                {
                    count ++;
                }
            }
                break;
            case 1:
            {
                if ([self validate:ftTextField] && [self validate:inchTextField])
                {
                    heightValue=[self convertintocm:[ftTextField.text doubleValue] inch:[inchTextField.text doubleValue]];
                    count ++;
                }
            }
                break;
            case 2:
            {
                if ([self validate:heightTextField])
                {
                    heightValue=heightValue * ONE_INCHES_IN_CM;
                    count ++;
                }
            }
                break;
            default:
                break;
        }
 
        DebugLog(@"height value %f",heightValue);
        if(count == 2)
        {
            switch (self.segmentControl.selectedSegmentIndex)
            {
                case 0: // DuBois
                    [self show:(0.007184*pow(heightValue,0.725)*pow(weightValue,0.425))];
                    break;
                case 1: // Mosteller
                    [self show:sqrt((heightValue*weightValue)/3600)];
                    break;
                default:
                    break;
            }
        }
        else
        {
            labelResult.text=@"....";
        }
    }
    else
    {
        labelResult.text=@"....";
    }
}


//-(BOOL)validate:(double)doublevalue checktype:(int)type viewTag:(int)tag
//{
//    DebugLog(@"in validate with checktype %d",type);
//    switch (type) {
//        case 0: // kg or lb
//                if ((doublevalue >=10 && doublevalue<=200) && [weightLabel.text isEqualToString:@"kg"])
//                {
//                    [self changeTextFieldColor:TRUE viewTag:tag];
//                    return TRUE;
//                }
//                else if ((doublevalue >=22 && doublevalue<=441) && [weightLabel.text isEqualToString:@"lb"])
//                {
//                    [self changeTextFieldColor:TRUE viewTag:tag];
//                    return TRUE;
//                }
//                else
//                {
//                    [self changeTextFieldColor:FALSE viewTag:tag];
//                    return FALSE;
//                }
//            break;        
//        case 1: //cm
//                if (doublevalue >=30 && doublevalue<=241.3)
//                {
//                    [self changeTextFieldColor:TRUE viewTag:tag];
//                    return TRUE;
//                    
//                }
//                else
//                {
//                    [self changeTextFieldColor:FALSE viewTag:tag];
//                    return FALSE;
//                }
//            break;
//        case 2: //ft 
//                if (doublevalue >= 1 && doublevalue<=7)
//                {
//                    [self changeTextFieldColor:TRUE viewTag:tag];
//                    return TRUE;
//                    
//                }
//                else
//                {
//                    [self changeTextFieldColor:FALSE viewTag:tag];
//                    return FALSE;
//                }
//            break;
//        case 3: // inch
//            if (doublevalue >= 0 && doublevalue<=11)
//            {
//                [self changeTextFieldColor:TRUE viewTag:tag];
//                return TRUE;
//                
//            }
//            else
//            {
//                [self changeTextFieldColor:FALSE viewTag:tag];
//                return FALSE;
//            }
//            break;
//        case 4: // only inches
//            if (doublevalue >= 11.8 && doublevalue<=95)
//            {
//                [self changeTextFieldColor:TRUE viewTag:tag];
//                return TRUE;
//                
//            }
//            else
//            {
//                [self changeTextFieldColor:FALSE viewTag:tag];
//                return FALSE;
//            }
//            break;
//       default:
//         break;
//    }
//    return FALSE;
//}
//-(void)changeTextFieldColor:(BOOL)type viewTag:(int)tag
//{
//    UIView *view=(UIView *)[self.view viewWithTag:tag];
//    if (type)
//    {
//        view.backgroundColor=TEXTFIELDVIEW_DEFAULT_BACKGROUNDCOLOR;
//        view.layer.borderColor=TEXTFIELDVIEW_DEFAULT_BORDERCOLOR;
//        
//    }
//    else
//    {
//        view.backgroundColor=TEXTFIELDVIEW_ERROR_BACKGROUNDCOLOR;
//        view.layer.borderColor=TEXTFIELDVIEW_ERROR_BORDERCOLOR;
//        labelResult.text=@"";
//    }
//}

/*
 The Mosteller¹ formula
 
 BSA (m²) = ( [Height(cm) x Weight(kg) ]/ 3600 )½        e.g. BSA = SQRT( (cm*kg)/3600 )
 
 or in inches and pounds:     BSA (m²) = ( [Height(in) x Weight(lbs) ]/ 3131 )½
 
 The DuBois and DuBois² formula
 
 BSA (m²) = 0.20247 x Height(m)^0.725 x Weight(kg)^0.425
 
 A variation of DuBois and DuBois15 thats gives virtually identical results is:
 
 BSA (m²) = 0.007184 x Height(cm)^0.725 x Weight(kg)^0.425
 
*/

//-(void)compute{
//    
//    
//    if (![weightTextField.text isEqualToString:@""] && (![heightTextField.text isEqualToString:@""] || (![ftTextField.text isEqualToString:@""] && ![inchTextField.text isEqualToString:@""])))
//    {
//         //formula ------ sqrt((height*weight)/3600)
//        heightValue = [heightTextField.text doubleValue];
//        weightValue = [weightTextField.text doubleValue];
//        
//        int count=0;
//        if ([self validate:weightValue checktype:0 viewTag:100])
//        {
//            if ([weightLabel.text isEqualToString:@"lb"])                
//                weightValue=[weightTextField.text doubleValue] * ONE_POUND_IN_KG;
//            
//            count ++;
//        }
//        switch (HeightViewFLAG) {
//            case 0: //cm
//                {
//                    if ([self validate:heightValue checktype:1 viewTag:101])
//                    {
//                        count ++;
//                    }
//                }
//                break;
//            case 1:
//            {
//                if ([self validate:[ftTextField.text doubleValue] checktype:2 viewTag:102] && [self validate:[inchTextField.text doubleValue] checktype:3 viewTag:102])
//                {
//                     heightValue=[self convertintocm:[ftTextField.text doubleValue] inch:[inchTextField.text doubleValue]];
//                    count ++;
//                }
//            }
//                break;
//            case 2:
//            {
//                if ([self validate:heightValue checktype:4 viewTag:101])
//                {
//                    heightValue=heightValue * ONE_INCHES_IN_CM;
//                    count ++;
//                }
//            }
//                break;
//            default:
//                break;
//        }
////        if (HeightViewFLAG &&  [self validate:heightValue checktype:1 viewTag:101]) 
////         {             
////             count ++;
////         }    
////         else if((!HeightViewFLAG) && ([self validate:[ftTextField.text doubleValue] checktype:2 viewTag:102] && [self validate:[inchTextField.text doubleValue] checktype:3 viewTag:102])) // ft inch
////         {
////             heightValue=[self convertintocm:[ftTextField.text doubleValue] inch:[inchTextField.text doubleValue]];
////             count ++;
////         }
//        DebugLog(@"height value %f",heightValue);
//        if(count == 2)
//        {
//            switch (self.segmentControl.selectedSegmentIndex)
//            {
//                case 0: // DuBois                   
//                    [self show:(0.007184*pow(heightValue,0.725)*pow(weightValue,0.425))];
//                    break;
//                case 1: // Mosteller
//                    [self show:sqrt((heightValue*weightValue)/3600)];
//                    break;
//                default:
//                    break;
//            }            
//        }
//        else
//        {
//            labelResult.text=@"....";
//        }
//       
//    }
//    else
//    {
//        labelResult.text=@"....";
//    }
//}

-(void)show:(double)result{
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [formatter setMaximumFractionDigits:2];
    [formatter setRoundingMode: NSNumberFormatterRoundCeiling];
     DebugLog(@"result %f",result);
    //labelResult.text=[NSString stringWithFormat:@"%@ %@",[formatter stringFromNumber:[NSNumber numberWithFloat:result]],M2];
    labelResult.text=[NSString stringWithFormat:@"%.2f %@",result,M2];
}
-(double)convertintocm:(double) feet inch:(double)inchvalue {

    double temp=(feet*12)+inchvalue;
    return (temp*2.54);
}

#pragma UITextField Delegate methods
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if([textField isEqual:weightTextField])
    {
        if (UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation)) {
            DebugLog(@"Potrait");
            [self scrollTobottom];
        }else{
            DebugLog(@"Landscape");
            [scrollView setContentOffset:CGPointMake(0,viewWeight.frame.origin.y-5) animated:YES];
        }
        [CommonCallback changeTextFieldColor:TEXT_EDITING view:viewWeight];
    }
    else if([textField isEqual:heightTextField])
    {
        if (UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation)) {
            DebugLog(@"Potrait");
            [scrollView setContentOffset:CGPointMake(0,viewHeightInCM.frame.origin.y-100) animated:YES];
        }else{
            DebugLog(@"Landscape");
            [scrollView setContentOffset:CGPointMake(0,viewHeightInCM.frame.origin.y-5) animated:YES];
        }
        [CommonCallback changeTextFieldColor:TEXT_EDITING view:viewHeightInCM];
    }
    else if([textField isEqual:ftTextField] || [textField isEqual:inchTextField])
    {
        if (UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation)) {
            DebugLog(@"Potrait");
            [scrollView setContentOffset:CGPointMake(0,viewHeightInFT.frame.origin.y-100) animated:YES];
        }else{
            DebugLog(@"Landscape");
            [scrollView setContentOffset:CGPointMake(0,viewHeightInFT.frame.origin.y-5) animated:YES];
        }
        [CommonCallback changeTextFieldColor:TEXT_EDITING view:viewHeightInFT];
    }
    scrollView.contentSize = CGSizeMake(scrollView.frame.size.width, scrollView.frame.size.height+200);
    return YES;
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if([textField isEqual:weightTextField])
    {
        [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:viewWeight];
    }
    else if([textField isEqual:heightTextField])
    {
        [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:viewHeightInCM];
    }
    else if([textField isEqual:ftTextField] || [textField isEqual:inchTextField])
    {
        [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:viewHeightInFT];
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    // allow backspace
    if (range.length > 0 && [string length] == 0) {
        return YES;
    }
    // do not allow . at the beggining
    //    if (range.location == 0 && [string isEqualToString:@"."]) {
    //        return NO;
    //    }
    
    NSRange temprange = [textField.text rangeOfString:@"."];
    if ((temprange.location != NSNotFound) && [string isEqualToString:@"."])
    {
        return NO;
    }
    else if (textField == ftTextField && (range.location == 0 && [string isEqualToString:@"0"]))
    {
        return NO;
    }
    else if ((textField == ftTextField ||  textField == inchTextField) && ([string isEqualToString:@"."]))
    {
        return NO;
    }
    else
    {
        return YES;
    }
}

-(void)scrollTobottom
{
    [UIView animateWithDuration:0.2 animations:^{
        scrollView.contentSize = CGSizeMake(scrollView.frame.size.width,scrollView.frame.size.height);
        CGPoint bottomOffset = CGPointMake(0, 0);
        [scrollView setContentOffset:bottomOffset animated:YES];
    }];
}


- (IBAction)infoButtonAction:(id)sender {
    NSDictionary *params = @{@"calculator_name":self.title};
    [INEventLogger logEvent:@"MedCalc_Calculator_Info_Image" withParams:params];
    
    MedicalInfoImageViewController *typedetailController = [[MedicalInfoImageViewController alloc] initWithimageName:@"bsa_info.png"];
    [self.navigationController pushViewController:typedetailController animated:YES];
}

-(void)setErrorMessageOnLabel:(NSString *)errorMessage label:(UILabel *)lbl{
    labelResult.text = @"....";
    lbl.hidden = NO;
    lbl.text = errorMessage;
    if ([lbl isEqual:lblErrWeight]) {
        [CommonCallback changeTextFieldColor:TEXT_ERROR view:viewWeight];
    }else if ([lbl isEqual:lblErrHeight]) {
        if (viewHeightInCM.hidden) {
            [CommonCallback changeTextFieldColor:TEXT_ERROR view:viewHeightInFT];
        }else{
            [CommonCallback changeTextFieldColor:TEXT_ERROR view:viewHeightInCM];
        }
    }
}

-(void)removeErrorMessageFromLabel:(UILabel *)lbl{
    lbl.hidden = YES;
    if ([lbl isEqual:lblErrWeight]) {
        [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:viewWeight];
    }else if ([lbl isEqual:lblErrHeight]) {
        if (viewHeightInCM.hidden) {
            [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:viewHeightInFT];
        }else{
            [CommonCallback changeTextFieldColor:TEXT_ENDEDITITNG view:viewHeightInCM];
        }
    }
}
@end
